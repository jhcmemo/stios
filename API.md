HOST: http://54.169.1.132/1.1/

# 拍拍賣 API
拍拍賣 API 總攬

> 以下參數或是回傳欄位為 ***粗斜體字*** 代表可為 null 或忽略.  
> ***???*** 代表不知道幹嘛用的.

## getNearProduct [POST]
回傳商品列表.

- Parameters
	- Lat : 使用者經度.
	- Lng : 使用者緯度.
	- CC  : 國家別 ( 目前僅支援台灣, 故帶 TW).

- Response

```
[
  {
    "ID" : "jack",
    "cc" : "TW",
    "contact" : null,
    "delivery" : "1",
    "des" : "This is a test",
    "favorite" : "0",
    "lat" : "25.056440",
    "lng" : "121.613022",
    "loc1" : "河濱足球埸",
    "loc2" : "台灣大學新體",
    "loc3" : null,
    "picNumber" : "3",
    "price" : "100",
    "productId" : "1",
    "time" : "2016-01-01 01:01:01",
    "title" : "test",
    "type" : "1"
  },
  ...
]
```

- 回傳欄位
	- ID : 上傳此商品的 User Id.
	- cc : 國家別, 目前未使用, 一定為 TW.
	- ***contact*** : 聯絡資料.
	- delivery : 交貨方式為, 1 = 全家, 2 = 自寄包裹, 4 = 面交
	- ***des*** : 商品描述.
	- favorite : 是否已加入我的最愛.
	- lat : 商品經度.
	- lng : 商品緯度.
	- ***loc1*** : 交易地點 1.
	- ***loc2*** : 交易地點 2.
	- ***loc3*** : 交易地點 3.
	- picNumber : 照片數量.
	- price : 商品價格.
	- productId : 商品 Id.
	- time : 上傳時間.
	- title : 商品 title.
	- type : 商品類型 1 = 其他, 2 = 3C, 3 = 服飾, 4 = 書籍, 5 = 票卷, 6 = 生活, 7 = 食品, 8 = 服務, 9 = 美妝, 10 = 寵物.

> delivery 為加總總和, 2 位元計算.

## login [POST]
登入

- Parameters
	- ID : 使用者電話 or ID, Base64Encoding 字串.
	- Pwd : 使用者密碼, Base64Encoding 字串.

- Response

```
{
  "ID" : "joe",
  "allpay_token" : null,
  "id" : "32",
  "mobile" : "0900114477",
  "result" : "success" ...
}
```

- 回傳欄位
	- ID : 使用者帳號 Id
	- allpay_token : ***???***
	- id : 資料庫的 id
	- mobile : 使用者電話
	- result : API 回傳狀態
  
> 當 result 為 success 字串才代表成功

## register [POST]
會員註冊

- Parameters
	- ID : 註冊名稱, Base64Encoding 字串.
	- Mobile : 註冊電話, Base64Encoding 字串.
	- Pwd : 註冊密碼, Base64Encoding 字串.
	- Sex : 註冊性別 1 = 男性, 2 = 女性.
	- Age : 註冊年齡. 1-10, 11-15 ... 61-70, 71-80.
	- CC : 註冊國別, 目前都輸入 TW.
	- City : 註冊城市.

- Response

```
{
  "pipimy_id" : 0,
  "result" : "success" ...
}
```

- 回傳欄位
	- pipimy_id : 會員資料庫 Id
	- result : API 回傳狀態

> result 為 success 字串才代表成功  
> 需要帶 City, 利用 CLGeocoder Class 轉換, 代表說註冊時一定要打開定位.

## getProductID [GET]
取得刊登商品的 Id

- Response

```
{
	result : "10"
}
```

- 回傳欄位
	- result : 刊登的商品 Id

## doneVideoM [POST]
影片上傳完成

- Parameters
	- ProductID : 商品 Id

- Response

```
{
	result : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## donePicM [POST]
圖片上傳完成

- Parameters
	- ProductID : 商品 Id
	- PicNumber : 圖片數量

- Response

```
{
	result : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## trace/updateStoreFlow [POST]
設置會員金物流

- Parameters
	- StoreCash : 金流設定, 1 = 信用卡, 2 = 超商, 4 = 轉帳, 8 = 餘額, 16 = 面交
	- StoreDelivery : 物流設定, 1 = 全家, 2 = 自寄, 4 = 面交
	- ***StoreFaceLoc1*** : 面交地點 1
	- ***StoreFaceLoc2*** : 面交地點 2
	- UpdateTime : 設定時間, 規格為 2015-01-01 00:00:00 字串
	- StoreDeliveryFree : 滿消費金額免運費
	- StoreDeliveryFee1 : 全家物流金額
	- StoreDeliveryFee1 : 自寄包裹物流金額

- Response

```
{
	result : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## doneTextM [POST]
刊登商品描述

- Parameters
	- ProductID : Id
	- Title : 名稱
	- Type : 類型 1 = 其他, 2 = 3C, 3 = 服飾, 4 = 書籍, 5 = 票卷, 6 = 生活, 7 = 食品, 8 = 服務, 9 = 美妝, 10 = 寵物
	- Price : 價格
	- CC : TW
	- Lat : 經度
	- Lng : 緯度
	- ***Des*** : 描述
	- PostTime : 上傳時間, 格式為 2015-01-01 00:00:00
	- Stock : 庫存, 必須 > 0

- Response

```
{
	result : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## trace/getStore [POST]
取回我的小店

- Parameters
	- MemberID : 小店擁有人 Id

- Response

```
{
  "average" : 0,
  "count" : 0,
  "result" : "success" ...
  "storeCash" : "1",
  "storeCity" : "台北市",
  "storeDelivery" : "2",
  "storeFaceLoc1" : "",
  "storeFaceLoc2" : "",
  "storeIntro" : null,
  "storeLat" : "25.055767",
  "storeLng" : "121.613037",
  "storeName" : null,
  "storeType" : null
}
```

- 回傳欄位
	- average : 評分平均
	- count : ***???***
	- result : API 回傳狀態
	- storeCash : 金流
	- storeCity : 所在地
	- storeDelivery : 物流
	- ***storeFaceLoc1*** : 面交地點 1
	- ***storeFaceLoc2*** : 面交地點 2
	- storeIntro : ***???***
	- storeLat : 經度
	- storeLng : 緯度
	- storeName : 店名
	- storeType : 類型

> result 為 success 字串才代表成功

## getMyPostProduct [POST]
我刊登的商品

- Parameters

- Response

```
[
  {
    "ID" : "joe",
    "cc" : "TW",
    "contact" : null,
    "delivery" : "2",
    "des" : "",
    "favorite" : "1",
    "lat" : "25.055767",
    "lng" : "121.613037",
    "loc1" : "",
    "loc2" : "",
    "loc3" : null,
    "picNumber" : "1",
    "price" : "100",
    "productId" : "303",
    "time" : "2015-01-31 22:39:15",
    "title" : "拖鞋",
    "type" : "3"
  },
  ......
]
```

- 回傳欄位
	- ID : 上傳此商品的 User Id.
	- cc : 國家別, 目前未使用, 一定為 TW.
	- ***contact*** : 聯絡資料.
	- delivery : 交貨方式為, 1 = 全家, 2 = 自寄包裹, 4 = 面交
	- ***des*** : 商品描述.
	- favorite : 是否已加入我的最愛.
	- lat : 商品經度.
	- lng : 商品緯度.
	- ***loc1*** : 交易地點 1.
	- ***loc2*** : 交易地點 2.
	- ***loc3*** : 交易地點 3.
	- picNumber : 照片數量.
	- price : 商品價格.
	- productId : 商品 Id.
	- time : 上傳時間.
	- title : 商品 title.
	- type : 商品類型 1 = 其他, 2 = 3C, 3 = 服飾, 4 = 書籍, 5 = 票卷, 6 = 生活, 7 = 食品, 8 = 服務, 9 = 美妝, 10 = 寵物.

> 沒有參數, 使用 cookie

## trace/listTraced [POST]
我的粉絲

- Parameters

- Response

```
[
  {
    "memberId" : 123,
    "addTime" : 2015-01-01 00:00:00
  },
  ......
]
```

- 回傳欄位
	- memberId : 粉絲 Id
	- addTime : 加入時間

> 沒有參數, 使用 cookie

## getMyDeal [POST]
我的交易

- Parameters

- Response

```
[
  {
    "userIDFrom" : 123,
    "userIDTo" : 123,
    "productId" : 123,
    "productTitle" : "測試",
    "productPrice" : 100,
    "time" : "2015-01-01 00:00:00",
    "ratingScore" : 5,
    "ratingComment" : "好賣家"
  },
  ......
]
```

- 回傳欄位
	- userIDFrom : 賣方 Id
	- userIDTo : 買方 Id
	- productId : 商品 Id
	- productTitle : 商品 Title
	- productPrice : 商品價格
	- time : 交易時間
	- ratingScore : -1
	- ***ratingComment*** : 買家評論

> 沒有參數, 使用 cookie  
> 當沒有 ratingComment, ratingScore = -1

## getSomeoneRating [POST]
取得某人評論

- Parameters
	- MemberID : 查詢的使用者 Id

- Response

```
[
  {
    "comment" : "賣家出",
    "dealTime" : "2015-01-11 15:23:51",
    "ratingTime" : "2015-01-29 18:42:08",
    "score" : "2",
    "userIDFrom" : "john"
  },
  ......,
  {
    "average" : 2,
    "count" : 1,
    "dealTimes" : "1",
    "lastOpenTime" : "2015-02-03 10:43:26",
    "openTimes" : 32,
    "registerTime" : "2014-12-15 16:43:33"
  }
]
```

- 回傳欄位
	- ***comment*** : 評論內容
	- dealTime : 交易時間
	- ratingTime : 評分時間
	- score : 評分等級
	- userIDFrom : 評分 User Id
	- average : 評分等級平均
	- count : 評分次數
	- dealTimes : 交易次數
	- lastOpenTime : ***???***
	- registerTime : ***???***

> 這隻 API 分為 2 種 class, lastObject 為總覽  
> 其他為個別評分內容

## updatePWD [POST]
修改密碼

- Parameters
	- ID : 修改的 User Id
	- Pwd : 新密碼

- Response

```
{
  "result" : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## trace/updateStoreLoc [POST]
跟新小店位置

- Parameters
	- StoreCity : 城市名稱
	- Lat : 經度
	- Lng : 緯度

- Response

```
{
  "result" : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## trace/updateStore [POST]
更新小店資訊

- Parameters
	- StoreName : 名稱
	- StoreIntro : 介紹
	- StoreType : 類型

- Response

```
{
  "result" : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## getMyProductDelete [POST]
刪除某商品

- Parameters
	- ProductID : 商品 Id

- Response

```
{
  "result" : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## getMyProductUpdate [POST]
更新某商品

- Parameters
	- ProductID : 商品 Id
	- Title : 名稱
	- Type : 類型
	- Price : 價格
	- CC : TW
	- Lat : 經度
	- Lng : 緯度
	- MemberID : 使用者名稱
	- ***Des*** : 描述

- Response

```
{
  "result" : "success" ...
}
```

- 回傳欄位
	- result : API 回傳狀態

> result 為 success 字串才代表成功

## getProductRating [POST]
取得某商品評價

- Parameters
	- ProductID : 商品 Id

- Response

```
[
  {
    "comment" : "賣家出",
    "dealTime" : "2015-01-11 15:23:51",
    "ratingTime" : "2015-01-29 18:42:08",
    "score" : "2",
    "userIDFrom" : "john"
  },
  {
    "average" : 2,
    "count" : 1,
    "dealTimes" : "1"
  }
]
```

- 回傳欄位, 參考 [getSomeoneRating]()