
#import <UIKit/UIKit.h>

@class TypeView;
@protocol TypeViewDelegat <NSObject>

@required
- (void)TypeView:(TypeView *)typeView didSelectedType:(NSString *)typeContent;
@end

@interface TypeView : UIView {
    UIView *backgroundView;
    UIView *contentView;
    UIButton *typeButton[10];
    UIImageView *typeBtnImage[10];
    int typeInt;
    NSArray *typeImage;
}

@property (nonatomic, weak) id<TypeViewDelegat> delegate;
@property (nonatomic) NSArray *typeContent;
@property (nonatomic) NSArray *typeValue;
@property (nonatomic) NSString *chooseButtonTitle;

- (int)getTypeInt;
- (void)setTypeInt:(int)value;
- (id)initWithFrame:(CGRect)frame source:(NSString *)source;
- (NSString *)getTypeName:(int)value;

@end
