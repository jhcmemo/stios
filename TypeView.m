
#import "TypeView.h"
#define buttonColor1 [UIColor colorWithRed:252.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1]
#define buttonColor2 [UIColor whiteColor]
//#define maskColor [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:0.5]
#define maskColor [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5]

@implementation TypeView

- (id)initWithFrame:(CGRect)frame source:(NSString *)source {
    self = [super initWithFrame:frame];
    if (self) {
        _typeContent = [NSArray arrayWithObjects:
                        NSLocalizedString(@"productType_clothes", @"衣飾"),
                        NSLocalizedString(@"productType_3c", @"3C"),
                        NSLocalizedString(@"productType_ticket", @"票券"),
                        NSLocalizedString(@"productType_cosmetics", @"美妝"),
                        NSLocalizedString(@"productType_life", @"生活"),
                        NSLocalizedString(@"productType_book", @"書籍"),
                        NSLocalizedString(@"productType_service", @"服務"),
                        NSLocalizedString(@"productType_food", @"食品"),
                        NSLocalizedString(@"productType_pet", @"寵物"),
                        NSLocalizedString(@"productType_others", @"其他"),
                        nil];
        _typeValue = [NSArray arrayWithObjects:@"3", @"2", @"5", @"9", @"6", @"4", @"8", @"7", @"10", @"1", nil];
        typeImage = [[NSArray alloc] initWithObjects:
                     [UIImage imageNamed:@"cloth.png"],
                     [UIImage imageNamed:@"3c.png"],
                     [UIImage imageNamed:@"ticket.png"],
                     [UIImage imageNamed:@"cosmatic.png"],
                     [UIImage imageNamed:@"life.png"],
                     [UIImage imageNamed:@"book.png"],
                     [UIImage imageNamed:@"service.png"],
                     [UIImage imageNamed:@"food.png"],
                     [UIImage imageNamed:@"pet.png"],
                     [UIImage imageNamed:@"other.png"], nil];

        int buttonCounts = (int)[_typeContent count];
        if (buttonCounts > 10) {
            buttonCounts = 10;
        }
        CGFloat btnHeight = 80;
        CGFloat topMargin = ([UIScreen mainScreen].bounds.size.height - btnHeight * 5) / 2;
        
        backgroundView = [[UIView alloc]initWithFrame:self.frame];
        [backgroundView setBackgroundColor:maskColor];
        [self addSubview:backgroundView];
        
        contentView = [[UIView alloc]initWithFrame:CGRectMake(20, topMargin, 280, btnHeight * 5)];
        [contentView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:contentView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapEvent:)];
        [backgroundView addGestureRecognizer:tapGesture];
        
        BOOL colorFlag = YES;
        for (int i = 0, j = 0; i < buttonCounts; i+=2, j++) {
            typeBtnImage[i] = [[UIImageView alloc] initWithFrame:CGRectMake(20 + 40, topMargin + btnHeight * j, 60, 60)];
            [typeBtnImage[i] setImage:[typeImage objectAtIndex:i]];
            typeButton[i] = [[UIButton alloc]initWithFrame:CGRectMake(20, topMargin + btnHeight * j, 140, btnHeight)];
            [typeButton[i] setTitle:[_typeContent objectAtIndex:i] forState:UIControlStateNormal];
            [typeButton[i] setTag:i];
            [typeButton[i] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [typeButton[i] addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [typeButton[i] setBackgroundColor:(colorFlag ? buttonColor1 : buttonColor2)];
            [[typeButton[i] titleLabel] setFont:[UIFont systemFontOfSize:12]];
            [typeButton[i] setTitleEdgeInsets:UIEdgeInsetsMake(60, 0, 10, 0)];

            typeBtnImage[i+1] = [[UIImageView alloc] initWithFrame:CGRectMake(20 + 180, topMargin + btnHeight * j, 60, 60)];
            [typeBtnImage[i+1] setImage:[typeImage objectAtIndex:i+1]];
            typeButton[i+1] = [[UIButton alloc]initWithFrame:CGRectMake(160, topMargin + btnHeight * j, 140, btnHeight)];
            [typeButton[i+1] setTitle:[_typeContent objectAtIndex:i+1] forState:UIControlStateNormal];
            [typeButton[i+1] setTag:i+1];
            [typeButton[i+1] setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [typeButton[i+1] addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [typeButton[i+1] setBackgroundColor:(colorFlag ? buttonColor2 : buttonColor1)];
            [[typeButton[i+1] titleLabel] setFont:[UIFont systemFontOfSize:12]];
            [typeButton[i+1] setTitleEdgeInsets:UIEdgeInsetsMake(60, 0, 10, 0)];
            
            colorFlag = !colorFlag;
            [self addSubview:typeButton[i]];
            [self addSubview:typeButton[i+1]];
            [self addSubview:typeBtnImage[i]];
            [self addSubview:typeBtnImage[i+1]];
        }
    }
    return self;
}

- (IBAction)buttonClick:(id)sender {
    typeInt = [_typeValue[[sender tag]] intValue];
    [self setChooseButtonTitle:[sender currentTitle]];
    [self removeFromSuperview];
    if ([_delegate respondsToSelector:@selector(TypeView:didSelectedType:)]) {
        [_delegate TypeView:self didSelectedType:[self chooseButtonTitle]];
    }
}
- (void)tapEvent:(UITapGestureRecognizer *)tapGesture {
    [self removeFromSuperview];
}
- (int)getTypeInt {
    return typeInt;
}
- (void)setTypeInt:(int)value {
    typeInt = value;
}
- (NSString *)getTypeName:(int)value {
    int i;
    BOOL haveMatch = NO;
    for (i = 0; i < _typeValue.count; i++) {
        if ([ _typeValue[i] isEqualToString:[NSString stringWithFormat:@"%d", value] ]) {
            haveMatch = YES;
            break;
        }
    }
    if (haveMatch) {
        return _typeContent[i];
    }else {
        return @"";
    }
}

@end
