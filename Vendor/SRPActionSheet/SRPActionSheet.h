// SRPActionSheet.h
// 
// Copyright (c) 2014年 Shinren Pan
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>

/**
 *  點擊按鈕後的反饋
 *
 *  @param sheet       執行點擊的 UIActionSheet
 *  @param buttonIndex 點中的按鈕 index
 */
typedef void(^SRPActionSheetCallback)(UIActionSheet *sheet, NSInteger buttonIndex);


/**
 *  使用 block 反饋的 UIActionSheet
 */
@interface SRPActionSheet : UIActionSheet


///-----------------------------------------------------------------------------
/// @name Public methods
///-----------------------------------------------------------------------------

/**
 *  Show from UIToolbar
 *
 *  @param view     UIToolbar
 *  @param callback 點擊按鈕後的反饋
 */
- (void)showFromToolbar:(UIToolbar *)view callback:(SRPActionSheetCallback)callback;

/**
 *  Show from UITabBar
 *
 *  @param view     UITabBar
 *  @param callback 點擊按鈕後的反饋
 */
- (void)showFromTabBar:(UITabBar *)view callback:(SRPActionSheetCallback)callback;

/**
 *  Show from UIBarButtonItem
 *
 *  @param item     UIBarButtonItem
 *  @param animated 是否動畫
 *  @param callback 點擊按鈕後的反饋
 */
- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated callback:
(SRPActionSheetCallback)callback;

/**
 *  Show from CGRect in UIView
 *
 *  @param rect     CGRect
 *  @param view     UIView
 *  @param animated 是否動畫
 *  @param callback 點擊按鈕後的反饋
 */
- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated callback:
(SRPActionSheetCallback)callback;

/**
 *  Show in UIView
 *
 *  @param view     UIView
 *  @param callback 點擊按鈕後的反饋
 */
- (void)showInView:(UIView *)view callback:(SRPActionSheetCallback)callback;

@end
