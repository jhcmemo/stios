// SRPActionSheet.m
// 
// Copyright (c) 2014年 Shinren Pan
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "SRPActionSheet.h"


@interface SRPActionSheet ()<UIActionSheetDelegate>

@property (nonatomic, copy) SRPActionSheetCallback callback;

@end


@implementation SRPActionSheet

#pragma mark - Show from UIToolbar
- (void)showFromToolbar:(UIToolbar *)view callback:(SRPActionSheetCallback)callback
{
    if(callback)
    {
        self.delegate = self;
        self.callback = callback;
    }
    
    [super showFromToolbar:view];
}

#pragma mark - Show from UITabBar
- (void)showFromTabBar:(UITabBar *)view callback:(SRPActionSheetCallback)callback
{
    if(callback)
    {
        self.delegate = self;
        self.callback = callback;
    }
    
    [super showFromTabBar:view];
}

#pragma mark - Show from UIBarButtonItem
- (void)showFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated callback:
(SRPActionSheetCallback)callback
{
    if(callback)
    {
        self.delegate = self;
        self.callback = callback;
    }
    
    [super showFromBarButtonItem:item animated:animated];
}

#pragma mark - Show from CGRect
- (void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated callback:
(SRPActionSheetCallback)callback
{
    if(callback)
    {
        self.delegate = self;
        self.callback = callback;
    }
    
    [super showFromRect:rect inView:view animated:animated];
}

#pragma mark - Show in UIView
- (void)showInView:(UIView *)view callback:(SRPActionSheetCallback)callback
{
    if(callback)
    {
        self.delegate = self;
        self.callback = callback;
    }
    
    [super showInView:view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(_callback)
    {
        _callback(actionSheet, buttonIndex);
        
        self.callback = nil;
    }
}

@end
