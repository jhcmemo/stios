// SRPPopupMenu.m
// 
// Copyright (c) 2014年 Shinren Pan
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "SRPPopupMenu.h"

// otherButtos 起始的角度, -90 度為正上方, 拍拍賣為逆時針排序
static const CGFloat OTHERBUTTONS_START_ANGLE = - 90.0;

// otherButtons 彈跳效果
static const CGFloat OTHERBUTTONS_ANIMATION_DAMPING = 0.4;

// otherButtons 動畫時間
static const CGFloat OTHERBUTTONS_ANIMATION_DURATION = 0.5;

// otherButtons 從中心點移動的距離
static const CGFloat OTHERBUTTONS_DISTANCE_FROM_CENTER = 100.0;

// mainButton 彈跳效果
static const CGFloat MAINBUTTON_ANIMATION_DAMPING = 0.6;

// mainButton 動畫時間
static const CGFloat MAINBUTTON_ANIMATION_DURATION = 0.5;

// Menu 背景透明度
static const CGFloat SRPPOPUPMENU_BACKGROUNDCOLOR_OPACITY = 0.5;

// Menu 點擊按鈕後的 Notification
NSString * const SRPPOPUPMENU_BUTTON_CLICKED = @"com.shinrenpan.SRPPopupMenu";


@interface SRPPopupMenu ()

@property (nonatomic, weak  ) IBOutlet UIButton *mainButton;// 主選單按鈕
@property (nonatomic, assign) BOOL     dragging;            // mainButton 是否正在拖動
@property (nonatomic, assign) BOOL     animating;           // 是否正在執行動畫
@property (nonatomic, assign) BOOL     menuOpened;          // 選單是否已經打開
@property (nonatomic, strong) UIColor  *bgColor;            // 選單打開時的背景顏色
@property (nonatomic, assign) CGPoint  mainButtonPrevCenter;// mainButton 上個位置
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *otherButtons; // 其他按鈕

@end


@implementation SRPPopupMenu

#pragma mark - LifeCycle
+ (instancetype)singleton
{
    static dispatch_once_t onceToken;
    static SRPPopupMenu *_singleton;
    
    dispatch_once(&onceToken, ^{
        
        UINib *nib = [UINib nibWithNibName:@"SRPPopupMenu" bundle:nil];
        _singleton = [nib instantiateWithOwner:nil options:nil][0];
    });
    
    return _singleton;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.hidden               = YES;
    self.layer.zPosition      = 101; // 利用 zPosition 將 SRPPopupMenu 一直置於 App 最上層
    self.frame                = [UIScreen mainScreen].bounds;
    self.autoresizingMask     = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.mainButtonPrevCenter = _mainButton.center;
    
    [self bringSubviewToFront:_mainButton];
    
    UITapGestureRecognizer *tap =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSelfToCloseMenu:)];
    
    [self addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(__deviceOrientationDidChange:)
                                                name:UIDeviceOrientationDidChangeNotification
                                              object:nil];
    
    //將mainButton設定背景為商品列表頁的預設型別
    [_mainButton setBackgroundImage:[UIImage imageNamed:@"PopMenu-New-Button"] forState:UIControlStateNormal];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if(_menuOpened)
    {
        [self __openMenuWithAnimated:NO];
    }
    else
    {
        [self __closeMenuWithAnimated:NO];
    }
}

#pragma mark - Properties Getter
- (CGPoint)mainButtonPrevCenter
{
    // 調整 mainButtonPrevCenter, 確保不會超出螢幕
    // 另外當螢幕旋轉時, 也一併調整
    
    CGFloat selfWidth        = CGRectGetWidth(self.bounds);
    CGFloat selfHeight       = CGRectGetHeight(self.bounds);
    CGFloat mainButtonWidth  = CGRectGetWidth(_mainButton.bounds);
    CGFloat mainButtonHeight = CGRectGetHeight(_mainButton.bounds);
    
    // 這裡發生情形基本上是, 直向時當 _mainButton 在右側, 轉成橫向時, _mainButton 會停留在原位置, 不會靠右
    if(_mainButtonPrevCenter.x > mainButtonWidth)
    {
        _mainButtonPrevCenter.x = selfWidth - mainButtonWidth / 2;
    }
    
    // 當 _mainButton 拖曳超過螢幕下方
    if(_mainButtonPrevCenter.y > selfHeight - mainButtonHeight / 2)
    {
        _mainButtonPrevCenter.y = selfHeight - mainButtonHeight / 2;
    }
    
    // 當 _mainButton 拖曳超過螢幕上方
    if(_mainButtonPrevCenter.y < mainButtonHeight / 2)
    {
        _mainButtonPrevCenter.y = mainButtonHeight / 2;
    }
    
    return _mainButtonPrevCenter;
}

#pragma mark - 傳遞 touch
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    __block UIView *view = self;
    
    // 隱藏時不要擋到下面的東西
    if ([self isHidden])
        return nil;
    
    // 點到 mainButton
    if(CGRectContainsPoint(_mainButton.frame, point))
    {
        return _mainButton;
    }
    
    // 選單未展開時
    if(!_menuOpened)
    {
        // return nil 代表可以穿透 SRPPopupMenu 操作下方 UIViewController
        return nil;
    }
    
    // 選單展開且可能按到 otherButtons
    else
    {
        for(UIButton *button in _otherButtons)
        {
            if(CGRectContainsPoint(button.frame, point))
            {
                view = button;
                break;
            }
        }
    }
    
    return view;
}

#pragma mark - Menu show / hide
- (void)showWithColor:(UIColor *)color
{
    if(!self.hidden)
    {
        return;
    }
    
    UIWindow *window = ({
        UIWindow *aWindow = [[UIApplication sharedApplication]keyWindow];
        
        if(!aWindow)
        {
            aWindow = [[UIApplication sharedApplication]windows][0];
        }
        
        aWindow;
    });
    
    if(self.superview != window)
    {
        [window addSubview:self];
    }
    
    if(!CGColorEqualToColor(_bgColor.CGColor, color.CGColor))
    {
        self.bgColor = [color colorWithAlphaComponent:SRPPOPUPMENU_BACKGROUNDCOLOR_OPACITY];
    }
    
    self.hidden = NO;
}

- (void)hide
{
    if(self.hidden)
    {
        return;
    }
    
    self.hidden = YES;
}

#pragma mark - 點擊關閉選單
- (void)tapSelfToCloseMenu:(id)handle
{
    if(_menuOpened)
    {
        [self __closeMenuWithAnimated:YES];
    }
}

#pragma mark - IBAction
#pragma mark 拖動 mainButton, UIControlEventTouchDragInside
- (IBAction)mainButtonDragging:(UIButton *)sender forEvent:(UIEvent *)event
{
    // 選單展開時, mainButton 不能拖動
    if(_menuOpened)
    {
        return;
    }
    
    self.dragging = YES;
    sender.center = ({
        UITouch *touch = [[event allTouches]anyObject];
        CGPoint point  = [touch locationInView:self];
        point;
    });
}

#pragma mark - 結束拖動 mainButton 或是點擊 mainButton, UIControlEventTouchUpInside
- (IBAction)mainButtonEndDragOrClicked:(UIButton *)sender
{
    // 這裡是 mainButton 拖動結束
    if(_dragging)
    {
        self.dragging = NO;
        
        self.mainButtonPrevCenter = ({
            CGPoint toPoint = CGPointZero;
            
            // 拖曳未超過一半, 向左
            if(_mainButton.center.x < CGRectGetMidX(self.bounds))
            {
                toPoint = CGPointMake(CGRectGetMidX(_mainButton.bounds), _mainButton.center.y);
            }
            
            // 拖曳超過一半, 向右
            else
            {
                toPoint = CGPointMake(CGRectGetMaxX(self.bounds) - CGRectGetMidX(_mainButton.bounds),
                                      _mainButton.center.y);
            }
            
            toPoint;
        });
        
        [UIView animateWithDuration:MAINBUTTON_ANIMATION_DURATION
                              delay:0.0
             usingSpringWithDamping:MAINBUTTON_ANIMATION_DAMPING
              initialSpringVelocity:0.0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             
            _mainButton.center = self.mainButtonPrevCenter;
        } completion:nil];
    }
    
    // 這裡是點擊到 mainButton
    else if(_menuOpened)
    {
        [self __closeMenuWithAnimated:YES];
    }
    else
    {
        [self __openMenuWithAnimated:YES];
    }
}

#pragma mark - 點中其他 Button
- (IBAction)otherButtonDidClicked:(UIButton *)sender
{
    [self __closeMenuWithAnimated:YES];
    
    NSUInteger tag = sender.tag - 1;
    
    if(tag < 4)
    {
        NSArray *images = [self __imageNames];
        NSString *imgName = images[tag];
        [_mainButton setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    }
    
    [self __waittingAnimationFinished:^{
        
        [[NSNotificationCenter defaultCenter]postNotificationName:SRPPOPUPMENU_BUTTON_CLICKED
                                                           object:@(sender.tag)];
    }];
}

#pragma mark - 等待動畫結束
- (void)__waittingAnimationFinished:(void (^)())finished
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        while (_animating) {}
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(finished)
            {
                finished();
            }
        });
    });
}

#pragma mark - Menu open / close animation
- (void)__openMenuWithAnimated:(BOOL)flag
{
    if(_animating)
    {
        return;
    }
    
    self.animating       = YES;
    self.menuOpened      = YES;
    self.backgroundColor = _bgColor;
    
    if(!flag)
    {
        _mainButton.center = self.center;
        
        [self __handleOtherButtonsOpen];
        
        self.animating = NO;
        
        return;
    }
    
    [UIView animateWithDuration:MAINBUTTON_ANIMATION_DURATION
                          delay:0.0
         usingSpringWithDamping:MAINBUTTON_ANIMATION_DAMPING
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
        _mainButton.center = self.center;
                         
    } completion:nil];
    
    // 因為要等 MainButton 動作完才展開其他按鈕, 所以 delay = MAINBUTTON_ANIMATION_DURATION
    [UIView animateWithDuration:OTHERBUTTONS_ANIMATION_DURATION
                          delay:MAINBUTTON_ANIMATION_DURATION
         usingSpringWithDamping:OTHERBUTTONS_ANIMATION_DAMPING
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
        [self __handleOtherButtonsOpen];

    } completion:^(BOOL finished) {
        
        self.animating = !finished;
    }];
    
}

- (void)__closeMenuWithAnimated:(BOOL)flag
{
    if(_animating)
    {
        return;
    }
    
    self.animating = YES;
    
    if(!flag)
    {
        _mainButton.center   = self.mainButtonPrevCenter;
        
        [self __handleOtherButtonClose];
        
        self.animating       = NO;
        self.menuOpened      = NO;
        self.backgroundColor = [UIColor clearColor];
        
        return;
    }
    
    [UIView animateWithDuration:OTHERBUTTONS_ANIMATION_DURATION
                          delay:0.0
         usingSpringWithDamping:OTHERBUTTONS_ANIMATION_DAMPING
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
        
        [self __handleOtherButtonClose];
        
    } completion:nil];
    
    // 因為要等其他按鈕收起來才執行, 所以 delay = OTHERBUTTONS_ANIMATION_DURATION
    [UIView animateWithDuration:MAINBUTTON_ANIMATION_DURATION
                          delay:OTHERBUTTONS_ANIMATION_DURATION
         usingSpringWithDamping:MAINBUTTON_ANIMATION_DAMPING
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
              
              _mainButton.center = self.mainButtonPrevCenter;
              
    } completion:^(BOOL finished) {
        
        self.animating       = !finished;
        self.menuOpened      = NO;
        self.backgroundColor = [UIColor clearColor];
    }];
}

#pragma mark - otherButtons open / close
- (void)__handleOtherButtonsOpen
{
    CGPoint startPoint = self.center;
    
    NSArray *titles = [self __titles];
    
    [_otherButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
        
        button.alpha  = 1.0;
        NSInteger tag = button.tag - 1;
        CGFloat angle = - (360.0 / _otherButtons.count);
        button.center = ({
            CGPoint toPoint = CGPointZero;
            CGFloat degree  = ((angle * tag) + OTHERBUTTONS_START_ANGLE) * (M_PI / 180);
            toPoint.x       = startPoint.x + cosf(degree) * OTHERBUTTONS_DISTANCE_FROM_CENTER;
            toPoint.y       = startPoint.y + sinf(degree) * OTHERBUTTONS_DISTANCE_FROM_CENTER;
            toPoint;
        });
        
        if(tag < 4)
        {
            [button setTitle:titles[tag] forState:UIControlStateNormal];
        }
    }];
}

- (void)__handleOtherButtonClose
{
    for(UIButton *button in _otherButtons)
    {
        button.center = self.center;
        button.alpha = 0.0;
    }
}

#pragma mark - device 旋轉
- (void)__deviceOrientationDidChange:(NSNotification *)notification
{
    self.frame = [[UIScreen mainScreen]bounds];
    
    [self setNeedsDisplay];
}

- (NSArray *)__titles
{
    return @[@"最 新", @"最熱門", @"最便宜", @"最 近"];
}

- (NSArray *)__imageNames
{
    return @[@"PopMenu-New-Button", @"PopMenu-Hot-Button", @"PopMenu-Cheap-Button", @"PopMenu-Near-Button"];
}

@end
