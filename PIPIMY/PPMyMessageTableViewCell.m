
#import "PPMyMessageTableViewCell.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PPAPIGetChatContentResponse.h"


@interface PPMyMessageTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImageView;

@end


@implementation PPMyMessageTableViewCell

#pragma mark - life cycles

-(void)awakeFromNib
{
    // screen - (time label + leading margin) - (trailing margin) - (middle margins * 3) - (safety margin)
    CGFloat width = [[UIScreen mainScreen] applicationFrame].size.width - 60 - 8 - (8 * 3) - 10;
    self.contentLabel.preferredMaxLayoutWidth = width;
    
    // bubble background
    UIImage *bubble = [UIImage imageNamed:@"message-bubble"];
    [self.bubbleImageView setImage:[bubble resizableImageWithCapInsets:UIEdgeInsetsMake(25, 24, 22, 35)
                                                          resizingMode:UIImageResizingModeStretch]];
}

#pragma mark - Configuration

- (void)configureCellWithMessage:(PPAPIGetChatContentResponse *)message chat:(NSDictionary *)chat
{
    // sent time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *timeString = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:message.sendTime]];
    
    _timeLabel.text = timeString;
    
    // chat message
    _contentLabel.text = message.content;
    
    if (message.isPending) {
        _contentLabel.textColor = [UIColor redColor];
    } else {
        _contentLabel.textColor = [UIColor whiteColor];
    }
}

@end
