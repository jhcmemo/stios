
/**
 *  列表 Cell 的型態
 */
typedef NS_ENUM(NSUInteger, DisplayCellType){
    /**
     *  小圖 Cell
     */
    DisplayCellTypeSmall = 0,
    /**
     *  中圖 Cell
     */
    DisplayCellTypeMedium = 1,
    /**
     *  大圖 Cell
     */
    DisplayCellTypeBig = 2
};

#import <UIKit/UIKit.h>
#import "PPProtocols.h"
#import "PPAPIGetNearProductResponse.h"


/**
 *  商品小圖式的 Cell
 */
@interface PPProductSmallCell : UICollectionViewCell



@property (nonatomic, weak) IBOutlet UIImageView *productIcon;  // 商品 Icon
@property (nonatomic, weak) IBOutlet UIButton    *favoriteButton;// 加入我的最愛按鈕
@property (nonatomic, weak) IBOutlet UILabel     *productPrice; // 商品價格
@property (nonatomic, weak) IBOutlet UILabel     *productName;  // 商品名稱
@property (nonatomic, weak) IBOutlet UIButton    *userIcon;     // 上傳者 Icon
@property (nonatomic, weak) IBOutlet UILabel     *userName;     // 上傳者姓名
@property (nonatomic, weak) IBOutlet UILabel     *distance;     // 使用者距離上傳位置
@property (nonatomic, weak) IBOutlet UILabel     *uploadDate;   // 上傳時間


/**
 *  按下最愛, 影片, 使用者頭像的委託
 */
@property (nonatomic, weak)id<PPProtocols>delegate;

+ (CGSize)layoutSizeWithCellType:(DisplayCellType)type InCollectionView:(UICollectionView *)view;



/**
 *  設置 Cell
 *
 *  @param product 商品
 */
- (void)configureWithProduct:(PPAPIGetNearProductResponse *)product;

@end
