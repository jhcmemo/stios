
#import "PPAPILoginResponse.h"

@implementation PPAPILoginResponse

#pragma mark - Key Map
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"Id" : @"id"};
}

#pragma mark - Properties Getter
- (BOOL)success
{
    return [_result isEqualToString:@"success"];
}

@end
