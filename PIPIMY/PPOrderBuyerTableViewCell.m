
#import "PPOrderBuyerTableViewCell.h"
#import "PPAPIGetOrderListResponse.h"
#import "PPOrderOptionsViewsModel.h"
#import "PPAPI.h"

@interface PPOrderBuyerTableViewCell () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellerLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;

@property (weak, nonatomic) IBOutlet UIButton *buyerConfirmPaymentButton;
@property (weak, nonatomic) IBOutlet UIButton *sellerConfirmShippingButton;
@property (weak, nonatomic) IBOutlet UIButton *buyerConfirmOrderFinishButton;

@property (weak, nonatomic) IBOutlet UILabel *buyerPaymentDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellerShippingDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyerReceiveDateLabel;

@property (assign, nonatomic) BOOL isBuyer;

@end

@implementation PPOrderBuyerTableViewCell

#pragma mark - Cell 設置

- (void)awakeFromNib
{
    // 判斷為買家或賣家訂單
    if ([self.reuseIdentifier isEqualToString:@"buyerCell"])
        self.isBuyer = YES;
    else if ([self.reuseIdentifier isEqualToString:@"sellerCell"])
        self.isBuyer = NO;
    
    // UI
    self.buyerConfirmPaymentButton.layer.borderWidth = 1.0;
    self.buyerConfirmPaymentButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.buyerConfirmPaymentButton.layer.cornerRadius = 4.0;
    self.buyerConfirmPaymentButton.layer.masksToBounds = YES;
    self.buyerConfirmPaymentButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.sellerConfirmShippingButton.layer.borderWidth = 1.0;
    self.sellerConfirmShippingButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.sellerConfirmShippingButton.layer.cornerRadius = 4.0;
    self.sellerConfirmShippingButton.layer.masksToBounds = YES;
    self.sellerConfirmShippingButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.buyerConfirmOrderFinishButton.layer.borderWidth = 1.0;
    self.buyerConfirmOrderFinishButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.buyerConfirmOrderFinishButton.layer.cornerRadius = 4.0;
    self.buyerConfirmOrderFinishButton.layer.masksToBounds = YES;
    self.buyerConfirmOrderFinishButton.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)configureCellWithOrder:(PPAPIGetOrderListResponse *)order
{
    self.order = order;
    self.timeLabel.text = [NSString stringWithFormat:@"訂單更新時間：%@", [self longDate:order.updatedAt]];
    self.sellerLabel.text = [NSString stringWithFormat:@"賣家：%@", order.sellerUserId];
    self.itemsLabel.text = [self stringWithItems:order.items];
    self.sumLabel.text = [NSString stringWithFormat:@"金額：$%@", order.sum];
    
    [self configureFirstButton];
    [self configureSecondButton];
    [self configureThirdButton];
}

- (void)configureFirstButton
{
    // 時間 label
    self.buyerPaymentDateLabel.text   = [self shortDate:self.order.paymentDate];
    
    // 是否為面交
    if ([self.order.paymentIsHand boolValue])
    {
        [self.buyerConfirmPaymentButton setTitle:@"買家付款\n(面交)" forState:UIControlStateNormal];
        self.buyerConfirmPaymentButton.backgroundColor = [UIColor colorWithWhite:220.0/255.0 alpha:1.0];
        
        // exit
        return;
    }
    else
    {
        [self.buyerConfirmPaymentButton setTitle:@"買家付款" forState:UIControlStateNormal];
    }
    
    // 完成狀態
    if ([self.order.paymentStatus boolValue])
        self.buyerConfirmPaymentButton.backgroundColor = [UIColor greenColor];
    else
        self.buyerConfirmPaymentButton.backgroundColor = [UIColor clearColor];
    
}

- (void)configureSecondButton
{
    // 時間 label
    self.sellerShippingDateLabel.text = [self shortDate:self.order.logisticsDate3];
    
    // 是否為面交
    if ([self.order.logisticsIsHand boolValue])
    {
        [self.sellerConfirmShippingButton setTitle:@"賣家出貨\n(面交)" forState:UIControlStateNormal];
        self.sellerConfirmShippingButton.backgroundColor = [UIColor colorWithWhite:220.0/255.0 alpha:1.0];
        self.sellerConfirmShippingButton.enabled = NO;
        
        // exit
        return;
    }
    else
    {
        [self.sellerConfirmShippingButton setTitle:@"賣家出貨" forState:UIControlStateNormal];
    }
    
    // 只有是賣家 & 使用自寄才能按
    if (!self.isBuyer && [self.order.logisticsType integerValue] == (1 << PPShippingOptionTypeCustom))
    {
        self.sellerConfirmShippingButton.enabled = YES;
        [self.sellerConfirmShippingButton setTitleColor:nil forState:UIControlStateNormal];
    }
    else
    {
        self.sellerConfirmShippingButton.enabled = NO;
        [self.sellerConfirmShippingButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    // 完成狀態
    self.sellerConfirmShippingButton.backgroundColor = [UIColor clearColor];
    
}

- (void)configureThirdButton
{
    // 時間 label
    self.buyerReceiveDateLabel.text = [self shortDate:self.order.logisticsDate5];
    
    // 是否為面交
    if ([self.order.logisticsIsHand boolValue])
    {
        [self.buyerConfirmOrderFinishButton setTitle:@"買家取貨\n(面交)" forState:UIControlStateNormal];
        self.buyerConfirmOrderFinishButton.backgroundColor = [UIColor colorWithWhite:220.0/255.0 alpha:1.0];
        self.buyerConfirmOrderFinishButton.enabled = NO;
        
        // exit
        return;
    }
    else
    {
        [self.buyerConfirmOrderFinishButton setTitle:@"買家取貨" forState:UIControlStateNormal];
    }
    
    // 只有是買家 & 已付款 & 未確認完成交易 & 物流為 1/2/4/8 才能按
    if (self.isBuyer && [self.order.paymentStatus boolValue] && [self.order.pending integerValue] != -1 &&
        ([self.order.logisticsType integerValue] & 0b1111))
    {
        self.buyerConfirmOrderFinishButton.enabled = YES;
        [self.buyerConfirmOrderFinishButton setTitleColor:nil forState:UIControlStateNormal];
    }
    else
    {
        self.buyerConfirmOrderFinishButton.enabled = NO;
        [self.buyerConfirmOrderFinishButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    // 完成狀態
    self.buyerConfirmOrderFinishButton.backgroundColor = [UIColor clearColor];
    
}

#pragma mark - 賣家出貨

- (IBAction)sellerConfirmShippedButtonClicked
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"寄件編號" message:@"請輸入寄件編號 (100 字元內)" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"送出", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if ([[alertView textFieldAtIndex:0].text length] > 100)
        return NO;
    else
        return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
        return;
    
    if ([alertView.title isEqualToString:@"寄件編號"])
    {
        NSDictionary *params = @{ @"orderId"       : self.order.Id,
                                  @"logisticsDesc" : [alertView textFieldAtIndex:0].text };
        
        [[PPAPI singleton] APIOrderSetOtherLogistics:params callback:^(NSError *err) {
            if (err)
            {
                UIAlertView *sendFailAlert = [[UIAlertView alloc] initWithTitle:@"錯誤" message:@"傳送失敗，請稍後再試" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
                [sendFailAlert show];
            }
            else
            {
                NSString *message = [NSString stringWithFormat:@"已設定寄件編號：\n%@", [alertView textFieldAtIndex:0].text];
                UIAlertView *sendSuccessAlert = [[UIAlertView alloc] initWithTitle:@"成功" message:message delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
                [sendSuccessAlert show];
            }
        }];
    }
}

#pragma mark - 買家取貨

- (IBAction)buyerConfirmOrderFinishButtonClicked
{
    NSDictionary *params = @{ @"orderId" : self.order.Id };
    [[PPAPI singleton] APIConfirmOrderFinish:params callback:^(PPAPIConfirmOrderFinishResponse *response, NSError *err) {
        if (err)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"錯誤" message:[err localizedDescription] delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            self.order.pending = response.pending;
            self.order.queue = response.queue;
            self.order.updatedAt = response.confirmDate;
            // Update display here
        }
    }];
}

#pragma mark - Helper functions

- (NSString *)stringWithItems:(NSArray *)items
{
    NSMutableString *string = [NSMutableString stringWithString:@"購買商品："];
    [string appendString:[items firstObject]];
    for (NSInteger i = 1; i < [items count]; i++)
    {
        [string appendString:@"，"];
        [string appendString:items[i]];
    }
    return string;
}

- (NSString *)longDate:(NSDate *)date
{
    if (!date)
        return @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormatter stringFromDate:date];
}

- (NSString *)shortDate:(NSDate *)date
{
    if (!date)
        return @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd HH:mm"];
    return [dateFormatter stringFromDate:date];
}

@end
