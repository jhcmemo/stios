
#import <Mantle.h>


/**
 *  API getProductID 回傳的 Response
 *
 *  基本上是新商品的 Id
 */
@interface PPAPIGetProductIDResponse : MTLModel<MTLJSONSerializing>



/**
 *  是否成功
 */
@property (nonatomic, readonly) BOOL success;

/**
 *  回傳資料
 */
@property (nonatomic, readonly) NSNumber *productId;

@end
