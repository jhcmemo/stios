
#import "PPUser.h"
#import "PPAWSS3.h"
#import "PPProductSmallCell.h"
#import <UIButton+WebCache.h>
#import <UIImageView+WebCache.h>
#import <CoreLocation/CoreLocation.h>
#import "PPProductMediumCell.h"


@implementation PPProductSmallCell

+ (CGSize)layoutSizeWithCellType:(DisplayCellType)type InCollectionView:(UICollectionView *)view
{
    CGSize superViewSize = view.frame.size;
    CGFloat padding = 6.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    
    if(type == DisplayCellTypeSmall)
    {
        width   = superViewSize.width - padding * 2;
        height  = 110;
    }
    else if(type == DisplayCellTypeMedium)
    {
        width   = (superViewSize.width - padding * 3) / 2.0;
        height  = width + 86.0;
    }
    else if(type == DisplayCellTypeBig)
    {
        width   = superViewSize.width - padding * 2;
        height  = width + 66.0;
    }
    
    return CGSizeMake(width, height);
}
#pragma mark - LifeCycle
- (void)awakeFromNib
{
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5;
    
    if([self.reuseIdentifier isEqualToString:@"smallCell"])
    {
        CGFloat userIconWidth         = CGRectGetWidth(_userIcon.bounds);
        _userIcon.layer.masksToBounds = YES;
        _userIcon.layer.cornerRadius  = userIconWidth / 2.0;
    }
    else if([self.reuseIdentifier isEqualToString:@"mediumCell"])
    {
        /*
        self.layer.cornerRadius       = 4.0;
        _userIcon.layer.masksToBounds = YES;
        _userIcon.layer.cornerRadius  = 4.0;
         */
        
        CGFloat userIconWidth         = CGRectGetWidth(_userIcon.bounds);
        _userIcon.layer.masksToBounds = YES;
        _userIcon.layer.cornerRadius  = userIconWidth / 2.0;
    }
    else if([self.reuseIdentifier isEqualToString:@"bigCell"])
    {
        /*
        self.layer.cornerRadius       = 4.0;
        CGFloat userIconWidth         = CGRectGetWidth(_userIcon.bounds);
        _userIcon.layer.masksToBounds = YES;
        _userIcon.layer.cornerRadius  = userIconWidth / 2.0;
         */
        
        CGFloat userIconWidth         = CGRectGetWidth(_userIcon.bounds);
        _userIcon.layer.masksToBounds = YES;
        _userIcon.layer.cornerRadius  = userIconWidth / 2.0;
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    _productPrice.text = nil;
    _productName.text  = nil;
    _userName.text     = nil;
    _distance.text     = nil;
    _uploadDate.text   = nil;
}

#pragma mark - 設置 Cell
- (void)configureWithProduct:(PPAPIGetNearProductResponse *)product
{
    NSURL *productIcon = [PPAWSS3 imageURLWithProductId:product.productId inIndex:1];
    NSURL *userIcon    = [PPAWSS3 userIconWithUserId:product.ID];
    _productPrice.text = [NSString stringWithFormat:@"NT$ %@", product.price];
    //_productName.text  = product.title;
    _productName.text =  [NSString stringWithFormat:@"%@%@",product.brand?
                                                    product.brand:@"",product.title?product.title:@""];

    //_userName.text     = product.ID.length ? product.ID : product.contact;
    _userName.text     = product.ID;
    
    _uploadDate.text   = [product.time substringWithRange:NSMakeRange(5, 5)];
    
    _distance.text = ({
        NSString *distance;
        CLLocation *userLoaction    = [PPUser singleton].location;
        CLLocationDegrees lat       = [product.lat doubleValue];
        CLLocationDegrees lng       = [product.lng doubleValue];
        CLLocation *produtcLocation = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
        CLLocationDistance dist     = [userLoaction distanceFromLocation:produtcLocation];
        double toKM                 = dist / 1000.0;
        distance = toKM > 10 ? @">10km" : [NSString stringWithFormat:@"%.1f km", toKM];
    });
    
    [_productIcon sd_setImageWithURL:productIcon placeholderImage:nil];

    [_userIcon sd_setBackgroundImageWithURL:userIcon forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"nohead"]];

    UIImage *likeImage;
    
    if([[PPUser singleton]isFavoriteWithProduct:product]) // 如果有加入過我的最愛...
    {
        likeImage = [UIImage imageNamed:@"Like-Enable-Button"];
    }
    else
    {
        likeImage = [UIImage imageNamed:@"Like-Disable-Button"];
    }
    
    [_favoriteButton setBackgroundImage:likeImage forState:UIControlStateNormal];
}

#pragma mark - IBAction
- (IBAction)videoButtonClicked:(id)sender
{
    if([_delegate respondsToSelector:@selector(productCell:videoButtonClickedAtIndex:)])
    {
        [_delegate productCell:self videoButtonClickedAtIndex:self.tag];
    }
}

- (IBAction)userIconClicked:(id)sender
{
    if([_delegate respondsToSelector:@selector(productCell:userIconClickedAtIndex:)])
    {
        [_delegate productCell:self userIconClickedAtIndex:self.tag];
    }
}

- (IBAction)favoruteButtonClicked:(id)sender
{
    if([_delegate respondsToSelector:@selector(productCell:favoriteClickedAtIndex:)])
    {
        [_delegate productCell:self favoriteClickedAtIndex:self.tag];
    }
}

@end
