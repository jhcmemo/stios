
#import <UIKit/UIKit.h>


/**
 *  某人的商店
 */
@interface PPSomeoneStoreController : UIViewController



/**
 *  某人的 Id
 */
@property (nonatomic, copy) NSString *memberId;

@end
