
#import <UIKit/UIKit.h>
@class  PPAPIGetChatContentResponse;


@protocol PPImageCellDelegate <NSObject>

@optional
- (void)goToProduct:(id)sender;

@end



@interface PPOtherImageTableViewCell : UITableViewCell

@property (nonatomic, weak) id delegate;

+ (CGFloat)cellHeight;

- (void)configureCellWithMessage:(PPAPIGetChatContentResponse *)message chat:(NSDictionary *)chat;

@end
