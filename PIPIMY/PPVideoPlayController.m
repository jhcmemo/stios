
#import "PPAWSS3.h"
#import "PPMediaPlayerView.h"
#import "PPVideoPlayController.h"
#import "PPAPIGetNearProductResponse.h"


@interface PPVideoPlayController ()

@property (nonatomic, strong) AVQueuePlayer *player;            // 播放器
@property (nonatomic, weak) IBOutlet UIView *menu;              // 暫停影片時的 menu
@property (nonatomic, weak) IBOutlet PPPlayerView *playerView;  // 播放頁面
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator; // 影片 Buffering View

@property (weak, nonatomic) IBOutlet UILabel *playerTitleLbl;


@end


@implementation PPVideoPlayController

#pragma mark - LifeCycle
- (void)dealloc
{
    [self __removeObserver];
}

- (void)playVideos:(NSArray *)videos inViewController:(UIViewController *)controller
{
    self.view.frame = controller.view.bounds;
    
    [controller addChildViewController:self];
    [controller.view addSubview:self.view];
    [self didMoveToParentViewController:controller];
    
    NSMutableArray *items = [NSMutableArray array];
    
    [videos enumerateObjectsUsingBlock:^(PPAPIGetNearProductResponse *obj, NSUInteger idx, BOOL *stop) {
        
        NSURL *videoURL    = [PPAWSS3 videoURLWithProductId:obj.productId];
        AVURLAsset *asset  = [AVURLAsset assetWithURL:videoURL];
        AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
        
        [items addObject:item];
    }];
    
    if (items.count ==1)
    {
        [_playerTitleLbl setText:@"商品播放"];
    }
    else if(items.count > 1)
    {
        [_playerTitleLbl setText:@"全部商品播放"];
    }
    
    self.player = [[AVQueuePlayer alloc]initWithItems:items];
    
    [self __addObserver];
    
    AVPlayerLayer *layer = (AVPlayerLayer *)_playerView.layer;
    layer.videoGravity   = AVLayerVideoGravityResizeAspectFill;
    layer.player         = _player;
}

- (void)exitPlayVideo
{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"status"])
    {
        AVPlayerStatus status = [change[NSKeyValueChangeNewKey]integerValue];
        
        if (status == AVPlayerStatusReadyToPlay)
        {
            _indicator.hidden = YES;
            [_player play];
        }
    }
    else if([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) // Buffering
    {
        if([_player.currentItem isPlaybackLikelyToKeepUp])
        {
            _indicator.hidden = YES;
            [_player play];
        }
        else
        {
            _indicator.hidden = NO;
            [_indicator startAnimating];
        }
    }
    else if ([keyPath isEqualToString:@"currentItem"]) // Bad or last video
    {
        
        if (change[NSKeyValueChangeNewKey] == [NSNull null])
        {
            [self exitPlayVideo];
        }
    }
}

#pragma mark - IBAction
- (IBAction)exitButtonClicked:(id)sender
{
    [self exitPlayVideo];
}

- (IBAction)showHideMenuButtonClicked:(id)sender
{
    if(_player.rate > 0.0)
    {
        _menu.hidden = NO;
        
        [_player pause];
    }
    else
    {
        _menu.hidden = YES;
        
        [_player play];
    }
}

- (IBAction)continueButtonClicked:(id)sender
{
    _menu.hidden = YES;
    
    [_player play];
}

- (IBAction)showDetailButtonClicked:(id)sender
{
    /*
     *  這裡是取正在播放的影片 URL, 再解出 ProductId
     *  影片 URL 格式為 d1yjq3haxfphfj.cloudfront.net/388-video.mp4
     */
    AVURLAsset *currentAsset    = (AVURLAsset *)_player.currentItem.asset;
    NSString *lastPathComponent = [[currentAsset URL]lastPathComponent];
    NSString *productId         = [lastPathComponent componentsSeparatedByString:@"-"].firstObject;
    
    if([_delegate respondsToSelector:@selector(videoPlayerController:shouldShowProductDetailWithId:)])
    {
        [_delegate videoPlayerController:self shouldShowProductDetailWithId:@([productId integerValue])];
    }
}


#pragma mark - Private methods
#pragma mark Observer
- (void)__addObserver
{
    [_player addObserver:self forKeyPath:@"status"
                 options:NSKeyValueObservingOptionNew context:nil];
    
    [_player addObserver:self forKeyPath:@"playbackLikelyToKeepUp"
                 options:NSKeyValueObservingOptionNew context:nil];
    
    [_player addObserver:self forKeyPath:@"currentItem"
                 options:NSKeyValueObservingOptionNew context:nil];

}

- (void)__removeObserver
{
    [_player removeObserver:self forKeyPath:@"status"];
    [_player removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    [_player removeObserver:self forKeyPath:@"currentItem"];
    [_player pause];
}

#pragma mark - 顯示錯誤訊息
- (void)__showErrorMessage
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:@"無法播放該影片"
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles: nil];
    [alert show];
}

@end
