
#import <Mantle.h>


/**
 *  API 回傳 success 的 response
 */
@interface PPAPISuccessResponse : MTLModel<MTLJSONSerializing>



/**
 *  是否成功
 */
@property (nonatomic, readonly) BOOL success;

/**
 *  狀態
 */
@property (nonatomic, readonly) NSString *result;

@end
