
#import <UIKit/UIKit.h>
@class PPAPIGetOrderListResponse;

typedef NS_ENUM(NSInteger, PPOrderStatusCode)
{
    PPOrderStatusCodeInitial                = 1,    // 訂單成立
    PPOrderStatusCodeBuyerPaid              = 2,    // 買家付款
    PPOrderStatusCodeSellerShipped          = 3,    // 賣家發貨
    PPOrderStatusCodeShipmentArrivedAtStore = 4,    // 貨品到門市
    PPOrderStatusCodeBuyerPickedUp          = 5,    // 買家取貨
    PPOrderStatusCodeReturnOrder            = 6     // 退貨
};

@interface PPOrderBuyerTableViewCell : UITableViewCell

@property (nonatomic, weak) PPAPIGetOrderListResponse *order;

- (void)configureCellWithOrder:(PPAPIGetOrderListResponse *)order;

@end
