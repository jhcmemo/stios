
#import <Mantle.h>
#import <Foundation/Foundation.h>


/**
 *  API getMyDeal Response
 *
 *  回傳我交易的資料
 */
@interface PPAPIGetMyDealResponse : MTLModel<MTLJSONSerializing>



/**
 *  賣方
 */
@property (nonatomic, readonly) NSString *userIDFrom;

/**
 *  買方
 */
@property (nonatomic, readonly) NSString *userIDTo;

/**
 *  商品 Id
 */
@property (nonatomic, readonly) NSNumber *productId;

/**
 *  商品名稱
 */
@property (nonatomic, readonly) NSString *productTitle;

/**
 *  商品價格
 */
@property (nonatomic, readonly) NSNumber *productPrice;

/**
 *  交易時間
 */
@property (nonatomic, readonly) NSString *time;

/**
 *  評分
 */
@property (nonatomic, readonly) NSNumber *ratingScore;

/**
 *  評論
 */
@property (nonatomic, readonly) NSString *ratingComment;

@end
