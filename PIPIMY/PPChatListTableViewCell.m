
#import "PPChatListTableViewCell.h"

#import "PPUser.h"
#import "PPAPI.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface PPChatListTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *productIcon;
@property (weak, nonatomic) IBOutlet UILabel *userIDFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *productTitle;
@property (weak, nonatomic) IBOutlet UILabel *lastSentence;
@property (weak, nonatomic) IBOutlet UILabel *updateTime;
@property (weak, nonatomic) IBOutlet UIView *badgeBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;

@end


@implementation PPChatListTableViewCell

- (void)configureCellForChat:(PPAPIGetChatIndexResponse *)chatResponse asSeller:(BOOL)isSeller {
    
    // 處理時間格式
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    
    // Setup subviews
    self.userIDFromLabel.text = isSeller ? chatResponse.userIDFrom : chatResponse.userIDTo;
    self.productTitle.text = chatResponse.productTitle;
    self.lastSentence.text = chatResponse.lastSentence;
    self.updateTime.text = [dateFormatter stringFromDate:chatResponse.updateTime];
    
    NSURL *productIconUrl = [PPAWSS3 imageURLWithProductId:chatResponse.productId inIndex:1];
    [self.productIcon sd_setImageWithURL:productIconUrl];
    
    NSString *badgeCount;
    if (isSeller) {
        badgeCount = chatResponse.userIDToBadge;
    } else {
        badgeCount = chatResponse.userIDFromBadge;
    }
    if ([badgeCount isEqualToString:@"0"]) {
        self.badgeBackgroundView.hidden = YES;
        self.badgeLabel.text = @"";
    } else {
        self.badgeLabel.text = badgeCount;
        self.badgeBackgroundView.hidden = NO;
    }

    // 整理點擊此 cell 時要下傳到聊天室的資訊
    self.chatSessionInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: chatResponse, @"chatResponse", badgeCount, @"badgeCount", nil];

    if (isSeller) {
        [_chatSessionInfo setObject:chatResponse.userIDFrom forKey:@"otherPersonName"];
        [_chatSessionInfo setObject:@"2" forKey:@"myMessageID"];
    } else {
        [_chatSessionInfo setObject:chatResponse.userIDTo forKey:@"otherPersonName"];
        [_chatSessionInfo setObject:@"1" forKey:@"myMessageID"];
    }
}


@end
