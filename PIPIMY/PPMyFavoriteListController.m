
#import "PPAPI.h"
#import "PPUser.h"
#import "PPAWSS3.h"
#import <UIImageView+WebCache.h>
#import "PPMyFavoriteListController.h"


@interface PPMyFavoriteListController ()
<
    UITableViewDelegate,
    UITableViewDataSource
>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *dataSource; // 追蹤賣家的資料

@end


@implementation PPMyFavoriteListController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([PPUser singleton].isLogin)
    {
        [self __callAPIListTracing];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // 當 UITableView editing = YES, relase this UIViewController 會 crash
    _tableView.editing = NO;
}

#pragma mark - UITableViewDelegate / dataSource
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return @"我的最愛";
    }
    else if(section == 1)
    {
        return @"我的關注";
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([PPUser singleton].isLogin)
    {
        return 2;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return [PPUser singleton].favorites.count;
    }
    else if(section == 1)
    {
        return _dataSource.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return [self __favoriteCellAtIndexPath:indexPath];
    }
    else
    {
        return [self __traceCellAtIndexPath:indexPath];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        // 刪除我的最愛
        if(indexPath.section == 0)
        {
            PPAPIGetNearProductResponse *product = [PPUser singleton].favorites[indexPath.row];
            
            [[PPUser singleton]updateFavorites:product];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

#pragma mark - Private methods
#pragma mark 返回客製化的 Cell
- (UITableViewCell *)__favoriteCellAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    PPAPIGetNearProductResponse *product = [PPUser singleton].favorites[indexPath.row];
    NSString *userName     = product.ID;
    NSString *produtcTitle = product.title;
    NSString *addTime      = product.addToFavoriteDate;
    NSNumber *productId    = product.productId;
    NSURL *productIcon     = [PPAWSS3 imageURLWithProductId:productId inIndex:1];
    
    cell.textLabel.text       = [NSString stringWithFormat:@"%@ (%@)", produtcTitle, userName];
    cell.detailTextLabel.text = addTime;
    
    [cell.imageView sd_setImageWithURL:productIcon placeholderImage:nil];
    
    return cell;
}

- (UITableViewCell *)__traceCellAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell          = [_tableView dequeueReusableCellWithIdentifier:@"Cell"];
    PPAPIListTracingResponse *item = _dataSource[indexPath.row];
    NSURL *userIcon                = [PPAWSS3 userIconWithUserId:item.tracingId];
    cell.textLabel.text            = item.tracingId;
    cell.detailTextLabel.text      = item.addTime;
    
    [cell.imageView sd_setImageWithURL:userIcon placeholderImage:nil];
    
    return cell;
}

#pragma mark - Call API
- (void)__callAPIListTracing
{
    [[PPAPI singleton]APIListTracing:nil callback:^(NSArray *people, NSError *err) {
       
        self.dataSource = people;
        
        [_tableView reloadData];
    }];
}

@end
