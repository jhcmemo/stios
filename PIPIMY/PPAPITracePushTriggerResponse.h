
#import <Mantle.h>

@interface PPAPITracePushTriggerResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *result;
@property (nonatomic, readonly) NSString *message;
@property (nonatomic, readonly) NSNumber *isSwitchOn;

@end
