
#import "PPAPIGetChatContentResponse.h"

@interface PPAPIGetChatContentResponse ()

@property (nonatomic, readwrite) NSString *content;
@property (nonatomic, readwrite) NSString *isFrom;
@property (nonatomic, readwrite) NSDate   *sendTime;
@property (nonatomic, readwrite) NSNumber *type;

@end

@implementation PPAPIGetChatContentResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)typeJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *typeString) {
        return @([typeString integerValue]);
    }];
}

+ (NSValueTransformer *)sendTimeJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *timeString) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [dateFormatter dateFromString:timeString];
    }];
}

- (instancetype) initWithContent:(NSString *)content isFrom:(NSString *)isFrom sendTime:(NSDate *)sendTime type:(NSNumber *)type
{
    self = [super init];
    
    self.content = content;
    self.isFrom = isFrom;
    self.sendTime = sendTime;
    self.type = type;
    self.isPending = NO;
    
    return self;
}

@end
