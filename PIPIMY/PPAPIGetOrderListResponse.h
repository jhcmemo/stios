
#import <Mantle.h>

@interface PPAPIGetOrderListResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *comment;
@property (nonatomic, readonly) NSDate   *createdAt;
@property (nonatomic, readonly) NSNumber *Id;
@property (nonatomic, readonly) NSArray  *items;
@property (nonatomic, readonly) NSNumber *logisticsCost;
@property (nonatomic, readonly) NSDate   *logisticsDate3;
@property (nonatomic, readonly) NSDate   *logisticsDate4;
@property (nonatomic, readonly) NSDate   *logisticsDate5;
@property (nonatomic, readonly) NSDate   *logisticsDate6;
@property (nonatomic, readonly) NSString *logisticsDesc;
@property (nonatomic, readonly) NSNumber *logisticsIsHand;
@property (nonatomic, readonly) NSString *logisticsStatus;
@property (nonatomic, readonly) NSNumber *logisticsType;
@property (nonatomic, readonly) NSString *otherLogistics;
@property (nonatomic, readonly) NSDate   *paymentDate;
@property (nonatomic, readonly) NSNumber *paymentIsHand;
@property (nonatomic, readonly) NSNumber *paymentStatus;
@property (nonatomic, readonly) NSNumber *paymentType;
@property (nonatomic, strong)   NSNumber *pending;
@property (nonatomic, readonly) NSDate   *pendingExpireDate;
@property (nonatomic, strong)   NSNumber *queue;
@property (nonatomic, readonly) NSString *rating;
@property (nonatomic, readonly) NSString *sellerUserId;
@property (nonatomic, strong)   NSNumber *status;
@property (nonatomic, readonly) NSString *subLogisticsType;
@property (nonatomic, readonly) NSString *subPaymentType;
@property (nonatomic, readonly) NSNumber *sum;
@property (nonatomic, readonly) NSString *tradeNo;
@property (nonatomic, strong)   NSDate   *updatedAt;
@property (nonatomic, readonly) NSString *userId;

@end
