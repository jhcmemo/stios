
#import "PPAPI.h"
#import "PPUser.h"
#import "SRPAlertView.h"
#import "PPStoreSettingController.h"


@interface PPStoreSettingController ()

@property (nonatomic, assign) NSInteger storeCash;     // 金流方式
@property (nonatomic, assign) NSInteger storeDelivery; // 物流方式

@property (nonatomic, weak) IBOutlet UITextField *receiverName;      // 寄件人姓名
@property (nonatomic, weak) IBOutlet UITextField *storeDeliveryFree; // 免運費金額
@property (nonatomic, weak) IBOutlet UITextField *storeDeliveryFee1; // 全家金額
@property (nonatomic, weak) IBOutlet UITextField *storeDeliveryFee2; // 7-11金額
@property (nonatomic, weak) IBOutlet UITextField *storeDeliveryFee4; // 自寄金額
@property (nonatomic, weak) IBOutlet UITextField *storeFaceLoc1;     // 面交地點1
@property (nonatomic, weak) IBOutlet UITextField *storeFaceLoc2;     // 面交地點2


@end


@implementation PPStoreSettingController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.editing = YES;
    
    PPUser *user = [PPUser singleton];
    
    if (user.isLogin == YES)
    {
        [self __callAPIGetStoreFlow];
    }
}

#pragma mark - UITableViewDataSource / delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 為了讓選取時, Cell 保持白色
    UITableViewCell *cell                = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    UIView *whiteView                    = [[UIView alloc]init];
    whiteView.backgroundColor            = [UIColor whiteColor];
    cell.multipleSelectionBackgroundView = whiteView;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 金流及物流才支援 Edit
    return indexPath.section < 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

#pragma mark - IBAction

- (IBAction)sendItemDidClicked:(id)sender
{
    [self __powerCastAndDelivery];
    
    if(![self __infomationCorrect])
    {
        return;
    }
 
    [self.view endEditing:YES];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"StoreCash"]          = @(_storeCash);
    params[@"StoreDelivery"]      = @(_storeDelivery);
    params[@"receiverName"]       = _receiverName.text;
    
    params[@"StoreDeliveryFree"]  = @([_storeDeliveryFree.text integerValue]);
    params[@"StoreDeliveryFee1"]  = @([_storeDeliveryFee1.text integerValue]);
    params[@"StoreDeliveryFee2"]  = @([_storeDeliveryFee2.text integerValue]);
    params[@"StoreDeliveryFee4"]  = @([_storeDeliveryFee4.text integerValue]);
    
    params[@"StoreDeliveryFee8"]  = @(0);
    params[@"StoreDeliveryFee16"] = @(0);
    params[@"StoreDeliveryFee32"] = @(0);
    params[@"StoreDeliveryFee64"] = @(0);
    
   /*
    "StoreCash            -> 金流
    StoreDelivery         -> 物流
    receiverName"         -> 寄件人姓名
    StoreDeliveryFree     -> 免物流消費金額
    StoreDeliveryFee1     -> 全家物流費
    StoreDeliveryFee2     -> 7-11物流費
    StoreDeliveryFee4     -> 自寄
    StoreFaceLoc1         -> 面交地點1
    StoreFaceLoc2         -> 面交地點2
    UpdateTime            -> 更新時間

    StoreDeliveryFee8     -> 不明 (但是必填 沒送的話可能會有詭異的事情發生)
    StoreDeliveryFee16    -> 不明 (但是必填 沒送的話可能會有詭異的事情發生)
    StoreDeliveryFee32    -> 不明 (但是必填 沒送的話可能會有詭異的事情發生)
    StoreDeliveryFee64    -> 不明 (但是必填 沒送的話可能會有詭異的事情發生)
    */
    
    params[@"UpdateTime"] = ({
        NSString *updateTime           = @"";
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat       = @"yyyy-MM-dd HH:mm:ss";
        updateTime                     = [dateFormatter stringFromDate:[NSDate date]];
    });

    if(_storeFaceLoc1.text.length)
    {
        params[@"StoreFaceLoc1"] = _storeFaceLoc1.text;
    }
    
    if(_storeFaceLoc2.text.length)
    {
        params[@"StoreFaceLoc2"] = _storeFaceLoc2.text;
    }
    
    [self __callAPIUpdateStoreFlow:params];
    
}

#pragma mark - Private methods

- (void)__callAPIUpdateStoreFlow:(NSDictionary *)params
{
    [[PPAPI singleton]APIUpdateStoreFlow:params callback:^(NSError *err) {

        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            [[PPUser singleton]updateStroeCash:@(_storeCash) delivery:@(_storeDelivery)];
            
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"成功"
                                                                 message:@"設置成功"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
                
            [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    }];
}


- (void)__callAPIGetStoreFlow
{
    [[PPAPI singleton]APIGetStoreFlow:nil callback:^(id jsonObject,NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            NSDictionary *settingDic = (NSDictionary*)jsonObject;
            
            //金流
            if ([settingDic objectForKey:@"storeCash"]!=[NSNull null] && [settingDic objectForKey:@"storeCash"])
            {
                NSNumber *storeCash = (NSNumber*)[settingDic objectForKey:@"storeCash"];
                [[PPUser singleton]updateStroeCash:storeCash];
            }
            
            //物流
            if ([settingDic objectForKey:@"storeDelivery"]!=[NSNull null] && [settingDic objectForKey:@"storeDelivery"])
            {
                NSNumber *storeDelivery = (NSNumber*)[settingDic objectForKey:@"storeDelivery"];
                [[PPUser singleton]updateStroeDelivery:storeDelivery];
            }
            
            //寄件人名稱
            if ([settingDic objectForKey:@"receiverName"]!=[NSNull null] && [settingDic objectForKey:@"receiverName"])
            {
                _receiverName.text = [settingDic objectForKey:@"receiverName"];
            }
            
            //免運金額
            if ([settingDic objectForKey:@"StoreDeliveryFree"]!=[NSNull null] && [settingDic objectForKey:@"StoreDeliveryFree"])
            {
                _storeDeliveryFree.text = [NSString stringWithFormat:@"%li",(long)[[settingDic objectForKey:@"StoreDeliveryFree"]integerValue]];
            }
            
            //feesDic
            if ([settingDic objectForKey:@"storeDeliveryFees"]!=[NSNull null] && [settingDic objectForKey:@"storeDeliveryFees"])
            {
                NSDictionary *feesDic = [settingDic objectForKey:@"storeDeliveryFees"];
                
                if ([feesDic objectForKey:@"1"]!=[NSNull null] && [feesDic objectForKey:@"1"])
                {
                    _storeDeliveryFee1.text = [NSString stringWithFormat:@"%li",(long)[[feesDic objectForKey:@"1"]integerValue]];
                }
                
                if ([feesDic objectForKey:@"2"]!=[NSNull null] && [feesDic objectForKey:@"2"])
                {
                    _storeDeliveryFee2.text = [NSString stringWithFormat:@"%li",(long)[[feesDic objectForKey:@"2"]integerValue]];
                }
                
                if ([feesDic objectForKey:@"4"]!=[NSNull null] && [feesDic objectForKey:@"4"])
                {
                    _storeDeliveryFee4.text = [NSString stringWithFormat:@"%li",(long)[[feesDic objectForKey:@"4"]integerValue]];
                }
            }
            
            
            //面交地點一 - 目前似乎不會回傳
            if ([settingDic objectForKey:@"StoreFaceLoc1"]!=[NSNull null] && [settingDic objectForKey:@"StoreFaceLoc1"])
            {
                _storeFaceLoc1.text = [settingDic objectForKey:@"StoreFaceLoc1"];
            }
            
            //面交地點二 - 目前似乎不會回傳
            if ([settingDic objectForKey:@"StoreFaceLoc2"]!=[NSNull null] && [settingDic objectForKey:@"StoreFaceLoc2"])
            {
                _storeFaceLoc2.text = [settingDic objectForKey:@"StoreFaceLoc2"];
            }
            
            //顯示金物設定選項
            PPUser *user = [PPUser singleton];
            if(user.isLogin)
            {
                [self __setupTableViewSelectedAtIndexPathWithPrevCash:[user.storeCash integerValue]];
                [self __setupTableViewSelectedAtIndexPathWithPrevDelivery:[user.storeDelivery integerValue]];
            }
        }
    }];
}


- (void)__powerCastAndDelivery
{
    self.storeCash     = 0;
    self.storeDelivery = 0;
    NSArray *selected  = [self.tableView indexPathsForSelectedRows];
    
    [selected enumerateObjectsUsingBlock:^(NSIndexPath *indexPath, NSUInteger idx, BOOL *stop) {
        
        // 金流
        if(indexPath.section == 0)
        {
            self.storeCash+=pow(2, indexPath.row);
        }
        
        // 物流
        else if(indexPath.section == 1)
        {
            //self.storeDelivery+=pow(2, indexPath.row);
            self.storeDelivery += indexPath.row == 0? 1  : 0 +  //全家
                                  indexPath.row == 1? 2  : 0 +  //7-11
                                  indexPath.row == 2? 16 : 0 ;  //面交
            

        }
    }];
    
}





- (BOOL)__infomationCorrect
{
    if(_storeCash <= 0)
    {
        [self __showAlertViewWithMessage:@"至少選擇一種金流方式"];
        
        return NO;
    }
    else if(_storeDelivery <=0)
    {
        [self __showAlertViewWithMessage:@"至少選擇一種物流方式"];
        
        return NO;
    }
    else if(!_receiverName.text.length)
    {
        [self __showAlertViewWithMessage:@"請輸入寄件人姓名"];
        
        return NO;
    }
    else if(!_storeDeliveryFree.text.length)
    {
        [self __showAlertViewWithMessage:@"請設定免運費金額"];
        
        return NO;
    }
    else if(!_storeDeliveryFee1.text.length)
    {
        [self __showAlertViewWithMessage:@"請設定全家運費金額"];
        
        return NO;
    }
    else if(!_storeDeliveryFee2.text.length)
    {
        [self __showAlertViewWithMessage:@"請設定7-11運費金額"];
        
        return NO;
    }
    else if(!_storeDeliveryFee4.text.length)
    {
        [self __showAlertViewWithMessage:@"請設定自寄包裹運費金額"];
        
        return NO;
    }
    
    return YES;
}

- (void)__setupTableViewSelectedAtIndexPathWithPrevCash:(NSInteger)storeCash
{
    NSArray *set = @[@1, @2, @4, @8, @16];
    
    [set enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
        
        NSInteger number = [obj integerValue];

        if(number > storeCash)
        {
            *stop = YES;
        }
        else if((storeCash & number) > 0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
            
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }];
}

- (void)__setupTableViewSelectedAtIndexPathWithPrevDelivery:(NSInteger)storeDelivery
{
    NSArray *set = @[@1, @2, @16];
    
    [set enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
        
        NSInteger number = [obj integerValue];
        
        if(number > storeDelivery)
        {
            *stop = YES;
        }
        else if((storeDelivery & number) > 0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:1];
            
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

@end
