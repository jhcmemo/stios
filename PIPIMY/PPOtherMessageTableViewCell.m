
#import "PPOtherMessageTableViewCell.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PPAPIGetChatContentResponse.h"

@interface PPOtherMessageTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImageView;

@end


@implementation PPOtherMessageTableViewCell

#pragma mark - life cycles

-(void)awakeFromNib
{
    // screen - (icon width) - (time label + trailing margin) - (leading margin) - (middle margins * 4) - (safety margin)
    CGFloat width = [[UIScreen mainScreen] applicationFrame].size.width - 60 - 60 - 4 - (8 * 4) - 10;
    self.contentLabel.preferredMaxLayoutWidth = width;
    
    // bubble background
    UIImage *bubble = [UIImage imageNamed:@"message-bubble-white"];
    [self.bubbleImageView setImage:[bubble resizableImageWithCapInsets:UIEdgeInsetsMake(25, 35, 22, 24)
                                                          resizingMode:UIImageResizingModeStretch]];
}

#pragma mark - Configuration

- (void)configureCellWithMessage:(PPAPIGetChatContentResponse *)message chat:(NSDictionary *)chat
{
    // time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    self.timeLabel.text = [dateFormatter stringFromDate:message.sendTime];
    
    // chat message
    self.contentLabel.text = message.content;
    
    // user icon
    NSURL *userIconURL = [PPAWSS3 userIconWithUserId:chat[@"otherPersonName"]];
    [self.userIcon sd_setImageWithURL:userIconURL placeholderImage:[UIImage imageNamed:@"nohead"]];
}

@end
