
#import <UIKit/UIKit.h>

@interface PPMessageDateTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
