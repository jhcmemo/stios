
#import "PPRootTabBarViewController.h"
#import "PPAPI.h"
#import "PPUser.h"
#import "PPChatMessageViewController.h"
#import "SRPPopupMenu.h"

@interface PPRootTabBarViewController ()

@end

@implementation PPRootTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refreshChatBadge:nil];

    // 聽取推播，更新未讀訊息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incrementChatBadge:) name:@"NewChatMessageNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshChatBadge:) name:UIApplicationDidBecomeActiveNotification object:nil];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[[SRPPopupMenu singleton]showWithColor:[UIColor blackColor]];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewChatMessageNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void) refreshChatBadge:(NSNotification *)notification {
    NSInteger __block badgeCounter = 0;
    
    [[PPAPI singleton]APIGetChatIndex:nil callback:^(NSArray *indices, NSError *err) {
        if(indices) {
            for(PPAPIGetChatIndexResponse *chatIndex in indices) {
                if([chatIndex.userIDFrom isEqualToString:[PPUser singleton].name]) {
                    badgeCounter += [chatIndex.userIDFromBadge integerValue];
                } else {
                    badgeCounter += [chatIndex.userIDToBadge integerValue];
                }
            }
            
            if (badgeCounter > 0) {
                UINavigationController *chatTab = [self.viewControllers objectAtIndex:2];
                chatTab.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld", (long)badgeCounter];
            }
        }
    }];
}

- (void) incrementChatBadge:(NSNotification *)notification {
    
    PPPushNotificationResponse *response = notification.object;
    // 增加聊天 tab 的未讀數，除非剛好在那個聊天室
    BOOL needsIncreaseBadge = YES;
    
    UINavigationController *chatTab = [self.viewControllers objectAtIndex:2];
    if ([chatTab.visibleViewController isKindOfClass:[PPChatMessageViewController class]]) {
        PPChatMessageViewController *chatMessageViewController = (PPChatMessageViewController *)chatTab.visibleViewController;
        if ([chatMessageViewController.chatResponse.chatId isEqualToString:response.userId]) {
            needsIncreaseBadge = NO;
        }
    }
    
    if (needsIncreaseBadge) {
        NSInteger oldBadgeCount = [chatTab.tabBarItem.badgeValue integerValue];
        chatTab.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld", (long)oldBadgeCount + 1];
    }
}






@end
