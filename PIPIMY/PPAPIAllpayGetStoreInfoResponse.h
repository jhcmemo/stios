
#import <Mantle.h>

@interface PPAPIAllpayGetStoreInfoResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong)   NSString *storeAddress;
@property (nonatomic, readonly) NSString *storeClose;
@property (nonatomic, readonly) NSString *storeName;
@property (nonatomic, readonly) NSString *storePhone;
@property (nonatomic, readonly) NSString *storeID;

@end
