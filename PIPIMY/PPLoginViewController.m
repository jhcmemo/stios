
#import "PPAPI.h"
#import "PPUser.h"
#import "UIColor+SRPKit.h"
#import "PPLoginViewController.h"


@interface PPLoginViewControllerTextField : UITextField
@end


@implementation PPLoginViewControllerTextField

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10.0f, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

@end


@interface PPLoginViewController ()<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *account;      // 帳號
@property (nonatomic, weak) IBOutlet UITextField *password;     // 密碼
@property (nonatomic, weak) IBOutlet UIButton *registerButton;  // 註冊鈕
@property (nonatomic, weak) IBOutlet UIButton *loginButton;     // 登入鈕

@end


@implementation PPLoginViewController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if(![PPUser singleton].location)
    {
        [self __showAlertViewWithMessage:@"請檢查定位狀態"];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - IBActions
#pragma mark 取消
- (IBAction)cancelItemDidClicked:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 登入
- (IBAction)loginButtonDidClicked:(id)sender
{
    if(!_account.text.length || !_password.text.length)
    {
        [self __showAlertViewWithMessage:@"帳號或密碼不可為空"];
    }
    else
    {
        [self.view endEditing:YES];
        [self __callAPILogin];
    }
}

#pragma mark - 意見反應
- (IBAction)feebackButtonDidClicked:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"mailto:pipimy.service@gmail.com"];
    
    [[UIApplication sharedApplication]openURL:url];
}

#pragma mark - 忘記密碼
- (IBAction)forgotPasswdButtonDidClicked:(id)sender
{
    
}

#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    _account.layer.cornerRadius        = 4.0;
    _account.layer.borderColor         = [UIColor lightGrayColor].CGColor;
    _account.layer.borderWidth         = 1.0;
    _password.layer.cornerRadius       = 4.0;
    _password.layer.borderColor        = [UIColor lightGrayColor].CGColor;
    _password.layer.borderWidth        = 1.0;
    _registerButton.layer.cornerRadius = 4.0;
    _registerButton.layer.borderColor  = [UIColor srp_colorWithHEX:@"#008080"].CGColor;
    _registerButton.layer.borderWidth  = 1.0;
    _loginButton.layer.cornerRadius    = 4.0;
}

#pragma mark - Call 登入 API
- (void)__callAPILogin
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // 需要 base64 string
    NSString *ID  = [[_account.text dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    
    NSString *Pwd = [[_password.text dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    
    NSDictionary *params = @{@"ID" : ID, @"Pwd" : Pwd};
    
    [[PPAPI singleton]APILogin:params callback:^(PPAPILoginResponse *result, NSError *err) {
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            [[PPUser singleton]login:result];
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

#pragma mark - 顯示錯誤資訊
- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

@end
