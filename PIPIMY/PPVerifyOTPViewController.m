
#import "PPVerifyOTPViewController.h"
#import "PPAPI.h"

@interface PPVerifyOTPViewController ()

@property (weak, nonatomic) IBOutlet UITextField *OTPTextField;

@end

@implementation PPVerifyOTPViewController

- (IBAction)sendOTP:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

@end
