
#import <UIKit/UIKit.h>
@class PPAPIGetChatIndexResponse;

@interface PPChatMessageViewController : UIViewController

@property (strong, nonatomic) NSDictionary *chatSessionInfo;
@property (strong, nonatomic) PPAPIGetChatIndexResponse *chatResponse;

@end
