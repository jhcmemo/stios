
#import "AppDelegate.h"
#import "AppDelegate+Setup.h"
#import "PPSomeoneStoreController.h"


@interface AppDelegate ()
@end


@implementation AppDelegate

#pragma mark - LifeCycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    application.applicationIconBadgeNumber = 0;
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType types = UIUserNotificationTypeBadge |
                                       UIUserNotificationTypeAlert |
                                       UIUserNotificationTypeSound;
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else
    {
        UIRemoteNotificationType types = UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeAlert |
                                         UIRemoteNotificationTypeSound;
        
        [application registerForRemoteNotificationTypes:types];
    }
    
    [self setup_Singletons];
    [self setup_UIAppearance];
    [self setup_defaultSetting];
    
    // 點推播訊息進來
    if (launchOptions)
    {
        [self presentStoreWithInfo:[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]];
    }

    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:
(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token  = [[deviceToken description] stringByTrimmingCharactersInSet:
                       [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    token            = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[PPUser singleton]updateDeviceToken:token];
}

#pragma mark - 推播
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // debug
    
    
    // handle message
    PPPushNotificationResponse *response = [MTLJSONAdapter modelOfClass:[PPPushNotificationResponse class] fromJSONDictionary:userInfo[@"aps"] error:nil];

    // 來自關注賣家推播
    if ([response.chatId isEqualToString:@"-2"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SellerPushMessageNotification" object:response];
    }
    // 來自聊天訊息
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewChatMessageNotification" object:response];
    }
    
    // 檢查是否點選 notification center 訊息進入
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    BOOL notificationClicked = (state == UIApplicationStateBackground || state == UIApplicationStateInactive);
    if (notificationClicked)
    {
        [self presentStoreWithInfo:userInfo];
    }
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler
{
    
}

#pragma mark 推播 helper

- (void)presentStoreWithInfo:(NSDictionary *)userInfo
{
    PPPushNotificationResponse *response = [MTLJSONAdapter modelOfClass:[PPPushNotificationResponse class] fromJSONDictionary:userInfo[@"aps"] error:nil];
    
    // 檢查是不是店家推播
    if ([response.chatId isEqualToString:@"-2"])
        return;
    
    UITabBarController *tabBarController = [self tabController];
    tabBarController.selectedIndex = 0;
    
    UINavigationController *tab1NavigationController = [self.tabController.viewControllers firstObject];
    
    UIStoryboard *tab1 = [UIStoryboard storyboardWithName:@"Tab1" bundle:nil];
    PPSomeoneStoreController *someoneStoreController = [tab1 instantiateViewControllerWithIdentifier:@"PPSomeoneStoreController"];
    someoneStoreController.memberId = response.userId;
    
    [tab1NavigationController pushViewController:someoneStoreController animated:YES];
}

#pragma mark - UITabBarControllerDelegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    BOOL login       = [PPUser singleton].isLogin;
    NSUInteger index = [tabBarController.viewControllers indexOfObject:viewController];
    
    if(!login && index > 0)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        id mvc              = [story instantiateInitialViewController];
        
        [self.tabController presentViewController:mvc animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Properties Getter
- (UITabBarController *)tabController
{
    id result = nil;
    
    if([self.window.rootViewController isKindOfClass:[UITabBarController class]])
    {
        result = self.window.rootViewController;
    }
    
    return result;
}

@end
