
#import <Mantle.h>


@interface OrderResourceOrder : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *logisticsStatus;
@property (nonatomic, readonly) NSString *logisticsType;
@property (nonatomic, readonly) NSString *paymentType;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *sum;

@end


@interface OrderResourceItems : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *price;
@property (nonatomic, readonly) NSString *amount;

@end


@interface OrderResourcePayment : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *bankCode;
@property (nonatomic, readonly) NSString *execTime;
@property (nonatomic, readonly) NSString *expireDate;
@property (nonatomic, readonly) NSString *merchantID;
@property (nonatomic, readonly) NSString *merchantTradeNo;
@property (nonatomic, readonly) NSString *paymentNo;
@property (nonatomic, readonly) NSString *paymentDate;
@property (nonatomic, readonly) NSString *paymentType;
@property (nonatomic, readonly) NSString *rtnCode;
@property (nonatomic, readonly) NSString *rtnMsg;
@property (nonatomic, readonly) NSString *simulatePaid;
@property (nonatomic, readonly) NSString *tradeAmt;
@property (nonatomic, readonly) NSString *tradeDate;
@property (nonatomic, readonly) NSString *tradeNo;
@property (nonatomic, readonly) NSString *createdAt;
@property (nonatomic, readonly) NSString *Id;
@property (nonatomic, readonly) BOOL isHand;
@property (nonatomic, readonly) NSString *vAccount;

@end


@interface OrderResourceLogistics : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *CVSPaymentNo;
@property (nonatomic, readonly) NSString *CVSValidationNo;
@property (nonatomic, readonly) NSString *logisticsSubType;
@property (nonatomic, readonly) NSString *logisticsType;
@property (nonatomic, readonly) NSString *receiverCellPhone;
@property (nonatomic, readonly) NSString *receiverName;
@property (nonatomic, readonly) NSString *receiverPhone;
@property (nonatomic, readonly) BOOL isHand;
@property (nonatomic, readonly) NSString *receiverStoreID;

@end



@interface PPAPIGetOrderResourceResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray                *items;
@property (nonatomic, strong) OrderResourceOrder     *order;
@property (nonatomic, strong) OrderResourcePayment   *payment;
@property (nonatomic, strong) OrderResourceLogistics *logistics;

@end
