
#import <UIKit/UIKit.h>


/**
 *  App delegate
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>



/**
 *  App Main window
 */
@property (strong, nonatomic) UIWindow *window;

/**
 *  Tab MVC
 */
@property (nonatomic, readonly) UITabBarController *tabController;

@end

