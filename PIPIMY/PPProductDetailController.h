
#import <UIKit/UIKit.h>
@class PPAPIGetNearProductResponse;


/**
 *  商品詳細 MVC
 */
@interface PPProductDetailController : UITableViewController



/**
 *  商品
 */
@property (nonatomic, weak) PPAPIGetNearProductResponse *product;

@end
