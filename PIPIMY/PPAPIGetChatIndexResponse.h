
#import <Mantle.h>


@interface PPAPIGetChatIndexResponse : MTLModel<MTLJSONSerializing>

@property (nonatomic, readonly) NSString *chatId;

@property (nonatomic)           NSString *lastSentence;

@property (nonatomic, readonly) NSNumber *productId;
@property (nonatomic, readonly) NSString *productTitle;
@property (nonatomic)           NSDate   *updateTime;
@property (nonatomic, readonly) NSString *userIDFrom;
@property (nonatomic)           NSString *userIDFromBadge;
@property (nonatomic, readonly) NSNumber *userIDFromHand;

@property (nonatomic, readonly) NSString *userIDTo;
@property (nonatomic)           NSString *userIDToBadge;
@property (nonatomic, readonly) NSNumber *userIDToHand;

@end
