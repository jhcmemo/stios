
#import "PPCartListHeaderView.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PPCartListHeaderView ()

@property (assign, nonatomic) NSInteger section;
@property (weak, nonatomic) id <PPCartListHeaderViewDelegate> delegate;
@property (assign, nonatomic) BOOL checked;

@property (weak, nonatomic) IBOutlet UIImageView *sellerIcon;
@property (weak, nonatomic) IBOutlet UILabel *sellerIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkMarkLabel;

@end

@implementation PPCartListHeaderView

- (void)configureHeaderWithID:(NSString *)storeID section:(NSInteger)section delegate:(id<PPCartListHeaderViewDelegate>)delegate checked:(BOOL)checked
{
    self.sellerIDLabel.text = storeID;
    self.section = section;
    self.delegate = delegate;
    self.checked = checked;
    NSURL *userIconURL = [PPAWSS3 userIconWithUserId:storeID];
    [self.sellerIcon sd_setImageWithURL:userIconURL placeholderImage:[UIImage imageNamed:@"nohead"]];

    [self updateCheckMark];
}

- (IBAction)checkMarkClicked
{
    self.checked = !self.checked;
    [self updateCheckMark];
    [self.delegate checkAllForSection:self.section];
}

- (void)updateCheckMark
{
    if (self.checked)
    {
        self.checkMarkLabel.textColor = [UIColor greenColor];
    }
    else
    {
        self.checkMarkLabel.textColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    }
}

@end
