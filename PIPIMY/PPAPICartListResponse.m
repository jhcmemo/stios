
#import "PPAPICartListResponse.h"

@implementation PPAPICartListResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"Id"          : @"id",
             @"productId"   : @"product_id",
             @"storeUserId" : @"store_user_id",
             @"userId"      : @"user_id"};
}

+ (NSValueTransformer *)hideJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^NSNumber *(NSString *hideString) {
        return @([hideString integerValue]);
    }];
}

+ (NSValueTransformer *)stockJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^NSNumber *(NSString *stockString) {
        return @([stockString integerValue]);
    }];
}

+ (NSValueTransformer *)onlineTimeJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^NSDate *(NSString *dateString) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [dateFormatter dateFromString:dateString];
    }];
}

@end
