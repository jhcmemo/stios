
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PPConvenienceStoreType)
{
    PPConvenienceStoreTypeFamilymart = 0,
    PPConvenienceStoreType711 = 1,
};


@interface PPConvenienceStores : NSObject

@property (nonatomic, assign) BOOL fetched711Stores;
@property (nonatomic, assign) BOOL fetchedFamilymartStores;

- (void)retrieveStoresOfType:(PPConvenienceStoreType)type;

- (NSArray *)countyListForStoreType:(PPConvenienceStoreType)type;
- (NSArray *)subdivisionForCounty:(NSString *)county storeType:(PPConvenienceStoreType)type;
- (NSArray *)storeListForSubdivision:(NSString *)subdivision county:(NSString *)county storeType:(PPConvenienceStoreType)type;

@end
