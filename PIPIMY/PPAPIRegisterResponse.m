
#import "PPAPIRegisterResponse.h"


@implementation PPAPIRegisterResponse

#pragma mark - Key Map
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

#pragma mark - Properties Getter
- (BOOL)success
{
    return [_result isEqualToString:@"success"];
}

@end
