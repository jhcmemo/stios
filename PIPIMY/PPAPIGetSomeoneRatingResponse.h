
#import <Mantle.h>


/**
 *  評分內容
 */
@interface Rating : MTLModel<MTLJSONSerializing>



/**
 *  內容
 */
@property (nonatomic, readonly) NSString *comment;

/**
 *  交易時間
 */
@property (nonatomic, readonly) NSString *dealTime;

/**
 *  評分時間
 */
@property (nonatomic, readonly) NSString *ratingTime;

/**
 *  分數
 */
@property (nonatomic, readonly) NSNumber *score;

/**
 *  評分人
 */
@property (nonatomic, readonly) NSString *userIDFrom;

@end


/**
 *  評分總覽
 */
@interface RatingDetail: MTLModel<MTLJSONSerializing>



/**
 *  平均分數
 */
@property (nonatomic, readonly) NSNumber *average;

/**
 *  評分數量
 */
@property (nonatomic, readonly) NSNumber *count;

/**
 *  交易數量
 */
@property (nonatomic, readonly) NSNumber *dealTimes;

/**
 *  未知
 */
@property (nonatomic, readonly) NSString *lastOpenTime;

/**
 *  未知
 */
@property (nonatomic, readonly) NSNumber *openTimes;

/**
 *  未知
 */
@property (nonatomic, readonly) NSString *registerTime;

@end


/**
 *  API getSomeoneRating Response
 *
 *  某個人的評分紀錄
 */
@interface PPAPIGetSomeoneRatingResponse : NSObject



/**
 *  評分列表
 */
@property (nonatomic, strong) NSArray *ratings;

/**
 *  評分詳細
 */
@property (nonatomic, strong) RatingDetail *detail;

@end
