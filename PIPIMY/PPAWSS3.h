
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


/**
 *  AWS S3 Helper
 */
@interface PPAWSS3 : NSObject



/**
 *  返回 Singleton 物件
 *
 *  @return 返回 Singleton 物件
 */
+ (instancetype)singleton;

/**
 *  返回商品照片 URL
 *
 *  @param Id    商品 Id
 *  @param index 第 N 張
 *
 *  @return 返回商品照片 URL
 */
+ (NSURL *)imageURLWithProductId:(NSNumber *)Id inIndex:(NSUInteger)index;

/**
 *  返回商品影片 URL
 *
 *  @param Id 商品 Id
 *
 *  @return 返回商品影片 URL
 */
+ (NSURL *)videoURLWithProductId:(NSNumber *)Id;

/**
 *  返回使用者頭像 URL
 *
 *  @param Id 使用者 Id
 *
 *  @return 返回使用者頭像 URL
 */
+ (NSURL *)userIconWithUserId:(NSString *)Id;


/**
 *  返回使用者背景 URL
 *
 *  @param Id 使用者 Id
 *
 *  @return 返回使用者背景 URL
 */
+ (NSURL *)userBackgroundWithUserId:(NSString *)Id;


/**
 *  上傳商品影片
 *
 *  @param productId 商品 Id
 *  @param progress  上傳進度
 *  @param success   上傳是否成功
 */
- (void)uploadVideoWithProductId:(NSNumber *)productId
                        progress:(void(^)(CGFloat progress))progress
                         success:(void(^)(BOOL success, NSError *error))success;

/**
 *  上傳商品圖片
 *
 *  @param productId 商品 Id
 *  @param progress  上傳進度
 *  @param success   上傳是否成功
 */
- (void)uploadImagesWithProductId:(NSNumber *)productId
                            files:(NSArray *)files
                         progress:(void(^)(CGFloat progress))progress
                          success:(void(^)(BOOL success, NSError *error))success;

/**
 *  上傳使用者頭像
 *
 *  @param userId   使用者 Id
 *  @param filePath 圖片 loacl path
 *  @param success  是否成功
 */
- (void)uploadUserIconWithUserId:(NSString *)userId
                        filePath:(NSURL *)filePath
                         success:(void(^)(BOOL success))success;

/**
 *  上傳使用者背景
 *
 *  @param userId   使用者 Id
 *  @param filePath 圖片 loacl path
 *  @param success  是否成功
 */
- (void)uploadUserBackgroundWithUserId:(NSString *)userId
                              filePath:(NSURL *)filePath
                               success:(void (^)(BOOL))success;
@end
