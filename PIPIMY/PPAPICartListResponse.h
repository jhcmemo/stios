
#import <Mantle.h>

@interface PPAPICartListResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *brand;
@property (nonatomic, readonly) NSString *buyerId;
@property (nonatomic, readonly) NSString *cash;
@property (nonatomic, readonly) NSString *complete;
@property (nonatomic, readonly) NSString *contact;
@property (nonatomic, readonly) NSString *country;
@property (nonatomic, readonly) NSString *delivery;
@property (nonatomic, readonly) NSString *des;
@property (nonatomic, readonly) NSString *favorite;
@property (nonatomic, readonly) NSNumber *hide;
@property (nonatomic, readonly) NSString *Id;
@property (nonatomic, readonly) NSString *lat;
@property (nonatomic, readonly) NSString *lng;
@property (nonatomic, readonly) NSString *loc1;
@property (nonatomic, readonly) NSString *loc2;
@property (nonatomic, readonly) NSString *memberId;
@property (nonatomic, readonly) NSDate   *onlineTime;
@property (nonatomic, readonly) NSString *parentId;
@property (nonatomic, readonly) NSString *pic;
@property (nonatomic, readonly) NSString *price;
@property (nonatomic, readonly) NSString *productId;
@property (nonatomic, readonly) NSString *report;
@property (nonatomic, readonly) NSNumber *stock;
@property (nonatomic, readonly) NSString *storeUserId;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSString *userId;
@property (nonatomic, readonly) NSString *video;

@end
