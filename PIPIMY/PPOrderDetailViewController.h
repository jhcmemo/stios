
#import <UIKit/UIKit.h>
@class PPAPIGetOrderListResponse;

typedef NS_ENUM(NSInteger, OrderDetailSection)
{
    OrderDetailSectionOrder     = 0,
    OrderDetailSectionPayment   = 1,
    OrderDetailSectionLogistics = 2,
    OrderDetailSectionItems     = 3
};

@interface PPOrderDetailViewController : UIViewController

@property (nonatomic, strong) PPAPIGetOrderListResponse *order;
@property (nonatomic, assign) BOOL asBuyer;

@end
