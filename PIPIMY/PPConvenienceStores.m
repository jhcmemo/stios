
#import "PPConvenienceStores.h"
#import "PPAPI.h"
#import "PPAPIAllpayGetStoreInfoResponse.h"

@interface PPConvenienceStores ()

@property (nonatomic, strong) NSDictionary *preset711CountiesList;
@property (nonatomic, strong) NSDictionary *presetFamilymartCountiesList;

@property (nonatomic, strong) NSDictionary *storesList711;
@property (nonatomic, strong) NSDictionary *storesListFamilymart;

@end

@implementation PPConvenienceStores

#pragma mark - Setters / Getters

- (NSDictionary *)presetFamilymartCountiesList
{
    if (!_presetFamilymartCountiesList)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"familymart_counties_list" ofType:@"plist"];
        _presetFamilymartCountiesList = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    
    return _presetFamilymartCountiesList;
}

- (NSDictionary *)preset711CountiesList
{
    if (!_preset711CountiesList)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"unimart_counties_list" ofType:@"plist"];
        _preset711CountiesList = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    
    return _preset711CountiesList;
}

#pragma mark - 取回縣市、區域、商店列表

- (void)retrieveStoresOfType:(PPConvenienceStoreType)type
{
    if (type == PPConvenienceStoreType711)
    {
        if (!_storesList711)
        {
            NSDictionary *params = @{ @"MerchantID" : @"2000132",
                                      @"StoreType" : @"U",
                                      @"CheckMacValue" : @"8E2FFAE8D76F05CD9142EB05F3BBBC45" };
            
            [[PPAPI singleton] APIAllpayGetStoreInfo:params callback:^(NSArray *stores, NSError *error) {
                
                [self parseStoresListOfType:type withArray:stores];
                
                self.fetched711Stores = YES;
            }];
        }
    }
    
    else if (type == PPConvenienceStoreTypeFamilymart)
    {
        if (!_storesListFamilymart)
        {
            NSDictionary *params = @{ @"MerchantID" : @"2000132",
                                      @"StoreType" : @"F",
                                      @"CheckMacValue" : @"6A11CDFF2761E553F532C73AA0DB2C00" };
            
            [[PPAPI singleton] APIAllpayGetStoreInfo:params callback:^(NSArray *stores, NSError *error) {
                
                [self parseStoresListOfType:type withArray:stores];

                self.fetchedFamilymartStores = YES;
            }];
        }
    }
}

- (NSArray *)countyListForStoreType:(PPConvenienceStoreType)type
{
    NSArray *counties;
    
    if (type == PPConvenienceStoreTypeFamilymart)
    {
        if (!self.storesListFamilymart)
            counties = [self.presetFamilymartCountiesList allKeys];
        else
            counties = [self.storesListFamilymart allKeys];
    }
    else if (type == PPConvenienceStoreType711)
    {
        if (!self.storesList711)
            counties = [self.preset711CountiesList allKeys];
        else
            counties = [self.storesList711 allKeys];
    }
    
    // sort counties before returning
    NSArray *orderedCounties = @[ @"基隆市", @"臺北市", @"新北市", @"桃園市", @"新竹縣", @"新竹市", @"苗栗縣", @"臺中市", @"彰化縣", @"南投縣", @"雲林縣", @"嘉義縣", @"嘉義市", @"臺南市", @"高雄市", @"屏東縣", @"宜蘭縣", @"花蓮縣", @"臺東縣", @"澎湖縣", @"金門縣", @"連江縣" ];
    
    NSArray *sortedCounties = [counties sortedArrayUsingComparator:^NSComparisonResult(NSString *county1, NSString *county2) {
        NSInteger index1 = [orderedCounties indexOfObject:county1];
        NSInteger index2 = [orderedCounties indexOfObject:county2];
        return [@(index1) compare:@(index2)];
    }];
    
    return sortedCounties;
}

- (NSArray *)subdivisionForCounty:(NSString *)county storeType:(PPConvenienceStoreType)type
{
    NSArray *subdivisions;
    
    if (type == PPConvenienceStoreTypeFamilymart)
    {
        if (!self.storesListFamilymart)
            subdivisions = [self.presetFamilymartCountiesList objectForKey:county];
        else
            subdivisions = [[self.storesListFamilymart objectForKey:county] allKeys];
    }
    else if (type == PPConvenienceStoreType711)
    {
        if (!self.storesList711)
            subdivisions = [self.preset711CountiesList objectForKey:county];
        else
            subdivisions = [[self.storesList711 objectForKey:county] allKeys];
    }
    
    return [subdivisions sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (NSArray *)storeListForSubdivision:(NSString *)subdivision county:(NSString *)county storeType:(PPConvenienceStoreType)type
{
    NSArray *stores;
    if (type == PPConvenienceStoreType711)
    {
        if (!self.storesList711)
            return nil;
        
        stores = [[self.storesList711 objectForKey:county] objectForKey:subdivision];
    }
    else if (type == PPConvenienceStoreTypeFamilymart)
    {
        if (!self.storesListFamilymart)
            return nil;
        
        stores = [[self.storesListFamilymart objectForKey:county] objectForKey:subdivision];
    }
    
    if (!stores)
        return @[];
    
    NSSortDescriptor *sortDesciptor = [[NSSortDescriptor alloc] initWithKey:@"storeAddress" ascending:YES];
    
    return [stores sortedArrayUsingDescriptors:@[sortDesciptor]];
}

#pragma mark - 拆解地址、分類列表

- (void)parseStoresListOfType:(PPConvenienceStoreType)type withArray:(NSArray *)stores
{
    // Temp fix for inconsistent county names
    NSDictionary *matchCountiesTable = @{ @"台北市" : @"臺北市",
                                          @"台中市" : @"臺中市",
                                          @"台南市" : @"臺南市",
                                          @"台南巿" : @"臺南市",
                                          @"台東縣" : @"臺東縣",
                                          @"台東市" : @"臺東縣",
                                          @"花蓮市" : @"花蓮縣",
                                          @"屏東市" : @"屏東縣",
                                          @"桃園縣" : @"桃園市" };

    // Temp fix for inconsistent subdivision names
    NSDictionary *matchSubdivisionsTable = @{ @"中壢市" : @"中壢區",
                                              @"八德市" : @"八德區",
                                              @"平鎮市" : @"平鎮區",
                                              @"桃園市" : @"桃園區",
                                              @"楊梅市" : @"楊梅區"
                                              };

    // Temp fix for bad addresses
    NSDictionary *matchAddressesTable = @{ @"桃園市區振興路12號                                          " : @"桃園市平鎮區振興路12號                                      ",
                                           @"桃園市 中壢區新中北路1018-1號1樓                            "  : @"桃園市中壢區新中北路1018-1號1樓                             ",
                                           @"台中市中清路508號                                           " : @"台中市北區中清路508號                                       ",
                                           @"嘉義市國華街５１９號１樓"                : @"嘉義市西區國華街５１９號１樓",
                                           @"高雄市楠梓加工區中央路２２號十一樓"        : @"高雄市楠梓區中央路２２號十一樓",
                                           @"台中市北屯路２１０之３號，２１０之５號１樓" : @"台中市北屯區北屯路２１０之３號，２１０之５號１樓",
                                           @"高雄市楠梓加工區經三路２６號地下一層"      : @"高雄市楠梓區經三路２６號地下一層",
                                           @"高雄市楠梓加工區經六路６６號"             : @"高雄市楠梓區經六路６６號",
                                           @"高雄市楠梓加工出口區開發路４７號"          : @"高雄市楠梓區開發路４７號",
                                           @"高雄市楠梓加工區開發路７３號"             : @"高雄市楠梓區開發路７３號",
                                           @"台北市立農街二段１５５號１樓"             : @"台北市北投區立農街二段１５５號１樓",
                                           @"花蓮市中央路三段７０７號（大愛樓地下室－超商櫃位）" : @"花蓮縣花蓮市中央路三段７０７號（大愛樓地下室－超商櫃位）",
                                           @"新竹市東大路四段２６９號１樓"             : @"新竹市北區東大路四段２６９號１樓",
                                           @"高雄市楠梓加工出口區內環北路１１５號"      : @"高雄市楠梓區內環北路１１５號",
                                           @"台中市南屯路一段２７３號"                : @"台中市南區南屯路一段２７３號",
                                           @"台中市西屯路二段９０之２２，２３號"        : @"台中市西屯區西屯路二段９０之２２，２３號",
                                           @"新竹市工業東二路１號１樓"                : @"新竹市東區工業東二路１號１樓",
                                           @"桃園市復興路１５９號１樓"                : @"桃園市桃園區復興路１５９號１樓",
                                           @"高雄市楠梓加工出口區環北路１０９號２樓"    : @"高雄市楠梓區環北路１０９號２樓",
                                           @"新竹市西濱路二段６０１號"                : @"新竹市香山區西濱路二段６０１號"
                                          };

    NSMutableDictionary *addressByArea = [[NSMutableDictionary alloc] init];
 
    NSString *address;
    NSString *county;
    NSString *subdivision;
    
    for (PPAPIAllpayGetStoreInfoResponse *store in stores)
    {
        address = store.storeAddress;

        // parse county & subdivision
        county = [address substringWithRange:NSMakeRange(0, 3)];
        county = [matchCountiesTable objectForKey:county] ? : county;
        subdivision = [self subdivisionFromAddress:address];

        // handle unrecognized subdivision (bad address)
        if (subdivision == nil)
        {
            address = [matchAddressesTable objectForKey:address];
            if (address == nil)
            {
                continue;   // log this if looking for new address exceptions
            }
            store.storeAddress = address;
            subdivision = [self subdivisionFromAddress:address];
        }
        subdivision = [matchSubdivisionsTable objectForKey:subdivision] ? : subdivision;

        if (![addressByArea objectForKey:county])
            [addressByArea setObject:[NSMutableDictionary dictionary] forKey:county];
        if (![addressByArea[county] objectForKey:subdivision])
            [addressByArea[county] setObject:[NSMutableArray array] forKey:subdivision];
        [addressByArea[county][subdivision] addObject:store];
    }

    // assign to appropriate store type
    if (type == PPConvenienceStoreType711)
        self.storesList711 = addressByArea;
    else if (type == PPConvenienceStoreTypeFamilymart)
        self.storesListFamilymart = addressByArea;
}

- (NSString *)subdivisionFromAddress:(NSString *)address
{
    NSString *subdivision;

    if      ([[address substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"鄉"])     // XX鄉
        subdivision = [address substringWithRange:NSMakeRange(3, 3)];
    else if ([[address substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"市"])     // XX市
        subdivision = [address substringWithRange:NSMakeRange(3, 3)];
    else if ([[address substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"鎮"])     // XX鎮
        subdivision = [address substringWithRange:NSMakeRange(3, 3)];
    else if ([[address substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"區"])     // XX區
        subdivision = [address substringWithRange:NSMakeRange(3, 3)];
    else if ([[address substringWithRange:NSMakeRange(4, 1)] isEqualToString:@"區"])     // X區
        subdivision = [address substringWithRange:NSMakeRange(3, 2)];
    else if ([[address substringWithRange:NSMakeRange(6, 1)] isEqualToString:@"鄉"])     // XXX鄉
        subdivision = [address substringWithRange:NSMakeRange(3, 4)];
    else if ([[address substringWithRange:NSMakeRange(5, 1)] isEqualToString:@"里"])     // XX里
        subdivision = [address substringWithRange:NSMakeRange(3, 3)];
    else
    {
        // 無法辨識的地址
        // subdivision = @"";
    }

    return subdivision;
}

- (void)saveCountiesAndSubdivisionsToFile:(PPConvenienceStoreType)type;
{
    NSMutableDictionary *print = [NSMutableDictionary dictionary];
    NSString *fileName;
    NSDictionary *addressByArea;
    
    if (type == PPConvenienceStoreTypeFamilymart)
    {
        addressByArea = self.storesListFamilymart;
        fileName = @"familymart_counties_list.plist";
    }
    else if (type == PPConvenienceStoreType711)
    {
        addressByArea = self.storesList711;
        fileName = @"unimart_counties_list.plist";
    }
    
    for (NSString *county in [addressByArea allKeys])
    {
        NSDictionary *names = addressByArea[county];
        [print setObject:[names allKeys] forKey:county];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    [print writeToFile:filePath atomically:YES];
}

@end
