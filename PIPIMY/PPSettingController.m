
#import "PPAPI.h"
#import "PPUser.h"
#import "AppDelegate.h"
#import "SRPAlertView.h"
#import "PPSettingController.h"
#import "PPMyDealsListController.h"
#import "PPAWSS3.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "UIImage+SRPKit.h"
#import "PPSomeoneStoreController.h"

enum ViewsTag
{
    k_Tag_UserIconImagePicker = 21000,
    k_Tag_UserBackgroundImagePicker,
};


@implementation PPSettingController 

#pragma mark - LifeCycle
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
    if([segue.destinationViewController isKindOfClass:NSClassFromString(@"PPMyDealsListController")])
    {
     
           //跳至我的交易 ViewController
     
           //這個 ViewController 分為買家 or 賣家, sender 傳過來是 UITableViewCell
           //取回 Cell 的 IndexPath, 設置 ViewController dealType
           //IndexPath.section = 0 為賣家, = 1 為買家
     
        UITableViewCell *cell        = sender;
        NSIndexPath *indexPath       = [self.tableView indexPathForCell:cell];
        PPMyDealsListController *mvc = segue.destinationViewController;
        mvc.dealType                 = indexPath.section;
    }
    */
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    // 跳至刊登商品前, 先判斷 User 是否已經有設置金物流, 沒有的話就跳 alert
    if([identifier isEqualToString:@"toPPPostViewController"])
    {
        BOOL storeSetting = [PPUser singleton].storeSetting;
        
        if(!storeSetting)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                             message:@"請先設定金物流"
                                                            delegate:nil
                                                   cancelButtonTitle:@"確定"
                                                   otherButtonTitles:nil];
            [alert show];
            
            return NO;
        }
    }
    
    return YES;
}

-(void)viewDidLoad
{
    CGFloat userIconWidth         = CGRectGetWidth(_userIconBtn.bounds);
    _userIconBtn.layer.masksToBounds = YES;
    _userIconBtn.layer.cornerRadius  = userIconWidth / 2.0;
    
    [self __updateUserIcon];
    [self __updateUserBackground];
}


- (void)__updateUserIcon
{
    NSURL *iconURL = [PPAWSS3 userIconWithUserId:[PPUser singleton].name];
    
    [_userIconBtn sd_setBackgroundImageWithURL:iconURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"nohead"]];
}

- (void)__updateUserBackground
{
    NSURL *backgroundURL = [PPAWSS3 userBackgroundWithUserId:[PPUser singleton].name];
    
    [_userBackgroundBtn sd_setBackgroundImageWithURL:backgroundURL forState:UIControlStateNormal placeholderImage:nil];
    
}

#pragma mark - TableView Delegates
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Set the text color of our header/footer text.
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
    header.textLabel.font = [UIFont boldSystemFontOfSize:15];
    
    // Set the background color of our header/footer.
    header.contentView.backgroundColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];//[UIColor srp_colorWithHEX:@"#FF514F4E"];
    
    
    // You can also do this to set the background color of our header/footer,
    //    but the gradients/other effects will be retained.
    // view.tintColor = [UIColor blackColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    // 點中會員那個 Section
    if(indexPath.section == 2)
    {
        // 點中更新密碼
        if(indexPath.row == 0)
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"修改密碼"
                                                                 message:@"請輸入新密碼"
                                                                delegate:nil
                                                       cancelButtonTitle:@"取消"
                                                       otherButtonTitles:@"確定", nil];
            
            alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
               
                if(buttonIndex != alert.cancelButtonIndex)
                {
                    UITextField *field = [alert textFieldAtIndex:0];
                    
                    if(field.text.length)
                    {
                        [self __callAPIUpdatePWD:field.text];
                    }
                }
            }];
        }
        
        // 點中意見反應
        else if(indexPath.row == 1)
        {
            NSURL *url = [NSURL URLWithString:@"mailto:pipimy.service@gmail.com"];
            
            [[UIApplication sharedApplication]openURL:url];
        }
    }
    */
}

#pragma mark - IBAction

- (IBAction)logoutItemDidClicked:(id)sender
{
    NSDictionary *params = @{@"OS" : @"ios"};
    
    [[PPAPI singleton]APILogout:params callback:^(BOOL success) {
       
        if(success)
        {
            [[PPUser singleton]logout];

            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            delegate.tabController.selectedIndex = 0;
        }
    }];
}

#pragma mark - Private methods

- (void)__callAPIUpdatePWD:(NSString *)newPasswd
{
    PPUser *user         = [PPUser singleton];
    NSDictionary *params = @{@"ID" : user.name, @"Pwd" : newPasswd};
    
    [[PPAPI singleton]APIUpdatePWD:params callback:^(NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"成功"
                                                           message:@"修改密碼成功"
                                                          delegate:nil
                                                 cancelButtonTitle:@"確定"
                                                 otherButtonTitles:nil];
            
            [alert show];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

#pragma mark - 更新使用者圖像
- (IBAction)userIconBtnTouchUpInside:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType               = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing            = YES;
    picker.delegate                 = self;
    picker.view.tag                 = k_Tag_UserIconImagePicker;
    [self.tabBarController presentViewController:picker animated:YES completion:nil];
}

#pragma mark - 更新使用者背景
- (IBAction)userBackgroundBtnTouchUpInside:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType               = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing            = YES;
    picker.delegate                 = self;
    picker.view.tag                 = k_Tag_UserBackgroundImagePicker;
    [self.tabBarController presentViewController:picker animated:YES completion:nil];
}


#pragma mark - 小賣家 IBAction

- (IBAction)storeIntroductionTouchUpInside:(id)sender
{
    
}

- (IBAction)storePreviewBtnTouchUpInside:(id)sender
{
    UIStoryboard *store = [UIStoryboard storyboardWithName:@"Tab1" bundle:nil];
    PPSomeoneStoreController *mvc = [store instantiateViewControllerWithIdentifier:@"PPSomeoneStoreController"];
    
    mvc.memberId = [PPUser singleton].name;
    
    [self.navigationController pushViewController:mvc animated:YES];
}

- (IBAction)updateProductBtnTouchUpInside:(id)sender
{
    
}

- (IBAction)updateLocationTouchUpInside:(id)sender
{
    SRPAlertView *alert = [[SRPAlertView alloc]initWithTitle:nil
                                                             message:@"更新小店位置將會影響所有商品位置"
                                                            delegate:nil
                                                   cancelButtonTitle:@"確定"
                                                   otherButtonTitles:@"取消", nil];
    
    [alert showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
                
                CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                
                [geocoder reverseGeocodeLocation:[PPUser singleton].location completionHandler:^ (NSArray *placemarks, NSError *error) {
                    
                    [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
                    
                    CLPlacemark *mark = placemarks.firstObject;
                    
                    // 目前 App 只在台灣, 台灣要取 city 大概就是用 subAdministrativeArea
    
                    
                    if(mark.subAdministrativeArea.length)
                    {
                        NSString *city = mark.subAdministrativeArea;
                        
                        [self __callAPIUpdateStoreLocWithStoreCity:city];
                    }
                    else
                    {
                        [self __showAlertViewWithMessage:@"請檢查定位狀態"];
                    }
                }];
            }
             break;
                
            case 1:
            {
                
            }
                break;
            default:
                break;
        }
    
    }];
}


- (IBAction)mySaleTradingBtnTouchUpInside:(id)sender
{
    PPMyDealsListController *viewController = [[UIStoryboard storyboardWithName:@"Tab4" bundle:nil] instantiateViewControllerWithIdentifier:@"PPMyDealsListController"];
    viewController.DealType = DealTypeSeller;
    [self.navigationController pushViewController:viewController animated:YES];
}


#pragma mark - 小買家 IBAction

- (IBAction)myBuyTradingBtnTouchUpInside:(id)sender
{
    PPMyDealsListController *viewController = [[UIStoryboard storyboardWithName:@"Tab4" bundle:nil] instantiateViewControllerWithIdentifier:@"PPMyDealsListController"];
    viewController.DealType = DealTypeBuyer;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - 會員 IBAction
- (IBAction)changePasswordBtnTouchUpInside:(id)sender
{
    SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"修改密碼"
                                                         message:@"請輸入新密碼"
                                                        delegate:nil
                                               cancelButtonTitle:@"取消"
                                               otherButtonTitles:@"確定", nil];
    
    alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
    
    [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
        
        if(buttonIndex != alert.cancelButtonIndex)
        {
            UITextField *field = [alert textFieldAtIndex:0];
            
            if(field.text.length)
            {
                [self __callAPIUpdatePWD:field.text];
            }
        }
    }];

}

- (IBAction)mailToBtnTouchUpInside:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"mailto:pipimy.service@gmail.com"];
    
    [[UIApplication sharedApplication]openURL:url];

}

- (void)__callAPIUpdateStoreLocWithStoreCity:(NSString*)storeCityStr
{
    PPUser *user = [PPUser singleton];
    
    NSDictionary *params = @{@"StoreCity" : storeCityStr,
                             @"Lat"       : @(user.location.coordinate.latitude),
                             @"Lng"       : @(user.location.coordinate.longitude)
                             };
    
    [[PPAPI singleton]APIUpdateStoreLoc:params callback:^(NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"位置更新成功" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil , nil];
            [alert show];
        }
    }];
}


#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    switch (picker.view.tag)
    {
        case k_Tag_UserIconImagePicker:
        {
            [picker dismissViewControllerAnimated:YES completion:^{
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    UIImage *originalImg = info[UIImagePickerControllerEditedImage];
                    UIImage *resizeImg   = [originalImg srp_resizedImageByMagick:@"320x320#"];
                    NSString *fileName   = @"UserIcon.jpg";
                    NSString *savePath   = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                    NSData *data         = UIImageJPEGRepresentation(resizeImg, 1.0);
                    
                    [data writeToFile:savePath atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        // 上傳新頭像
                        [[PPAWSS3 singleton]uploadUserIconWithUserId:[PPUser singleton].name
                                                            filePath:[NSURL fileURLWithPath:savePath]
                                                             success:^(BOOL success)
                        {
                            [_userIconBtn setBackgroundImage:resizeImg forState:UIControlStateNormal];
                                                             }];
                        
                        
                    });
                });
            }];
        }
            break;
            
        case k_Tag_UserBackgroundImagePicker:
        {
            [picker dismissViewControllerAnimated:YES completion:^{
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    UIImage *originalImg = info[UIImagePickerControllerEditedImage];
                    UIImage *resizeImg   = [originalImg srp_resizedImageByMagick:@"320x150#"];
                    NSString *fileName   = @"UserBackground.jpg";
                    NSString *savePath   = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                    NSData *data         = UIImageJPEGRepresentation(resizeImg, 1.0);
                    
                    [data writeToFile:savePath atomically:YES];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                
                        [[PPAWSS3 singleton]uploadUserBackgroundWithUserId:[PPUser singleton].name
                                                            filePath:[NSURL fileURLWithPath:savePath]
                                                             success:^(BOOL success)
                        {
                            [_userBackgroundBtn setBackgroundImage:resizeImg forState:UIControlStateNormal];
                                                             }];

                        
                    });
                });
            }];
        }
            break;
            
        default:
            break;
    }
   
}

@end
