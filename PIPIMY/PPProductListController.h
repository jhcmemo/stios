
#import <UIKit/UIKit.h>
#import "PPProductSmallCell.h"

/**
 *  商品列表 MVC
 *
 *  注意: 此 ViewController 將所有 delgate 抽出成 PPProductListController+Delegates.h
 */
@interface PPProductListController : UIViewController




/*
 *  顯示的dataSource型別
 */
typedef NS_ENUM(NSUInteger, ContentDataSourceType)
{
    /**
     *  關鍵字搜尋 介接 getSearchProduct.php
     */
    ContentDataSourceTypeKeyWord,
    /**
     *  分類搜尋 介接 getTypeProduct.php
     */
    ContentDataSourceTypeCategory,
    /**
     *  使用者地理位置 介接 getNearProduct.php
     */
    ContentDataSourceTypeDefault,
};


/*
 *  dataSource排序 型別
 */
typedef NS_ENUM(NSUInteger, ContentDataSourceSortType)
{
    /**
     *  最新
     */
    ContentDataSourceSortTypeByTime = 1,
    /**
     *  最熱門
     */
    ContentDataSourceSortTypeByHot,
    /**
     *  最便宜
     */
    ContentDataSourceSortTypeByPrice,
    /**
     *  最近
     */
    ContentDataSourceSortTypeByLocation,
};



-(void)callAPIGetTypeProduct;
-(void)callAPISearchProductWithKeyWord;


/**
 *  商品列表資料
 */
@property (nonatomic, readonly) NSArray *dataSource;

/**
 *  Cell 顯示的方式
 */
@property (nonatomic, readonly) DisplayCellType cellType;

/**
 *  當 cellType = DisplayCellTypeSmall, 用這個列表呈現
 */
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;


/**
 *  當 使用者點選navbar上 displayBarItem時，所要顯示給使用者點選的view
 */
@property (nonatomic, weak) IBOutlet UIView *selectDisplayTypeView;

/**
 *  當 使用者點選navbar上 displayBarItem時，所要顯示給使用者點選的view
 */
@property (nonatomic,readwrite) ContentDataSourceType dataSourceType;

/**
 *  當 使用者點選navbar上 displayBarItem時，所要顯示給使用者點選的view
 */
@property (nonatomic,readwrite) ContentDataSourceSortType dataSourceSortType;

@property (nonatomic, strong) NSString         *searchKeyWord;  //搜尋的關鍵字

@end
