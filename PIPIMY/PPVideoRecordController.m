
#import "SRPAlertView.h"
#import "PPMediaPlayerView.h"
#import "PPVideoRecordController.h"


@interface PPVideoRecordController ()<AVCaptureFileOutputRecordingDelegate>

@property (nonatomic, strong) AVPlayer                 *player;  // 影片播放器
@property (nonatomic, strong) AVCaptureSession         *session; // session
@property (nonatomic, strong) AVCaptureMovieFileOutput *recorder;// 錄影

@property (nonatomic, weak) IBOutlet PPCaptureView  *recorderView;  // 錄影 display View;
@property (nonatomic, weak) IBOutlet PPPlayerView   *playerView;    // 播放 display View
@property (nonatomic, weak) IBOutlet UIProgressView *videoProgress; // 播放 / 錄影進度
@property (nonatomic, weak) IBOutlet UIButton       *uploadButton;  // 上傳按鈕
@property (nonatomic, weak) IBOutlet UIButton       *recordButton;  // 錄影按鈕
@property (nonatomic, weak) IBOutlet UIButton       *playButton;    // 播放按鈕

@end


@implementation PPVideoRecordController

#pragma mark - LifeCycle
- (void)dealloc
{
    [_recorder removeObserver:self forKeyPath:@"recording"];
    [_player removeObserver:self forKeyPath:@"rate"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(![_session isRunning])
    {
        [_session startRunning];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if([_session isRunning])
    {
        [_session stopRunning];
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"rate"])
    {
        BOOL playing = [change[@"new"]boolValue];
        
        if(playing)
        {
            AVPlayerItem *currentItem = _player.currentItem;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                // 影片播放時要更新進度條
                while (_player.rate > 0.0)
                {
                    CGFloat duration = CMTimeGetSeconds(currentItem.currentTime);
                    CGFloat time     = CMTimeGetSeconds(currentItem.duration);
                    CGFloat progress = duration / time;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        _videoProgress.progress = progress;
                    });
                }
            });
        }
        else
        {
            [self __setupUI];
            
            // 播放完畢時要 seek 到最前面, 才可以繼續觀看
            [_player seekToTime:kCMTimeZero];
        }
    }
    else if([keyPath isEqualToString:@"recording"])
    {
        BOOL recording = [change[@"new"]boolValue];
    
        if(recording)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                // 錄影時要跟新進度條
                while (_recorder.isRecording)
                {
                    CGFloat duration = CMTimeGetSeconds(_recorder.recordedDuration);
                    CGFloat time     = CMTimeGetSeconds(_recorder.maxRecordedDuration);
                    CGFloat progress = duration / time;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        _videoProgress.progress = progress;
                    });
                }
            });
        }
        else
        {
            [self __setupUI];
        }
    }
}

#pragma mark - Delegates

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
      fromConnections:(NSArray *)connections error:(NSError *)error
{
    if(![self __removeTempMP4Video])
    {
        // AVAssetExportSession can't override file, need to delete prev mp4 video
        
        return;
    }
    
    AVURLAsset *savedAsset = [AVURLAsset assetWithURL:outputFileURL];
    
    /**
     *  可以從 AVAssetExportPreset 來決定拍攝的解析度，因而產出的檔案而有大小區別。
     */
    AVAssetExportSession *export = [AVAssetExportSession exportSessionWithAsset:savedAsset
                                                                     presetName:AVAssetExportPresetMediumQuality];
    NSURL *exportMP4URL   = [NSURL fileURLWithPath:[self __tempMP4VideoPath]];
    export.outputURL      = exportMP4URL;
    export.outputFileType = AVFileTypeMPEG4;
    
    [export exportAsynchronouslyWithCompletionHandler:^{
        
        if(export.status == AVAssetExportSessionStatusCompleted)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
               
                [self __removeTempMOVVideo];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    AVURLAsset *asset  = [AVURLAsset assetWithURL:exportMP4URL];
                    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
                    
                    [_player replaceCurrentItemWithPlayerItem:item];
                    [self __setupUI];
                });
            });
        }
        else
        {
            // convert mov to mp4 fail
        }
    }];
}

#pragma mark - IBAction

- (IBAction)recordButtonClicked:(id)sender
{
    NSURL *URL = [NSURL fileURLWithPath:[self __tempMOVVideoPath]];
    
    [_recorder startRecordingToOutputFileURL:URL recordingDelegate:self];
    
    // delay 一下 UI 呈現會比較好
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self __setupUI];
    });
}

- (IBAction)playButtonClicked:(id)sender
{
    [_player play];
    
    // delay 一下 UI 呈現會比較好
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self __setupUI];
    });
}

- (IBAction)uploadButtonClicked:(id)sender
{
    if([_delegate respondsToSelector:@selector(uploadButtonClickedInVideoRecordController:)])
    {
        [_delegate uploadButtonClickedInVideoRecordController:self];
    }
}

#pragma mark - Private methods

- (void)__setup
{
    // 存取相機權限
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        
        if(!granted)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
            
                [self __showMessageForDenyUseingCameraOrRecord];
            });
        }
        else
        {
            // 取存麥克風權限
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
                
                if(!granted)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                       
                        [self __showMessageForDenyUseingCameraOrRecord];
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        
                        [self __setupRecorder];
                        [self __setupPlayer];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self __setupUI];
                        });

                    });
                }
            }];
        }
    }];
}

- (void)__setupRecorder
{
    _session                          = [[AVCaptureSession alloc]init];
    _recorder                         = [[AVCaptureMovieFileOutput alloc]init];
    AVCaptureDevice *videoDevice      = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *videoInput  = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];
    AVCaptureDevice *audioDevice      = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    AVCaptureDeviceInput *audioInput  = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:nil];
    AVCaptureVideoPreviewLayer *layer = (AVCaptureVideoPreviewLayer *)_recorderView.layer;
    layer.videoGravity                = AVLayerVideoGravityResizeAspectFill;
    layer.session                     = _session;
    _recorder.maxRecordedDuration     = CMTimeMake(10, 1);
    _recorder.minFreeDiskSpaceLimit   = 1024 * 1024;
    
    [_session setSessionPreset:AVCaptureSessionPresetMedium];
    [_recorder addObserver:self forKeyPath:@"recording" options:NSKeyValueObservingOptionNew context:nil];
    [_session addInput:videoInput];
    [_session addInput:audioInput];
    [_session addOutput:_recorder];
}

- (void)__setupPlayer
{
    _player              = [[AVPlayer alloc]init];
    AVPlayerLayer *layer = (AVPlayerLayer *)_playerView.layer;
    layer.player         = _player;
    layer.videoGravity   = AVLayerVideoGravityResizeAspectFill;
    NSURL *videoURL      = [NSURL fileURLWithPath:[self __tempMP4VideoPath]];
    AVURLAsset *asset    = [AVURLAsset assetWithURL:videoURL];
    AVPlayerItem *item   = [AVPlayerItem playerItemWithAsset:asset];
    
    [_player addObserver:self forKeyPath:@"rate" options:NSKeyValueObservingOptionNew context:nil];
    [_player replaceCurrentItemWithPlayerItem:item];
}

- (void)__setupUI
{
    _playerView.hidden    = YES;
    _recorderView.hidden  = YES;
    _uploadButton.enabled = YES;
    _recordButton.enabled = YES;
    _playButton.enabled   = YES;
    
    // 錄影時...
    if(_recorder.isRecording)
    {
        _recorderView.hidden  = NO;
        _uploadButton.enabled = NO;
        _recordButton.enabled = NO;
        _playButton.enabled   = NO;
    }
    
    // 播放時...
    if(_player.rate > 0.0)
    {
        _playerView.hidden    = NO;
        _uploadButton.enabled = NO;
        _recordButton.enabled = NO;
        _playButton.enabled   = NO;
    }
    
    NSString *path = [self __tempMP4VideoPath];
    
    // 當有暫存影片時
    if([[NSFileManager defaultManager]fileExistsAtPath:path])
    {
        _playerView.hidden = NO;
    }
    else
    {
        _recorderView.hidden  = NO;
        _uploadButton.enabled = NO;
        _playButton.enabled   = NO;
    }
}

- (NSString *)__tempMOVVideoPath
{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"tempVideo.mov"];
}

- (BOOL)__removeTempMOVVideo
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath         = [self __tempMOVVideoPath];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        return YES;
    }
    else
    {
        return [fileManager removeItemAtPath:filePath error:nil];
    }
}

- (NSString *)__tempMP4VideoPath
{
    return [NSTemporaryDirectory() stringByAppendingPathComponent:@"tempVideo.mp4"];
}

- (BOOL)__removeTempMP4Video
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath         = [self __tempMP4VideoPath];
    
    if(![fileManager fileExistsAtPath:filePath])
    {
        return YES;
    }
    else
    {
        return [fileManager removeItemAtPath:filePath error:nil];
    }
}

- (void)__showMessageForDenyUseingCameraOrRecord
{
    SRPAlertView *alert = [[SRPAlertView alloc]initWithTitle:nil
                                                     message:@"請至設定允許使用相機跟麥克風"
                                                    delegate:nil
                                           cancelButtonTitle:@"確定"
                                           otherButtonTitles:nil];
    
    [alert showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

@end
