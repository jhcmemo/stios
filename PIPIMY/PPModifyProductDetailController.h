
#import <UIKit/UIKit.h>
@class PPAPIGetNearProductResponse;


/**
 *  商品詳細 MVC
 */
@interface PPModifyProductDetailController : UITableViewController <UITextFieldDelegate,UITextViewDelegate>



/**
 *  商品
 */
@property (nonatomic, weak) PPAPIGetNearProductResponse *product;

@property (weak, nonatomic) IBOutlet UITextField *titleFiled;

@property (weak, nonatomic) IBOutlet UITextField *priceFiled;

@property (weak, nonatomic) IBOutlet UITextView *productDesc;



@end
