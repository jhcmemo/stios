
#import "PPAPITracePushTriggerResponse.h"

@implementation PPAPITracePushTriggerResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return  @{ @"isSwitchOn" : @"switch" };
}

+ (NSValueTransformer *)isSwitchOnJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^NSNumber *(NSString *switchString) {
        if (!switchString)
            return nil;
        if ([switchString isEqualToString:@"on"])
            return @YES;
        else if ([switchString isEqualToString:@"off"])
            return @NO;
        else
            return nil;
    }];
}

@end
