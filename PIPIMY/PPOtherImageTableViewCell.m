
#import "PPAPIGetChatContentResponse.h"
#import "PPOtherImageTableViewCell.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PPAPIGetChatIndexResponse.h"

@interface PPOtherImageTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *userIDLabel;
@property (weak, nonatomic) IBOutlet UIView *imageBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *imageMessage;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImageView;

@end

@implementation PPOtherImageTableViewCell

+ (CGFloat)cellHeight
{
    return 92.0;
}

- (void)awakeFromNib
{
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProduct)];
    [singleTap setNumberOfTapsRequired:1];
    [self.imageBackgroundView addGestureRecognizer:singleTap];
    
    // bubble background
    UIImage *bubble = [UIImage imageNamed:@"message-bubble-white"];
    [self.bubbleImageView setImage:[bubble resizableImageWithCapInsets:UIEdgeInsetsMake(25, 35, 22, 24)
                                                          resizingMode:UIImageResizingModeStretch]];
}

- (void)configureCellWithMessage:(PPAPIGetChatContentResponse *)message chat:(NSDictionary *)chat
{
    // user ID and time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    _userIDLabel.text = [dateFormatter stringFromDate:message.sendTime];
    
    // user icon
    NSURL *userIconURL = [PPAWSS3 userIconWithUserId:chat[@"otherPersonName"]];
    [_userIcon sd_setImageWithURL:userIconURL placeholderImage:[UIImage imageNamed:@"nohead"]];
    _userIcon.contentMode = UIViewContentModeScaleAspectFit;
    _userIcon.backgroundColor = [UIColor blueColor];
    
    // message
    self.imageMessage.text = message.content;
}

- (void)goToProduct
{
    if ([self.delegate respondsToSelector:@selector(goToProduct:)])
        [self.delegate goToProduct:nil];
}

@end
