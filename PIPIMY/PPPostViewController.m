
#import "PPAPI.h"
#import "PPUser.h"
#import "PPAWSS3.h"
#import "PPPostViewsModel.h"
#import "SRPAlertView.h"
#import "PPPostViewController.h"
#import "PPVideoRecordController.h"
#import "PPImageCaptureController.h"


#define k_TackPhotoBtnsStart_Tag        20000
#define k_TackPhotoProgressStart_Tag    30000
#define k_TackPhotoViewsStart_Tag       40000

@interface PPPostViewController()<PPProtocols>
{
    int currentTakePohtoTag; //當下進行的照片
    int lastUpdatePhotoTag;  //最後上傳成功的編號
}



@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *tackPhotosBtns;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *tackPhotosViews;
@property (strong, nonatomic) IBOutletCollection(UIProgressView) NSArray *tackPhotosrogressViews;


@property (nonatomic, assign) BOOL videoUploaded;        // 影片是否上傳
@property (nonatomic, assign) BOOL imageUploaded;        // 圖片是否上傳
@property (nonatomic, readonly) NSNumber *productId;     // 上傳的商品 Id
@property (nonatomic, weak) IBOutlet PPPostViewsModel *viewsModel; // Subviews 管理的 Model



@end


@implementation PPPostViewController

#pragma mark - LifeCycle
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 每次進來都要去取回 ProductId
    [self __callAPIGetProductID];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPPVideoRecordController"])
    {
        PPVideoRecordController *mvc = segue.destinationViewController;
        mvc.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"toPPImageCaptureController"])
    {
        PPImageCaptureController *mvc = segue.destinationViewController;
        mvc.delegate = self;
    }
}

#pragma mark - Properties Getter

- (NSNumber *)productId
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"kProductId"];
}

#pragma mark - Delegates

- (void)uploadButtonClickedInVideoRecordController:(PPVideoRecordController *)mvc
{
    [mvc.navigationController popViewControllerAnimated:YES];
    [_viewsModel updateVideoUploadProgress:0.05];
    
    if(self.productId)
    {
        [self __uploadVideo];
    }
}

- (void)imageCaptureController:(PPImageCaptureController *)mvc uploadButtonClicked:(NSArray *)filesName
{
    /*
    [mvc.navigationController popViewControllerAnimated:YES];
    [_viewsModel updateImagesUploadProgress:0.05];
    
    if(self.productId || filesName.count)
    {
        [self __uploadImages:filesName];
    }
     */
}

#pragma mark - IBActions

- (IBAction)sendItemDidClicked:(id)sender
{
    if(!self.productId)
    {
        return;
    }
    if(!_videoUploaded)
    {
        [self __showAlertViewWithMessage:@"尚未上傳影片"];
        
        return;
    }
    if(!_imageUploaded)
    {
        [self __showAlertViewWithMessage:@"尚未上傳圖片"];
        
        return;
    }
    if(![PPUser singleton].location)
    {
        [self __showAlertViewWithMessage:@"請檢查網路定位"];
        
        return;
    }
    
    NSMutableDictionary *params = [_viewsModel apiParams];
    
    if(params.count)
    {
        [self __callAPIDoneTextMWith:params];
    }
}

#pragma mark - Private methods

- (void)__callAPIGetProductID
{
    // 如果已經有的話, 就不用取了
    if(self.productId)
    {
        return;
    }
    
    [[PPAPI singleton]APIGetProductID:^(NSNumber *newProductId, NSError *err) {
    
        // 沒取到要繼續取
        if(!newProductId)
        {
            [self __callAPIGetProductID];
        }
        
        // 取到後存到 NSUserDefault, key = kProductId
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:newProductId forKey:@"kProductId"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }];
}

- (void)__callAPIDoneVideoM
{
    NSDictionary *params = @{@"ProductID" : self.productId};
    
    [[PPAPI singleton]APIDoneVideoM:params callback:^(NSError *err) {
        
        if(err)
        {
            [_viewsModel videoUploadSuccess:NO];
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            self.videoUploaded = YES;
            
            [_viewsModel videoUploadSuccess:YES];
        }
    }];
}

- (void)__callAPIDonePicM:(NSInteger)picCount;
{
    NSDictionary *params = @{@"ProductID" : self.productId, @"PicNumber" : @(picCount+1)};
    
    [[PPAPI singleton]APIDonePicM:params callback:^(NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            self.imageUploaded = YES;
            
            for (UIView *view in _tackPhotosViews)
            {
                if (view.tag == currentTakePohtoTag + k_TackPhotoViewsStart_Tag +1)
                {
                    if (view.hidden == YES)
                    {
                        view.hidden = NO;
                    }
                    break;
                }
            }
        }
    }];
}

- (void)__callAPIDoneTextMWith:(NSMutableDictionary *)params
{
    PPUser *user         = [PPUser singleton];
    params[@"ProductID"] = self.productId;
    params[@"CC"]        = @"TW";
    params[@"Lat"]       = @(user.location.coordinate.latitude);
    params[@"Lng"]       = @(user.location.coordinate.longitude);
    params[@"PostTime"]  = ({
        NSString *postTime             = @"";
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat       = @"yyyy-MM-dd HH:mm:ss";
        postTime                       = [dateFormatter stringFromDate:[NSDate date]];
        postTime;
    });
    
    [[PPAPI singleton]APIDoneTextM:params callback:^(NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:nil
                                                                 message:@"刊登商品成功"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
                
                // 刊登成功要把舊的商品 Id 刪除
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"kProductId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    }];
}

- (void)__uploadVideo
{
    [[PPAWSS3 singleton]uploadVideoWithProductId:self.productId progress:^(CGFloat progress) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_viewsModel updateVideoUploadProgress:progress];
            
            
        });
    } success:^(BOOL success, NSError *error) {

        if(success)
        {
            [self __callAPIDoneVideoM];
        }
        else
        {
            
        }
    }];
}

- (void)__uploadImages:(NSArray *)files
{

    [[PPAWSS3 singleton]uploadImagesWithProductId:self.productId files:files progress:^(CGFloat progress)
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIProgressView *currentProgress = nil;
            
            for (UIProgressView *progress in _tackPhotosrogressViews)
            {
                if (progress.tag-k_TackPhotoProgressStart_Tag == currentTakePohtoTag)
                {
                    currentProgress = progress;
                    break;
                }
            }

            if (currentProgress)
            {
                currentProgress.progress = progress;
            }
        });
    } success:^(BOOL success, NSError *error) {
        
        if(success)
        {
            [self __callAPIDonePicM:currentTakePohtoTag];
        }
        else
        {
            
        }
    }];
    
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}



- (IBAction)takePhotoBtnsTouchUpInsid:(id)sender
{
    UIButton *currnetTakePhotoBtn = (UIButton*)sender;
    
    currentTakePohtoTag = (int)currnetTakePhotoBtn.tag - k_TackPhotoBtnsStart_Tag;
    
    CustomImagePickerController *picker = [[CustomImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [picker setIsSingle:YES];
        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    [picker setCustomDelegate:self];
    //[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:nil];
}


- (void)cameraPhoto:(UIImage *)image  //选择完图片
{
    ImageFilterProcessViewController *fitler = [[ImageFilterProcessViewController alloc] init];
    [fitler setDelegate:self];
    fitler.currentImage = image;
    [self presentViewController:fitler animated:YES completion:nil];
    
}
- (void)imageFitlerProcessDone:(UIImage *)image //图片处理完
{

    UIButton *cureentBtn = nil;
    
    for (UIButton *btn in _tackPhotosBtns)
    {
        if (btn.tag == currentTakePohtoTag + k_TackPhotoBtnsStart_Tag)
        {
            cureentBtn = btn;
            break;
        }
    }
    
    
    if (cureentBtn)
    {
        [cureentBtn setBackgroundImage:image forState:UIControlStateNormal];
        
        
        NSData *saveData     = UIImageJPEGRepresentation(image, 1.0);
        NSString *fileNameStr   = [NSString stringWithFormat:@"tempImage%@.jpg", @(currentTakePohtoTag)];
        NSString *savePath   = [NSTemporaryDirectory() stringByAppendingPathComponent:fileNameStr];
        
 
        if([saveData writeToFile:savePath atomically:YES])
        {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:fileNameStr,@"fileNameStr",@(currentTakePohtoTag+1),@"currentUpdate" ,nil];
            
             NSArray *filesName = [[NSArray alloc]initWithObjects:dic, nil];
            [self __uploadImages:filesName];
        }

    }
    
}

- (void)cancelCamera
{
}


@end
