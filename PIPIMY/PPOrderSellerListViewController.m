
#import "PPOrderSellerListViewController.h"
#import "PPOrderBuyerTableViewCell.h"
#import "PPOrderDetailViewController.h"
#import "PPAPI.h"

@interface PPOrderSellerListViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *orderList;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PPOrderSellerListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[PPAPI singleton] APIGetOrderSellerList:nil callback:^(NSArray *orderList, NSError *err) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"updatedAt" ascending:NO];
        self.orderList = [orderList sortedArrayUsingDescriptors:@[sortDescriptor]];
        [self.tableView reloadData];
    }];
    
    // remove empty separator lines
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Table view delegate / datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.orderList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetOrderListResponse *order = [self.orderList objectAtIndex:indexPath.row];
    PPOrderBuyerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sellerCell"];
    [cell configureCellWithOrder:order];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 190.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[PPOrderBuyerTableViewCell class]])
    {
        PPOrderDetailViewController *destination = segue.destinationViewController;
        destination.order = ((PPOrderBuyerTableViewCell *)sender).order;
        destination.asBuyer = NO;
    }
}

@end
