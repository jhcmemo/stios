
#import <S3.h>
#import "PPAWSS3.h"


NSString * const S3_IMAGE_DOMAIN             = @"https://d2pqiif4nwmp3r.cloudfront.net";
NSString * const S3_VIDEO_DOMAIN             = @"https://d1yjq3haxfphfj.cloudfront.net";
NSString * const S3_USERICON_DOMAIN          = @"https://s3.amazonaws.com/pipimy-stores";
NSString * const S3_USERBACKGROUND_DOMAIN    = @"https://s3.amazonaws.com/pipimy-backgrounds";
NSString * const S3_VIDEO_BUCKETNAME         = @"pipimy-videos";
NSString * const S3_IMAGE_BUCKETNAME         = @"pipimy-pics";
NSString * const S3_ICON_BUCKETNAME          = @"pipimy-stores";
NSString * const S3_BACKGROUND_BUCKETNAME    = @"pipimy-backgrounds";
NSString * const S3_ACCOUNT_ID               = @"252079443835";
NSString * const S3_COGNITOPOOL_ID           = @"us-east-1:b43a25bf-34a0-4465-964c-68a0159413e6";
NSString * const S3_COGNITOROLE_UNAUTH       = @"arn:aws:iam::252079443835:role/Cognito_SmallTradeCognitoUnauth_DefaultRole";


@interface PPAWSS3 ()

@property (nonatomic, strong) AWSS3TransferManager *s3Manager;                      // S3 manager
@property (nonatomic, assign) BOOL                 videoUploading;                  // 是否正在上傳影片
@property (nonatomic, assign) BOOL                 imagesUploading;                 // 是否正在上傳圖片
@property (nonatomic, assign) CGFloat              totalImageBytesSent;             // 圖片已上傳 byte
@property (nonatomic, assign) CGFloat              totalImageBytesExpectedToSend;   // 圖片需要上傳 byte
@property (nonatomic, assign) NSInteger            uploadImageCount;                // 需要上傳的圖片數量
@property (nonatomic, assign) NSInteger            uploadImageSueecssCount;         // 上傳圖片成功的數量

@end


@implementation PPAWSS3

+ (instancetype)singleton
{
    static PPAWSS3 *_singleton = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _singleton = [[PPAWSS3 alloc]init];
        
        AWSCognitoCredentialsProvider *credentialsProvider = [AWSCognitoCredentialsProvider
                                                              credentialsWithRegionType:AWSRegionUSEast1
                                                              accountId:S3_ACCOUNT_ID
                                                              identityPoolId:S3_COGNITOPOOL_ID
                                                              unauthRoleArn:S3_COGNITOROLE_UNAUTH
                                                              authRoleArn:nil];
        
        AWSServiceConfiguration *configuration = [AWSServiceConfiguration configurationWithRegion:AWSRegionUSEast1
                                                                              credentialsProvider:credentialsProvider];
        
        [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
        
        _singleton.s3Manager = [AWSS3TransferManager defaultS3TransferManager];
    });
    
    return _singleton;
}

#pragma mark - 返回商品 Image URL
+ (NSURL *)imageURLWithProductId:(NSNumber *)Id inIndex:(NSUInteger)index
{
    NSString *url = [NSString stringWithFormat:@"%@/%@-pic%@.jpg", S3_IMAGE_DOMAIN, Id, @(index)];
    
    return [NSURL URLWithString:url];
}

#pragma mark - 返回商品 Video URL
+ (NSURL *)videoURLWithProductId:(NSNumber *)Id
{
    NSString *url = [NSString stringWithFormat:@"%@/%@-video.mp4", S3_VIDEO_DOMAIN, Id];
    
    return [NSURL URLWithString:url];
}

#pragma 返回使用者頭像 URL
+ (NSURL *)userIconWithUserId:(NSString *)Id
{
    NSString *url = [NSString stringWithFormat:@"%@/%@.jpg", S3_USERICON_DOMAIN, Id];
    
    return [NSURL URLWithString:url];
}


#pragma 返回使用者背景 URL
+ (NSURL *)userBackgroundWithUserId:(NSString *)Id
{
    NSString *url = [NSString stringWithFormat:@"%@/%@-bg.jpg", S3_USERBACKGROUND_DOMAIN, Id];
    
    return [NSURL URLWithString:url];
}

#pragma mark - 上傳商品影片
- (void)uploadVideoWithProductId:(NSNumber *)productId
                        progress:(void (^)(CGFloat))progress
                         success:(void (^)(BOOL, NSError *))success
{
    if(_videoUploading)
    {
        return;
    }
    
    NSString *tempPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"tempVideo.mp4"];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:tempPath])
    {
        NSDictionary *info = @{NSLocalizedDescriptionKey : @"temp video is empty"};
        NSError *error     = [[NSError alloc]initWithDomain:@"com.pipimy.uploadVideo" code:-1 userInfo:info];
        
        success(NO, error);
        
        return;
    }
    
    self.videoUploading                       = YES;
    NSString *videoName                       = [NSString stringWithFormat:@"%@-video.mp4", productId];
    AWSS3TransferManagerUploadRequest *upload = [[AWSS3TransferManagerUploadRequest alloc]init];
    upload.bucket                             = S3_VIDEO_BUCKETNAME;
    upload.key                                = videoName;
    upload.ACL                                = AWSS3ObjectCannedACLPublicRead;
    upload.contentType                        = @"video/mp4";
    upload.body                               = [NSURL fileURLWithPath:tempPath];
    
    upload.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
        CGFloat progressing = (CGFloat)totalBytesSent / (CGFloat)totalBytesExpectedToSend;
        progress(progressing);
    };
    
    [[_s3Manager upload:upload]continueWithSuccessBlock:^id(BFTask *task) {
       
        self.videoUploading = NO;
        
        if(task.error)
        {
            success(NO, task.error);
        }
        else
        {
            success(YES, nil);
        }
        
        return nil;
    }];
}

#pragma mark - 上傳商品圖片
- (void)uploadImagesWithProductId:(NSNumber *)productId
                            files:(NSArray *)files
                         progress:(void (^)(CGFloat))progress
                          success:(void (^)(BOOL, NSError *))success
{
    if(_imagesUploading)
    {
        return;
    }
    
    if(!files.count)
    {
        NSDictionary *info = @{NSLocalizedDescriptionKey : @"No files to upload"};
        NSError *err = [[NSError alloc]initWithDomain:@"cim.pipimy.s3uploadImage" code:-1 userInfo:info];
        
        success(NO, err);
        
        return;
    }
    
    self.uploadImageCount              = files.count;
    self.uploadImageSueecssCount       = 0;
    self.imagesUploading               = YES;
    self.totalImageBytesSent           = 0.0;
    self.totalImageBytesExpectedToSend = 0.0;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    [files enumerateObjectsUsingBlock:^(NSDictionary *dic, NSUInteger idx, BOOL *stop)
    {
        
        //NSString *uploadName = [NSString stringWithFormat:@"%@-pic%@.jpg", productId,@"test"];
        
        NSString *uploadName = [NSString stringWithFormat:@"%@-pic%@.jpg", productId,[dic objectForKey:@"currentUpdate"]];
        
        NSString *fileName = [dic objectForKey:@"fileNameStr"];
        
        NSString *filePath   = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        
        self.totalImageBytesExpectedToSend += [[fileManager attributesOfItemAtPath:filePath error:nil]fileSize];
        
        AWSS3TransferManagerUploadRequest *request = [[AWSS3TransferManagerUploadRequest alloc]init];
        request.bucket      = S3_IMAGE_BUCKETNAME;
        request.key         = uploadName;
        request.ACL         = AWSS3ObjectCannedACLPublicRead;
        request.contentType = @"image/jpeg";
        request.body        = [NSURL fileURLWithPath:filePath];

        request.uploadProgress = ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
            
            self.totalImageBytesSent += (CGFloat)bytesSent;
            
            progress(_totalImageBytesSent / _totalImageBytesExpectedToSend);
        };
        
        [[_s3Manager upload:request]continueWithBlock:^id(BFTask *task) {
            
            if(task.error)
            {
                self.imagesUploading = NO;
                
                success(NO, task.error);
            }
            else
            {
                self.uploadImageSueecssCount++;
                
                if(_uploadImageSueecssCount == _uploadImageCount)
                {
                    self.imagesUploading = NO;
                    
                    progress(1.0);
                    success(YES, nil);
                }
            }
            
            return nil;
        }];
    }];    
}

#pragma mark - 上傳 User Icon
- (void)uploadUserIconWithUserId:(NSString *)userId filePath:(NSURL *)filePath success:(void (^)(BOOL))success
{
    NSString *uploadName = [NSString stringWithFormat:@"%@.jpg", userId];
    AWSS3TransferManagerUploadRequest *request = [[AWSS3TransferManagerUploadRequest alloc]init];
    request.bucket      = S3_ICON_BUCKETNAME;
    request.key         = uploadName;
    request.ACL         = AWSS3ObjectCannedACLPublicRead;
    request.contentType = @"image/jpeg";
    request.body        = filePath;

    [[_s3Manager upload:request]continueWithBlock:^id(BFTask *task) {
      
        success(!task.error);
        
        return nil;
    }];
}


#pragma mark - 上傳 User Background Image
- (void)uploadUserBackgroundWithUserId:(NSString *)userId filePath:(NSURL *)filePath success:(void (^)(BOOL))success
{
    NSString *uploadName = [NSString stringWithFormat:@"%@-bg.jpg", userId];
    AWSS3TransferManagerUploadRequest *request = [[AWSS3TransferManagerUploadRequest alloc]init];
    request.bucket      = S3_BACKGROUND_BUCKETNAME;
    request.key         = uploadName;
    request.ACL         = AWSS3ObjectCannedACLPublicRead;
    request.contentType = @"image/jpeg";
    request.body        = filePath;
    
    [[_s3Manager upload:request]continueWithBlock:^id(BFTask *task) {
        
        success(!task.error);
        
        return nil;
    }];
}

@end
