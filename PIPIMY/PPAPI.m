
#import "PPAPI.h"
#import "PPUser.h"
#import <Mantle.h>
#import "SVHTTPRequest.h"


@implementation PPAPI

static NSString *const PIPIMY_DEVELOPMENT_URL = @"http://54.169.1.132/1.1/";

static NSString *const PIPIMY_PRODUCT_URL = @"http://www.pipimy.net/1.1/";


#pragma mark - LifeCycle
+ (instancetype)singleton
{
    static PPAPI *_singleton;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        _singleton     = [[PPAPI alloc]init];
        
        // 開發環境 URL
        if(DEBUG)
        {
            _singleton.basePath = PIPIMY_DEVELOPMENT_URL;   //_singleton.basePath = @"http://54.169.1.132/1.1/";
            NSLog(@"PIPIMY_DEVELOPMENT_URL...");
        }
        // 正式環境 URL
        else
        {
            _singleton.basePath = PIPIMY_PRODUCT_URL;
            NSLog(@"PIPIMY_PRODUCT_URL...");
        }
    });
    
    return _singleton;
}

#pragma mark - 取得 default 商品列表
- (void)APIGetNearProduct:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"getNearProduct";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        NSArray *products;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                error = [self __errorForKey:response[@"result"] withAPI:path];
            }
            else
            {
                products = [MTLJSONAdapter modelsOfClass:[PPAPIGetNearProductResponse class]
                                           fromJSONArray:response
                                                   error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    error = [self __errorForKey:@"format" withAPI:path];
                }
                
                // API 回傳商品列表為空
                else if(!products.count)
                {
                    error = [self __errorForKey:@"noProduct" withAPI:path];
                }
            }
        }
        
        if(error)
        {
            [self __showAlertMessageWithError:error];
        }
        
        callback(products, error);
    }];
}


#pragma mark 取得 類別 商品列表

- (void)APIGetTypeProduct:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"getTypeProduct";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        NSArray *products;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                error = [self __errorForKey:response[@"result"] withAPI:path];
            }
            else
            {
                products = [MTLJSONAdapter modelsOfClass:[PPAPIGetNearProductResponse class]
                                           fromJSONArray:response
                                                   error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    error = [self __errorForKey:@"format" withAPI:path];
                }
                
                // API 回傳商品列表為空
                else if(!products.count)
                {
                    error = [self __errorForKey:@"noProduct" withAPI:path];
                }
            }
        }
        
        if(error)
        {
            [self __showAlertMessageWithError:error];
        }
        
        callback(products, error);
    }];
}


#pragma mark 取得 關鍵字 商品列表
- (void)APIGetSearchProduct:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"getSearchProduct";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        NSArray *products;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                error = [self __errorForKey:response[@"result"] withAPI:path];
            }
            else
            {
                products = [MTLJSONAdapter modelsOfClass:[PPAPIGetNearProductResponse class]
                                           fromJSONArray:response
                                                   error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    error = [self __errorForKey:@"format" withAPI:path];
                }
                
                // API 回傳商品列表為空
                else if(!products.count)
                {
                    error = [self __errorForKey:@"noProduct" withAPI:path];
                }
            }
        }
        
        if(error)
        {
            [self __showAlertMessageWithError:error];
        }
        
        callback(products, error);
    }];
}

#pragma mark 取得 單一 商品資訊
/* 取得單一商品資訊：目前單純適用於聊天室議價相關使用
   因為這支不同於取得商品列表，在於商品若改過價，則 call API 時 params 裡含的 productID 會是回傳時的 parentID，而回傳的 productID 是議價後的新商品 ID
   改價商品的判定值為 hide = 4 */
- (void)APIGetOneProduct:(NSDictionary *)params callback:(void (^)(PPAPIGetNearProductResponse *, NSError *))callback
{
    NSString *path = @"getOneProduct";
    
    [self cancelRequestsWithPath:path];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        PPAPIGetNearProductResponse *product;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            product = [MTLJSONAdapter modelOfClass:[PPAPIGetNearProductResponse class]
                                fromJSONDictionary:response
                                             error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        if(error)
        {
            [self __showAlertMessageWithError:error];
        }
        
        callback(product, error);
    }];
}


#pragma mark - 登入
- (void)APILogin:(NSDictionary *)params callback:(void (^)(PPAPILoginResponse *, NSError *))callback
{
    NSString *path = @"login";
    
    [self cancelRequestsWithPath:path];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
        
            PPAPILoginResponse *result = [MTLJSONAdapter modelOfClass:[PPAPILoginResponse class]
                                                   fromJSONDictionary:response
                                                                error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage]cookiesForURL:urlResponse.URL];
                
                if(cookies.count)
                {
                    [[PPUser singleton]updateCookies:cookies];
                }

                callback(result, nil);
            }
        }
    }];
}

#pragma mark - 註冊
- (void)APIRegister:(NSDictionary *)params callback:(void (^)(PPAPIRegisterResponse *, NSError *))callback
{
    NSString *path = @"register";
    
    [self cancelRequestsWithPath:path];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            PPAPIRegisterResponse *result = [MTLJSONAdapter modelOfClass:[PPAPIRegisterResponse class]
                                                      fromJSONDictionary:response
                                                                   error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                callback(result, nil);
            }
        }
    }];
}

#pragma mark - 確認ID是否註冊
- (void)APICheckID:(NSDictionary *)params callback:(void (^)(PPAPICheckIDResponse *, NSError *))callback
{
    //checkID.php
    NSString *path = @"checkID";
    
    [self cancelRequestsWithPath:path];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            PPAPICheckIDResponse *result = [MTLJSONAdapter modelOfClass:[PPAPICheckIDResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                callback(result, nil);
            }
        }
    }];
}

#pragma mark - 取回 ProductId
- (void)APIGetProductID:(void (^)(NSNumber *, NSError *))callback
{
    NSString *path = @"getProductID";
    
    [self cancelRequestsWithPath:path];
    [self GET:path parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        PPAPIGetProductIDResponse *result = [MTLJSONAdapter modelOfClass:[PPAPIGetProductIDResponse class]
                                                      fromJSONDictionary:response
                                                                   error:&error];
        callback(result.productId, error);
    }];
}

#pragma mark - 會員影片上傳完成
- (void)APIDoneVideoM:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"doneVideoM";
    
    [self cancelRequestsWithPath:path];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 會員圖片上傳完成
- (void)APIDonePicM:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"donePicM";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}


#pragma mark - 取回會員金物流設定
- (void)APIGetStoreFlow:(NSDictionary *)params callback:(void (^)(id jsonObject,NSError *err))callback
{
    NSString *path = @"trace/getStoreFlow";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];

        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil,err);
        }
        else
        {
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil,err);
            }

            if ([NSJSONSerialization isValidJSONObject:response]==YES)
            {
                callback(response,nil);
            }
            
        }
    }];
}


#pragma mark 設置會員金物流
- (void)APIUpdateStoreFlow:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"trace/updateStoreFlow";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

- (void)APIDoneTextM:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"doneTextM";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 取回店家資料
- (void)APIGetStore:(NSDictionary *)params callback:(void (^)(PPAPIGetStoreResponse *, NSError *))callback
{
    NSString *path = @"trace/getStore";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            PPAPIGetStoreResponse *result = [MTLJSONAdapter modelOfClass:[PPAPIGetStoreResponse class]
                                                      fromJSONDictionary:response
                                                                   error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                callback(result, nil);
            }
        }
    }];
}

#pragma mark -
- (void)APIGetMyPostProduct:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"getMyPostProduct";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
    
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                NSError *err = [self __errorForKey:response[@"result"] withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                NSArray *products = [MTLJSONAdapter modelsOfClass:[PPAPIGetNearProductResponse class]
                                                    fromJSONArray:response
                                                            error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    NSError *err = [self __errorForKey:@"format" withAPI:path];
                    
                    callback(nil, err);
                }
                
                // API 回傳商品列表為空
                else if(!products.count)
                {
                    NSError *err = [self __errorForKey:@"noProduct" withAPI:path];
                    
                    callback(nil, err);
                }
                else
                {
                    callback(products, nil);
                }
            }
        }
    }];
}

#pragma mark - 粉絲列表
- (void)APIListTraced:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"trace/listTraced";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                NSError *err = [self __errorForKey:response[@"result"] withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                NSArray *products = [MTLJSONAdapter modelsOfClass:[PPAPIListTracedResponse class]
                                                    fromJSONArray:response
                                                            error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    NSError *err = [self __errorForKey:@"format" withAPI:path];
                    
                    callback(nil, err);
                }
                else
                {
                    callback(products, nil);
                }
            }
        }
    }];
}

#pragma mark - 交易列表
- (void)APIGetMyDeal:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"getMyDeal";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
    
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                NSError *err = [self __errorForKey:response[@"result"] withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                NSArray *products = [MTLJSONAdapter modelsOfClass:[PPAPIGetMyDealResponse class]
                                                    fromJSONArray:response
                                                            error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    NSError *err = [self __errorForKey:@"format" withAPI:path];
                    
                    callback(nil, err);
                }
                else
                {
                    callback(products, nil);
                }
            }
        }
    }];
}

#pragma mark - 評分列表
- (void)APIGetSomeoneRating:(NSDictionary *)params callback:(void (^)(PPAPIGetSomeoneRatingResponse *, NSError *))callback
{
    NSString *path = @"getSomeoneRating";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                NSError *err = [self __errorForKey:response[@"result"] withAPI:path];
                
                callback(nil, err);
            }
            else if([response isKindOfClass:[NSArray class]])
            {
                // 這隻 API 比較特別
                // 返回一定要 Array
                // 且 lastObject 為 Detail class
                // 其餘 Object 才是 Rating class
                NSMutableArray *array = [NSMutableArray arrayWithArray:response];
                
                PPAPIGetSomeoneRatingResponse *item = [[PPAPIGetSomeoneRatingResponse alloc]init];
                
                item.detail = [MTLJSONAdapter modelOfClass:[RatingDetail class] fromJSONDictionary:array.lastObject error:nil];
                
                if(array.count > 1)
                {
                    [array removeLastObject];
                    
                    item.ratings = [MTLJSONAdapter modelsOfClass:[Rating class] fromJSONArray:array error:nil];
                }
                
                callback(item, nil);
            }
            else
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
        }
    }];
}

#pragma mark - 修改密碼
- (void)APIUpdatePWD:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"updatePWD";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 更新小店位置
- (void)APIUpdateStoreLoc:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"trace/updateStoreLoc";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
    
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 跟新小店資料
- (void)APIUpdateStore:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"trace/updateStore";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 刪除商品
- (void)APIGetMyProductDelete:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"getMyProductDelete";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 修改商品
- (void)APIGetMyProductUpdate:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"getMyProductUpdate";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 某商品評價
- (void)APIGetProductRating:(NSDictionary *)params callback:(void (^)(PPAPIGetSomeoneRatingResponse *, NSError *))callback
{
    NSString *path = @"getProductRating";
    
    [self cancelRequestsWithPath:path];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        
        PPAPIGetSomeoneRatingResponse *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                error = [self __errorForKey:response[@"result"] withAPI:path];
            }
            else if([response isKindOfClass:[NSArray class]])
            {
                // 這隻 API 比較特別
                // 返回一定要 Array
                // 且 lastObject 為 Detail class
                // 其餘 Object 才是 Rating class
                NSMutableArray *array = [NSMutableArray arrayWithArray:response];
                
                result = [[PPAPIGetSomeoneRatingResponse alloc]init];
                
                result.detail = [MTLJSONAdapter modelOfClass:[RatingDetail class] fromJSONDictionary:array.lastObject error:nil];
                
                if(array.count > 1)
                {
                    [array removeLastObject];
                    
                    result.ratings = [MTLJSONAdapter modelsOfClass:[Rating class] fromJSONArray:array error:nil];
                }
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
}

#pragma mark - 購物車
#pragma mark 商品加入購物車

- (void)APIAddToCart:(NSDictionary *)params callback:(void(^)(NSError *err))callback {
    NSString *path = @"cart/add";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            // note: 可能回傳 statusCode 400，result = cannotbuyselfProduct (自己的商品) 或 productError (商品不存在)
            // note: 可能回傳 statusCode 200，result = fail, msg = hasItem (已加入)
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark 列出購物車內商品

- (void)APICartList:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"cart/list";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        
        NSArray *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            if([response isKindOfClass:[NSArray class]])
            {
                result = [MTLJSONAdapter modelsOfClass:[PPAPICartListResponse class] fromJSONArray:response error:&error];
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
}
#pragma mark 移除購物車內商品

- (void)APICartDelete:(NSDictionary *)params callback:(void(^)(NSError *err))callback
{
    NSString *path = @"cart/delete";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 訂單

#pragma mark 取得超商列表

- (void)APIAllpayGetStoreInfo:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    // 暫時改變 basePath
    NSString *basePath = self.basePath;
    self.basePath = @"";
    
    // Test environment URL
    NSString *path = @"http://logistics-stage.allpay.com.tw/Helper/GetStoreInfo";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:@"allpayGetStoreInfo"];
            
            callback(nil, err);
        }
        else
        {
            NSArray *result;
            
            if (!error)
            {
                id json = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&error];
                if ([json isKindOfClass:[NSArray class]])
                {
                    result = [MTLJSONAdapter modelsOfClass:[PPAPIAllpayGetStoreInfoResponse class] fromJSONArray:json error:&error];
                }
            }
            
            callback(result, error);
        }
    }];
    
    // 回復 basePath
    self.basePath = basePath;
}

#pragma mark 檢查費用

- (void)APICartCheckPay:(NSDictionary *)params callback:(void (^)(PPAPICartCheckPayResponse *response, NSError *err))callback
{
    NSString *path = @"cart/checkpay";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if (urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
            
        }
        else
        {
            PPAPICartCheckPayResponse *result = [MTLJSONAdapter modelOfClass:[PPAPICartCheckPayResponse class]
                                                          fromJSONDictionary:response
                                                                       error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
            
            // API 回傳狀態非 success
            else if (![result.result isEqualToString:@"success"])
            {
                NSMutableString *errorString = [NSMutableString string];
                for (NSString *entry in [response allKeys])
                {
                    [errorString appendString:@"\n"];
                    [errorString appendString:[NSString stringWithFormat:@"%@: %@", entry, response[entry]]];
                }
                NSError *err = [self __errorForKey:errorString withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                callback(result, nil);
            }
        }
    }];
}

#pragma mark 結帳

- (void)APICartPay:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"cart/pay";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if (urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
            
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSMutableString *errorString = [NSMutableString string];
                for (NSString *entry in [response allKeys])
                {
                    [errorString appendString:@"\n"];
                    [errorString appendString:[NSString stringWithFormat:@"%@: %@", entry, response[entry]]];
                }
                NSError *err = [self __errorForKey:errorString withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark OTP 驗證

- (void)APIVerifyOTP:(NSDictionary *)params callback:(void (^)(NSError *))callback
{
    NSString *path = @"cart/verifyOTP";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark 賣家輸入自寄物流單號

- (void)APIOrderSetOtherLogistics:(NSDictionary *)params callback:(void (^)(NSError *err))callback
{
    NSString *path = @"order/setOtherLogistics";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark 買家確認收貨

- (void)APIConfirmOrderFinish:(NSDictionary *)params callback:(void (^)(PPAPIConfirmOrderFinishResponse *response, NSError *))callback
{
    NSString *path = @"order/confirmOrderFinish";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil, err);
        }
        else
        {
            PPAPIConfirmOrderFinishResponse *result = [MTLJSONAdapter modelOfClass:[PPAPIConfirmOrderFinishResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil, err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(nil, err);
            }
            else
            {
                callback(result, nil);
            }
        }
    }];
}

#pragma mark 取得訂單列表 (買家)
- (void)APIGetOrderBuyerList:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"order/buyer-list";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        [self __hideHud];
        
        NSArray *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            if([response isKindOfClass:[NSArray class]])
            {
                result = [MTLJSONAdapter modelsOfClass:[PPAPIGetOrderListResponse class] fromJSONArray:response error:&error];
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
}

#pragma mark 取得訂單列表 (賣家)
- (void)APIGetOrderSellerList:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"order/seller-list";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        [self __hideHud];
        
        NSArray *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            if([response isKindOfClass:[NSArray class]])
            {
                result = [MTLJSONAdapter modelsOfClass:[PPAPIGetOrderListResponse class] fromJSONArray:response error:&error];
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
}

#pragma mark 取得訂單整合明細

- (void)APIGetOrderResource:(NSDictionary *)params callback:(void (^)(PPAPIGetOrderResourceResponse *orderResource, NSError *err))callback
{
    NSString *path = @"order/resource";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        PPAPIGetOrderResourceResponse *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else if (![response isKindOfClass:[NSDictionary class]])
        {
            error = [self __errorForKey:@"format" withAPI:path];
        }
        else
        {
            if ([response objectForKey:@"order"] == nil || [response objectForKey:@"items"] == nil || [response objectForKey:@"payment"] == nil || [response objectForKey:@"logistics"] == nil )
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
            else
            {
                result = [MTLJSONAdapter modelOfClass:[PPAPIGetOrderResourceResponse class] fromJSONDictionary:response error:nil];
            }
        }
        
        callback(result, error);
    }];
}

#pragma mark 買家評論訂單

- (void)APIPushOrderRating:(NSDictionary *)params callback:(void (^)(NSError *err))callback
{
    NSString *path = @"rating/pushOrderRating";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 聊天
#pragma mark 取得聊天列表

- (void)APIGetChatIndex:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"push/getChatIndex";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        NSArray *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            if([response isKindOfClass:[NSArray class]])
            {
                result = [MTLJSONAdapter modelsOfClass:[PPAPIGetChatIndexResponse class] fromJSONArray:response error:&error];
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
}

#pragma mark 清除未讀數字 (並取得聊天列表)

- (void)APICleanChatBadge:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"push/cleanChatBadge";
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        NSArray *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            if([response isKindOfClass:[NSArray class]])
            {
                result = [MTLJSONAdapter modelsOfClass:[PPAPIGetChatIndexResponse class] fromJSONArray:response error:&error];
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
    
}


#pragma mark 刪除聊天

- (void)APIDeleteChatIndex:(NSDictionary *)params callback:(void(^)(NSError *err))callback
{
    NSString *path = @"push/deleteChatIndex";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}


#pragma mark 取得聊天內容

- (void)APIGetChatContent:(NSDictionary *)params callback:(void(^)(NSArray *indices, NSError *err))callback
{
    NSString *path = @"push/getChatContent";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {

        [self __hideHud];
        
        NSArray *result;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            if([response isKindOfClass:[NSArray class]])
            {
                result = [MTLJSONAdapter modelsOfClass:[PPAPIGetChatContentResponse class] fromJSONArray:response error:&error];
            }
            else
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        callback(result, error);
    }];
}


#pragma mark 送出聊天內容

- (void)APISendChatContent:(NSDictionary *)params callback:(void(^)(NSError *err))callback
{
    // Using developer path
    NSString *path = @"push/sendChatContentDev";
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark 開始聊天

- (void)APIStartChatContent:(NSDictionary *)params callback:(void(^)(NSError *err))callback
{
    NSString *path = @"push/startChatContentDev";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark 賣家發送推播給粉絲
- (void)APITraceBroadcast:(NSDictionary *)params callback:(void(^)(NSError *err))callback
{
    NSString *path = @"trace/broadcastDev";
    
    [self cancelRequestsWithPath:path];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}


#pragma mark - 賣家設定議價商品
- (void)APIBargainProduct:(NSDictionary *)params callback:(void(^)(NSError *err))callback
{
    NSString *path = @"BargainProduct";
    
    [self cancelRequestsWithPath:path];
    
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(err);
        }
        else
        {
            PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                     fromJSONDictionary:response
                                                                  error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(err);
            }
            
            // API 回傳狀態非 success
            else if(!result.success)
            {
                NSError *err = [self __errorForKey:result.result withAPI:path];
                
                callback(err);
            }
            else
            {
                callback(nil);
            }
        }
    }];
}

#pragma mark - 某人商品列表
- (void)APIGetSomeoneProduct:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"getSomeoneProduct";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        NSArray *products;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            // 不該出現 NSDictionary 的
            if([response isKindOfClass:[NSDictionary class]])
            {
                error = [self __errorForKey:response[@"result"] withAPI:path];
            }
            else
            {
                products = [MTLJSONAdapter modelsOfClass:[PPAPIGetNearProductResponse class]
                                           fromJSONArray:response
                                                   error:&error];
                
                // API 回傳格式錯誤
                if(error)
                {
                    error = [self __errorForKey:@"format" withAPI:path];
                }
                
                // API 回傳商品列表為空
                else if(!products.count)
                {
                    error = [self __errorForKey:@"noProduct" withAPI:path];
                }
            }
        }
        
        callback(products, error);
    }];
}

#pragma mark - 是否關注某賣家
- (void)APICheckTrace:(NSDictionary *)params callback:(void (^)(BOOL))callback
{
    NSString *path = @"trace/checkTrace";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
       
        [self __hideHud];
        
        BOOL result = [response isKindOfClass:[NSDictionary class]] && [response[@"result"]isEqualToString:@"alreadyTrace"];
        
        callback(result);
    }];
}

#pragma mark - 關注某賣家
- (void)APIAddTrace:(NSDictionary *)params callback:(void (^)(BOOL))callback
{
    NSString *path = @"trace/addTrace";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                 fromJSONDictionary:response
                                                              error:&error];
        
        callback(result.success);
    }];
}

#pragma mark - 刪除某關注
- (void)APIDeleteTrace:(NSDictionary *)params callback:(void (^)(BOOL))callback
{
    NSString *path = @"trace/deleteTrace";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                 fromJSONDictionary:response
                                                              error:&error];
        
        callback(result.success);
    }];
}

#pragma mark - 我關注的列表
- (void)APIListTracing:(NSDictionary *)params callback:(void (^)(NSArray *, NSError *))callback
{
    NSString *path = @"trace/listTracing";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self GET:path parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        NSArray *result = [MTLJSONAdapter modelsOfClass:[PPAPIListTracingResponse class]
                                          fromJSONArray:response
                                                  error:&error];
        
        callback(result, error);
    }];
}

#pragma mark - 切換關注賣家的推播接收設定
- (void)APITracePushTrigger:(NSDictionary *)params callback:(void (^)(PPAPITracePushTriggerResponse *response, NSError *err))callback
{
    NSString *path = @"trace/pushTrigger";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        PPAPITracePushTriggerResponse *triggerResponse;
        
        if(urlResponse.statusCode != 200)
        {
            error = [self __errorForKey:@"404" withAPI:path];
        }
        else
        {
            triggerResponse = [MTLJSONAdapter modelOfClass:[PPAPITracePushTriggerResponse class]
                                fromJSONDictionary:response
                                             error:&error];
            
            // API 回傳格式錯誤
            if(error)
            {
                error = [self __errorForKey:@"format" withAPI:path];
            }
        }
        
        if(error)
        {
            [self __showAlertMessageWithError:error];
        }
        
        callback(triggerResponse, error);
    }];
}

- (void)APILogout:(NSDictionary *)params callback:(void (^)(BOOL))callback
{
    NSString *path = @"logout";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        PPAPISuccessResponse *result = [MTLJSONAdapter modelOfClass:[PPAPISuccessResponse class]
                                                 fromJSONDictionary:response
                                                              error:nil];
        
        callback(result.success);
    }];
}

- (void)APIReportProduct:(NSDictionary *)params callback:(void (^)(id jsonObject,NSError *err))callback
{
    NSString *path = @"reportProduct";
    
    [self cancelRequestsWithPath:path];
    [self __showHud];
    [self POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        [self __hideHud];
        
        if(urlResponse.statusCode != 200)
        {
            NSError *err = [self __errorForKey:@"404" withAPI:path];
            
            callback(nil,err);
        }
        else
        {
            // API 回傳格式錯誤
            if(error)
            {
                NSError *err = [self __errorForKey:@"format" withAPI:path];
                
                callback(nil,err);
            }
            
            if ([NSJSONSerialization isValidJSONObject:response]==YES)
            {
                callback(response,nil);
            }
            
        }
    }];
}

#pragma mark - 返回對應的 NSError
- (NSError *)__errorForKey:(NSString *)key withAPI:(NSString *)api
{
    NSString *errorDesc = [NSString stringWithFormat:@"發生未知錯誤：%@", key];
    
    if([key isEqualToString:@"404"])
    {
        errorDesc = @"連線失敗, 請稍候再試";
    }
    else if([key isEqualToString:@"empty"])
    {
        errorDesc = @"參數錯誤";
    }
    else if([key isEqualToString:@"fail"])
    {
        errorDesc = @"執行失敗";
    }
    else if([key isEqualToString:@"format"])
    {
        errorDesc = @"回傳格式錯誤";
    }
    else if([key isEqualToString:@"noProduct"])
    {
        errorDesc = @"查無相關商品";
    }
    else if([key isEqualToString:@"cookieError"])
    {
        errorDesc = @"登入時間已過期";
    }
    else if([key isEqualToString:@"specialWords"])
    {
        errorDesc = @"不可包含特殊字元";
    }
    
    NSDictionary *info = @{NSLocalizedDescriptionKey : errorDesc};

    return [NSError errorWithDomain:api code:-1 userInfo:info];
}

#pragma mark - 顯示錯誤訊息視窗
- (void)__showAlertMessageWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:error.localizedDescription
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

#pragma mark - 顯示 Hud
- (void)__showHud
{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    
    if(!window)
    {
        window = [[[UIApplication sharedApplication]windows]lastObject];
    }
    
    [MBProgressHUD showHUDAddedTo:window.rootViewController.view animated:YES];
}

#pragma mark - 隱藏 Hud
- (void)__hideHud
{
    UIWindow *window = [[UIApplication sharedApplication]keyWindow];
    
    if(!window)
    {
        window = [[[UIApplication sharedApplication]windows]lastObject];
    }
    
    [MBProgressHUD hideAllHUDsForView:window.rootViewController.view animated:YES];
}

@end
