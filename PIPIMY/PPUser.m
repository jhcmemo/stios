
#import "PPAPI.h"
#import "PPUser.h"

NSString * const USER_LOCATION_UPDATED = @"user_location_updated";


@interface PPUser ()<CLLocationManagerDelegate>

@property (nonatomic, strong) NSArray *cookies;
@property (nonatomic, strong) CLLocationManager *locationManager; // 定位

@end


@implementation PPUser

#pragma mark - LifeCycle
+ (instancetype)singleton
{
    static PPUser *_singleton;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
        if([userDefault objectForKey:@"kUser"])
        {
            id data = [userDefault objectForKey:@"kUser"];
            _singleton = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        }
        else
        {
            _singleton = [[PPUser alloc]init];
        }
        
        [_singleton __setup];
    });
    
    return _singleton;
}

#pragma mark - decode
- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    
    if(self)
    {
        _login         = [[coder decodeObjectForKey:@"login"]boolValue];
        _phone         = [coder decodeObjectForKey:@"phone"];
        _name          = [coder decodeObjectForKey:@"name"];
        _Id            = [coder decodeObjectForKey:@"Id"];
        _cookies       = [coder decodeObjectForKey:@"cookies"];
        _deviceToken   = [coder decodeObjectForKey:@"deviceToken"];
        _storeCash     = [coder decodeObjectForKey:@"storeCash"];
        _storeDelivery = [coder decodeObjectForKey:@"storeDelivery"];
        
        NSArray *favorites = [coder decodeObjectForKey:@"favorites"];
        
        if(favorites)
        {
            _favorites = [NSMutableArray arrayWithArray:favorites];
        }
        
        if(_cookies)
        {
            [self __addCookies];
        }
    }
    
    return self;
}

#pragma mark - encode
- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:@(_login) forKey:@"login"];
    
    if(_name)
    {
        [coder encodeObject:_name forKey:@"name"];
    }
    
    if(_phone)
    {
        [coder encodeObject:_phone forKey:@"phone"];
    }
    
    if(_Id)
    {
        [coder encodeObject:_name forKey:@"Id"];
    }
    
    if(_cookies.count)
    {
        [coder encodeObject:_cookies forKey:@"cookies"];
    }
    
    if(_deviceToken)
    {
        [coder encodeObject:_deviceToken forKey:@"deviceToken"];
    }
    
    if(_storeCash)
    {
        [coder encodeObject:_storeCash forKey:@"storeCash"];
    }
    
    if(_storeDelivery)
    {
        [coder encodeObject:_storeDelivery forKey:@"storeDelivery"];
    }
    
    if(_favorites.count)
    {
        [coder encodeObject:_favorites forKey:@"favorites"];
    }
}

#pragma mark - Properties Getter
- (BOOL)storeSetting
{
    return (_storeCash != nil && _storeDelivery != nil);
}

- (CLLocation *)location
{
    if(!_locationManager.location)
    {
        // Default 設在南軟捷運站
        CLLocation *defaultLocation = [[CLLocation alloc]initWithLatitude:25.0524319 longitude:121.6062806];
        
        return defaultLocation;
    }
    
    return _locationManager.location;
}

#pragma mark - Porperties Setter
- (void)setDeviceToken:(NSString *)deviceToken
{
    _deviceToken = deviceToken;
    
    [self __sendDeviceTokenToServer];
}

#pragma mark - 登入
- (void)login:(PPAPILoginResponse *)login
{
    _login = YES;
    _name  = login.ID;
    _phone = login.mobile;
    _Id    = login.Id;
    
    [self __save];
    [self __sendDeviceTokenToServer];
}

#pragma mark - 登出
- (void)logout
{
    _login         = NO;
    _name          = nil;
    _phone         = nil;
    _Id            = nil;
    _storeCash     = nil;
    _storeDelivery = nil;
    
    [self __removeCookies];
    [self __save];
}

#pragma mark - 更新 cookie
- (void)updateCookies:(NSArray *)cookies
{
    self.cookies = cookies;
    
    [self __save];
    [self __addCookies];
}

#pragma mark - 更新 deviceToken
- (void)updateDeviceToken:(NSString *)deviceToken
{
    _deviceToken = deviceToken;
    
    [self __save];
    [self __sendDeviceTokenToServer];
}

#pragma mark - 更新金物流
- (void)updateStroeCash:(NSNumber *)cash delivery:(NSNumber *)delivery
{
    _storeCash     = cash;
    _storeDelivery = delivery;
    [self __save];
}

- (void)updateStroeCash:(NSNumber *)cash
{
    _storeCash     = cash;
    [self __save];
}

- (void)updateStroeDelivery:(NSNumber *)delivery
{
    _storeDelivery = delivery;
    [self __save];
}

#pragma mark - 更新的最愛
- (void)updateFavorites:(PPAPIGetNearProductResponse *)product
{
    if(![self isFavoriteWithProduct:product])
    {
        [_favorites addObject:product];
    }
    else
    {
        [_favorites enumerateObjectsUsingBlock:^(PPAPIGetNearProductResponse *obj, NSUInteger idx, BOOL *stop) {
            
            if([obj.productId integerValue] == [product.productId integerValue])
            {
                [_favorites removeObject:obj];
                
                *stop = YES;
            }
        }];
    }
    
    [self __save];
}

#pragma mark - 是否加入最愛
- (BOOL)isFavoriteWithProduct:(PPAPIGetNearProductResponse *)product
{
    return [[_favorites valueForKey:@"productId"]containsObject:product.productId];
}

#pragma mark - 更新註冊資訊
- (void)updateRigisterInfo:(NSDictionary *)info
{
    _name  = info[@"name"];
    _Id    = info[@"Id"];
    _phone = info[@"phone"];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [[NSNotificationCenter defaultCenter]postNotificationName:USER_LOCATION_UPDATED object:nil];
}

#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    if(!_favorites)
    {
        _favorites = [NSMutableArray array];
    }
    
    _locationManager = [[CLLocationManager alloc]init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = 100.0f;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    // for iOS 8
    if([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [_locationManager requestWhenInUseAuthorization];
    }
    
    [_locationManager startUpdatingLocation];
    
    [self __sendDeviceTokenToServer];
}

#pragma mark - 儲存
- (void)__save
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:@"kUser"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma mark - 新增 Cookie
- (void)__addCookies
{
    for(NSHTTPCookie *cookie in _cookies)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage]setCookie:cookie];
    }
}

#pragma mark - 移除 Cookie
- (void)__removeCookies
{
    for(NSHTTPCookie *cookie in _cookies)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage]deleteCookie:cookie];
    }
    
    self.cookies = nil;
}

#pragma mark - 送出 device token 到 server
- (void)__sendDeviceTokenToServer
{
    if(!_login || !_deviceToken.length)
    {
        return;
    }

    NSString *path       = @"push/updateIOSToken";
    NSDictionary *params = @{@"Token" : _deviceToken};
    
    [[PPAPI singleton]POST:path parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error) {
        
    }];
}

@end
