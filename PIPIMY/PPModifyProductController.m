
#import "PPAPI.h"
#import "PPUser.h"
#import "SRPAlertView.h"
#import "SRPActionSheet.h"
#import "PPModifyProductController.h"


@interface PPModifyProductController ()

@property (nonatomic, strong) NSNumber *lat; // 商品經度
@property (nonatomic, strong) NSNumber *lng; // 商品緯度

@property (nonatomic, weak) IBOutlet UITextField *titleFiled; // 商品 Title
@property (nonatomic, weak) IBOutlet UITextField *priceFiled; // 商品價格
@property (nonatomic, weak) IBOutlet UIButton    *typeButton; // 商品類型
@property (nonatomic, weak) IBOutlet UITextView  *desTextView;// 商品描述

@end


@implementation PPModifyProductController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

#pragma mark - IBAction

- (IBAction)updateButtonClicked:(id)sender
{
    if(!_titleFiled.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                       message:@"請輸入 Title"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    if(!_priceFiled.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                       message:@"請輸入價格"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    [self __callAPIGetMyProductUpdate];
}

- (IBAction)deleteButtonClicked:(id)sender
{
    NSDictionary *params = @{@"ProductID" : _product.productId};
    
    [[PPAPI singleton]APIGetMyProductDelete:params callback:^(NSError *err) {
        
        if(!err)
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"成功"
                                                                 message:@"已刪除此商品"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
               
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        else
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
    }];
}

- (IBAction)updateLocationItemClicked:(id)sender
{
    self.lat = @([PPUser singleton].location.coordinate.latitude);
    self.lng = @([PPUser singleton].location.coordinate.longitude);
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"成功"
                                                   message:@"商品位置更新成功\n將在送出後更新"
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (IBAction)typeButtonDidClicked:(UIButton *)sender
{
    SRPActionSheet *aSheet = [[SRPActionSheet alloc]init];
    NSArray *types         = [self __productTypesWithUnknow:NO];
    
    [types enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        
        [aSheet addButtonWithTitle:title];
    }];
    
    aSheet.cancelButtonIndex = [aSheet addButtonWithTitle:@"取消"];
    
    [aSheet showFromTabBar:self.tabBarController.tabBar callback:^(UIActionSheet *sheet, NSInteger buttonIndex) {
       
        if(buttonIndex != sheet.cancelButtonIndex)
        {
            [sender setTitle:[sheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark - Private methods

- (void)__setup
{
    _lat              = _product.lat;
    _lng              = _product.lng;
    _titleFiled.text  = _product.title;
    _priceFiled.text  = [NSString stringWithFormat:@"%@", _product.price];
    _desTextView.text = _product.des;
    NSInteger index   = [_product.type integerValue];
    NSArray *types    = [self __productTypesWithUnknow:YES];

    [_typeButton setTitle:types[index] forState:UIControlStateNormal];
}

- (void)__callAPIGetMyProductUpdate
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"ProductID"] = _product.productId;
    params[@"Title"]     = _titleFiled.text;
    params[@"Price"]     = @([_priceFiled.text integerValue]); // 因為 User 可能輸入 0100 這種鬼東西
    params[@"CC"]        = @"TW";
    params[@"Lat"]       = _lat;
    params[@"Lng"]       = _lng;
    params[@"MemberID"]  = [PPUser singleton].name;
    
    params[@"Type"] = ({
        NSNumber *type = @(0);
        NSArray *types          = [self __productTypesWithUnknow:YES];
        type = @([types indexOfObject:[_typeButton titleForState:UIControlStateNormal]]);
    });
    
    if(_desTextView.text.length)
    {
        params[@"Des"] = _desTextView.text;
    }
    
    [[PPAPI singleton]APIGetMyProductUpdate:params callback:^(NSError *err) {
        
        if(!err)
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"成功"
                                                                 message:@"商品更新成功"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
               
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        else
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (NSArray *)__productTypesWithUnknow:(BOOL)flag
{
    if(flag)
    {
        return @[@"未知", @"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
    }
    
    return @[@"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}

@end
