
#import <UIKit/UIKit.h>
@class PPAPIGetNearProductResponse;

/**
 *  修改商品
 */
@interface PPModifyProductController : UIViewController



/**
 *  要修改的商品
 */
@property (nonatomic, weak) PPAPIGetNearProductResponse *product;

@end
