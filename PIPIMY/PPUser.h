
@import CoreLocation;
#import "PPAPILoginResponse.h"
#import <Foundation/Foundation.h>
#import "PPAPIGetNearProductResponse.h"


extern NSString * const USER_LOCATION_UPDATED; // User 位置更新


/**
 *  User 物件
 */
@interface PPUser : NSObject




/**
 *  是否登入
 */
@property (nonatomic, readonly, getter=isLogin) BOOL login;

/**
 *  使用者名稱
 */
@property (nonatomic, readonly) NSString *name;

/**
 *  使用者電話
 */
@property (nonatomic, readonly) NSString *phone;

/**
 *  使用者 Id
 */
@property (nonatomic, readonly) NSNumber *Id;

/**
 *  iOS Device Token
 */
@property (nonatomic, readonly) NSString *deviceToken;

/**
 *  金流方式
 */
@property (nonatomic, readonly) NSNumber *storeCash;

/**
 *  物流方式
 */
@property (nonatomic, readonly) NSNumber *storeDelivery;

/**
 *  是否設定金物流
 */
@property (nonatomic, readonly) BOOL storeSetting;

/**
 *  User 位置
 */
@property (nonatomic, readonly) CLLocation *location;

/**
 *  我的最愛
 */
@property (nonatomic, readonly) NSMutableArray *favorites;



/**
 *  返回 Singleton User 物件
 *
 *  @return 返回 Singleton User 物件
 */
+ (instancetype)singleton;

/**
 *  登入
 *
 *  @param login API 返回的登入資訊
 */
- (void)login:(PPAPILoginResponse *)login;

/**
 *  登出
 */
- (void)logout;

/**
 *  設置 cookie
 *
 *  @param cookies cookie
 */
- (void)updateCookies:(NSArray *)cookies;

/**
 *  更新 device token
 *
 *  @param deviceToken 新的 token
 */
- (void)updateDeviceToken:(NSString *)deviceToken;

/**
 *  設置金物流方法
 *
 *  @param cash     金流
 *  @param delivery 物流
 */
- (void)updateStroeCash:(NSNumber *)cash delivery:(NSNumber *)delivery;
- (void)updateStroeCash:(NSNumber *)cash;
- (void)updateStroeDelivery:(NSNumber *)delivery;

/**
 *  更新我的最愛
 *
 *  @param product 需要判斷的商品
 */
- (void)updateFavorites:(PPAPIGetNearProductResponse *)product;

/**
 *  返回是否加入最愛
 *
 *  @param product 判斷的商品
 *
 *  @return 返回是否加入最愛
 */
- (BOOL)isFavoriteWithProduct:(PPAPIGetNearProductResponse *)product;

/**
 *  更新註冊資訊
 *
 *  @param info 註冊資訊
 */
- (void)updateRigisterInfo:(NSDictionary *)info;

@end
