
#import "PPAPI.h"
#import "PPUser.h"
#import "SRPActionSheet.h"
#import "PPRegisterController.h"
#import "PPAPICheckIDResponse.h"


@interface PPRegisterControllerTextField : UITextField
@end


@implementation PPRegisterControllerTextField

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10.0f, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

@end


@interface PPRegisterController()<UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UILabel *checkIDMesssage;  //顯示文字，確認ID是否註冊

@property (nonatomic, weak) IBOutlet UITextField *account;      // 帳號
@property (nonatomic, weak) IBOutlet UITextField *phone;        // 電話
@property (nonatomic, weak) IBOutlet UITextField *password;     // 密碼
@property (nonatomic, weak) IBOutlet UITextField *rePassword;   // 確認密碼
@property (nonatomic, weak) IBOutlet UIButton *registerButton;  // 註冊

@end


@implementation PPRegisterController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

#pragma mark - IBAction
#pragma mark - 送出
- (IBAction)registerButtonDidClicked:(id)sender
{
    if(![PPUser singleton].location)
    {
        // 因為需要上傳城市, 所以必須要有定位
        [self __showAlertViewWithMessage:@"請檢查定位狀態"];
    }
    else if([self __infomationCorrect])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        [geocoder reverseGeocodeLocation:[PPUser singleton].location completionHandler:^ (NSArray *placemarks, NSError *error) {
             
             CLPlacemark *mark = placemarks.firstObject;
            
            // 目前 App 只在台灣, 台灣要取 city 大概就是用 subAdministrativeArea
             if(mark.subAdministrativeArea.length)
             {
                 NSString *city = mark.subAdministrativeArea;
                 
                 [self __callAPIRegisterWithCity:city];
             }
             else
             {
                 [self __showAlertViewWithMessage:@"請檢查定位狀態"];
             }
         }];
    }
}

#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    _account.layer.cornerRadius        = 4.0;
    _account.layer.borderColor         = [UIColor lightGrayColor].CGColor;
    _account.layer.borderWidth         = 1.0;
    _phone.layer.cornerRadius          = 4.0;
    _phone.layer.borderColor           = [UIColor lightGrayColor].CGColor;
    _phone.layer.borderWidth           = 1.0;
    _password.layer.cornerRadius       = 4.0;
    _password.layer.borderColor        = [UIColor lightGrayColor].CGColor;
    _password.layer.borderWidth        = 1.0;
    _rePassword.layer.cornerRadius     = 4.0;
    _rePassword.layer.borderColor      = [UIColor lightGrayColor].CGColor;
    _rePassword.layer.borderWidth      = 1.0;
    _registerButton.layer.cornerRadius = 4.0;
    
    
    _checkIDMesssage.textColor = [UIColor blackColor];
    _checkIDMesssage.text = @"";
    
    
    //設定UITextFieldTextDidChangeNotification通知事件
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldChange:) name:UITextFieldTextDidChangeNotification object:nil];
    //註冊UIControlEventValueChanged事件
    [self.account addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Call API 註冊
- (void)__callAPIRegisterWithCity:(NSString *)city
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    NSData *toData;
    
    // Id, mobile, pwd 需轉成 base64 string
    NSString *Id     = _account.text;
    NSString *mobile = _phone.text;
    NSLog(@"mobile : %@", mobile);  //ivankatest
    NSString *pwd    = _password.text;
    toData           = [Id dataUsingEncoding:NSUTF8StringEncoding];
    Id               = [toData base64EncodedStringWithOptions:0];
    toData           = [mobile dataUsingEncoding:NSUTF8StringEncoding];
    mobile           = [toData base64EncodedStringWithOptions:0];
    toData           = [pwd dataUsingEncoding:NSUTF8StringEncoding];
    pwd              = [toData base64EncodedStringWithOptions:0];
    
    
    NSDictionary *params = @{@"ID"      : Id,
                             @"Mobile"  : mobile,
                             @"Pwd"     : pwd,
                             @"City"    : city
                             };

    [[PPAPI singleton]APIRegister:params callback:^(PPAPIRegisterResponse *result, NSError *err) {
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            NSDictionary *info = @{@"name" : _account.text,
                                   @"Id"   : result.pipimy_id,
                                   @"phoe" : _phone.text
                                   };
            NSLog(@"_phone.text : %@", _phone.text);    //ivankatest
            NSLog(@"info : %@", info);
            [[PPUser singleton]updateRigisterInfo:info];
            [self performSegueWithIdentifier:@"toPPAllPayWebController" sender:nil];
        }
    }];
}

#pragma mark - Call API 確認ID是否註冊
- (void)__callAPICheckID
{
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    NSData *toData;
    
    // Id 需轉成 base64 string
    NSString *Id     = _account.text;
    toData           = [Id dataUsingEncoding:NSUTF8StringEncoding];
    Id               = [toData base64EncodedStringWithOptions:0];
    
    NSDictionary *params = @{@"ID"      : Id,
                             };
    
    [[PPAPI singleton]APICheckID:params callback:^(PPAPICheckIDResponse *result, NSError *err) {
        
        [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
        
        if(err)
        {
            //sameID
            _checkIDMesssage.textColor = [UIColor redColor];
            _checkIDMesssage.text = @"您使用的ID已被註冊，請重新更換";    //_checkIDMesssage.text = (NSString*)err.localizedDescription;
            NSLog(@"checkID success is : %@", (result.success) ? @"YES" : @"NO");
        }
        else
        {
            //success ID
            _checkIDMesssage.textColor = [UIColor blackColor];
            _checkIDMesssage.text = @"可以使用的ID";
        }
        
    }];
}


#pragma mark - ID欄位，檢查確認ID是否註冊
- (void)textFieldChange:(NSNotification*)notifice
{
    UITextField *field=[notifice object];
    NSLog(@"text=%@",field.text);
    
    [self __callAPICheckID];
}


#pragma mark - 檢查輸入資料
- (BOOL)__infomationCorrect
{
    if(!_account.text.length)
    {
        
        [self __showAlertViewWithMessage:@"ID不能空白"];
        return NO;
    }
    else if(_account.text.length <= 3)
    {
        [self __showAlertViewWithMessage:@"ID不可小於3個字"];
        return NO;
    }
    else if(_account.text.length >= 13)
    {
        [self __showAlertViewWithMessage:@"ID不可大於13個字"];
        return NO;
    }
    else if(!_phone.text.length)
    {
        [self __showAlertViewWithMessage:@"請輸入電話"];
        
        return NO;
    }
    else if(![self __validPhoneNumber])
    {
        [self __showAlertViewWithMessage:@"電話格式錯誤"];
        
        return NO;
    }
    else if(!_password.text.length)
    {
        [self __showAlertViewWithMessage:@"請輸入密碼"];
        
        return NO;
    }
    else if(!_rePassword.text.length)
    {
        [self __showAlertViewWithMessage:@"確認密碼不可為空"];
        
        return NO;
    }
    else if(![_password.text isEqualToString:_rePassword.text])
    {
        [self __showAlertViewWithMessage:@"兩次密碼不相同"];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - 返回電話格式是否正確
- (BOOL)__validPhoneNumber
{
    NSDataDetector *detecotor = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                                error:nil];
    
    return [detecotor numberOfMatchesInString:_phone.text
                                      options:0
                                        range:NSMakeRange(0, _phone.text.length)];
}

#pragma mark - 顯示錯誤資訊
- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

@end
