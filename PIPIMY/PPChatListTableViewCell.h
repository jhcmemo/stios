
#import <UIKit/UIKit.h>

@interface PPChatListTableViewCell : UITableViewCell

@property (strong, nonatomic) NSMutableDictionary *chatSessionInfo;

- (void)configureCellForChat:(NSDictionary *)chat asSeller:(BOOL)isSeller;

@end
