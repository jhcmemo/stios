
#import "PPAPI.h"
#import "PPUser.h"
#import "PPMyDealsListController.h"


@interface PPMyDealsListController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *dataSource; // 交易資料
@property (nonatomic, weak) IBOutlet UITableView *tableView; // 列表

@end


@implementation PPMyDealsListController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = _dealType == DealTypeSeller ? @"我是小賣家" : @"我是小買家";
    
    [self __callAPIGetMyDeal];
}

#pragma mark - Properties Setter
- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    
    if(!_dataSource.count)
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UILabel *emptyLabel       = [[UILabel alloc]initWithFrame:_tableView.bounds];
        emptyLabel.textAlignment  = NSTextAlignmentCenter;
        emptyLabel.text           = @"沒有交易紀錄";
        _tableView.backgroundView = emptyLabel;
    }
    else
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.backgroundView = nil;
    }
    
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *celll = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    PPAPIGetMyDealResponse *item = _dataSource[indexPath.row];
    
    celll.textLabel.text = item.productTitle;
    
    return celll;
}

#pragma mark - Private methods

- (void)__callAPIGetMyDeal
{
    [[PPAPI singleton]APIGetMyDeal:nil callback:^(NSArray *deals, NSError *err) {

        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            NSPredicate *filter;
            
            // 過濾我是買家 or 賣家的資料
            if(_dealType == DealTypeSeller)
            {
                filter = [NSPredicate predicateWithFormat:@"userIDFrom = %@", [PPUser singleton].name];
            }
            else
            {
                filter = [NSPredicate predicateWithFormat:@"userIDTo = %@", [PPUser singleton].name];
            }
            
            self.dataSource = [deals filteredArrayUsingPredicate:filter];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

@end
