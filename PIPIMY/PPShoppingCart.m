
#import <UIKit/UITableView.h>
#import "PPShoppingCart.h"
#import "PPAPICartListResponse.h"
#import "PPAPI.h"

@interface PPShoppingCart ()

@property (nonatomic, strong) NSMutableArray *itemsByMember;
@property (nonatomic, strong) NSMutableDictionary *quantities;
@property (nonatomic)  NSMutableSet *checkedProductIds;

@end

@implementation PPShoppingCart

#pragma mark - Setter / Getter

- (NSMutableSet *)checkedProductIds
{
    // lazy instantiation
    if (!_checkedProductIds)
        _checkedProductIds = [NSMutableSet set];
    return _checkedProductIds;
}

#pragma mark - Create or update cart with items list

- (void)updateCartWithBlock:(void (^)(NSError *))callback
{
    [[PPAPI singleton] APICartList:nil callback:^(NSArray *response, NSError *error)
     {
         if (error)
         {
             // error handling
             [self updateCartWithItems:@[]];
         }
         else
         {
             [self updateCartWithItems:response];
         }
         
         callback(error);
     }];
}

- (void)updateCartWithItems:(NSArray *)items
{
    [self updateItemsByMember:items];
    [self updateQuantitiesWithItems:items];
    [self updateChecksWithItems:items];
}

- (void)updateItemsByMember:(NSArray *)items
{
    // sort all items by member ID, then walk through the list to split items into groups by member ID
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"memberId" ascending:YES selector:@selector(compare:)];
    NSArray *sortedItems = [items sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    NSString *memberId;
    NSMutableArray *newItemsByMember = [[NSMutableArray alloc] init];
    NSMutableDictionary *store;
    
    for (PPAPICartListResponse *item in sortedItems)
    {
        if ([item.memberId isEqualToString:memberId])
        {
            // add to existing group
            [store[@"items"] addObject:item];
        }
        else
        {
            // if not empty (i.e. initial) group, add group to array
            if (store)
            {
                [newItemsByMember addObject:store];
            }
            
            // create a new group
            memberId = item.memberId;
            store = [[NSMutableDictionary alloc] initWithObjectsAndKeys:memberId, @"memberId", item.storeUserId, @"storeUserId", [NSMutableArray arrayWithArray:@[item]], @"items", nil];
        }
    }
    
    // add last dictionary if it is not nil
    if (store)
    {
        [newItemsByMember addObject:store];
    }
    
    self.itemsByMember = newItemsByMember;
}

- (void)updateQuantitiesWithItems:(NSArray *)items
{
    NSMutableDictionary *newQuantities = [NSMutableDictionary dictionary];
    
    for (PPAPICartListResponse *item in items)
    {
        NSString *productId = item.productId;
        NSNumber *quantity = [self.quantities objectForKey:productId];
        if (quantity == nil)
        {
            // 特價物品需買全部
            if ([item.hide isEqualToNumber:@4])
                [newQuantities setObject:item.stock forKey:productId];
            else
            {
                if ([item.stock integerValue] > 0)
                    [newQuantities setObject:@1 forKey:productId];
                else
                    [newQuantities setObject:@0 forKey:productId];
            }
        }
        else
        {
            [newQuantities setObject:quantity forKey:productId];
        }
    }
    
    self.quantities = newQuantities;
}

- (void)updateChecksWithItems:(NSArray *)items
{
    NSMutableSet *newCheckedSet = [NSMutableSet set];
    for (PPAPICartListResponse *item in items)
    {
        [newCheckedSet addObject:item.productId];
    }
    [newCheckedSet intersectSet:self.checkedProductIds];
    self.checkedProductIds = newCheckedSet;
}

#pragma mark - Table view helpers
#pragma mark counters

- (NSInteger)numberOfSections
{
    return [self.itemsByMember count];
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section
{
    NSArray *items = self.itemsByMember[section][@"items"];
    return [items count];
}

#pragma mark store ID

- (NSString *)storeUserIdForSection:(NSInteger)section
{
    NSDictionary *store = self.itemsByMember[section];
    return store[@"storeUserId"];
}

#pragma mark checkbox

- (BOOL)boxCheckedForItem:(PPAPICartListResponse *)item
{
    return [self.checkedProductIds containsObject:item.productId];
}

- (void)toggleCheckBoxForItem:(PPAPICartListResponse *)item
{
    if ([self.checkedProductIds containsObject:item.productId])
        [self.checkedProductIds removeObject:item.productId];
    else
        [self.checkedProductIds addObject:item.productId];
}

- (BOOL)allBoxesCheckedForSection:(NSInteger)section
{
    BOOL allItemsChecked = YES;
    for (PPAPICartListResponse *item in self.itemsByMember[section][@"items"])
    {
        if ([self.checkedProductIds containsObject:item.productId] == NO)
        {
            allItemsChecked = NO;
            break;
        }
    }
    
    return allItemsChecked;
}

- (void)toggleCheckBoxForSection:(NSInteger)section
{
    if ([self allBoxesCheckedForSection:section])
    {
        for (PPAPICartListResponse *item in self.itemsByMember[section][@"items"])
        {
            [self.checkedProductIds removeObject:item.productId];
        }
        
    }
    else
    {
        for (PPAPICartListResponse *item in self.itemsByMember[section][@"items"])
        {
            [self.checkedProductIds addObject:item.productId];
        }
        
    }
}

- (NSArray *)orderListForSection:(NSInteger)section
{
    NSMutableArray *orderList = [NSMutableArray array];
    
    NSArray *items = self.itemsByMember[section][@"items"];
    for (PPAPICartListResponse *item in items) {
        if ([self boxCheckedForItem:item])
        {
            NSNumber *quantity = [self quantityForItem:item];
            if ([quantity integerValue] > 0)
            {
                NSDictionary *itemWithQuantity = @{ @"item": item, @"quantity": quantity };
                [orderList addObject:itemWithQuantity];
            }
        }
    }
    
    return orderList;
}

#pragma mark get item / remove item(s)

- (PPAPICartListResponse *)itemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *items = self.itemsByMember[indexPath.section][@"items"];
    return items[indexPath.row];
}

- (NSInteger)removeItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *items = self.itemsByMember[indexPath.section][@"items"];
    PPAPICartListResponse *item = items[indexPath.row];
    
    [items removeObject:item];
    [self.checkedProductIds removeObject:item.productId];
    [self.quantities removeObjectForKey:item.productId];
    
    // call API
    NSDictionary *params = @{ @"ProductID": item.productId };
    [[PPAPI singleton] APICartDelete:params callback:^(NSError *err) { }];
    
    if ([items count] == 0)
    {
        [self.itemsByMember removeObjectAtIndex:indexPath.section];
        return 0;
    }
    
    return [items count];
}

- (NSArray *)indexPathsForCheckedItemsInSection:(NSInteger)section
{
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSArray *items = self.itemsByMember[section][@"items"];
    for (PPAPICartListResponse *item in items)
    {
        if ([self.checkedProductIds containsObject:item.productId])
        {
            NSInteger row = [items indexOfObject:item];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [indexPaths addObject:indexPath];
        }
    }
    return indexPaths;
}

- (NSInteger)removeCheckedItemsInSection:(NSInteger)section
{
    NSMutableString *removedIDsString = [NSMutableString string];
    
    NSArray *items = self.itemsByMember[section][@"items"];
    NSMutableArray *newItems = [NSMutableArray array];
    
    // pull out unchecked items
    for (PPAPICartListResponse *item in items)
    {
        if ([self.checkedProductIds containsObject:item.productId])
        {
            [self.checkedProductIds removeObject:item.productId];
            [self.quantities removeObjectForKey:item.productId];
            [removedIDsString appendString:[NSString stringWithFormat:@",%@", item.productId]];
        }
        else
        {
            [newItems addObject:item];
        }
    }
    
    self.itemsByMember[section][@"items"] = newItems;
    
    // call API
    if ([removedIDsString length] > 0)
    {
        [removedIDsString deleteCharactersInRange:NSMakeRange(0, 1)];
    
        NSDictionary *params = @{ @"ProductID": removedIDsString };
        
        [[PPAPI singleton] APICartDelete:params callback:^(NSError *err) { }];
    }
    
    // return values
    if ([newItems count] == 0)
    {
        [self.itemsByMember removeObjectAtIndex:section];
        return 0;
    }
    else
    {
        return [newItems count];
    }
}

- (NSString *)removeAllItemsInSection:(NSInteger)section
{
    NSMutableString *removedIDsString = [NSMutableString string];
    
    NSMutableArray *items = self.itemsByMember[section][@"items"];
    for (PPAPICartListResponse *item in items)
    {
        [self.checkedProductIds removeObject:item.productId];
        [self.quantities removeObjectForKey:item.productId];
        [removedIDsString appendString:[NSString stringWithFormat:@",%@", item.productId]];
    }
    
    [self.itemsByMember removeObjectAtIndex:section];
    
    if ([removedIDsString length] > 0)
        [removedIDsString deleteCharactersInRange:NSMakeRange(0, 1)];
    return removedIDsString;
}

#pragma mark quantity

- (NSNumber *)quantityForItem:(PPAPICartListResponse *)item
{
    return [self.quantities objectForKey:item.productId];
}

- (void)setQuantity:(NSNumber *)quantity forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *store = self.itemsByMember[indexPath.section];
    PPAPICartListResponse *item = store[@"items"][indexPath.row];
    [self.quantities setObject:quantity forKey:item.productId];
}




@end
