
#import <UIKit/UIKit.h>

@protocol PPQuantityPickerDelegate <NSObject>

- (void)setQuantity:(NSNumber *)quantity forIndexPath:(NSIndexPath *)indexPath;

@end

@interface PPQuantityPickerViewController : UIViewController

@property (assign, nonatomic) NSInteger maxStock;
@property (assign, nonatomic) NSNumber *previousQuantity;
@property (weak, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) id <PPQuantityPickerDelegate> delegate;

@end
