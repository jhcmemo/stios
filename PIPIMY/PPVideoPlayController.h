
#import <UIKit/UIKit.h>
#import "PPProtocols.h"


/**
 *  商品影片播放 MVC
 */
@interface PPVideoPlayController : UIViewController



/**
 *  觀看影片時跳轉到 detail 頁面的委託
 */
@property (nonatomic, weak) id<PPProtocols>delegate;



/**
 *  在某個 ViewController 播放影片
 *
 *  此 ViewController 是利用 addChildViewController 來加入.
 *
 *  @param videos     影片, PPAPIGetNearProductResponse 的集合
 *  @param controller 某個 ViewController
 */
- (void)playVideos:(NSArray *)videos inViewController:(UIViewController *)controller;

/**
 *  離開播放頁面
 *
 *  使用 removeFromParentViewController 離開
 */
- (void)exitPlayVideo;

@end
