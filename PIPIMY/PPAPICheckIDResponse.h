
#import <Mantle.h>


/**
 *  API CheckID 回傳的 Response
 *
 *  確認ID是否註冊，用已經註冊的ID來測sameID
 */

@interface PPAPICheckIDResponse : MTLModel<MTLJSONSerializing>



/**
 *  是否成功 ==> JSON result: success/sameID
 */
@property (nonatomic, readonly) BOOL success;

/**
 *  狀態
 */
@property (nonatomic, readonly) NSString *result;

@end
