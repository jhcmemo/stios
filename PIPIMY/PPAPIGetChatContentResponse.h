
#import <Mantle.h>

@interface PPAPIGetChatContentResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *content;
@property (nonatomic, readonly) NSString *isFrom;
@property (nonatomic, readonly) NSDate   *sendTime;
@property (nonatomic, readonly) NSNumber *type;

@property (nonatomic) BOOL isPending;

- (instancetype) initWithContent:(NSString *)content isFrom:(NSString *)isFrom sendTime:(NSDate *)sendTime type:(NSNumber *)type;

@end
