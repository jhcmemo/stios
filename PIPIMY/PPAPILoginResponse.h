
#import <Mantle.h>

/**
 *  API login 回傳的 Response
 *
 *  基本上是登入的資訊
 */
@interface PPAPILoginResponse : MTLModel<MTLJSONSerializing>



/**
 *  是否成功
 */
@property (nonatomic, readonly) BOOL success;

/**
 *  使用者 Id
 */
@property (nonatomic, readonly) NSString *ID;

/**
 *  token
 */
@property (nonatomic, readonly) NSString *allpay_token;

/**
 *  database Id
 */
@property (nonatomic, readonly) NSNumber *Id;

/**
 *  手機
 */
@property (nonatomic, readonly) NSString *mobile;

/**
 *  狀態
 */
@property (nonatomic, readonly) NSString *result;

@end
