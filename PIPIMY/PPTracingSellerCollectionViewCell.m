
#import "PPAWSS3.h"
#import "PPAPI.h"
#import "PPAPIListTracingResponse.h"
#import <UIImageView+WebCache.h>
#import "PPTracingSellerCollectionViewCell.h"

#define PPTracingCellAlertRemoveTrace      1
#define PPTracingCellAlertStartReceivePush 2
#define PPTracingCellAlertStopReceivePush  3

@interface PPTracingSellerCollectionViewCell () <UIAlertViewDelegate>

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *productImages;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *sellerIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *addTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *doesReceivePushButton;

@property (weak, nonatomic) PPAPIListTracingResponse *seller;

@end


@implementation PPTracingSellerCollectionViewCell

- (void)configureCellWithTracedSeller:(PPAPIListTracingResponse *)seller
{
    self.seller = seller;
    
    NSURL *userIconURL                = [PPAWSS3 userIconWithUserId:seller.tracingId];
    self.sellerIDLabel.text           = seller.tracingId;
    self.addTimeLabel.text            = seller.addTime;
    
    if ([seller.push isEqualToNumber:@1])
        [self.doesReceivePushButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    else if ([seller.push isEqualToNumber:@2])
        [self.doesReceivePushButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    [self.userIcon sd_setImageWithURL:userIconURL placeholderImage:[UIImage imageNamed:@"nohead"]];
    for (NSInteger i = 0; i < [seller.productIDs count] && i < 3; i++)
    {
        NSURL *imageURL = [PPAWSS3 imageURLWithProductId:@([seller.productIDs[i] intValue]) inIndex:1];
        UIImageView *imageView = self.productImages[i];
        [imageView sd_setImageWithURL:imageURL];
    }
}

- (IBAction)doesReceivePushButtonClicked:(id)sender {
    if ([self.seller.push isEqualToNumber:@1])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"接收推播"
                                                            message:[NSString stringWithFormat:@"開始接收 %@ 的推播嗎？", self.seller.tracingId]
                                                           delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        alertView.tag = PPTracingCellAlertStartReceivePush;
        [alertView show];
    }
    else if ([self.seller.push isEqualToNumber:@2])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"停止接收推播"
                                                            message:[NSString stringWithFormat:@"停止接收 %@ 的推播嗎？", self.seller.tracingId]
                                                           delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        alertView.tag = PPTracingCellAlertStopReceivePush;
        [alertView show];
    }
}

- (IBAction)removeTraceButtonClicked:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"停止關注"
                                                        message:[NSString stringWithFormat:@"確定要停止關注 %@ 嗎？", self.seller.tracingId]
                                                       delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    alertView.tag = PPTracingCellAlertRemoveTrace;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
        return;
    
    switch (alertView.tag) {
        case PPTracingCellAlertRemoveTrace:
            [self.delegate removeTracedSeller:self.seller];
            break;
            
        case PPTracingCellAlertStartReceivePush:
            [self toggleTracingReceive];
            break;
            
        case PPTracingCellAlertStopReceivePush:
            [self toggleTracingReceive];
            break;
            
        default:
            break;
    }
}

- (void)toggleTracingReceive
{
    NSDictionary *params = @{ @"TracingID" : self.seller.tracingId };
    
    [[PPAPI singleton] APITracePushTrigger:params callback:^(PPAPITracePushTriggerResponse *response, NSError *err) {
        if ([response.result isEqualToString:@"success"] && response.isSwitchOn != nil)
        {
            if ([response.isSwitchOn boolValue])
                [self.doesReceivePushButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            else
                [self.doesReceivePushButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }];
}

@end
