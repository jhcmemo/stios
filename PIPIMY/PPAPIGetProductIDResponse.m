
#import "PPAPIGetProductIDResponse.h"


@implementation PPAPIGetProductIDResponse

#pragma mark - Key Map
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"productId" : @"result"};
}

#pragma mark - Properties
- (BOOL)success
{
    return _productId != nil;
}

@end
