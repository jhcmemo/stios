
#import "PPOrderDetailViewController.h"
#import "PPOrderOptionsViewsModel.h"
#import "PPAPIGetOrderListResponse.h"
#import "PPOrderRatingSubmissionViewController.h"
#import "PPAPI.h"

@interface PPOrderDetailViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) PPAPIGetOrderResourceResponse *orderResource;
@property (strong, nonatomic) NSArray *orderTextGroups;
@property (strong, nonatomic) NSArray *sectionTitles;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PPOrderDetailViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDictionary *params = @{ @"orderId" : self.order.Id };
    [[PPAPI singleton] APIGetOrderResource:params callback:^(PPAPIGetOrderResourceResponse *orderResource, NSError *err) {
        if (!err)
        {
            self.orderResource = orderResource;
            [self setupOrderDescription];
            [self setupRatingButton];
            [self.tableView reloadData];
        }
        else
        {
            // error handling
        }
    }];
    
    // remove empty separator lines
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)setupOrderDescription
{
    self.sectionTitles = @[ @"訂單資訊", @"付費資訊", @"物流資訊", @"商品明細" ];
    
    PPAPIGetOrderResourceResponse *orderResource = self.orderResource;
    
    NSMutableArray *section1Text = [NSMutableArray array];
    NSMutableArray *section2Text = [NSMutableArray array];
    NSMutableArray *section3Text = [NSMutableArray array];
    NSMutableArray *itemsDescriptions = [NSMutableArray arrayWithCapacity:[orderResource.items count] * 3];
    
    // 訂單資訊
    [section1Text addObject:[NSString stringWithFormat:@"賣家: %@",             self.order.sellerUserId]];
    [section1Text addObject:[NSString stringWithFormat:@"訂單編號：%@",          self.order.Id ]];
    [section1Text addObject:[NSString stringWithFormat:@"金額：$%@",            orderResource.order.sum]];
    
    // 付費資訊
    NSString *paymentTypeCategory;
    if ([orderResource.payment.paymentType length] >= 3)
        paymentTypeCategory = [orderResource.payment.paymentType substringToIndex:3];
    if (orderResource.payment.isHand)
    {
        [section2Text addObject:@"繳款方式：面交"];
    }
    else if ([orderResource.payment.paymentType isEqualToString:@"Credit_CreditCard"])
    {
        [section2Text addObject:@"繳款方式：信用卡"];
    }
    else if ([orderResource.payment.paymentType isEqualToString:@"COLLECTION_UNIMARTC2C"])
    {
        [section2Text addObject:@"繳款方式：統一超商貨到付款"];
    }
    else if ([orderResource.payment.paymentType isEqualToString:@"COLLECTION_FAMIC2C"])
    {
        [section2Text addObject:@"繳款方式：全家超商貨到付款"];
    }
    else if ([orderResource.payment.paymentType isEqualToString:@"CVS_CVS"])
    {
        [section2Text addObject:@"繳款方式：超商繳款"];
    }
    else if ([paymentTypeCategory isEqualToString:@"ATM"])
    {
        [section2Text addObject:@"繳款方式：轉帳"];
    }
    
        // credit card
    if (orderResource.payment.paymentDate)
        [section2Text addObject:[NSString stringWithFormat:@"日期：%@",             orderResource.payment.paymentDate]];
        // CVS
    if (orderResource.payment.paymentNo)
        [section2Text addObject:[NSString stringWithFormat:@"繳費代碼：%@",       orderResource.payment.paymentNo]];
        // 轉帳
    if ([paymentTypeCategory isEqualToString:@"ATM"])
    {
        NSString *bankCode = [orderResource.payment.paymentType substringFromIndex:4];
        NSInteger bankNameIndex = [[PPOrderOptionsViewsModel bankCodes] indexOfObject:bankCode];
        NSString *bankName = [[PPOrderOptionsViewsModel bankNames] objectAtIndex:bankNameIndex];
        [section2Text addObject:[NSString stringWithFormat:@"轉帳銀行：%@",           bankName]];
        [section2Text addObject:[NSString stringWithFormat:@"銀行代碼：%@",        orderResource.payment.bankCode]];
        [section2Text addObject:[NSString stringWithFormat:@"銀行代號：%@",        orderResource.payment.vAccount]];
    }
    if (orderResource.payment.expireDate)
        [section2Text addObject:[NSString stringWithFormat:@"有效期限：%@",        orderResource.payment.expireDate]];
    if (orderResource.payment.tradeAmt)
        [section2Text addObject:[NSString stringWithFormat:@"金額：$%@",          orderResource.payment.tradeAmt]];

    // 物流資訊
    if (orderResource.logistics.isHand)
    {
        [section3Text insertObject:@"收件方式：面交" atIndex:0];
    }
    else if ([orderResource.logistics.logisticsSubType isEqualToString:@"UNIMARTC2C"])
    {
        [section3Text insertObject:@"收件方式：統一超商店到店" atIndex:0];
    }
    else if ([orderResource.logistics.logisticsSubType isEqualToString:@"FAMIC2C"])
    {
        [section3Text insertObject:@"收件方式：全家超商店到店" atIndex:0];
    }
    
    if (orderResource.logistics.CVSPaymentNo)
    {
        if (orderResource.logistics.CVSValidationNo)
            [section3Text addObject:[NSString stringWithFormat:@"寄貨編號：%@%@",   orderResource.logistics.CVSPaymentNo, orderResource.logistics.CVSValidationNo]];
        else
            [section3Text addObject:[NSString stringWithFormat:@"寄貨編號：%@",     orderResource.logistics.CVSPaymentNo]];
    }
    if (orderResource.logistics.receiverName)
        [section3Text addObject:[NSString stringWithFormat:@"收件人：%@",            orderResource.logistics.receiverName]];
    if (orderResource.logistics.receiverPhone)
        [section3Text addObject:[NSString stringWithFormat:@"收件人電話：%@",         orderResource.logistics.receiverPhone]];
    if (orderResource.logistics.receiverCellPhone)
        [section3Text addObject:[NSString stringWithFormat:@"收件人手機：%@",         orderResource.logistics.receiverCellPhone]];
    
    
    // 商品明細
    for (OrderResourceItems *item in orderResource.items)
    {
        [itemsDescriptions addObject:[NSString stringWithFormat:@"商品名稱：%@",  item.title]];
        [itemsDescriptions addObject:[NSString stringWithFormat:@"單價：$%@",  item.price]];
        [itemsDescriptions addObject:[NSString stringWithFormat:@"數量：%@", item.amount]];
    }
    
    self.orderTextGroups = @[section1Text, section2Text, section3Text, itemsDescriptions];
}

#pragma mark - 評論按鈕

- (void)setupRatingButton
{
    // add nav bar item if satisfying requirements: paymentType = 1248, paymentStatus = 1
    if (self.asBuyer && [self.order.paymentStatus integerValue] == 1 && ([self.order.paymentType integerValue] & 0b1111))
    {
        UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"評價" style:UIBarButtonItemStylePlain target:self action:@selector(ratingButtonClicked)];
        self.navigationItem.rightBarButtonItem = barButton;
    }
}

- (void)ratingButtonClicked
{
    [self performSegueWithIdentifier:@"goToBuyerComment" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goToBuyerComment"])
    {
        // grab destination view controller and pass on the necessary parameters
        PPOrderRatingSubmissionViewController *viewController = segue.destinationViewController;
        viewController.orderId = self.order.Id;
    }
}

#pragma mark - Table view delegate / datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.orderTextGroups count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *groupTexts = self.orderTextGroups[section];
    return [groupTexts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.textLabel.text = self.orderTextGroups[indexPath.section][indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 28.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.sectionTitles objectAtIndex:section];
}

@end
