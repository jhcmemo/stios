
#import "PPAWSS3.h"
#import "UIColor+SRPKit.h"
#import "AppDelegate+Setup.h"
#import <SDWebImage/SDImageCache.h>


@implementation AppDelegate (Setup)

#pragma mark - 設置 Singleton 物件
- (void)setup_Singletons
{
    [PPAPI singleton];
    [PPUser singleton];
    [PPAWSS3 singleton];
}

#pragma mark - 設置 UIAppearance
- (void)setup_UIAppearance
{
    UIColor *color1 = [UIColor lightGrayColor];
    //UIColor *color2 = [UIColor srp_colorWithHEX:@"#7fcdcd"];
    UIColor *color3 = [UIColor whiteColor];
    
    // 下方 TabBarItem Normal color
    [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName : color1}
                                            forState:UIControlStateNormal];
    
    // 下方 TabBarItem 文字 select color
    [[UITabBarItem appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor srp_colorWithHEX:@"#FF514F4E"]}
                                            forState:UIControlStateSelected];
    
    // 下方 Icon 選中的顏色
    [[UITabBar appearance]setTintColor:[UIColor srp_colorWithHEX:@"#FF514F4E"]];
    
    // 下方 TabBarTintColor
    [[UITabBar appearance]setBarTintColor:[UIColor srp_colorWithHEX:@"#FFECECEC"]];
    
    // NavagationBar
    //[[UINavigationBar appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName : color3}];
    [[UINavigationBar appearance]setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor srp_colorWithHEX:@"#FF514F4E"]}];
    [[UINavigationBar appearance]setBarTintColor:[UIColor srp_colorWithHEX:@"#FFFFFFFF"]];
    //[[UINavigationBar appearance]setTintColor:color3];
    [[UINavigationBar appearance]setTintColor:[UIColor srp_colorWithHEX:@"#FF514F4E"]];
    
    // SearchBar 裡的 Cancel Item 字體顏色
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]
                         setTitleTextAttributes:@{NSForegroundColorAttributeName : color3}
                                       forState:UIControlStateNormal];
}

#pragma mark - 初始設置
- (void)setup_defaultSetting
{
    [[SDImageCache sharedImageCache]cleanDisk];
    
    if(self.tabController)
    {
        self.tabController.delegate = self;
        
        UIStoryboard *storboard1     = [UIStoryboard storyboardWithName:@"Tab1" bundle:nil];
        UINavigationController *nav1 = [storboard1 instantiateInitialViewController];
        
        UIStoryboard *storboard2     = [UIStoryboard storyboardWithName:@"Tab2" bundle:nil];
        UINavigationController *nav2 = [storboard2 instantiateInitialViewController];
        
        UIStoryboard *storboard3     = [UIStoryboard storyboardWithName:@"Tab3" bundle:nil];
        UINavigationController *nav3 = [storboard3 instantiateInitialViewController];
        
        UIStoryboard *storboard4     = [UIStoryboard storyboardWithName:@"Tab4" bundle:nil];
        UINavigationController *nav4 = [storboard4 instantiateInitialViewController];
        
        self.tabController.viewControllers = @[nav1, nav2, nav3, nav4];
    }
}

@end
