
#import <UIKit/UIKit.h>
#import "PPProtocols.h"

/**
 *  客製化錄影 MVC
 */
@interface PPVideoRecordController : UIViewController



/**
 *  按下上傳按鈕的委託
 */
@property (nonatomic, weak) id<PPProtocols>delegate;

@end
