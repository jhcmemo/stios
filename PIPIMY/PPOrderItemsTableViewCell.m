
#import "PPOrderItemsTableViewCell.h"
#import "PPAPICartListResponse.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PPOrderItemsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productTitle;
@property (weak, nonatomic) IBOutlet UILabel *productQuantity;
@property (weak, nonatomic) IBOutlet UILabel *productPrice;


@end

@implementation PPOrderItemsTableViewCell

- (void)configureCellWithItem:(PPAPICartListResponse *)item quantity:(NSNumber *)quantity
{
    self.productTitle.text    = [NSString stringWithFormat:@"%@", item.title];
    self.productQuantity.text = [NSString stringWithFormat:@"購買數量：%@", quantity];
    self.productPrice.text    = [NSString stringWithFormat:@"價格：NT$ %@", item.price];
    
    NSURL *productIconUrl = [PPAWSS3 imageURLWithProductId:@([item.productId intValue]) inIndex:1];
    [self.productImage sd_setImageWithURL:productIconUrl];
}

@end
