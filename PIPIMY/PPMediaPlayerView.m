
#import "PPMediaPlayerView.h"


@implementation PPCaptureView

+ (Class)layerClass
{
    return [AVCaptureVideoPreviewLayer class];
}

@end


@implementation PPPlayerView

+ (Class)layerClass
{
    return [AVPlayerLayer class];
}

@end
