
#import <Foundation/Foundation.h>
#import "PPAPIGetNearProductResponse.h"
#import "PPAPIGetSomeoneRatingResponse.h"


/**
 *  PPProductDetailController 的 SubView 管理 model
 */
@interface PPModifyProductDetailViewsModel : NSObject <UITextFieldDelegate>



/**
 *  評分 Table 高
 */
@property (nonatomic, readonly) CGFloat scoreTableHeight;


/**
 *  商品介紹 cell 高
 */
@property (nonatomic, readonly) CGFloat s0c1Height;

@property (weak, nonatomic) IBOutlet UIButton *typeButton;


/**
 *  設置初始商品 UI
 *
 *  @param product 商品內容
 */
- (void)configureWithProduct:(PPAPIGetNearProductResponse *)product;

/**
 *  設置初始_ImageScrollerView及塞圖
 *
 *  @param product 商品內容
 */
- (void)setupImageScrollerWithProduct:(PPAPIGetNearProductResponse *)product;

/**
 *  更新評分詳細
 *
 *  @param detail 詳細
 */
- (void)updateRatingDetail:(RatingDetail *)detail;

/**
 *  更新評分資料
 *
 *  @param dataSource 資料來源
 */
- (void)updateRatingDatasource:(NSArray *)dataSource;


@end
