
#import <Mantle.h>


/**
 *  API ListTracing 的 response
 *
 *  基本上是我關注的賣家列表裡的賣家資訊
 */
@interface PPAPIListTracingResponse : MTLModel<MTLJSONSerializing>



/**
 *  關注時間
 */
@property (nonatomic, readonly) NSString *addTime;

/**
 *  賣家商品
 */
@property (nonatomic, readonly) NSArray *productIDs;

/**
 *  是否接收推播, 1 = 不接收 (預設)， 2 = 接收
 */
@property (nonatomic, readonly) NSNumber *push;

/**
 *  關注 Id
 */
@property (nonatomic, readonly) NSString *tracingId;

@end
