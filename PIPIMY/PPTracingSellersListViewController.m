
#import "PPAPI.h"
#import "PPUser.h"
#import "PPAWSS3.h"
#import <UIImageView+WebCache.h>
#import "PPTracingSellersListViewController.h"
#import "PPTracingSellerCollectionViewCell.h"

@interface PPTracingSellersListViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PPTracingCellerCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataSource; // 追蹤賣家的資料

@end

@implementation PPTracingSellersListViewController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([PPUser singleton].isLogin)
    {
        [self __callAPIListTracing];
    }
}

#pragma mark - UICollectionView delegate / data source
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPTracingSellerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    PPAPIListTracingResponse *item = _dataSource[indexPath.row];
    [cell configureCellWithTracedSeller:item];
    cell.delegate = self;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat margin = 8.0;
    CGFloat width = self.view.frame.size.width - 2 * margin;
    CGFloat productImageHeight = (width - 2 * margin) / 3.0;
    CGFloat userIconHeight = 60.0;
    
    CGFloat height = productImageHeight + userIconHeight + 3 * margin;
    return CGSizeMake(width, height);
}

#pragma mark - cell delegate

- (void)removeTracedSeller:(PPAPIListTracingResponse *)seller
{
    // Call API
    NSDictionary *params = @{@"TracingID" : seller.tracingId};
    
    [[PPAPI singleton]APIDeleteTrace:params callback:^(BOOL success) {
        if(success)
        {
            NSInteger sellerIndex = [self.dataSource indexOfObject:seller];
            [self.dataSource removeObject:seller];
            [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:sellerIndex inSection:0]]];
        }
    }];

}

#pragma mark - Call API
- (void)__callAPIListTracing
{
    [[PPAPI singleton]APIListTracing:nil callback:^(NSArray *people, NSError *err) {
        
        self.dataSource = [people mutableCopy];
        
        [_collectionView reloadData];
    }];
}

@end
