
#import "PPAPIConfirmOrderFinishResponse.h"

@implementation PPAPIConfirmOrderFinishResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)confirmDateJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^NSDate *(NSString *dateString) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [dateFormatter dateFromString:dateString];
    }];
}

+ (NSValueTransformer *)pendingJSONTransformer
{
        return [MTLValueTransformer transformerWithBlock:^NSNumber *(NSString *pendingString) {
            return @([pendingString integerValue]);
        }];
}

+ (NSValueTransformer *)queueJSONTransformer
{
        return [MTLValueTransformer transformerWithBlock:^NSNumber *(NSString *queueString) {
            return @([queueString integerValue]);
        }];
}

- (BOOL)success
{
    return [_result isEqualToString:@"success"];
}

@end
