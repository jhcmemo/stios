
#import "PPAPI.h"
#import "PPUser.h"
#import "AppDelegate.h"


/**
 *  AppDelegate 設置 Category
 */
@interface AppDelegate (Setup)<UITabBarControllerDelegate>



/**
 *  設置 Singleton 物件
 */
- (void)setup_Singletons;

/**
 *  設置 UIAppearance
 */
- (void)setup_UIAppearance;

/**
 *  初始設置
 */
- (void)setup_defaultSetting;

@end
