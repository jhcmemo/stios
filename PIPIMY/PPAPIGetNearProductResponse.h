
#import <Mantle.h>


/**
 *  API getNearProduct 回傳的 Response
 *
 *  基本上是商品的資訊
 */
@interface PPAPIGetNearProductResponse : MTLModel<MTLJSONSerializing>



/**
 *  上傳人 Id
 */
@property (nonatomic, readonly) NSString *ID;

/**
 *  品牌
 */
@property (nonatomic, readonly) NSString *brand;

/**
 *  議價的對象買家
 */
@property (nonatomic, readonly) NSString *buyerId;

/**
 *  國別
 */
@property (nonatomic, readonly) NSString *cc;

/**
 *  交貨方式
 */
@property (nonatomic, readonly) NSNumber *delivery;

/**
 *  描述
 */
@property (nonatomic, readonly) NSString *des;

/**
 *  加入最愛次數
 */
@property (nonatomic, readonly) NSString *favorite;

/**
 *  假如這個值為4則為 議價商品
 */
@property (nonatomic, readonly) NSNumber *hide;

/**
 *  經度
 */
@property (nonatomic, readonly) NSNumber *lat;

/**
 *  緯度
 */
@property (nonatomic, readonly) NSNumber *lng;

/**
 *  交貨地點 1
 */
@property (nonatomic, readonly) NSString *loc1;

/**
 *  交貨地點 2
 */
@property (nonatomic, readonly) NSString *loc2;

/**
 *  商品新舊
 */
@property (nonatomic, readonly) NSString *newold;

/**
 *  對應商品新舊的敘述 string
 */
- (NSString *)getNewOldStatusStr;

/**
 *  議價之前的原物品的 ID
 */
@property (nonatomic, readonly) NSString *parentId;

/**
 *  收費方式
 */
@property (nonatomic, readonly) NSNumber *payment;

/**
 *  照片數量
 */
@property (nonatomic, readonly) NSNumber *picNumber;

/**
 *  價格
 */
@property (nonatomic, readonly) NSString *price;

/**
 *  商品 Id
 */
@property (nonatomic, readonly) NSNumber *productId;

/**
 *  成功與否
 */
@property (nonatomic, readonly) NSString *result;

/**
 *  商品編號 (特殊的商品編號)
 */
@property (nonatomic, readonly) NSString *sno;

/**
 *  庫存
 */
@property (nonatomic, readonly) NSNumber *stock;

/**
 *  上傳時間
 */
@property (nonatomic, readonly) NSString *time;

/**
 *  名稱
 */
@property (nonatomic, readonly) NSString *title;

/**
 *  分類
 */
@property (nonatomic, readonly) NSNumber *type;

/**
 *  收藏時間
 */
@property (nonatomic, copy) NSString *addToFavoriteDate;

/**
 *  與使用者座標的距離
 */
@property (nonatomic, assign) double distance;

@end
