#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@class PPCreditCardTextField;

typedef NS_ENUM(NSInteger, PPOptionsActionSheetTag)
{
    PPOptionsActionSheetTagMainPayment = 0,     // 付費方式主選單
    PPOptionsActionSheetTagPaymentWireTransfer, // 付費方式選銀行
    PPOptionsActionSheetTagPaymentStore,        // 付費方式選超商
    PPOptionsActionSheetTagMainShipping,        // 寄送方式主選單
    PPOptionsActionSheetTagShippingCounty,      // 寄送方式選縣市
    PPOptionsActionSheetTagShippingSubdivision, // 寄送方式選區域
    PPOptionsActionSheetTagShippingStore        // 寄送方式選店
};

typedef NS_ENUM(NSInteger, PPPaymentOptionTypes)
{
    PPPaymentOptionTypeCC               = 0, // 信用卡
    PPPaymentOptionTypeConvenienceStore = 1, // 超商
    PPPaymentOptionTypeWireTransfer     = 2, // 轉帳
    PPPaymentOptionTypeRemainder        = 3, // 餘額
    PPPaymentOptionTypeInPerson         = 4, // 面交
    PPPaymentOptionTypeFamilymartCOD    = 5, // 超商貨到付款
    PPPaymentOptionType711COD           = 6  // 統一貨到付款
};

typedef NS_ENUM(NSInteger, PPShippingOptionTypes)
{
    PPShippingOptionTypeFamilymart    = 0, // 全家
    PPShippingOptionType711           = 1, // 7-ELEVEN
    PPShippingOptionTypeCustom        = 2, // 自訂
    PPShippingOptionTypeExpress       = 3, // 宅配
    PPShippingOptionTypeInPerson      = 4, // 面交
    PPShippingOptionTypeFamilymartCOD = 5, // 超商貨到付款
    PPShippingOptionType711COD        = 6  // 統一貨到付款
};


@class PPAPIAllpayGetStoreInfoResponse;

@interface PPOrderOptionsViewsModel : NSObject

@property (strong, nonatomic) NSArray *paymentOptions;
@property (strong, nonatomic) NSArray *shippingOptions;

@property (assign, nonatomic) NSNumber *mainPaymentSelection;
@property (assign, nonatomic) NSNumber *subPaymentSelection;
@property (assign, nonatomic) NSNumber *mainShippingSelection;
@property (assign, nonatomic) NSString *shippingCountySelection;
@property (assign, nonatomic) NSString *shippingSubdivisionSelection;
@property (assign, nonatomic) PPAPIAllpayGetStoreInfoResponse *shippingStoreSelection;

@property (weak, nonatomic) IBOutlet PPCreditCardTextField *creditCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *creditCardExpireMonth;
@property (weak, nonatomic) IBOutlet UITextField *creditCardExpireYear;
@property (weak, nonatomic) IBOutlet UITextField *creditCardCVC;
@property (weak, nonatomic) IBOutlet UITextField *creditCardName;
@property (weak, nonatomic) IBOutlet UITextField *creditCardPhone;

@property (readonly, nonatomic) NSArray *paymentOptionsDefaultText;
@property (readonly, nonatomic) NSArray *storeTypeOptions;
@property (readonly, nonatomic) NSArray *storeTypeCodes;
@property (readonly, nonatomic) NSArray *shippingOptionsDefaultText;

+ (NSArray *)bankNames;
+ (NSArray *)bankCodes;

@property (weak, nonatomic) UIViewController *parentViewController;

- (void)setupPaymentMethodsView;
- (void)setupShippingMethodsView;

- (void)formatCreditCardNumber:(UITextField *)textField;

@end
