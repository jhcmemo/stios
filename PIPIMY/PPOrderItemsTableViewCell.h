
#import <UIKit/UIKit.h>
@class PPAPICartListResponse;

@interface PPOrderItemsTableViewCell : UITableViewCell

- (void)configureCellWithItem:(PPAPICartListResponse *)item quantity:(NSNumber *)quantity;

@end
