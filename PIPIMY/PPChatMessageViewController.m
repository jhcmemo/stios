
#import "PPChatMessageViewController.h"
#import "PPMyMessageTableViewCell.h"
#import "PPMyMessageTableViewCell.h"
#import "PPOtherMessageTableViewCell.h"
#import "PPOtherImageTableViewCell.h"
#import "PPMessageDateTableViewCell.h"
#import "PPProductDetailController.h"
#import "PPAPI.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface PPChatMessageViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIAlertViewDelegate>

@property (weak,   nonatomic) IBOutlet UITableView        *tableView;
@property (weak,   nonatomic) IBOutlet NSLayoutConstraint *keyboardHeightConstraint;
@property (weak,   nonatomic) IBOutlet UITextView         *messageTextView;
@property (weak,   nonatomic) IBOutlet UINavigationItem   *chatTitleLabel;

@property (strong, nonatomic) NSMutableArray              *messageArray;
@property (strong, nonatomic) PPAPIGetNearProductResponse *product;

@property (strong, nonatomic) NSDictionary   *sizingCells;
@property (strong, nonatomic) NSMutableArray *cellHeightCache;
@property (assign, nonatomic) CGFloat         width;

@end


@implementation PPChatMessageViewController

#pragma mark - Life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Basic setup
    self.chatResponse             = [self.chatSessionInfo objectForKey:@"chatResponse"];
    
    // table view settings
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView setContentInset:UIEdgeInsetsMake(5,0,0,0)];
    
    // UI sizing elements
    self.sizingCells = @{ @"myCell"    : [self.tableView dequeueReusableCellWithIdentifier:@"myCell"],
                          @"otherCell" : [self.tableView dequeueReusableCellWithIdentifier:@"otherCell"] };
    self.cellHeightCache = [NSMutableArray array];
    
    // Text entry area
    [self.messageTextView setScrollEnabled:NO];
    self.messageTextView.layer.cornerRadius = 6.0;
    self.messageTextView.clipsToBounds = YES;
    
    // Navigation bar 商品圖示按鈕
    CGFloat barHeight        = self.navigationController.navigationBar.frame.size.height;
    UIImageView *productIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, barHeight - 4, barHeight - 4)];
    NSURL *productIconUrl    = [PPAWSS3 imageURLWithProductId:@([self.chatResponse.productId intValue]) inIndex:1];
    [productIcon sd_setImageWithURL:productIconUrl];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProduct:)];
    [singleTap setNumberOfTapsRequired:1];
    [productIcon addGestureRecognizer:singleTap];

    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"調整價格" style:UIBarButtonItemStylePlain target:self action:@selector(discountButtonClicked:)];
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:productIcon];

    // 我是買家
    if ([[self.chatSessionInfo objectForKey:@"myMessageID"] isEqual:@"1"])
    {
        self.navigationItem.rightBarButtonItems = @[ barButton2 ];
    }
    // 我是賣家，可調商品價格
    else if ([[self.chatSessionInfo objectForKey:@"myMessageID"] isEqual:@"2"])
    {
        self.navigationItem.rightBarButtonItems = @[ barButton, barButton2 ];
    }
    
    // 取得和這個聊天室相關的商品 (若賣家有降價，會拿到修改後的商品)
    [self updateOneProduct];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    // listen for keyboard show/hide
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // listen for new messages push notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addOtherMessage:) name:@"NewChatMessageNotification" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    // 取得聊天室歷史，Call API
    NSDictionary *param = @{@"ChatId": self.chatResponse.chatId, @"IsFrom": self.chatSessionInfo[@"myMessageID"]};
    
    [[PPAPI singleton] APIGetChatContent:param callback:^(NSArray *messages, NSError *err)
     {
        if(messages)
        {
            // 反轉訊息的順序
            self.messageArray = [NSMutableArray array];
            for (PPAPIGetChatContentResponse *message in [[messages reverseObjectEnumerator] allObjects])
            {
                [self appendMessage:message];
            }
            
            [self.tableView reloadData];
            
            // scroll 到底
            if ([self.messageArray count])
            {
                NSInteger lastRow = [self.messageArray count] - 1;
                [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        }
    }];
    
    // 計算文字輸入區的實際文字欄寬
    self.width = self.messageTextView.textContainer.size.width - self.messageTextView.textContainerInset.left - self.messageTextView.textContainerInset.right - self.messageTextView.textContainer.lineFragmentPadding * 2;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    // stop all message send requests
    [[PPAPI singleton] cancelAllRequests];
    
    // stop listening for keyboard show/hide
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    // stop listen for new messages push notification
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewChatMessageNotification" object:nil];
    
    // clear badge for this chat
    NSDictionary *params = @{ @"ChatId": self.chatResponse.chatId, @"IsFrom": self.chatSessionInfo[@"myMessageID"] };
    [[PPAPI singleton] APICleanChatBadge:params callback:^(NSArray * response, NSError * err) { }];
}

#pragma mark - Table view datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.messageArray count];
}

# pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // cache height
    if ([self.cellHeightCache count] < indexPath.row + 1)
    {
        for (NSInteger i = [self.cellHeightCache count]; i <= indexPath.row; i++)
        {
            CGFloat height = [self tableView:tableView calculateHeightForRowAtIndexPath:indexPath];
            [self.cellHeightCache addObject:[NSNumber numberWithFloat:height]];
        }
    }
    
    return [self.cellHeightCache[indexPath.row] floatValue];
}

- (CGFloat)tableView:(UITableView *)tableView calculateHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetChatContentResponse *message = self.messageArray[indexPath.row];
    
    // My cell
    if ([message.isFrom isEqualToString:self.chatSessionInfo[@"myMessageID"]])
    {
        // fill the sizing cell to calculate actual size
        PPMyMessageTableViewCell *cell = self.sizingCells[@"myCell"];
        [cell configureCellWithMessage:message chat:self.chatSessionInfo];
        [cell setNeedsUpdateConstraints];
        [cell updateConstraintsIfNeeded];
        cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.frame), CGRectGetHeight(cell.bounds));
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
        
        CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1.0;
        return height;
    }
    else
    {
        // date cell
        if ([message.type isEqualToNumber:@48])
            return 30.0;
        // Other's cell
        else if ([message.type isEqualToNumber:@2])
        {
            return [PPOtherImageTableViewCell cellHeight];
        }
        else
        {
            PPOtherMessageTableViewCell *cell = self.sizingCells[@"otherCell"];
            [cell configureCellWithMessage:message chat:self.chatSessionInfo];
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            
            CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1.0;
            return height;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetChatContentResponse *message = self.messageArray[indexPath.row];
    // My cell
    if ([message.isFrom isEqualToString:self.chatSessionInfo[@"myMessageID"]])
    {
        PPMyMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
        [cell configureCellWithMessage:message chat:self.chatSessionInfo];
        return cell;
    }
    // Other's cell
    else
    {
        // Special cell
        if ([message.type isEqualToNumber:@48])
        {
            PPMessageDateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dateCell" forIndexPath:indexPath];
            cell.dateLabel.text = message.content;
            return cell;
        }
        else if ([message.type isEqualToNumber:@2])
        {
            PPOtherImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"otherImageCell" forIndexPath:indexPath];
            [cell configureCellWithMessage:message chat:self.chatSessionInfo];
            cell.delegate = self;
            return cell;
        }
        // Normal cell (or bad info)
        else
        {
            PPOtherMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"otherCell" forIndexPath:indexPath];
            [cell configureCellWithMessage:message chat:self.chatSessionInfo];
            return cell;
        }
    }
}

#pragma mark - keyboard notification delegates

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];

    CGFloat height = keyboardFrame.size.height;
    
    
    self.keyboardHeightConstraint.constant = height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
    
    NSInteger lastRow = [self.messageArray count] - 1;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.keyboardHeightConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - text view delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // check for resulting height
    NSString *resultingText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if ([text isEqualToString:@"\n"]) {     // insert a character at the end to correctly judge its height
        resultingText = [NSString stringWithFormat:@"%@ ", resultingText];
    }
    CGRect sizingRect = [resultingText boundingRectWithSize:CGSizeMake(self.width, CGFLOAT_MAX)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{
                                                     NSFontAttributeName: [UIFont systemFontOfSize:14.0]}
                                           context:nil];
    
    
    return (sizingRect.size.height <= 80);
}

#pragma mark - alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSInteger price = [[alertView textFieldAtIndex:1].text integerValue];
        NSInteger stock = [[alertView textFieldAtIndex:0].text integerValue];
        
        // 簡單防錯
        if (stock > [self.product.stock integerValue])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"錯誤" message:@"數量不得大於庫存" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [alertView show];
            return;
        }
        
        NSDictionary *params = @{ @"ProductID" : self.product.productId,
                                  @"BuyerID"   : self.chatSessionInfo[@"otherPersonName"],
                                  @"Price"     : @(price),
                                  @"Stock"     : @(stock) };
        
        [[PPAPI singleton] APIBargainProduct:params callback:^(NSError *err) {
            if (err)
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"錯誤" message:@"改價發生錯誤，請稍後再試" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {
                [self updateOneProduct];
                NSString *successMessage = [NSString stringWithFormat:@"改價成功，數量：%ld，單價：$%ld", (long)stock, (long)price];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"成功" message:successMessage delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
                [alertView show];
                
                // 在聊天室裡送出特殊訊息
                NSDictionary *params = @{
                                        @"ChatId": self.chatResponse.chatId,
                                        @"IsFrom": self.chatSessionInfo[@"myMessageID"],
                                        @"Content": @"re-price success",
                                        @"TargetID": self.chatSessionInfo[@"otherPersonName"],
                                        @"Type": @"2"
                                        };
                
                [self showNewMessageWithContent:params[@"Content"] isFrom:params[@"IsFrom"] sendTime:[NSDate date]];
                
                // call API
                [[PPAPI singleton] APISendChatContent:params callback:^(NSError *err) { }];

            }
        }];
    }
}

#pragma mark - push notification new message event handler

- (void)addOtherMessage:(NSNotification *)notification
{
    PPPushNotificationResponse *response = notification.object;
    
    if ([response.chatId isEqualToString:self.chatResponse.chatId] & ![response.isFrom isEqualToString:self.chatSessionInfo[@"myMessageID"]])
    {
        [self showNewMessageWithContent:response.content isFrom:response.isFrom sendTime:response.sendTime];
    }
}

#pragma mark - Gestures and button actions

- (IBAction)tapToHideKeyboard:(id)sender
{
    [self.messageTextView resignFirstResponder];
}

- (IBAction)sendButtonWasPressed:(id)sender
{
    if ([self.messageTextView.text length] == 0){
        return;
    }
    
    // POST parameters
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];

    NSDictionary *param = @{
                            @"ChatId": self.chatResponse.chatId,
                            @"IsFrom": self.chatSessionInfo[@"myMessageID"],
                            @"Content": self.messageTextView.text,
                            @"SendTime": dateString,
                            @"TargetID": self.chatSessionInfo[@"otherPersonName"],
                            @"Type": @"1"
                            };
    
    self.messageTextView.text = @"";
    
    // 顯示訊息為傳送未完成 (紅色)
    PPAPIGetChatContentResponse *message = [self showNewMessageWithContent:param[@"Content"] isFrom:param[@"IsFrom"] sendTime:[NSDate date]];
    message.isPending = YES;
    
    // call API
    [[PPAPI singleton] APISendChatContent:param callback:^(NSError *err)
     {
        if (err)
        {
            // send chat error
        }
        else
        {
            message.isPending = NO;
            NSInteger messageIndex = [self.messageArray indexOfObject:message];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:messageIndex inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

- (void)goToProduct:(id)sender
{
    // Product is pre-fetched on viewDidLoad.  If haven't got it, don't do anything (possibly do KVO here)
    if (self.product == nil)
        return;
    
    PPProductDetailController *viewController = [[UIStoryboard storyboardWithName:@"Tab1" bundle:nil] instantiateViewControllerWithIdentifier:@"PPProductDetailController"];
    viewController.product = self.product;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)discountButtonClicked:(id)sender
{
    if (!self.product)
        return;
    
    UIAlertView *priceEntryAlertView = [[UIAlertView alloc] initWithTitle:@"更改價格" message:@"請輸入商品件數及價格" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    [priceEntryAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    UITextField *quantityField = [priceEntryAlertView textFieldAtIndex:0];
    UITextField *priceField = [priceEntryAlertView textFieldAtIndex:1];
    
    quantityField.placeholder = [NSString stringWithFormat:@"數量 (庫存：%@)", self.product.stock];
    priceField.placeholder = [NSString stringWithFormat:@"新價格 (原價：$%@)", self.product.price];
    
    [quantityField setKeyboardType:UIKeyboardTypeNumberPad];
    [priceField setKeyboardType:UIKeyboardTypeNumberPad];
    [priceField setSecureTextEntry:NO];
    
    [priceEntryAlertView show];
}

#pragma mark - Helper function

- (PPAPIGetChatContentResponse *)showNewMessageWithContent:(NSString *)content isFrom:(NSString *)isFrom sendTime:(NSDate *)sendTime
{
    // display message
    PPAPIGetChatContentResponse *message = [[PPAPIGetChatContentResponse alloc] initWithContent:content
                                                                                         isFrom:isFrom
                                                                                       sendTime:sendTime
                                                                                           type:@1];
    NSInteger addedCount = [self appendMessage:message];
    NSInteger lastRow = [self.messageArray count] - 1;
    if (addedCount == 1)
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:lastRow inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    else if (addedCount == 2)
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:lastRow - 1 inSection:0],
                                                 [NSIndexPath indexPathForRow:lastRow     inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    
    // scroll to end
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    return message;
}

- (NSInteger)appendMessage:(PPAPIGetChatContentResponse *)message
{
    NSInteger addedCount = 1;
    // if new date, add a date cell
    PPAPIGetChatContentResponse *lastMessage = [self.messageArray lastObject];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"M/dd (EEE)"];
    NSString *previousDate = [dateFormatter stringFromDate:lastMessage.sendTime];
    NSString *newDate      = [dateFormatter stringFromDate:message.sendTime];
    if (![previousDate isEqualToString:newDate])
    {
        [self.messageArray addObject:[[PPAPIGetChatContentResponse alloc] initWithContent:newDate isFrom:nil sendTime:message.sendTime type:@48]];
        addedCount += 1;
    }
    
    // actual append
    [self.messageArray addObject:message];
    
    return addedCount;
}

- (void)updateOneProduct
{
    NSDictionary *params = @{ @"ProductID" : self.chatResponse.productId };
    [[PPAPI singleton] APIGetOneProduct:params callback:^(PPAPIGetNearProductResponse *product, NSError *err)
     {
         self.product = product;
     }];
}



@end
