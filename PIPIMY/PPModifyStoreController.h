
#import <UIKit/UIKit.h>
@class PPAPIGetStoreResponse;


/**
 *  修改我的小店 MVC
 */
@interface PPModifyStoreController : UIViewController



/**
 *  商店資訊
 */
@property (nonatomic, weak) PPAPIGetStoreResponse *store;

@end
