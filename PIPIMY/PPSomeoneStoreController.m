
#import "PPAPI.h"
#import "PPUser.h"
#import "PPAWSS3.h"
#import "PPProductMediumCell.h"
#import "PPSomeoneStoreController.h"
#import "PPProductDetailController.h"
#import <UIImageView+WebCache.h>
#import "PPProductMediumCell.h"


@interface PPSomeoneStoreController()
<
    PPProtocols,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout,
    UIAlertViewDelegate
>

@property (nonatomic, weak) IBOutlet UIImageView      *userIcon;        // User 頭像
@property (nonatomic, weak) IBOutlet UILabel          *userName;        // User name
@property (nonatomic, weak) IBOutlet UILabel          *storeCity;       // Store city
@property (nonatomic, weak) IBOutlet UILabel          *storeName;       // Store name
@property (nonatomic, weak) IBOutlet UILabel          *storeType;       // Store 分類
@property (nonatomic, weak) IBOutlet UILabel          *storeIntro;      // Store 介紹
@property (nonatomic, weak) IBOutlet UILabel          *storeAverage;    // Store 平均評分
@property (nonatomic, weak) IBOutlet UILabel          *storeCount;      // Store 評分次數
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;  // 商品列表

@property (weak, nonatomic) IBOutlet UIImageView *userBackgroundImageView;

@property (nonatomic, strong) NSArray *dataSource;

@end


@implementation PPSomeoneStoreController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
    [self __callAPIGetStore];
    [self __callAPIGetSomeoneProduct];
    [self __callAPICheckTrace];
    [self __updateUserBackground];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPPProductDetailController"])
    {
        PPProductDetailController *mvc = segue.destinationViewController;
        mvc.product = sender;
    }
}


#pragma mark - UICollectionView delegate / datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetNearProductResponse *product = _dataSource[indexPath.row];
    
    PPProductMediumCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mediumCell" forIndexPath:indexPath];
    cell.tag                 = indexPath.row;
    cell.delegate            = self;
    
    [cell configureWithProduct:product];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [PPProductSmallCell layoutSizeWithCellType:DisplayCellTypeMedium InCollectionView:collectionView];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetNearProductResponse *item = _dataSource[indexPath.row];
    
    [self performSegueWithIdentifier:@"toPPProductDetailController" sender:item];
}

#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    self.title     = _memberId;
    UINib *nib     = [UINib nibWithNibName:@"PPProductMediumCell" bundle:nil];
    
    [_collectionView registerNib:nib forCellWithReuseIdentifier:@"mediumCell"];
    
    _userName.text = _memberId;
    NSURL *iconURL = [PPAWSS3 userIconWithUserId:_memberId];
    
    [_userIcon sd_setImageWithURL:iconURL placeholderImage:[UIImage imageNamed:@"nohead"]];
}

#pragma mark - call API
- (void)__callAPIGetStore
{
    NSDictionary *params = @{@"MemberID" : _memberId};

    [[PPAPI singleton]APIGetStore:params callback:^(PPAPIGetStoreResponse *result, NSError *err) {
       
        if(result)
        {
            _storeCity.text    = result.storeCity;
            _storeName.text    = result.storeName;
            _storeIntro.text   = result.storeIntro;
           
            /*
            _storeAverage.text = [NSString stringWithFormat:@"%@", result.average];
            _storeCount.text   = [NSString stringWithFormat:@"%@", result.count];
             */
            
            _storeAverage.text = [NSString stringWithFormat:@"%1.1f", [result.average floatValue]];
            _storeCount.text   = [NSString stringWithFormat:@"(%@)", result.count];
            
            NSArray *category  = [self __categories];
            _storeType.text    = category[[result.storeType integerValue]];
            
        }
    }];
}

- (void)__callAPIGetSomeoneProduct
{
    NSDictionary *params = @{@"MemberID" : _memberId};
    
    [[PPAPI singleton]APIGetSomeoneProduct:params callback:^(NSArray *products, NSError *err) {
        
        self.dataSource = products;
        
        [_collectionView reloadData];
    }];
}

- (void)__callAPICheckTrace;
{
    NSDictionary *params = @{@"TracingID" : _memberId};
    
    // 如果不是自己的商店，才有關注狀態的需求。
    
    if(![_memberId isEqualToString:[PPUser singleton].name])
    {
        [[PPAPI singleton]APICheckTrace:params callback:^(BOOL traced) {
            
            if(traced)
            {
                self.navigationItem.rightBarButtonItem = [self __disableTraceItem];
            }
            else
            {
                self.navigationItem.rightBarButtonItem = [self __traceItem];
            }
        }];

    }
}

- (void)__callAPIAddTrace
{
    // 問使用者要不要接收推播
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"關注賣家"
                                                        message:@"接收這個賣家的推播訊息？"
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                              otherButtonTitles:@"接收", @"不接收", nil];
    [alertView show];
}

- (void)__callAPIDeleteTrace
{
    NSDictionary *params = @{@"TracingID" : _memberId};
    
    [[PPAPI singleton]APIDeleteTrace:params callback:^(BOOL success) {
        
        if(success)
        {
            self.navigationItem.rightBarButtonItem = [self __traceItem];
        }
    }];
}

#pragma mark - 詢問推播 alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSNumber *push;
    switch (buttonIndex) {
        case 1:
            // 接收
            push = @2;
            break;
            
        case 2:
            // 不接收
            push = @1;
            break;
            
        default:
            // 取消
            return;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *addTime = [formatter stringFromDate:[NSDate date]];
    
    NSDictionary *params = @{ @"TracingID" : _memberId,
                              @"AddTime"   : addTime,
                              @"Push"      : push };
    
    [[PPAPI singleton]APIAddTrace:params callback:^(BOOL success) {
       
        if(success)
        {
            self.navigationItem.rightBarButtonItem = [self __disableTraceItem];
        }
    }];
}


#pragma mark - 返回類型
- (NSArray *)__categories
{
    return @[@"未知", @"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}

#pragma mark - 關注 or 取消關注 item / action
- (UIBarButtonItem *)__traceItem
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"關注"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(__traceItemDidClicked:)];
    
    return item;
}

- (void)__traceItemDidClicked:(id)sender
{
    [self __callAPIAddTrace];
}

- (UIBarButtonItem *)__disableTraceItem
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"取消關注"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(__disableTraceItemDidClicked:)];
    
    return item;
}

- (void)__disableTraceItemDidClicked:(id)sender
{
    [self __callAPIDeleteTrace];
}

- (void)__updateUserBackground
{
    NSURL *backgroundURL = [PPAWSS3 userBackgroundWithUserId:_memberId];
    
    [_userBackgroundImageView sd_setImageWithURL:backgroundURL];
}

@end
