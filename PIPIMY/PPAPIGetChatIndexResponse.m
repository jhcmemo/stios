
#import "PPAPIGetChatIndexResponse.h"

@implementation PPAPIGetChatIndexResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)updateTimeJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *timeString) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [dateFormatter dateFromString:timeString];
    }];
}

@end
