
#import "PPRatingCell.h"


@interface PPRatingCell ()

@property (nonatomic, weak) IBOutlet UILabel *userName; // 姓名
@property (nonatomic, weak) IBOutlet UILabel *score;    // 分數
@property (nonatomic, weak) IBOutlet UILabel *comment;  // 評論
@property (nonatomic, weak) IBOutlet UILabel *time;     // 時間

@end


@implementation PPRatingCell

- (void)configureWithRating:(Rating *)rating
{
    _userName.text = rating.userIDFrom;
    _score.text    = [NSString stringWithFormat:@"%@", rating.score];
    _comment.text  = rating.comment.length ? rating.comment : @"沒有評論";
    _time.text     = rating.ratingTime;
}

@end
