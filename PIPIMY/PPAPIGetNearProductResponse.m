
#import "PPAPIGetNearProductResponse.h"


@implementation PPAPIGetNearProductResponse

#pragma mark - Key Map
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

-(NSString *)getNewOldStatusStr
{
    NSString *statusStr = nil;
    
    if (_newold)
    {
        NSInteger statusInt = [self.newold integerValue];
        
        switch (statusInt)
        {
            case 1:
            {
                statusStr = @"全新品";
            }
                break;
            case 2:
            {
                statusStr = @"二手品-未使用";
            }
                break;
            case 3:
            {
                statusStr = @"二手品-使用過";
            }
                break;
            case 4:
            {
                statusStr = @"二手品-有明顯使用痕跡";
            }
                break;
            
            default:
            {
                statusStr = @"沒有狀態";
            }
                break;
        }
    }
    
    return statusStr;
}

@end
