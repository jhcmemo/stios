
#import "PPCartListFooterView.h"

@interface PPCartListFooterView ()

@property (assign, nonatomic) NSInteger section;
@property (weak, nonatomic) id<PPCartListFooterViewDelegate> delegate;

@end

@implementation PPCartListFooterView

- (void)configureFooterWithSection:(NSInteger)section delegate:(id<PPCartListFooterViewDelegate>)delegate
{
    self.section = section;
    self.delegate = delegate;
}

- (IBAction)removeButtonClicked {
    [self.delegate removeCheckedItemsInSection:self.section];
}

- (IBAction)payButtonClicked {
    [self.delegate createOrderFromSection:self.section];
}

@end
