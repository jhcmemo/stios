
#import <Mantle.h>
#import <Foundation/Foundation.h>


/**
 *  API listTraced Response
 *
 *  回傳粉絲資料
 */
@interface PPAPIListTracedResponse : MTLModel<MTLJSONSerializing>



/**
 *  名稱
 */
@property (nonatomic, readonly) NSString *memberId;

/**
 *  加入時間
 */
@property (nonatomic, readonly) NSString *addTime;

@end
