
#import "PPChatListViewController.h"
#import "PPChatListTableViewCell.h"
#import "PPChatMessageViewController.h"
#import "UIColor+SRPKit.h"
#import "SRPAlertView.h"
#import "PPUser.h"
#import "PPAPI.h"

@interface PPChatListViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UINavigationItem *chatListTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *chatAsBuyer;
@property (strong, nonatomic) NSMutableArray *chatAsSeller;

@property (strong, nonatomic) NSNumber *buyerUnreadCount;
@property (strong, nonatomic) NSNumber *sellerUnreadCount;

@property (nonatomic) BOOL isShowingBuyerList;  // if not, it's showing seller's chat list
@property (weak, nonatomic) IBOutlet UIButton *buyerListTabButton;
@property (weak, nonatomic) IBOutlet UIButton *sellerListTabButton;

@property (assign, nonatomic) CGPoint buyerListContentOffset;
@property (assign, nonatomic) CGPoint sellerListContentOffset;

@end


@implementation PPChatListViewController

#pragma mark - Life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // UI setup
    self.isShowingBuyerList = YES;
    self.buyerListTabButton.backgroundColor = [UIColor whiteColor];
    [self.buyerListTabButton setTitleColor:[UIColor srp_colorWithHEX:@"#4a9ed8"] forState:UIControlStateNormal];
    self.sellerListTabButton.backgroundColor = [UIColor srp_colorWithHEX:@"#ececec"];
    [self.sellerListTabButton setTitleColor:[UIColor srp_colorWithHEX:@"#5e5e5e"] forState:UIControlStateNormal];
    
    // remove empty separator lines
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // update user name (might have changed if re-logged in)
    self.chatListTitle.title = [PPUser singleton].name;
    
    // listen for new messages push notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadge:) name:@"NewChatMessageNotification" object:nil];
    
    // Refresh
    [self refreshChatIndex];
}

- (void)viewWillDisappear:(BOOL)animated {
    // stop listen for new messages push notification
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewChatMessageNotification" object:nil];
}

#pragma mark - Table view datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isShowingBuyerList)
        return [self.chatAsBuyer count];
    else
        return [self.chatAsSeller count];
}

#pragma mark - Table view delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PPChatListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (self.isShowingBuyerList)
        [cell configureCellForChat:self.chatAsBuyer[indexPath.row] asSeller:NO];
    else
        [cell configureCellForChat:self.chatAsSeller[indexPath.row] asSeller:YES];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"刪除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        SRPAlertView *confirmDeleteChat = [[SRPAlertView alloc] initWithTitle:@"刪除訊息" message:@"您確定要刪除此訊息？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        [confirmDeleteChat showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
            
            // 確定刪除
            if (buttonIndex == 1) {
                
                PPAPIGetChatIndexResponse *chat;
                NSDictionary *params;
                
                if (self.isShowingBuyerList) {
                    chat = _chatAsBuyer[indexPath.row];
                    params = @{ @"ChatId": chat.chatId, @"IsFrom": @"1" };  // 買家 == 1
                    [_chatAsBuyer removeObjectAtIndex:indexPath.row];
                } else {
                    chat = _chatAsSeller[indexPath.row];
                    params = @{ @"ChatId": chat.chatId, @"IsFrom": @"2" };  // 賣家 == 2
                    [_chatAsSeller removeObjectAtIndex:indexPath.row];
                }
                
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                // call API
                [[PPAPI singleton] APIDeleteChatIndex:params callback:^(NSError *err) {
                    if (!err)
                        [self refreshChatIndex];
                }];
                
            } else {
                // 取消
                [tableView setEditing:NO animated:YES];
            }
        }];
    }
}

#pragma mark - Helper functions

- (IBAction)switchToList:(UIButton *)sender {
    BOOL switchToBuyerList = (sender.tag == 0); // YES: buyer button clicked, NO: seller button clicked
    
    UIButton *activeButton   = switchToBuyerList ? self.buyerListTabButton  : self.sellerListTabButton;
    UIButton *inactiveButton = switchToBuyerList ? self.sellerListTabButton : self.buyerListTabButton;
    
    // scroll position
    
    if (!switchToBuyerList && self.isShowingBuyerList)
    {
        self.buyerListContentOffset = self.tableView.contentOffset;
        [self.tableView setContentOffset:self.sellerListContentOffset animated:NO];
    }
    else if (switchToBuyerList && !self.isShowingBuyerList)
    {
        self.sellerListContentOffset = self.tableView.contentOffset;
        [self.tableView setContentOffset:self.buyerListContentOffset animated:NO];
    }
    
    // switch
    if (switchToBuyerList != self.isShowingBuyerList)
    {
        self.isShowingBuyerList = switchToBuyerList;
        [self.tableView reloadData];
        activeButton.backgroundColor = [UIColor whiteColor];
        [activeButton setTitleColor:[UIColor srp_colorWithHEX:@"#4a9ed8"] forState:UIControlStateNormal];
        inactiveButton.backgroundColor = [UIColor srp_colorWithHEX:@"#ececec"];
        [inactiveButton setTitleColor:[UIColor srp_colorWithHEX:@"#5e5e5e"] forState:UIControlStateNormal];
    }
    else
    {
        //do nothing
        return;
    }
}

- (void)updateBadge:(NSNotification *)notification {
    
    PPPushNotificationResponse *pushResponse = notification.object;
    // Search for this chat in buyer list
    for (PPAPIGetChatIndexResponse *chat in self.chatAsBuyer)
    {
        if ([pushResponse.userId isEqualToString:chat.chatId]) {
            chat.userIDFromBadge = [@([chat.userIDFromBadge integerValue] + 1) stringValue];
            chat.lastSentence = pushResponse.content;
            chat.updateTime = pushResponse.sendTime;
            
            self.buyerUnreadCount = @([self.buyerUnreadCount integerValue] + 1);
            [self.buyerListTabButton setTitle:[NSString stringWithFormat:@"小買家 (%@)", self.buyerUnreadCount] forState:UIControlStateNormal];
            
            [self.tableView reloadData];
            return;
        }
    }
    
    // Search for this chat in seller list
    for (PPAPIGetChatIndexResponse *chat in self.chatAsSeller)
    {
        if ([pushResponse.userId isEqualToString:chat.chatId]) {
            chat.userIDToBadge = [@([chat.userIDToBadge integerValue] + 1) stringValue];
            chat.lastSentence = pushResponse.content;
            chat.updateTime = pushResponse.sendTime;
            
            self.sellerUnreadCount = @([self.sellerUnreadCount integerValue] + 1);
            [self.sellerListTabButton setTitle:[NSString stringWithFormat:@"小賣家 (%@)", self.sellerUnreadCount] forState:UIControlStateNormal];
            
            [self.tableView reloadData];
            return;
        }
    }
    
    // Not found; this is a new chat
    [self refreshChatIndex];
}

- (void)refreshChatIndex {
    
    [[PPAPI singleton]APIGetChatIndex:nil callback:^(NSArray *indices, NSError *err) {
        if(indices) {
            NSInteger buyerBadgeCounter = 0;
            NSInteger sellerBadgeCounter = 0;
            
            self.chatAsSeller = [[NSMutableArray alloc] init];
            self.chatAsBuyer = [[NSMutableArray alloc] init];
            
            // 分類買家 & 賣家
            for(PPAPIGetChatIndexResponse *chatIndex in indices) {
                if([chatIndex.userIDFrom isEqualToString:[PPUser singleton].name]) {
                    [self.chatAsBuyer addObject:chatIndex];
                    buyerBadgeCounter += [chatIndex.userIDFromBadge integerValue];
                } else {
                    [self.chatAsSeller addObject:chatIndex];
                    sellerBadgeCounter += [chatIndex.userIDToBadge integerValue];
                }
            }
            
            // tab badges
            NSInteger totalBadgeCounter = buyerBadgeCounter + sellerBadgeCounter;
            self.buyerUnreadCount = @(buyerBadgeCounter);
            self.sellerUnreadCount = @(sellerBadgeCounter);
            
            if (totalBadgeCounter > 0)
                self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld", (long)totalBadgeCounter];
            else
                self.navigationController.tabBarItem.badgeValue = nil;
            
            if (buyerBadgeCounter > 0)
                [self.buyerListTabButton setTitle:[NSString stringWithFormat:@"小買家 (%ld)", (long)buyerBadgeCounter] forState:UIControlStateNormal];
            else
                [self.buyerListTabButton setTitle:@"小買家" forState:UIControlStateNormal];
            
            if (sellerBadgeCounter > 0)
                [self.sellerListTabButton setTitle:[NSString stringWithFormat:@"小賣家 (%ld)", (long)sellerBadgeCounter] forState:UIControlStateNormal];
            else
                [self.sellerListTabButton setTitle:@"小賣家" forState:UIControlStateNormal];
            
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    PPChatMessageViewController *destinationViewController = [segue destinationViewController];
    
    NSDictionary *chatSessionInfo = [(PPChatListTableViewCell *)sender chatSessionInfo];
    destinationViewController.chatSessionInfo = chatSessionInfo;
    
    // 更新 tab badge (純顯示，未 call API)
    NSInteger totalBadgeCount = [self.navigationController.tabBarItem.badgeValue integerValue];
    totalBadgeCount -= [chatSessionInfo[@"badgeCount"] integerValue];
    if (totalBadgeCount > 0)
        self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld", (long)totalBadgeCount];
    else
        self.navigationController.tabBarItem.badgeValue = nil;
    
    // back 鍵旁拿來顯示對方 ID
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:chatSessionInfo[@"otherPersonName"] style:UIBarButtonItemStylePlain target:nil action:nil];
}

@end





