
#import "PPAPI.h"
#import "PPUser.h"
#import "PPAWSS3.h"
#import "SRPAlertView.h"
#import "SRPActionSheet.h"
#import "UIImage+SRPKit.h"
#import <UIButton+WebCache.h>
#import "PPAPIGetStoreResponse.h"
#import "PPModifyStoreController.h"


@interface PPModifyStoreController ()
<
    UINavigationControllerDelegate,
    UIImagePickerControllerDelegate
>

@property (nonatomic, weak) IBOutlet UIButton    *userIcon;     // 店主頭像
@property (nonatomic, weak) IBOutlet UITextField *storeName;    // 小店名稱
@property (nonatomic, weak) IBOutlet UIButton    *storeCity;    // 小店位置
@property (nonatomic, weak) IBOutlet UIButton    *storeType;    // 小店類型
@property (nonatomic, weak) IBOutlet UITextView  *storeIntro;   // 小店描述

@property (weak, nonatomic) IBOutlet UILabel *memberIDLbl;


@end


@implementation PPModifyStoreController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            UIImage *originalImg = info[UIImagePickerControllerEditedImage];
            UIImage *resizeImg   = [originalImg srp_resizedImageByMagick:@"320x320#"];
            NSString *fileName   = @"UserIcon.jpg";
            NSString *savePath   = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
            NSData *data         = UIImageJPEGRepresentation(resizeImg, 1.0);

            [data writeToFile:savePath atomically:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                // 上傳新頭像
                [[PPAWSS3 singleton]uploadUserIconWithUserId:[PPUser singleton].name
                                                    filePath:[NSURL fileURLWithPath:savePath]
                                                     success:^(BOOL success) {
                }];
                
                [_userIcon setBackgroundImage:resizeImg forState:UIControlStateNormal];
            });
        });
    }];
}

#pragma mark - IBAction

- (IBAction)userIconClicked:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType               = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing            = YES;
    picker.delegate                 = self;
    
    [self.tabBarController presentViewController:picker animated:YES completion:nil];
}

- (IBAction)storeCityClicked:(UIButton *)sender
{
    
    [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:[PPUser singleton].location completionHandler:^ (NSArray *placemarks, NSError *error) {
    
        [MBProgressHUD hideAllHUDsForView:self.tabBarController.view animated:YES];
        
        CLPlacemark *mark = placemarks.firstObject;
        
        // 目前 App 只在台灣, 台灣要取 city 大概就是用 subAdministrativeArea
        if(mark.subAdministrativeArea.length)
        {
            NSString *city = mark.subAdministrativeArea;
            
            [sender setTitle:city forState:UIControlStateNormal];
            [self __callAPIUpdateStoreLoc];
        }
        else
        {
            [self __showAlertViewWithMessage:@"請檢查定位狀態"];
        }
    }];
}

- (IBAction)storeTypeClicked:(UIButton *)sender
{
    NSArray *storeTypes = [self __storeTypesWithUnknow:NO];
    
    SRPActionSheet *aSheet = [[SRPActionSheet alloc]initWithTitle:@"請選擇類別"
                                                         delegate:nil
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
    
    [storeTypes enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        
        [aSheet addButtonWithTitle:title];
    }];
    
    aSheet.cancelButtonIndex = [aSheet addButtonWithTitle:@"取消"];
    
    [aSheet showFromTabBar:self.tabBarController.tabBar callback:^(UIActionSheet *sheet, NSInteger buttonIndex) {
        
        if(buttonIndex != sheet.cancelButtonIndex)
        {
            NSString *title = [sheet buttonTitleAtIndex:buttonIndex];
            
            [sender setTitle:title forState:UIControlStateNormal];
        }
    }];
}

- (IBAction)sendItemClicked:(id)sender
{
    if(!_storeName.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"請輸入店家名稱"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
  
    NSString *storeType = [_storeType titleForState:UIControlStateNormal];
    
    if([storeType isEqualToString:@"未知"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"請輸入店家類型"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    [self __callAPIUpdateStore];
}

#pragma mark - Private methods

- (void)__setup
{
    NSArray *storeTypes = [self __storeTypesWithUnknow:NO];
    //NSURL *iconURL      = [PPAWSS3 userIconWithUserId:[PPUser singleton].name];
    _storeName.text     = _store.storeName.length ? _store.storeName : @"未設定";
    _storeIntro.text    = _store.storeIntro;
    
    //[_userIcon sd_setBackgroundImageWithURL:iconURL forState:UIControlStateNormal];
    //[_storeCity setTitle:_store.storeCity forState:UIControlStateNormal];
    
    
    NSDictionary *params = @{@"MemberID" : [PPUser singleton].name};
    
    [[PPAPI singleton]APIGetStore:params callback:^(PPAPIGetStoreResponse *result, NSError *err) {
        
        if(result)
        {
            _storeName.text    = result.storeName;
            _memberIDLbl.text  = [PPUser singleton].name;
        
            
            NSString *storeTypeStr = [storeTypes objectAtIndex:[result.storeType intValue]-1];
            
            [_storeType setTitle:storeTypeStr forState:UIControlStateNormal];
            _storeIntro.text = result.storeIntro;
        }
    }];
    
    
}

- (void)__callAPIUpdateStoreLoc
{
    PPUser *user = [PPUser singleton];
    
    NSDictionary *params = @{@"StoreCity" : [_storeCity titleForState:UIControlStateNormal],
                             @"Lat"       : @(user.location.coordinate.latitude),
                             @"Lng"       : @(user.location.coordinate.longitude)
                             };
    
    [[PPAPI singleton]APIUpdateStoreLoc:params callback:^(NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            _store.storeCity = [_storeCity titleForState:UIControlStateNormal];
        }
    }];
}

- (void)__callAPIUpdateStore
{
    NSArray *productTypes       = [self __storeTypesWithUnknow:NO];
    NSString *currentType       = [_storeType titleForState:UIControlStateNormal];
    NSInteger index             = [productTypes indexOfObject:currentType];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"StoreName"]        = _storeName.text;
    params[@"StoreType"]        = @(index + 1);
    
    if(_storeIntro.text.length)
    {
        params[@"StoreIntro"] = _storeIntro.text;
    }
    
    [[PPAPI singleton]APIUpdateStore:params callback:^(NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"成功"
                                                                 message:@"更新成功"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
               
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (NSArray *)__storeTypesWithUnknow:(BOOL)flag
{
    if(flag)
    {
        return @[@"未知", @"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
    }
    
     return @[@"其他", @"3C", @"衣飾", @"書籍", @"票券", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}

@end
