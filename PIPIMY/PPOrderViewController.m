
#import "PPOrderViewController.h"
#import "PPAPICartListResponse.h"
#import "PPOrderItemsTableViewCell.h"
#import "PPOrderOptionsViewsModel.h"
#import "PPVerifyOTPViewController.h"
#import "PPCreditCardTextField.h"
#import "PPAPI.h"

@interface PPOrderViewController () <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIAlertViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) NSNumber *totalItemsCost;
@property (strong, nonatomic) NSDictionary *payParameters;

@property (nonatomic, weak) IBOutlet PPOrderOptionsViewsModel *viewsModel; // 管理金物流選項 views

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@end

@implementation PPOrderViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 判斷適用於所有商品的金流、物流方式
    // 統計總價
    
    PPAPICartListResponse *firstItem = [self.orderedItems firstObject][@"item"];
    NSInteger storeCash     = [firstItem.cash integerValue];
    NSInteger storeDelivery = [firstItem.delivery integerValue];
    NSInteger cost          = 0;
    
    for (NSDictionary *entry in self.orderedItems)
    {
        PPAPICartListResponse *item = entry[@"item"];
        cost += [entry[@"quantity"] integerValue] * [item.price integerValue];
        storeCash &= [item.cash integerValue];
        storeDelivery &= [item.delivery integerValue];
    }
    
    NSMutableArray *paymentOptions  = [[NSMutableArray alloc] init];
    NSMutableArray *shippingOptions = [[NSMutableArray alloc] init];
    for (int i = 0; (1 << i) <= storeCash; i++)
    {
        if ((1 << i) & storeCash)
            [paymentOptions addObject:@(i)];
    }
    
    for (int i = 0; (1 << i) <= storeDelivery; i++)
    {
        if ((1 << i) & storeDelivery)
            [shippingOptions addObject:@(i)];
    }
    
    // 不需顯示貨到付款 & 面交選項 (綁定金流選項)
    // 暫不使用宅配選項
    [shippingOptions removeObject:@(PPShippingOptionTypeFamilymartCOD)];
    [shippingOptions removeObject:@(PPShippingOptionType711COD)];
    [shippingOptions removeObject:@(PPShippingOptionTypeInPerson)];
    [shippingOptions removeObject:@(PPShippingOptionTypeExpress)];
    

    self.viewsModel.paymentOptions  = [paymentOptions copy];
    self.viewsModel.shippingOptions = [shippingOptions copy];
    
    // 總價
    self.totalItemsCost = @(cost);
    
    // Layout options subviews
    [self.viewsModel setupPaymentMethodsView];
    [self.viewsModel setupShippingMethodsView];
    
    // 設定商品 table 的高
    [self.tableView layoutIfNeeded];
    self.tableViewHeight.constant = [self.tableView contentSize].height;

    // 信用卡號碼的文字處理
    [self.viewsModel.creditCardNumber addTarget:self.viewsModel action:@selector(formatCreditCardNumber:) forControlEvents:UIControlEventEditingChanged];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.viewsModel.parentViewController = self;
}

#pragma mark - Table view delegate / datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.orderedItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 96.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPOrderItemsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    PPAPICartListResponse *item = self.orderedItems[indexPath.row][@"item"];
    NSNumber *quantity = self.orderedItems[indexPath.row][@"quantity"];
    
    [cell configureCellWithItem:item quantity:quantity];
    
    return cell;
}

#pragma mark - 結帳

- (IBAction)payButtonClicked
{
    // 暫時直接帶入收件人的姓名電話，未檢查
    
    // 檢查使用超商的最大金額 ($19999)
    if ([self usingStoreForpaymentOrShipping] && [self.totalItemsCost compare:@19999] == NSOrderedDescending)
    {
        [self showAlertWithTitle:@"請重新選擇" message:@"金額二萬元以上時，無法使用超商服務"];
        return;
    }
    
    // 初始參數
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"" forKey:@"subLogisticsType"];
    [params setObject:@"" forKey:@"subPaymentType"];
    [params setObject:@"" forKey:@"receiverStoreID"];
    
    // 金流、物流參數
    NSDictionary *paymentParams = [self paymentOptionParameters];
    if (!paymentParams)
        return;
    
    NSDictionary *shippingParams = [self shippingOptionParameters];
    if (!shippingParams)
        return;
    
    [params addEntriesFromDictionary:paymentParams];
    [params addEntriesFromDictionary:shippingParams];
    
    // 購物單參數 string
    NSMutableString *cartString = [NSMutableString string];
    for (NSDictionary *entry in self.orderedItems)
    {
        PPAPICartListResponse *item = entry[@"item"];
        NSString *quantity = entry[@"quantity"];
        [cartString appendString:[NSString stringWithFormat:@"%@:%@,", item.Id, quantity]];
    }
    
    [params setObject:[cartString substringToIndex:[cartString length] - 1] forKey:@"cart"];
    
    // 暫存 params 在 placeOrder 時使用
    self.payParameters = params;
    
    // 讓伺服器計算運費
    NSDictionary *costParams = @{ @"cart"          : params[@"cart"],
                                  @"paymentType"   : params[@"paymentType"],
                                  @"logisticsType" : params[@"logisticsType"] };
    [self checkTotalCost:costParams];
}

- (void)checkTotalCost:(NSDictionary *)params
{
    [[PPAPI singleton] APICartCheckPay:params callback:^(PPAPICartCheckPayResponse *response, NSError *err) {
        if ([response.productsAmount isEqualToNumber:self.totalItemsCost])
        {
            [self placeOrder];
        }
        else
        {
            [self showAlertWithTitle:@"錯誤" message:@"金額有誤，請稍候再試"];
        }
    }];
}

- (void)placeOrder
{
    if (!self.payParameters)
        return;     // 依程式流程不應發生；payParameters 在按下結帳鈕時產生
    else
    {
        NSLog(@"%@", self.payParameters);
        [self performSegueWithIdentifier:@"showCostConfirmation" sender:nil];
    }
    
    // Call API
}

- (BOOL)usingStoreForpaymentOrShipping
{
    if (!self.viewsModel.mainPaymentSelection)
        return NO;
    if (!self.viewsModel.mainShippingSelection)
        return NO;
    
    NSInteger payment = [self.viewsModel.mainPaymentSelection integerValue];
    NSInteger shipping = [self.viewsModel.mainShippingSelection integerValue];
    return payment == PPPaymentOptionType711COD || payment == PPPaymentOptionTypeFamilymartCOD || payment == PPPaymentOptionTypeConvenienceStore || shipping == PPShippingOptionType711 || shipping == PPShippingOptionType711COD || shipping == PPShippingOptionTypeFamilymart || shipping == PPShippingOptionTypeFamilymartCOD;
}

- (NSDictionary *)paymentOptionParameters
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    // 金流主選項
    if (self.viewsModel.mainPaymentSelection == nil)
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"未選擇付款方式"];
        return nil;
    }
    else
    {
        NSInteger paymentSelectionDigit = [self.viewsModel.mainPaymentSelection integerValue];
        [params setObject:@(1 << paymentSelectionDigit) forKey:@"paymentType"];
    }
    
    // 金流次選項
    switch ([self.viewsModel.mainPaymentSelection integerValue]) {
            
        case PPPaymentOptionTypeCC:
        {
            NSDictionary *creditCardParams = [self creditCardParameters];
            if (!creditCardParams)
                return nil;
            else
                [params addEntriesFromDictionary:creditCardParams];
        }
            break;
            
        case PPPaymentOptionTypeWireTransfer:
        {
            if (self.viewsModel.subPaymentSelection == nil)
            {
                [self showAlertWithTitle:@"欄位錯誤" message:@"未選擇轉帳銀行"];
                return nil;
            }
            else
            {
                NSInteger bankIndex = [self.viewsModel.subPaymentSelection integerValue];
                NSString *bankCode = [[PPOrderOptionsViewsModel bankCodes] objectAtIndex:bankIndex];
                [params setObject:bankCode forKey:@"subPaymentType"];
            }
        }
            break;
            
        case PPPaymentOptionTypeConvenienceStore:
        {
            if (self.viewsModel.subPaymentSelection == nil)
            {
                [self showAlertWithTitle:@"欄位錯誤" message:@"未選擇超商類別"];
                return nil;
            }
            else
            {
                NSInteger storeTypeIndex = [self.viewsModel.subPaymentSelection integerValue];
                NSString *storeTypeCode = [self.viewsModel.storeTypeCodes objectAtIndex:storeTypeIndex];
                [params setObject:storeTypeCode forKey:@"subPaymentType"];
            }
        }
            break;
            
        default:
            break;
    }

    return params;
}

- (NSDictionary *)creditCardParameters
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    // 信用卡卡號：可考慮用 Luhn Algorithm 檢查
    // 暫時只偵查是否為正確長度的數字串
    NSString *cleanedCreditCardNumber = [self.viewsModel.creditCardNumber.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    if ([self string:cleanedCreditCardNumber isNumericOfMinLength:16 maxLength:16])
    {
        [params setObject:cleanedCreditCardNumber forKey:@"CardNumber"];
    }
    else
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"信用卡號需為 16 個數字"];
        return nil;
    }
    
    // 有效月份
    if ([self string:self.viewsModel.creditCardExpireMonth.text isNumericOfMinLength:2 maxLength:2])
    {
        [params setObject:self.viewsModel.creditCardExpireMonth.text forKey:@"CardValidMM"];
    }
    else
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"月份需為 2 位數字"];
        return nil;
    }
    
    // 有效年份
    if ([self string:self.viewsModel.creditCardExpireYear.text isNumericOfMinLength:4 maxLength:4])
    {
        [params setObject:self.viewsModel.creditCardExpireYear.text forKey:@"CardValidYY"];
    }
    else
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"年份需為 4 位數字"];
        return nil;
    }
    
    // 安全碼
    if ([self string:self.viewsModel.creditCardCVC.text isNumericOfMinLength:3 maxLength:3])
    {
        [params setObject:self.viewsModel.creditCardCVC.text forKey:@"CardCVV2"];
    }
    else
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"安全碼需為 3 位數字"];
        return nil;
    }
    
    // 姓名
    if ([self.viewsModel.creditCardName.text length] > 0)
    {
        [params setObject:self.viewsModel.creditCardName.text forKey:@"CreditHolder"];
    }
    else
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"未輸入信用卡姓名"];
        return nil;
    }
    
    // 電話
    if ([self string:self.viewsModel.creditCardPhone.text isNumericOfMinLength:1 maxLength:10])
    {
        [params setObject:self.viewsModel.creditCardPhone.text forKey:@"PhoneNumber"];
    }
    else
    {
        if ([self.viewsModel.creditCardPhone.text length] == 0)
            [self showAlertWithTitle:@"欄位錯誤" message:@"未輸入信用卡電話"];
        else
            [self showAlertWithTitle:@"欄位錯誤" message:@"電話必須是數字"];
        return nil;
    }
    
    return params;
}

- (NSDictionary *)shippingOptionParameters
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    // 物流主選項
    if (self.viewsModel.mainShippingSelection == nil)
    {
        [self showAlertWithTitle:@"欄位錯誤" message:@"未選擇取貨方式"];
        return nil;
    }
    else
    {
        NSInteger shippingSelectionDigit = [self.viewsModel.mainShippingSelection integerValue];
        [params setObject:@(1 << shippingSelectionDigit) forKey:@"logisticsType"];
    }
    
    
    // 物流子選項
    switch ([self.viewsModel.mainShippingSelection integerValue]) {
            // 全家
        case PPShippingOptionTypeFamilymart:
        case PPShippingOptionTypeFamilymartCOD:
            if (self.viewsModel.shippingStoreSelection == nil)
            {
                [self showAlertWithTitle:@"欄位錯誤" message:@"未選擇店家"];
                return nil;
            }
            else
            {
                PPAPIAllpayGetStoreInfoResponse *store = self.viewsModel.shippingStoreSelection;
                [params setObject:@"FAMIC2C" forKey:@"subLogisticsType"];
                [params setObject:store.storeID forKey:@"receiverStoreID"];
            }
            break;
            
            // 統一
        case PPShippingOptionType711:
        case PPShippingOptionType711COD:
            if (self.viewsModel.shippingStoreSelection == nil)
            {
                [self showAlertWithTitle:@"欄位錯誤" message:@"未選擇店家"];
                return nil;
            }
            else
            {
                PPAPIAllpayGetStoreInfoResponse *store = self.viewsModel.shippingStoreSelection;
                [params setObject:@"UNIMARTC2C" forKey:@"subLogisticsType"];
                [params setObject:store.storeID forKey:@"receiverStoreID"];
            }
            break;
            
        case PPShippingOptionTypeExpress:
            [params setObject:@"TCAT" forKey:@"subLogisticsType"];
            break;
            
        default:
            break;
    }

    return params;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[PPVerifyOTPViewController class]])
         {
             PPVerifyOTPViewController *OPTViewController = segue.destinationViewController;
             // Need to read API response for tradeNo... but first need to modify API to accept this value
             OPTViewController.tradeNo = @"";
         }
}

#pragma mark - alert view delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView title] isEqualToString:@"簡訊認證"])
    {
        [self performSegueWithIdentifier:@"VerifyOTP" sender:self];
    }
    else if ([[alertView title] isEqualToString:@"成功下單"])
    {
        if (buttonIndex == OrderSuccessButtonTypeSeeList)
        {
            UITabBarController *tabBarController = [[UIStoryboard storyboardWithName:@"Tab2" bundle:nil] instantiateViewControllerWithIdentifier:@"OrderListTabBarController"];
            NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            [viewControllers removeLastObject];
            [viewControllers addObject:tabBarController];
            [self.navigationController setViewControllers:viewControllers animated:YES];
        }
        else if (buttonIndex == OrderSuccessButtonTypeBackToCart)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if ([[alertView title] isEqualToString:@"確認金額"])
    {
        if (buttonIndex != alertView.cancelButtonIndex)
        {
            [self placeOrder];
        }
    }
}

- (IBAction)dismissKeyboardOnTouch:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - text field delegates
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger resultLength = [textField.text length] + [string length] - range.length;
    
    NSInteger maxLength;
    switch (textField.tag) {
        case 1:
            maxLength = 19;
            break;
        case 2:
            maxLength = 2;
            break;
        case 3:
            maxLength = 4;
            break;
        case 4:
            maxLength = 3;
            break;
        case 5:
            maxLength = 10;
            break;
        case 6:
            maxLength = 10;
            break;
        default:
            maxLength = 0;
            break;
    }
    
    return (resultLength <= maxLength);
}

#pragma mark - helper functions

- (BOOL)string:(NSString *)string isNumericOfMinLength:(NSInteger)minLength maxLength:(NSInteger)maxLength
{
    if ([string length] < minLength)
        return NO;
    if ([string length] > maxLength)
        return NO;
    
    NSCharacterSet *numberChars = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringAsChars = [NSCharacterSet characterSetWithCharactersInString:string];
    return [numberChars isSupersetOfSet:stringAsChars];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}


@end




