
#import "PPAPI.h"
#import "PPMyFansListController.h"


@interface PPMyFansListController () <UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong) NSArray *dataSource; // 粉絲資料
@property (nonatomic, weak) IBOutlet UITableView *tableView; // 粉絲列表

@end


@implementation PPMyFansListController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __callAPIListTraced];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"發送推播" style:UIBarButtonItemStylePlain target:self action:@selector(sendBroadcast:)];
}

#pragma mark - Properties Setter
- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    
    if(!_dataSource.count)
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UILabel *emptyLabel       = [[UILabel alloc]initWithFrame:_tableView.bounds];
        emptyLabel.textAlignment  = NSTextAlignmentCenter;
        emptyLabel.text           = @"沒有粉絲";
        _tableView.backgroundView = emptyLabel;
    }
    else
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.backgroundView = nil;
    }
    
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    PPAPIListTracedResponse *item = _dataSource[indexPath.row];
    
    cell.textLabel.text = item.memberId;
    
    return cell;
}

#pragma mark - Private methods

- (void)__callAPIListTraced
{
    [[PPAPI singleton]APIListTraced:nil callback:^(NSArray *fans, NSError *err) {
       
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            self.dataSource = fans;
        }
    }];
}

- (void)sendBroadcast:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"傳送推播" message:@"請輸入要給粉絲的訊息 (限 20 個字元)" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"送出", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertView show];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if ([[alertView textFieldAtIndex:0].text length] > 20)
        return NO;
    else
        return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // 取消
    if (buttonIndex == alertView.cancelButtonIndex)
        return;
    
    NSString *message = [[alertView textFieldAtIndex:0] text];
    // 檢查訊息長度
    if ([message length] > 20)
    {
        UIAlertView *messageTooLongAlert = [[UIAlertView alloc] initWithTitle:@"訊息過長" message:@"推播訊息不得超過 20 字" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
        [messageTooLongAlert show];
        return;
    }
    
    // Call API
    NSDictionary *param = @{ @"Message" : message };
    [[PPAPI singleton]APITraceBroadcast:param callback:^(NSError *err) {
        if (!err)
        {
            UIAlertView *sendSuccessAlert = [[UIAlertView alloc] initWithTitle:@"成功發送" message:@"您的訊息已成功送出" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [sendSuccessAlert show];
        }
        else
        {
            UIAlertView *sendFailedAlert = [[UIAlertView alloc] initWithTitle:@"發送失敗" message:@"訊息發送失敗，請稍後再試" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [sendFailedAlert show];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

@end
