
#import "PPQuantityPickerViewController.h"

@interface PPQuantityPickerViewController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end

@implementation PPQuantityPickerViewController

- (void)viewWillAppear:(BOOL)animated
{
    if (self.maxStock > 0)
        [self.picker selectRow:([self.previousQuantity integerValue] - 1) inComponent:0 animated:NO];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.maxStock == 0)
        return 1;
    else
        return self.maxStock;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.maxStock == 0)
        return @"0";
    else
        return [NSString stringWithFormat:@"%ld", (long)(row + 1)];
}

- (IBAction)confirmButtonClicked {
    NSNumber *quantity;
    if (self.maxStock == 0)
        quantity = @0;
    else
        quantity = @([self.picker selectedRowInComponent:0] + 1);
    [self.delegate setQuantity:quantity forIndexPath:self.indexPath];
    [self backButtonClicked];
}

- (IBAction)backButtonClicked {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
