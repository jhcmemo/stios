
#import "PPConfirmPurchaseViewController.h"

@interface PPConfirmPurchaseViewController ()

@property (weak, nonatomic) IBOutlet UILabel *itemsSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsToBottomConstraint;

@end

@implementation PPConfirmPurchaseViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set border color here
    self.cancelButton.layer.borderColor = [UIColor colorWithRed:137.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0].CGColor;
    self.cancelButton.layer.borderWidth = 1.0;
    self.cancelButton.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // listen for keyboard show/hide
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    // stop listening for keyboard show/hide
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    CGFloat height = keyboardFrame.size.height;
    
    self.buttonsToBottomConstraint.constant = height + 8;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.buttonsToBottomConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Button actions

- (IBAction)dismissKeyboardButtonClicked {
    [self.view endEditing:YES];
}

- (IBAction)confirmButtonClicked {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"功能尚未完成" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)cancelButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
