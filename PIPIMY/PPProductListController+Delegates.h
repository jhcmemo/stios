
#import "PPProductMediumCell.h"
#import "PPProductSmallCell.h"
#import "PPProductBigCell.h"
#import "PPVideoPlayController.h"
#import "PPProductListController.h"
#import "TypeView.h"


/**
 *  將 PPProductListController delegate 拆出來
 */
@interface PPProductListController (Delegates)
<
    PPProtocols,
    UICollectionViewDelegate,
    UICollectionViewDataSource,
    UICollectionViewDelegateFlowLayout,
    UIAlertViewDelegate,
    TypeViewDelegat
>

enum ViewsTag
{
    k_Tag_searchItemAlert = 21000,
};

@end
