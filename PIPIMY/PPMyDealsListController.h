
#import <UIKit/UIKit.h>

/**
 *  交易型態
 */
typedef NS_ENUM(NSUInteger, DealType){
    /**
     *  我是賣家
     */
    DealTypeSeller,
    /**
     *  我是買家
     */
    DealTypeBuyer
};


/**
 *  我的交易列表 MVC
 */
@interface PPMyDealsListController : UIViewController



/**
 *  交易型態
 */
@property (nonatomic, assign) DealType dealType;

@end
