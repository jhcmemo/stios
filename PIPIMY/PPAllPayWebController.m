
#import "PPUser.h"
#import "SRPAlertView.h"
#import "CocoaSecurity.h"
#import "PPAllPayWebController.h"

static NSString *const ALLPAY_DEVELOPMENT_URL = @"http://member-stage.allpay.com.tw/App/Platform/BindAccount";

static NSString *const ALLPAY_HASH_KEY = @"5294y06JbISpM5x9";

static NSString *const ALLPAY_HASH_IV = @"v77hoKGq4kWxNNIS";

static NSString *const ALLPAY_PLATFORM_ID = @"2000132";

static NSString *const ALLPAY_PRODUCT_URL = @"https://member.allpay.com.tw/App/Platform/BindAccount";

static NSString *const ALLPAY_PRODUCT_HASH_KEY = @"KPW2XIv40bQaJ9B0";

static NSString *const ALLPAY_PRODUCT_HASH_IV = @"T12tvLwYRDmh1MVR";

static NSString *const ALLPAY_PRODUCT_PLATFORM_ID = @"1084329";


@interface PPAllPayWebController ()<UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end


@implementation PPAllPayWebController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    NSMutableURLRequest *request = [self __encodeRequest];
    
    [super viewDidLoad];
    [_webView loadRequest:request];
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *URL = request.URL;
    
    if([URL.scheme isEqualToString:@"allpayapi"])
    {
        NSDictionary *dic    = [self __decodeResponse:URL.lastPathComponent];
        NSInteger resultCode = [dic[@"ResultCode"]integerValue];
        
        SRPAlertView *alert;
        
        // 綁定歐付寶成功
        if(resultCode == 1)
        {
            alert = [[SRPAlertView alloc]initWithTitle:@"成功"
                                               message:@"註冊成功"
                                              delegate:nil
                                     cancelButtonTitle:@"確定"
                                     otherButtonTitles:nil];
            
            [alert showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }
        
        // 失敗
        else
        {
            alert = [[SRPAlertView alloc]initWithTitle:@"失敗"
                                               message:@"註冊失敗"
                                              delegate:nil
                                     cancelButtonTitle:@"確定"
                                     otherButtonTitles:nil];
            
            [alert showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{

}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{

}

#pragma mark - 返回 POST 帶加密參數的 Request
- (NSMutableURLRequest *)__encodeRequest
{
    NSURL *url;
    
    // 開發環境 URL
    if(DEBUG)
    {
        url = [NSURL URLWithString:ALLPAY_DEVELOPMENT_URL];
    }
    
    // 正式環境 URL
    else
    {
        url = [NSURL URLWithString:ALLPAY_PRODUCT_URL];
    }
    
    // 取得 timeStamp 並轉成 NSString
    NSTimeInterval timeStamp = [[NSDate date]timeIntervalSince1970];
    NSString *timeStampToStr = [NSString stringWithFormat:@"%.f", timeStamp];
    //ivankatest
    NSLog(@"A------timeStampToStr %@", timeStampToStr);   // Date
    NSLog(@"A------timeStamp %f", timeStamp);    // Date to Timesatmp
    NSLog(@"[PPUser singleton].phone = %@", [PPUser singleton].phone);
    NSLog(@"[PPUser singleton].Id = %@", [PPUser singleton].Id);
    
    // Request POST 第二個參數, 並須為 JSON String 並加密
    NSDictionary *platformData  = @{@"TimeStamp" : timeStampToStr,
                                    @"CellPhone" : [PPUser singleton].phone,
                                    @"ID"        : [PPUser singleton].Id};
    
    NSData *platformDataToJson  = [NSJSONSerialization dataWithJSONObject:platformData
                                                                  options:NSJSONWritingPrettyPrinted
                                                                    error:nil];
    
    // 開發環境 URL
    if(DEBUG)
    {
        // 加密過程
        NSData *key                 = [ALLPAY_HASH_KEY dataUsingEncoding:NSUTF8StringEncoding];
        NSData *iv                  = [ALLPAY_HASH_IV dataUsingEncoding:NSUTF8StringEncoding];
        CocoaSecurityResult *resutl = [CocoaSecurity aesEncryptWithData:platformDataToJson key:key iv:iv];
        NSString *toAES             = resutl.hex;
        
        // Request
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        // Request 最後參數
        NSString *params = [NSString stringWithFormat:@"PlatformID=%@&PlatformData=%@", ALLPAY_PLATFORM_ID, toAES];
        
        // 參數 to body
        NSData *body         = [params dataUsingEncoding:NSUTF8StringEncoding];
        NSString *postLength = [@(body.length) stringValue];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:body];
        
        return request;
    }
    // 正式環境 URL
    else
    {
        // 加密過程
        NSData *key                 = [ALLPAY_PRODUCT_HASH_KEY dataUsingEncoding:NSUTF8StringEncoding];
        NSData *iv                  = [ALLPAY_PRODUCT_HASH_IV dataUsingEncoding:NSUTF8StringEncoding];
        CocoaSecurityResult *resutl = [CocoaSecurity aesEncryptWithData:platformDataToJson key:key iv:iv];
        NSString *toAES             = resutl.hex;
        
        // Request
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        // Request 最後參數
        NSString *params = [NSString stringWithFormat:@"PlatformID=%@&PlatformData=%@", ALLPAY_PRODUCT_PLATFORM_ID, toAES];
        
        // 參數 to body
        NSData *body         = [params dataUsingEncoding:NSUTF8StringEncoding];
        NSString *postLength = [@(body.length) stringValue];
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:body];
        
        return request;
    }
}

#pragma mark - 返回 AllPay 回傳解密後的結果
- (NSDictionary *)__decodeResponse:(NSString *)encodeStr
{
    NSData *key                   = [ALLPAY_HASH_KEY dataUsingEncoding:NSUTF8StringEncoding];
    NSData *iv                    = [ALLPAY_HASH_IV dataUsingEncoding:NSUTF8StringEncoding];
    CocoaSecurityDecoder *decoder = [[CocoaSecurityDecoder alloc]init];
    NSData *decodeData            = [decoder hex:encodeStr];
    CocoaSecurityResult *result   = [CocoaSecurity aesDecryptWithData:decodeData key:key iv:iv];
    NSString *jonStr              = result.utf8String;
    NSData *toData                = [jonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic             = [NSJSONSerialization JSONObjectWithData:toData
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:nil];
    
    return dic;
}

@end
