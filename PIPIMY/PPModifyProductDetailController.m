
#import "PPAPI.h"
#import "PPUser.h"
#import "PPRatingCell.h"
#import "PPModifyProductDetailController.h"
#import "PPModifyProductDetailViewsModel.h"
#import "SRPAlertView.h"
#import "SRPActionSheet.h"

#define kAlertViewEnterMessage 1
#define kAlertViewMessageSent 2

enum ViewsTag
{
    k_Tag_ReportProductAlert = 21000,
};



@interface PPModifyProductDetailController () <UIAlertViewDelegate>

@property (nonatomic, strong) NSNumber *lat; // 商品經度
@property (nonatomic, strong) NSNumber *lng; // 商品緯度

@property (nonatomic, weak) IBOutlet PPModifyProductDetailViewsModel *viewsModel; // Subviews 管理的 Model





@end
@implementation PPModifyProductDetailController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
    [_viewsModel configureWithProduct:_product];
    [self __callAPIGetProductRating];

    
    
}

- (void)viewWillAppear:(BOOL)animated
{
   //self.navigationController.navigationBar.topItem.title = @"商品資訊";
}

- (void)viewDidAppear:(BOOL)animated
{
    [_viewsModel setupImageScrollerWithProduct:_product];
}

- (void)viewWillDisappear:(BOOL)animated
{
    //self.navigationController.navigationBar.topItem.title = @"";
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section)
    {
        case 0:
        {
            if (indexPath.row == 1)
            {
                return _viewsModel.s0c1Height;
            }
        }
            break;
        case 2:
        {
            return _viewsModel.scoreTableHeight;
        }
            break;
            
        default:
            break;
    }

    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - IBAction
#pragma mark 聯絡賣家
- (IBAction)contactItemDidClicked:(id)sender
{
    // 顯示可輸入訊息的對話框
    UIAlertView *startChatAlert = [[UIAlertView alloc] initWithTitle:@"交易訊息" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"送出", nil];
    startChatAlert.tag = kAlertViewEnterMessage;
    startChatAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[startChatAlert textFieldAtIndex:0] setAutocorrectionType:UITextAutocorrectionTypeYes];
    [[startChatAlert textFieldAtIndex:0] becomeFirstResponder];
    [startChatAlert show];
}

#pragma mark 加入我的最愛
- (IBAction)favoriteBtnTouchUpInside:(id)sender
{
    
    NSDateFormatter *formatter           = [[NSDateFormatter alloc]init];
    formatter.dateFormat                 = @"MM-dd";
    NSString *addToFavoriteDate          = [formatter stringFromDate:[NSDate date]];
    _product.addToFavoriteDate            = addToFavoriteDate;
    [[PPUser singleton]updateFavorites:_product];
    
}




#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    self.title = @"更改商品資訊";

    
    _lat              = _product.lat;
    _lng              = _product.lng;
    
    _priceFiled.text = [NSString stringWithFormat:@"%@", _product.price];
    
    _titleFiled.text = [NSString stringWithFormat:@"%@%@",_product.brand?
                        _product.brand:@"",_product.title?_product.title:@""];
    
    _productDesc.text = _product.des.length ? _product.des : @"沒有描述";
}


#pragma mark - Call API getProductRating
- (void)__callAPIGetProductRating
{
    NSDictionary *params = @{@"ProductID" : _product.productId};
    
    [[PPAPI singleton]APIGetProductRating:params callback:^(PPAPIGetSomeoneRatingResponse *item, NSError *err) {
       
        if(!err)
        {
            [_viewsModel updateRatingDetail:item.detail];
            [_viewsModel updateRatingDatasource:item.ratings];
            [self.tableView reloadData];
        }
    }];
}



#pragma mark - IBAction

- (IBAction)updateButtonClicked:(id)sender
{
    if(!_titleFiled.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                       message:@"請輸入 Title"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    if(!_priceFiled.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                       message:@"請輸入價格"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    [self __callAPIGetMyProductUpdate];
}

- (IBAction)deleteButtonClicked:(id)sender
{
    NSDictionary *params = @{@"ProductID" : _product.productId};
    
    [[PPAPI singleton]APIGetMyProductDelete:params callback:^(NSError *err) {
        
        if(!err)
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"成功"
                                                                 message:@"已刪除此商品"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        else
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
    }];
}


- (IBAction)updateLocationItemClicked:(id)sender
{
    self.lat = @([PPUser singleton].location.coordinate.latitude);
    self.lng = @([PPUser singleton].location.coordinate.longitude);
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"成功"
                                                   message:@"商品位置更新成功\n將在送出後更新"
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (IBAction)typeButtonDidClicked:(UIButton *)sender
{
    SRPActionSheet *aSheet = [[SRPActionSheet alloc]init];
    NSArray *types         = [self __productTypesWithUnknow:NO];
    
    [types enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        
        [aSheet addButtonWithTitle:title];
    }];
    
    aSheet.cancelButtonIndex = [aSheet addButtonWithTitle:@"取消"];
    
    [aSheet showFromTabBar:self.tabBarController.tabBar callback:^(UIActionSheet *sheet, NSInteger buttonIndex) {
        
        if(buttonIndex != sheet.cancelButtonIndex)
        {
            [sender setTitle:[sheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
        }
    }];
}


- (void)__callAPIGetMyProductUpdate
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"ProductID"] = _product.productId;
    params[@"Title"]     = _titleFiled.text;
    params[@"Price"]     = @([_priceFiled.text integerValue]); // 因為 User 可能輸入 0100 這種鬼東西
    params[@"CC"]        = @"TW";
    params[@"Lat"]       = _lat;
    params[@"Lng"]       = _lng;
    params[@"MemberID"]  = [PPUser singleton].name;
    
    params[@"Type"] = ({
        NSNumber *type = @(0);
        NSArray *types          = [self __productTypesWithUnknow:YES];
        type = @([types indexOfObject:[_viewsModel.typeButton titleForState:UIControlStateNormal]]);
    });
    
    if(_productDesc.text.length)
    {
        params[@"Des"] = _productDesc.text;
    }
    
    [[PPAPI singleton]APIGetMyProductUpdate:params callback:^(NSError *err) {
        
        if(!err)
        {
            SRPAlertView *alertView = [[SRPAlertView alloc]initWithTitle:@"成功"
                                                                 message:@"商品更新成功"
                                                                delegate:nil
                                                       cancelButtonTitle:@"確定"
                                                       otherButtonTitles:nil];
            
            [alertView showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        else
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (NSArray *)__productTypesWithUnknow:(BOOL)flag
{
    if(flag)
    {
        return @[@"未知", @"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
    }
    
    return @[@"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}


#pragma mark - UITextFieldDeleage

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = -100;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  
    [textField resignFirstResponder];
 
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = -100;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    return YES;
}




@end
