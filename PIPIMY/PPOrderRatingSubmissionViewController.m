
#import "PPAPI.h"
#import "PPOrderRatingSubmissionViewController.h"

@interface PPOrderRatingSubmissionViewController () <UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSNumber *rating;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *ratingStarButtons;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *entryAreaBottomConstraint;

@end

@implementation PPOrderRatingSubmissionViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.sendButton.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // listen for keyboard show/hide
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    
    // stop listening for keyboard show/hide
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Keyboard notification delegates

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    CGFloat height = keyboardFrame.size.height;
    
    self.entryAreaBottomConstraint.constant = height + 1;

    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.entryAreaBottomConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"錯誤"])
        return;
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Button actions

- (IBAction)cancelButtonClicked {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)starButtonClicked:(id)sender {
    
    self.sendButton.enabled = YES;
    
    NSInteger tag = ((UIButton *)sender).tag;
    
    self.rating = [NSNumber numberWithInteger:tag];
    
    for (NSInteger i = 0; i < tag; i++)
    {
        UIButton *button = self.ratingStarButtons[i];
        [button setImage:[UIImage imageNamed:@"star_on"] forState:UIControlStateNormal];
    }
    for (NSInteger i = tag; i < 5; i++)
    {
        UIButton *button = self.ratingStarButtons[i];
        [button setImage:[UIImage imageNamed:@"star_off"] forState:UIControlStateNormal];
    }
}

- (IBAction)sendButtonClicked {
    
    NSDictionary *params = @{ @"orderID"       : self.orderId,
                              @"ratingScore"   : self.rating,
                              @"ratingComment" : self.commentTextField.text };
    
    [[PPAPI singleton] APIPushOrderRating:params callback:^(NSError *err) {
        if (err)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"錯誤" message:@"評價送出失敗，請稍後再試" delegate:self cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [alertView show];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"評價已送出" message:nil delegate:self cancelButtonTitle:@"確定" otherButtonTitles:nil];
            [alertView show];
        }
    }];
}

@end
