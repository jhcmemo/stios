
#import "PPAPIGetOrderResourceResponse.h"

@implementation OrderResourceOrder

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

@end

@implementation OrderResourceItems

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

@end

@implementation OrderResourcePayment

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"bankCode"        : @"BankCode",
              @"execTime"        : @"ExecTime",
              @"expireDate"      : @"ExpireDate",
              @"merchantID"      : @"MerchantID",
              @"merchantTradeNo" : @"MerchantTradeNo",
              @"paymentNo"       : @"PaymentNo",
              @"paymentDate"     : @"PaymentDate",
              @"paymentType"     : @"PaymentType",
              @"rtnCode"         : @"RtnCode",
              @"rtnMsg"          : @"RtnMsg",
              @"simulatePaid"    : @"SimulatePaid",
              @"tradeAmt"        : @"TradeAmt",
              @"tradeDate"       : @"TradeDate",
              @"tradeNo"         : @"TradeNo",
              @"createdAt"       : @"created_at",
              @"Id"              : @"id" };
}

@end

@implementation OrderResourceLogistics

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"logisticsSubType"  : @"LogisticsSubType",
              @"logisticsType"     : @"LogisticsType",
              @"receiverCellPhone" : @"ReceiverCellPhone",
              @"receiverName"      : @"ReceiverName",
              @"receiverPhone"     : @"ReceiverPhone" };
}

@end


@implementation PPAPIGetOrderResourceResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)orderJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^(NSDictionary *orderDictionary) {
        return [MTLJSONAdapter modelOfClass:[OrderResourceOrder class] fromJSONDictionary:orderDictionary error:nil];
    }];
}

+ (NSValueTransformer *)itemsJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^(NSArray *itemsArray) {
        return [MTLJSONAdapter modelsOfClass:[OrderResourceItems class] fromJSONArray:itemsArray error:nil];
    }];
}

+ (NSValueTransformer *)paymentJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^(NSDictionary *paymentDictionary) {
        return [MTLJSONAdapter modelOfClass:[OrderResourcePayment class] fromJSONDictionary:paymentDictionary error:nil];
    }];
}

+ (NSValueTransformer *)logisticsJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^(NSDictionary *logisticsDictionary) {
        return [MTLJSONAdapter modelOfClass:[OrderResourceLogistics class] fromJSONDictionary:logisticsDictionary error:nil];
    }];
}

@end
