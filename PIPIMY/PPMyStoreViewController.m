
#import "PPAPI.h"
#import "PPUser.h"
#import "PPAWSS3.h"
#import "PPProductMediumCell.h"
#import <UIImageView+WebCache.h>
#import "PPMyStoreViewController.h"
#import "PPModifyStoreController.h"
#import "PPModifyProductController.h"


@interface PPMyStoreViewController ()
<
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout
>

@property (nonatomic, strong) NSArray               *dataSource; // 店家商品 data
@property (nonatomic, strong) PPAPIGetStoreResponse *store;      // 店家資訊

@property (nonatomic, weak) IBOutlet UIImageView      *userIcon;        // 店主 Icon
@property (nonatomic, weak) IBOutlet UILabel          *userName;        // 店主姓名
@property (nonatomic, weak) IBOutlet UILabel          *storeCity;       // 店家所在城市
@property (nonatomic, weak) IBOutlet UILabel          *storeName;       // 店名
@property (nonatomic, weak) IBOutlet UILabel          *storeType;       // 店家類型
@property (nonatomic, weak) IBOutlet UILabel          *storeIntro;      // 店家簡介
@property (nonatomic, weak) IBOutlet UILabel          *storeAverage;    // 店家平均評分
@property (nonatomic, weak) IBOutlet UILabel          *storeCount;      // 店家被評分次數
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;  // 店家商品列表

@end


@implementation PPMyStoreViewController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //[self __callAPIGetStore];//這似乎是不需要了
    
    [self __callAPIGetMyPostProduct];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPPModifyStoreController"])
    {
        PPModifyStoreController *mvc = segue.destinationViewController;
        mvc.store = _store;
    }
    else if([segue.identifier isEqualToString:@"toPPModifyProductController"])
    {
        PPModifyProductController *mvc = segue.destinationViewController;
        mvc.product = sender;
    }
}

#pragma mark - Properties Setter
- (void)setStore:(PPAPIGetStoreResponse *)store
{
    _store             = store;
    _userName.text     = [PPUser singleton].name;
    _storeCity.text    = _store.storeCity;
    _storeName.text    = _store.storeName;
    _storeIntro.text   = _store.storeIntro;
    //_storeAverage.text = [NSString stringWithFormat:@"%@", _store.average];
    _storeAverage.text = [NSString stringWithFormat:@"%1.1f", [_store.average floatValue]];
    _storeCount.text   = [NSString stringWithFormat:@"(%@)", _store.count];

    _storeType.text = ({
        NSString *result = @"";
        NSArray *storeTypes = [self __storeTypes];
        result = storeTypes[[_store.storeType integerValue]];
    });
}

#pragma mark - Delegate / DataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPProductMediumCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mediumCell" forIndexPath:indexPath];
    PPAPIGetNearProductResponse *item = _dataSource[indexPath.row];
    
    [cell configureWithProduct:item];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [PPProductSmallCell layoutSizeWithCellType:DisplayCellTypeMedium InCollectionView:collectionView];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetNearProductResponse *item = _dataSource[indexPath.row];
    
    [self performSegueWithIdentifier:@"toPPModifyProductController" sender:item];
    
}

#pragma mark - Private methods

- (void)__setup
{
    NSURL *iconURL = [PPAWSS3 userIconWithUserId:[PPUser singleton].name];
    UINib *cell    = [UINib nibWithNibName:@"PPProductMediumCell" bundle:nil];
    
    [_userIcon sd_setImageWithURL:iconURL placeholderImage:[UIImage imageNamed:@"nohead"]];
    
    [_collectionView registerNib:cell forCellWithReuseIdentifier:@"mediumCell"];
}

- (void)__callAPIGetStore
{
    NSDictionary *params = @{@"MemberID" : [PPUser singleton].name};
    
    [[PPAPI singleton]APIGetStore:params callback:^(PPAPIGetStoreResponse *result, NSError *err) {

        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            self.store = result;
        }
    }];
}

- (void)__callAPIGetMyPostProduct
{
    [[PPAPI singleton]APIGetMyPostProduct:nil callback:^(NSArray *products, NSError *err) {
       
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            self.dataSource = products;
            
            [_collectionView reloadData];
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

- (NSArray *)__storeTypes
{
    return @[@"未知", @"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}

@end
