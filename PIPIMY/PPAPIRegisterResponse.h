
#import <Mantle.h>


/**
 *  API Register 回傳的 Response
 *
 *  基本上是註冊的資訊
 */
@interface PPAPIRegisterResponse : MTLModel<MTLJSONSerializing>



/**
 *  是否成功
 */
@property (nonatomic, readonly) BOOL success;

/**
 *  database Id
 */
@property (nonatomic, readonly) NSNumber *pipimy_id;

/**
 *  狀態
 */
@property (nonatomic, readonly) NSString *result;

@end
