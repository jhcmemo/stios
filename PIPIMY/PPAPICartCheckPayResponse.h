
#import <Mantle.h>

@interface PPAPICartCheckPayResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSNumber     *amount;
@property (nonatomic, readonly) NSNumber     *deliveryCost;
@property (nonatomic, readonly) NSNumber     *productsAmount;
@property (nonatomic, readonly) NSString     *result;
@property (nonatomic, readonly) NSDictionary *storeDeliveryFees;
@property (nonatomic, readonly) NSNumber     *storeDeliveryFree;

@end
