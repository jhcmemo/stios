
#import "PPCartListTableViewCell.h"

#import "PPUser.h"
#import "PPAPI.h"
#import "PPAWSS3.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PPAPICartListResponse.h"


@interface PPCartListTableViewCell ()
    
@property (weak, nonatomic) IBOutlet UIImageView *productIcon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *onlineTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *quantityButton;
@property (weak, nonatomic) IBOutlet UIView *checkbox;
@property (weak, nonatomic) IBOutlet UILabel *checkmark;

@property (assign, nonatomic) BOOL checkmarkIsChecked;

@end

@implementation PPCartListTableViewCell

-(void)awakeFromNib
{
    // border for quantity button
    self.quantityButton.layer.borderColor = [UIColor grayColor].CGColor;
    self.quantityButton.layer.borderWidth = 1.0;
    self.quantityButton.layer.cornerRadius = 2.0;
    self.quantityButton.layer.masksToBounds = YES;
}

- (void)configureCellWithCartItem:(PPAPICartListResponse *)item quantity:(NSNumber *)quantity checked:(BOOL)checked
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    
    self.title.text = [NSString stringWithFormat:@"%@", item.title];
    self.price.text = [NSString stringWithFormat:@"NT$ %@", item.price];
    self.onlineTimeLabel.text = [NSString stringWithFormat:@"上架時間：%@", [dateFormatter stringFromDate:item.onlineTime]];
 
    [self.quantityButton setTitle:[NSString stringWithFormat:@"%@", quantity] forState:UIControlStateNormal];
    self.checkmarkIsChecked = checked;
    [self setCheckmarkColor];
    
    // 判斷議價商品，顯示正確圖片
    NSURL *productIconUrl;
    
    if ([item.hide isEqualToNumber:@4])
        productIconUrl = [PPAWSS3 imageURLWithProductId:@([item.parentId integerValue]) inIndex:1];
    else
        productIconUrl = [PPAWSS3 imageURLWithProductId:@([item.productId integerValue]) inIndex:1];
    
    [self.productIcon sd_setImageWithURL:productIconUrl];
}

- (IBAction)quantityClicked
{
    [self.delegate quantityButtonClickedInCell:self value:@1];
}

- (IBAction)checkboxClicked
{
    self.checkmarkIsChecked = !self.checkmarkIsChecked;
    [self setCheckmarkColor];
    
    if([self.delegate respondsToSelector:@selector(checkboxClickedInCell:)])
    {
        [self.delegate checkboxClickedInCell:self];
    }
}

- (void)setCheckmarkColor
{
    if (self.checkmarkIsChecked)
    {
        self.checkmark.textColor = [UIColor greenColor];
    }
    else
    {
        self.checkmark.textColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    }
}

@end
