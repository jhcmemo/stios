
#import <Mantle.h>
#import <Foundation/Foundation.h>

/**
 *  API getStore Response
 *
 *  回傳我的店家資料
 */
@interface PPAPIGetStoreResponse : MTLModel<MTLJSONSerializing>



/**
 *  是否成功
 */
@property (nonatomic, readonly) BOOL success;

/**
 *  平均評分
 */
@property (nonatomic, readonly) NSNumber *average;

/**
 *  評分數量
 */
@property (nonatomic, readonly) NSNumber *count;

/**
 *  API 回傳狀態
 */
@property (nonatomic, readonly) NSString *result;

/**
 *  金流
 */
@property (nonatomic, readonly) NSNumber *storeCash;

/**
 *  城市
 */
@property (nonatomic, copy) NSString *storeCity;

/**
 *  物流
 */
@property (nonatomic, readonly) NSNumber *storeDelivery;

/**
 *  面交地點 1
 */
@property (nonatomic, readonly) NSString *storeFaceLoc1;

/**
 *  面交地點 2
 */
@property (nonatomic, readonly) NSString *storeFaceLoc2;

/**
 *  描述
 */
@property (nonatomic, readonly) NSString *storeIntro;

/**
 *  經度
 */
@property (nonatomic, readonly) NSNumber *storeLat;

/**
 *  緯度
 */
@property (nonatomic, readonly) NSNumber *storeLng;

/**
 *  名稱
 */
@property (nonatomic, readonly) NSString *storeName;

/**
 *  類型
 */
@property (nonatomic, readonly) NSNumber *storeType;

@end
