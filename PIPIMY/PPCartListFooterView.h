
#import <UIKit/UIKit.h>

@protocol PPCartListFooterViewDelegate <NSObject>

- (void)createOrderFromSection:(NSInteger)section;
- (void)removeCheckedItemsInSection:(NSInteger)section;

@end


@interface PPCartListFooterView : UITableViewHeaderFooterView

- (void)configureFooterWithSection:(NSInteger) section delegate:(id<PPCartListFooterViewDelegate>)delegate;

@end
