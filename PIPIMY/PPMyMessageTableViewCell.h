
#import <UIKit/UIKit.h>
@class PPAPIGetChatContentResponse;

@interface PPMyMessageTableViewCell : UITableViewCell

- (void)configureCellWithMessage:(PPAPIGetChatContentResponse *)message chat:(NSDictionary *)chat;

@end
