
#import <Foundation/Foundation.h>
@class PPAPICartListResponse;

@interface PPShoppingCart : NSObject

- (void)updateCartWithBlock:(void (^)(NSError *error))callback;
- (void)updateCartWithItems:(NSArray *)items;

- (void)setQuantity:(NSNumber *)quantity forItemAtIndexPath:(NSIndexPath *)indexPath;
- (NSNumber *)quantityForItem:(PPAPICartListResponse *)item;

- (BOOL)boxCheckedForItem:(PPAPICartListResponse *)item;
- (BOOL)allBoxesCheckedForSection:(NSInteger)section;
- (void)toggleCheckBoxForItem:(PPAPICartListResponse *)item;
- (void)toggleCheckBoxForSection:(NSInteger)section;

- (NSArray *)orderListForSection:(NSInteger)section;

- (PPAPICartListResponse *)itemAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)removeItemAtIndexPath:(NSIndexPath *)indexPath;

- (NSArray *)indexPathsForCheckedItemsInSection:(NSInteger)section;
- (NSString *)removeAllItemsInSection:(NSInteger)section;   // unused
- (NSInteger)removeCheckedItemsInSection:(NSInteger)section;

- (NSString *)storeUserIdForSection:(NSInteger)section;

- (NSInteger)numberOfSections;

- (NSInteger)numberOfItemsInSection:(NSInteger)section;

@end
