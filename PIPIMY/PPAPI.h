
#import "SVHTTPClient.h"
#import "PPAPIResponse.h"
#import <MBProgressHUD.h>


/**
 *  API 管理物件
 */
@interface PPAPI : SVHTTPClient



/**
 *  返回 Singleton 物件
 *
 *  @return 返回 Singleton 物件
 */
+ (instancetype)singleton;




/**
 *  取得 default 商品列表 (getNearProduct.php)
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetNearProduct:(NSDictionary *)params callback:(void (^)(NSArray *products, NSError *err))callback;

/**
 *  取得 類別 商品列表 (getTypeProduct.php)
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetTypeProduct:(NSDictionary *)params callback:(void (^)(NSArray *products, NSError *err))callback;

/**
 *  取得 關鍵字 商品列表 (getSearchProduct.php)
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetSearchProduct:(NSDictionary *)params callback:(void (^)(NSArray *products, NSError *err))callback;

/**
 *  取得 單一 商品資訊 (getOneProduct.php)
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetOneProduct:(NSDictionary *)params callback:(void (^)(PPAPIGetNearProductResponse *product, NSError *err))callback;


/**
 *  登入
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APILogin:(NSDictionary *)params callback:(void (^)(PPAPILoginResponse *result, NSError *err))callback;

/**
 *  註冊
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIRegister:(NSDictionary *)params callback:(void (^)(PPAPIRegisterResponse *result, NSError *err))callback;

/**
 *  確認ID是否註冊
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICheckID:(NSDictionary *)params callback:(void (^)(PPAPICheckIDResponse *, NSError *))callback;

/**
 *  取回 ProductId
 *
 *  @param callback API 返回的資料
 */
- (void)APIGetProductID:(void (^)(NSNumber *newProductId, NSError *err))callback;

/**
 *  會員影片上傳完成
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIDoneVideoM:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  會員圖片上傳完成
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIDonePicM:(NSDictionary *)params callback:(void (^)(NSError *err))callback;


/**
 *  取得會員金物流設定
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetStoreFlow:(NSDictionary *)params callback:(void (^)(id jsonObject,NSError *err))callback;


/**
 *  設置會員金物流
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIUpdateStoreFlow:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  會員文字上傳完成
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIDoneTextM:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  取回店家資料
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetStore:(NSDictionary *)params callback:(void (^)(PPAPIGetStoreResponse *result, NSError *err))callback;

/**
 *  取回我刊登的商品
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetMyPostProduct:(NSDictionary *)params callback:(void (^)(NSArray *products, NSError *err))callback;

/**
 *  取回我的紛絲列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIListTraced:(NSDictionary *)params callback:(void (^)(NSArray *fans, NSError *err))callback;

/**
 *  取回我交易的列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetMyDeal:(NSDictionary *)params callback:(void (^)(NSArray *deals, NSError *err))callback;

/**
 *  取回某人評分列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetSomeoneRating:(NSDictionary *)params callback:(void (^)(PPAPIGetSomeoneRatingResponse *item, NSError *err))callback;

/**
 *  修改密碼
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIUpdatePWD:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  更新小店位置
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIUpdateStoreLoc:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  更新小店資訊
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIUpdateStore:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  刪除某個商品
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetMyProductDelete:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  修改某樣商品
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetMyProductUpdate:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  取得某商品評論
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetProductRating:(NSDictionary *)params callback:(void (^)(PPAPIGetSomeoneRatingResponse *item, NSError *err))callback;

/**
 *  把商品加入購物車
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIAddToCart:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  把商品從購物車中移除
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICartDelete:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  列出購物車內商品
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICartList:(NSDictionary *)params callback:(void (^)(NSArray *items, NSError *err))callback;

/**
 *  取得超商列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIAllpayGetStoreInfo:(NSDictionary *)params callback:(void (^)(NSArray *stores, NSError *err))callback;

/**
 *  檢查含運費的總價
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICartCheckPay:(NSDictionary *)params callback:(void (^)(PPAPICartCheckPayResponse *response, NSError *err))callback;

/**
 *  結帳
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICartPay:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  OTP 驗證
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIVerifyOTP:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  賣家輸入自寄物流單號
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIOrderSetOtherLogistics:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  買家確認收貨
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIConfirmOrderFinish:(NSDictionary *)params callback:(void (^)(PPAPIConfirmOrderFinishResponse *response, NSError *err))callback;

/**
 *  取得訂單列表 (買家)
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetOrderBuyerList:(NSDictionary *)params callback:(void (^)(NSArray *orderList, NSError *err))callback;

/**
 *  取得訂單列表 (賣家)
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetOrderSellerList:(NSDictionary *)params callback:(void (^)(NSArray *orderList, NSError *err))callback;

/**
 *  取得訂單整合明細
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetOrderResource:(NSDictionary *)params callback:(void (^)(PPAPIGetOrderResourceResponse *orderResource, NSError *err))callback;

/**
 *  買家評論訂單
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIPushOrderRating:(NSDictionary *)params callback:(void (^)(NSError *err))callback;

/**
 *  取得聊天列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetChatIndex:(NSDictionary *)params callback:(void(^)(NSArray *indices, NSError *err))callback;

/**
 *  清除某聊天未讀，並取得聊天列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICleanChatBadge:(NSDictionary *)params callback:(void (^)(NSArray *indices, NSError *err))callback;

/**
 *  刪除聊天
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIDeleteChatIndex:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  取得聊天內容
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetChatContent:(NSDictionary *)params callback:(void(^)(NSArray *indices, NSError *err))callback;

/**
 *  送出聊天內容
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APISendChatContent:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  聯絡賣家
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIStartChatContent:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  賣家發送推播給粉絲
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APITraceBroadcast:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  賣家設定議價商品
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIBargainProduct:(NSDictionary *)params callback:(void(^)(NSError *err))callback;

/**
 *  取得某人商品列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIGetSomeoneProduct:(NSDictionary *)params callback:(void(^)(NSArray *products, NSError *err))callback;

/**
 *  檢查是否關注過賣家
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APICheckTrace:(NSDictionary *)params callback:(void(^)(BOOL traced))callback;

/**
 *  關注某賣家
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIAddTrace:(NSDictionary *)params callback:(void(^)(BOOL success))callback;

/**
 *  取消關注某賣家
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIDeleteTrace:(NSDictionary *)params callback:(void(^)(BOOL success))callback;

/**
 *  取得我關注的賣家列表
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIListTracing:(NSDictionary *)params callback:(void (^)(NSArray *people, NSError *err))callback;

/**
 *  切換關注賣家的推播接收設定
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APITracePushTrigger:(NSDictionary *)params callback:(void (^)(PPAPITracePushTriggerResponse *response, NSError *err))callback;

/**
 *  登出
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APILogout:(NSDictionary *)params callback:(void(^)(BOOL success))callback;

/**
 *  檢舉商品
 *
 *  @param params   API 參數
 *  @param callback API 返回的資料
 */
- (void)APIReportProduct:(NSDictionary *)params callback:(void (^)(id jsonObject,NSError *err))callback;

@end
