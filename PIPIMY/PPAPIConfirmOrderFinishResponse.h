
#import <Mantle.h>

@interface PPAPIConfirmOrderFinishResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSDate   *confirmDate;
@property (nonatomic, readonly) NSString *orderId;
@property (nonatomic, readonly) NSNumber *pending;
@property (nonatomic, readonly) NSNumber *queue;
@property (nonatomic, readonly) NSString *result;

@property (nonatomic, readonly) BOOL success;

@end
