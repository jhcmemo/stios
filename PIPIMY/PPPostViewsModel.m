
#import "PPPostViewsModel.h"
#import "SRPActionSheet.h"


@interface PPPostViewsModel ()<UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIProgressView *videoProgress;     // 上傳影片進度
@property (nonatomic, weak) IBOutlet UIImageView    *videoStateImage;   // 上傳影片狀態


@property (nonatomic, weak) IBOutlet UITextField    *titleTextField;    // 輸入 Title
@property (nonatomic, weak) IBOutlet UIImageView    *titleStateImage;   // 輸入 Title 狀態

@property (weak, nonatomic) IBOutlet UITextField    *brandTextField;    // 輸入 brand
@property (weak, nonatomic) IBOutlet UIImageView    *brandStateImage;   // 輸入 brand 狀態


@property (nonatomic, weak) IBOutlet UITextField    *priceTextField;    // 輸入價格
@property (nonatomic, weak) IBOutlet UIImageView    *priceStateImage;   // 輸入價格狀態

@property (nonatomic, weak) IBOutlet UITextField    *stockTextField;    // 輸入庫存
@property (nonatomic, weak) IBOutlet UIImageView    *stockStateImage;   // 輸入庫存狀態

@property (nonatomic, weak) IBOutlet UIButton       *typeButton;        // 選取分類
@property (nonatomic, weak) IBOutlet UIImageView    *typeStateImage;    // 選取分類狀態


@property (weak, nonatomic) IBOutlet UIButton       *productUseStatuButton;    // 選取商品狀態
@property (weak, nonatomic) IBOutlet UIImageView    *productUseStateImage;     // 選取商品狀態狀態


@property (nonatomic, weak) IBOutlet UITextField    *snoTextField;    // 輸入 商品編號
@property (nonatomic, weak) IBOutlet UIImageView    *snoStateImage;   // 輸入 商品編號 狀態


@property (nonatomic, weak) IBOutlet UITextView     *descTextView;      // 商品描述
@property (nonatomic, weak) IBOutlet UITextField    *descPlaceholder;   // 商品描述 placeHolder

@end


@implementation PPPostViewsModel

#pragma mark - Public methods

- (void)updateVideoUploadProgress:(CGFloat)progress
{
    _videoProgress.progress = progress;
}


- (void)videoUploadSuccess:(BOOL)success
{
    _videoStateImage.backgroundColor = success ? [UIColor greenColor] : [UIColor redColor];
}


- (NSMutableDictionary *)apiParams
{
    if(!_titleTextField.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"請輸入商品名稱"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return nil;
    }
    else if(!_priceTextField.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"請輸入商品價格"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return nil;
    }
    else if([_stockTextField.text integerValue] <= 0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"請輸入商品庫存且不為 0"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return nil;
    }
    else if([[_typeButton titleForState:UIControlStateNormal]isEqualToString:@"選擇類別"])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil
                                                       message:@"請選擇商品類別"
                                                      delegate:nil
                                             cancelButtonTitle:@"確定"
                                             otherButtonTitles:nil];
        
        [alert show];
        
        return nil;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    dic[@"Title"] = _titleTextField.text;
    dic[@"Price"] = @([_priceTextField.text integerValue]);
    dic[@"Stock"] = @([_stockTextField.text integerValue]);
    dic[@"Type"]  = ({
        NSNumber *type         = @(0);
        NSArray *productTypes  = [self __productTypes];
        NSString *selectedType = [_typeButton titleForState:UIControlStateNormal];
        type                   = @([productTypes indexOfObject:selectedType] + 1); // 需要 +1, 因為後台是從 1 開始
        type;
    });
    
    
    dic[@"NewOld"]  = ({
        NSNumber *type         = @(0);
        NSArray *productTypes  = [self __productNewOldTypes];
        NSString *selectedType = [_productUseStatuButton titleForState:UIControlStateNormal];
        type                   = @([productTypes indexOfObject:selectedType] + 1); // 需要 +1, 因為後台是從 1 開始
        type;
    });

    
    // 如果有輸入描述
    if(_descTextView.text.length)
    {
        dic[@"Des"] = _descTextView.text;
    }

    // 如果有輸入品牌名稱
    if (_brandTextField.text.length)
    {
        dic[@"Brand"] = _brandTextField.text;
    }
    
    // 如果有輸入商品編號
    if (_snoTextField.text.length)
    {
        dic[@"SNO"] = _snoTextField.text;
    }
    
    return dic;
}

#pragma mark - Delegates

- (void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length)
    {
        _descPlaceholder.placeholder = nil;
    }
    else
    {
        _descPlaceholder.placeholder = @"請輸入商品描述";
    }
}

#pragma mark - IBAction

- (IBAction)textFieldEditingDidChanged:(UITextField *)sender
{
    NSUInteger length = sender.text.length;
    
    UIColor *red = [UIColor redColor];
    UIColor *green = [UIColor greenColor];

    if(length == 0)
    {
        if(sender ==  _titleTextField)
        {
            _titleStateImage.backgroundColor = red;
        }
        else if(sender == _priceTextField)
        {
            _priceStateImage.backgroundColor = red;
        }
        else if(sender == _stockTextField)
        {
            _stockStateImage.backgroundColor = red;
        }
        else if(sender == _brandTextField)
        {
            _brandStateImage.backgroundColor = red;
        }
        else if(sender == _snoTextField)
        {
            _snoStateImage.backgroundColor   = red;
        }
            
    }
    else
    {
        if(sender ==  _titleTextField)
        {
            _titleStateImage.backgroundColor = green;
        }
        else if(sender == _priceTextField)
        {
            _priceStateImage.backgroundColor = green;
        }
        else if(sender == _stockTextField)
        {
            _stockStateImage.backgroundColor = green;
        }
        else if(sender == _brandTextField)
        {
            _brandStateImage.backgroundColor = green;
        }
        else if(sender == _snoTextField)
        {
            _snoStateImage.backgroundColor   = green;
        }

    }
}

- (IBAction)typeButtonClicked:(UIButton *)sender
{
    SRPActionSheet *aSheet = [[SRPActionSheet alloc]initWithTitle:@"請選擇類別"
                                                         delegate:nil
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
    
    NSArray *productTypes = [self __productTypes];
    
    [productTypes enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        
        [aSheet addButtonWithTitle:title];
    }];
    
    aSheet.cancelButtonIndex             = [aSheet addButtonWithTitle:@"取消"];
    UIViewController *rootViewController = [[UIApplication sharedApplication]keyWindow].rootViewController;

    // 顯示在 RootViewController 比較安全
    [aSheet showInView:rootViewController.view callback:^(UIActionSheet *sheet, NSInteger buttonIndex) {
       
        if(buttonIndex != sheet.cancelButtonIndex)
        {
            _typeStateImage.backgroundColor = [UIColor greenColor];
            [sender setTitle:productTypes[buttonIndex] forState:UIControlStateNormal];
        }
    }];
}

- (IBAction)productStatusButtonClicked:(id)sender
{
    SRPActionSheet *aSheet = [[SRPActionSheet alloc]initWithTitle:@"請選擇商品狀態"
                                                         delegate:nil
                                                cancelButtonTitle:nil
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:nil];
    
    NSArray *productTypes = [self __productNewOldTypes];
    
    [productTypes enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
        
        [aSheet addButtonWithTitle:title];
    }];
    
    aSheet.cancelButtonIndex             = [aSheet addButtonWithTitle:@"取消"];
    UIViewController *rootViewController = [[UIApplication sharedApplication]keyWindow].rootViewController;
    
    // 顯示在 RootViewController 比較安全
    [aSheet showInView:rootViewController.view callback:^(UIActionSheet *sheet, NSInteger buttonIndex) {
        
        if(buttonIndex != sheet.cancelButtonIndex)
        {
            _productUseStateImage.backgroundColor = [UIColor greenColor];
            [sender setTitle:productTypes[buttonIndex] forState:UIControlStateNormal];
        }
    }];
}



#pragma mark - Private methods

- (NSArray *)__productTypes
{
    return @[@"其他", @"3C", @"衣飾", @"書籍", @"票券", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}

- (NSArray *)__productNewOldTypes
{
    return @[@"全新品", @"二手品-未使用", @"二手品-使用過", @"二手品-有明顯使用痕跡"];
}

@end
