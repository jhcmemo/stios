
#import "PPOrderOptionsViewsModel.h"
#import "PPConvenienceStores.h"
#import "PPAPIAllpayGetStoreInfoResponse.h"
#import <MBProgressHUD.h>


@interface PPOrderOptionsViewsModel () <UIActionSheetDelegate>

@property (strong, nonatomic) PPConvenienceStores *stores;

@property (weak, nonatomic) IBOutlet UIView *paymentMethodsView;
@property (weak, nonatomic) IBOutlet UIView *shippingMethodsView;


@property (weak, nonatomic) IBOutlet UIView *creditCardView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creditCardViewHeight;

@property (weak, nonatomic) UIButton *mainPaymentSelectButton;
@property (weak, nonatomic) UIButton *subPaymentSelectButton;

@property (weak, nonatomic) UIButton *mainShippingSelectButton;
@property (weak, nonatomic) UIView   *subShippingStoreSelectView;
@property (weak, nonatomic) UIButton *shippingCountySelectButton;
@property (weak, nonatomic) UIButton *shippingSubdivisionSelectButton;
@property (weak, nonatomic) UIButton *shippingStoreSelectButton;

@property (strong, nonatomic) NSArray *countyOptions;
@property (strong, nonatomic) NSArray *subdivisionOptions;
@property (strong, nonatomic) NSArray *storeOptions;

@end


@implementation PPOrderOptionsViewsModel

#pragma mark - Custom setters/getters

- (PPConvenienceStores *)stores
{
    if (!_stores)
    {
        _stores = [[PPConvenienceStores alloc] init];
    }
    return _stores;
}

- (NSArray *)paymentOptionsDefaultText
{
    return @[@"信用卡", @"超商", @"轉帳", @"餘額", @"面交", @"超商貨到付款", @"統一貨到付款"];
}

+ (NSArray *)bankNames
{
    return @[ @"台新銀行", @"華南銀行", @"玉山銀行", @"台北富邦銀行", @"台灣銀行", @"中國信託", @"第一銀行" ];
}

+ (NSArray *)bankCodes
{
    return @[ @"TAISHIN", @"HUANAN", @"ESUN", @"FUBON", @"BOT", @"CHINATRUST", @"FIRST" ];
}

- (NSArray *)storeTypeOptions
{
    return @[ @"全家超商、OK超商及萊爾富超商" ];    //, @"統一超商" ];
}

- (NSArray *)storeTypeCodes
{
    return @[ @"CVS", @"IBON" ];
}

- (NSArray *)shippingOptionsDefaultText
{
    return @[@"全家店到店", @"7-ELEVEN 店到店", @"自寄", @"宅配", @"面交", @"超商貨到付款", @"統一貨到付款", @"請選擇..."];
}


#pragma mark - 金流、物流選單初始設定

- (void)setupPaymentMethodsView
{
    UIButton *button = [self buttonWithAction:@selector(selectPaymentMethodButtonClicked:)];
    self.mainPaymentSelectButton = button;
    [self.paymentMethodsView addSubview:self.mainPaymentSelectButton];
    
    [self.paymentMethodsView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainPaymentSelectButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.paymentMethodsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    NSDictionary *views = @{ @"mainPaymentSelectButton" : self.mainPaymentSelectButton,
                             @"creditCardView"          : self.creditCardView };
    [self.paymentMethodsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[mainPaymentSelectButton]-(8@500)-[creditCardView]|" options:0 metrics:nil views:views]];
    self.creditCardViewHeight.constant = 0.0;
}

- (void)setupShippingMethodsView
{
    UIButton *button = [self buttonWithAction:@selector(selectShippingMethodButtonClicked:)];
    [button setTitle:@"請先選擇付款方式" forState:UIControlStateDisabled];
    button.enabled = NO;
    self.mainShippingSelectButton = button;
    
    [self.shippingMethodsView addSubview:self.mainShippingSelectButton];
    
    [self.shippingMethodsView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainShippingSelectButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.shippingMethodsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    NSDictionary *views = @{ @"mainShippingSelectButton" : self.mainShippingSelectButton };
    [self.shippingMethodsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[mainShippingSelectButton]-(8@500)-|" options:0 metrics:nil views:views]];
}


#pragma mark - Button actions
#pragma mark Payment

- (void)selectPaymentMethodButtonClicked:(id)sender
{
    NSMutableArray *paymentButtonsText = [NSMutableArray array];
    for (NSNumber *option in self.paymentOptions)
        [paymentButtonsText addObject:self.paymentOptionsDefaultText[[option integerValue]]];
        
    [self presentActionSheetWithTitle:@"請選擇付款方式" buttons:paymentButtonsText tag:PPOptionsActionSheetTagMainPayment];
}

- (void)paymentWireTransferButtonClicked:(id)sender
{
    // 產生選擇銀行按鈕
    self.subPaymentSelection = nil;
    UIButton *button = [self buttonWithAction:@selector(paymentWireTransferButtonClicked:)];
    self.subPaymentSelectButton = button;
    [self.paymentMethodsView addSubview:self.subPaymentSelectButton];
    
    // auto layout
    [self.paymentMethodsView addConstraint:[NSLayoutConstraint constraintWithItem:self.subPaymentSelectButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.paymentMethodsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    NSDictionary *views = @{@"subPaymentSelectButton": self.subPaymentSelectButton,
                            @"mainPaymentSelectButton": self.mainPaymentSelectButton };
    [self.paymentMethodsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mainPaymentSelectButton]-[subPaymentSelectButton]-(8@510)-|" options:0 metrics:nil views:views]];
    
    // 呈現銀行選單
    [self presentActionSheetWithTitle:@"請選擇轉帳銀行" buttons:[PPOrderOptionsViewsModel bankNames] tag:PPOptionsActionSheetTagPaymentWireTransfer];
}

- (void)paymentConvenienceStoreButtonClicked:(id)sender
{
    // 產生選擇超商類別按鈕
    self.subPaymentSelection = nil;
    UIButton *button = [self buttonWithAction:@selector(paymentConvenienceStoreButtonClicked:)];
    self.subPaymentSelectButton = button;
    [self.paymentMethodsView addSubview:self.subPaymentSelectButton];
    
    // auto layout
    [self.paymentMethodsView addConstraint:[NSLayoutConstraint constraintWithItem:self.subPaymentSelectButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.paymentMethodsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    NSDictionary *views = @{@"subPaymentSelectButton": self.subPaymentSelectButton,
                            @"mainPaymentSelectButton": self.mainPaymentSelectButton };
    [self.paymentMethodsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mainPaymentSelectButton]-[subPaymentSelectButton]-(8@510)-|" options:0 metrics:nil views:views]];
    
    // 呈現超商類別選單
    [self presentActionSheetWithTitle:@"請選擇超商類別" buttons:self.storeTypeOptions tag:PPOptionsActionSheetTagPaymentStore];
}

- (void)paymentFamilymartCODButtonClicked:(id)sender
{
    self.subPaymentSelection = nil;
    
    [self selectShippingMethodByCode:PPShippingOptionTypeFamilymartCOD];
}

- (void)payment711CODButtonClicked:(id)sender
{
    self.subPaymentSelection = nil;
    
    [self selectShippingMethodByCode:PPShippingOptionType711COD];
}

- (void)paymentInPersonButtonClicked:(id)sender
{
    self.subPaymentSelection = nil;
    
    [self selectShippingMethodByCode:PPShippingOptionTypeInPerson];
}

#pragma mark Shipping

- (void)selectShippingMethodButtonClicked:(id)sender
{
    // 先選金流才能選物流
    if (!self.mainPaymentSelection)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"選擇金流" message:@"請先選擇付費方式" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    // 呈現收件方式選單
    NSMutableArray *shippingButtonsText = [NSMutableArray array];
    for (NSNumber *option in self.shippingOptions)
        [shippingButtonsText addObject:self.shippingOptionsDefaultText[[option integerValue]]];
    [self presentActionSheetWithTitle:@"請選擇收件方式"  buttons:shippingButtonsText tag:PPOptionsActionSheetTagMainShipping];
}

- (void)shippingConvenienceStoreButtonClicked:(id)sender
{
    // 叫 PPConvenienceStore 去抓商店清店
    [self.stores retrieveStoresOfType:[self shippingStoreType]];

    // 橫向裝三個按鈕的 UIView
    [self.subShippingStoreSelectView removeFromSuperview];
    self.shippingStoreSelection = nil;
    UIView *subShippingStoreSelectView = [[UIView alloc] init];
    subShippingStoreSelectView.translatesAutoresizingMaskIntoConstraints = NO;
    self.subShippingStoreSelectView = subShippingStoreSelectView;
    [self.shippingMethodsView addSubview:subShippingStoreSelectView];
    
    // 縣市 button
    UIButton *button = [self buttonWithAction:@selector(shippingConvenienceStoreButtonClicked:)];
    self.shippingCountySelectButton = button;
    [subShippingStoreSelectView addSubview:self.shippingCountySelectButton];
    
    // auto layout
    [self.shippingMethodsView addConstraint:[NSLayoutConstraint constraintWithItem:subShippingStoreSelectView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.shippingMethodsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    NSDictionary *views = @{@"subShippingStoreSelectView": subShippingStoreSelectView,
                            @"mainShippingSelectButton": self.mainShippingSelectButton,
                            @"shippingCountySelectButton": self.shippingCountySelectButton };
    [self.shippingMethodsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mainShippingSelectButton]-[subShippingStoreSelectView]-(8@510)-|" options:0 metrics:nil views:views]];
    [self.shippingMethodsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(>=8)-[subShippingStoreSelectView]-(>=8)-|" options:0 metrics:nil views:views]];
    [subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[shippingCountySelectButton]|" options:0 metrics:nil views:views]];
    [subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[shippingCountySelectButton]-(0@500)-|" options:0 metrics:nil views:views]];
    
    // 縣市選單
    self.countyOptions = [self.stores countyListForStoreType:[self shippingStoreType]];
    [self presentActionSheetWithTitle:@"請選擇縣市"  buttons:self.countyOptions tag:PPOptionsActionSheetTagShippingCounty];
}

- (void)shippingCountyClicked:(id)sender
{
    [self.shippingSubdivisionSelectButton removeFromSuperview];
    [self.shippingStoreSelectButton removeFromSuperview];
    self.shippingStoreSelection = nil;
    
    // 鄉市鎮區 button
    UIButton *button = [self buttonWithAction:@selector(shippingCountyClicked:)];
    self.shippingSubdivisionSelectButton = button;
    [self.subShippingStoreSelectView addSubview:self.shippingSubdivisionSelectButton];
    
    // auto layout
    NSDictionary *views = @{ @"shippingCountySelectButton": self.shippingCountySelectButton, @"shippingSubdivisionSelectButton": self.shippingSubdivisionSelectButton };
    [self.subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[shippingSubdivisionSelectButton]|" options:0 metrics:nil views:views]];
    [self.subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[shippingCountySelectButton]-[shippingSubdivisionSelectButton]-(0@510)-|" options:0 metrics:nil views:views]];
    
    // 鄉市鎮區選單
    self.subdivisionOptions = [self.stores subdivisionForCounty:self.shippingCountySelection storeType:[self shippingStoreType]];
    [self presentActionSheetWithTitle:@"請選擇區域" buttons:self.subdivisionOptions tag:PPOptionsActionSheetTagShippingSubdivision];
}

- (void)shippingSubdivisionClicked:(id)sender
{
    
    NSArray *stores = [self.stores storeListForSubdivision:self.shippingSubdivisionSelection county:self.shippingCountySelection storeType:[self shippingStoreType]];
    // 若拿到 nil，開啟等待 HUD (callback 時關閉)
    if (stores == nil)
    {
        NSString *keyPath;
        if ([self shippingStoreType] == PPConvenienceStoreTypeFamilymart)
            keyPath = NSStringFromSelector(@selector(fetchedFamilymartStores));
        else if ([self shippingStoreType] == PPConvenienceStoreType711)
            keyPath = NSStringFromSelector(@selector(fetched711Stores));
        else
            return;
        
        [self.stores addObserver:self forKeyPath:keyPath options:NSKeyValueObservingOptionNew context:nil];
        
        [self __showHud];
        
        return;
    }
    else
    {
        self.storeOptions = stores;
    }
    
    [self.shippingStoreSelectButton removeFromSuperview];
    self.shippingStoreSelection = nil;
    
    // 商店 button
    UIButton *button = [self buttonWithAction:@selector(shippingSubdivisionClicked:)];
    self.shippingStoreSelectButton = button;
    [self.shippingStoreSelectButton.titleLabel setNumberOfLines:0];
    [self.shippingStoreSelectButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.subShippingStoreSelectView addSubview:self.shippingStoreSelectButton];
    
    // auto layout
    NSDictionary *views = @{ @"shippingSubdivisionSelectButton": self.shippingSubdivisionSelectButton, @"shippingStoreSelectButton": self.shippingStoreSelectButton };
    [self.subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[shippingStoreSelectButton]|" options:0 metrics:nil views:views]];
    [self.subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[shippingStoreSelectButton]-(>=0)-|" options:0 metrics:nil views:views]];
    [self.subShippingStoreSelectView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[shippingSubdivisionSelectButton]-[shippingStoreSelectButton]-(0@520)-|" options:0 metrics:nil views:views]];
    
    // 商店選單
    NSMutableArray *streetAddresses = [NSMutableArray array];
    for (PPAPIAllpayGetStoreInfoResponse *store in self.storeOptions)
    {
        NSString *address = [store.storeAddress substringFromIndex:[self.shippingSubdivisionSelection length] + 3];
        [streetAddresses addObject:[address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    [self presentActionSheetWithTitle:@"請選擇商店" buttons:streetAddresses tag:PPOptionsActionSheetTagShippingStore];
}


#pragma mark - Payment & Shipping action sheet delegates
#pragma mark 切換選項

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
        return;
    
    switch (actionSheet.tag)
    {
        case PPOptionsActionSheetTagMainPayment:
            [self selectPaymentMethodByIndex:buttonIndex];
            break;
            
        case PPOptionsActionSheetTagPaymentWireTransfer:
            [self selectBankByIndex:buttonIndex];
            break;
            
        case PPOptionsActionSheetTagPaymentStore:
            [self selectStoreTypeByIndex:buttonIndex];
            break;
        
        case PPOptionsActionSheetTagMainShipping:
            [self selectShippingMethodByIndex:buttonIndex];
            break;
            
        case PPOptionsActionSheetTagShippingCounty:
            [self selectCountyByIndex:buttonIndex];
            break;
            
        case PPOptionsActionSheetTagShippingSubdivision:
            [self selectSubdivisionByIndex:buttonIndex];
            break;
            
        case PPOptionsActionSheetTagShippingStore:
            [self selectStoreByIndex:buttonIndex];
            break;
            
        default:
            break;
    }
}

#pragma mark Payment

- (void)selectPaymentMethodByIndex:(NSInteger)buttonIndex
{
    // 啟動取貨選項按鈕
    self.mainShippingSelectButton.enabled = YES;
    
    NSInteger methodCode = [self.paymentOptions[buttonIndex] integerValue];
    [self selectPaymentMethodByCode:methodCode];
}

- (void)selectPaymentMethodByCode:(NSInteger)methodCode
{
    [self prepareForPaymentSelection:methodCode];
    
    switch (methodCode)
    {
        case PPPaymentOptionTypeCC:
            break;
            
        case PPPaymentOptionTypeConvenienceStore:
            [self paymentConvenienceStoreButtonClicked:nil];
            break;

        case PPPaymentOptionTypeFamilymartCOD:
            [self paymentFamilymartCODButtonClicked:nil];
            break;
            
        case PPPaymentOptionType711COD:
            [self payment711CODButtonClicked:nil];
            break;
            
        case PPPaymentOptionTypeWireTransfer:
            [self paymentWireTransferButtonClicked:nil];
            break;
            
        case PPPaymentOptionTypeInPerson:
            [self paymentInPersonButtonClicked:nil];
            break;
            
        default:
            break;
    }
}

- (void)prepareForPaymentSelection:(NSInteger)methodCode
{
    // 若先前金流選擇，是會同時綁定物流的，即 COD (到貨付款) 或面交，而現在改選其他金流，則重設物流
    BOOL paymentWasTied = (self.mainPaymentSelection != nil &&
                           ([self.mainPaymentSelection integerValue] == PPPaymentOptionTypeFamilymartCOD ||
                            [self.mainPaymentSelection integerValue] == PPPaymentOptionType711COD ||
                            [self.mainPaymentSelection integerValue] == PPPaymentOptionTypeInPerson));
    if (paymentWasTied && methodCode != PPPaymentOptionTypeFamilymartCOD && methodCode != PPPaymentOptionType711COD)
        [self clearShippingSelection];
    
    // 更新 UI
    self.mainPaymentSelection = [NSNumber numberWithInteger:methodCode];
    [self.mainPaymentSelectButton setTitle:self.paymentOptionsDefaultText[methodCode] forState:UIControlStateNormal];
    
    [self.subPaymentSelectButton removeFromSuperview];
    
    // 顯示/隱藏 credit card view
    if (methodCode == PPPaymentOptionTypeCC)
        self.creditCardViewHeight.constant = 193.0;
    else
        self.creditCardViewHeight.constant = 0.0;
    
    // 若選擇貨到付款，或面交，則鎖定物流
    if (methodCode == PPPaymentOptionTypeFamilymartCOD || methodCode == PPPaymentOptionType711COD)
        self.mainShippingSelectButton.enabled = NO;
    else if (methodCode == PPPaymentOptionTypeInPerson)
        self.mainShippingSelectButton.enabled = NO;
    else
        self.mainShippingSelectButton.enabled = YES;
    
}

- (void)selectBankByIndex:(NSInteger)bankIndex
{
    self.subPaymentSelection = [NSNumber numberWithInteger:bankIndex];
    [self.subPaymentSelectButton setTitle:[PPOrderOptionsViewsModel bankNames][bankIndex] forState:UIControlStateNormal];
}

- (void)selectStoreTypeByIndex:(NSInteger)storeTypeIndex
{
    self.subPaymentSelection = [NSNumber numberWithInteger:storeTypeIndex];
    [self.subPaymentSelectButton setTitle:self.storeTypeOptions[storeTypeIndex] forState:UIControlStateNormal];
}

#pragma mark Shipping

- (void)selectShippingMethodByIndex:(NSInteger)buttonIndex
{
    // 按鍵位置 -> 實際選項
    NSInteger methodCode = [self.shippingOptions[buttonIndex] integerValue];
    [self selectShippingMethodByCode:methodCode];
}

- (void)selectShippingMethodByCode:(NSInteger)methodCode
{
    // 儲存選項
    self.mainShippingSelection = [NSNumber numberWithInteger:methodCode];
    // 更新 button text
    if (self.mainShippingSelectButton.enabled)
        [self.mainShippingSelectButton setTitle:self.shippingOptionsDefaultText[methodCode] forState:UIControlStateNormal];
    else
        [self.mainShippingSelectButton setTitle:self.shippingOptionsDefaultText[methodCode] forState:UIControlStateDisabled];
    
    switch (methodCode)
    {
        case PPShippingOptionType711:
        {
            [self shippingConvenienceStoreButtonClicked:nil];
        }
            break;
            
        case PPShippingOptionTypeFamilymart:
        {
            [self shippingConvenienceStoreButtonClicked:nil];
        }
            break;
            
        case PPShippingOptionType711COD:
        {
            [self shippingConvenienceStoreButtonClicked:nil];
        }
            break;
            
        case PPShippingOptionTypeFamilymartCOD:
        {
            [self shippingConvenienceStoreButtonClicked:nil];
        }
            break;
            
        default:
            [self.subShippingStoreSelectView removeFromSuperview];
            break;
    }
}

- (void)selectCountyByIndex:(NSInteger)countyIndex
{
    self.shippingCountySelection = self.countyOptions[countyIndex];
    [self.shippingCountySelectButton setTitle:self.shippingCountySelection forState:UIControlStateNormal];
    [self shippingCountyClicked:nil];
}

- (void)selectSubdivisionByIndex:(NSInteger)subdivisionIndex
{
    self.shippingSubdivisionSelection = [self.subdivisionOptions objectAtIndex:subdivisionIndex];
    [self.shippingSubdivisionSelectButton setTitle:self.shippingSubdivisionSelection forState:UIControlStateNormal];
    [self shippingSubdivisionClicked:nil];
}

- (void)selectStoreByIndex:(NSInteger)storeIndex;
{
    self.shippingStoreSelection = [self.storeOptions objectAtIndex:storeIndex];
    NSString *title = [self.shippingStoreSelection.storeAddress substringFromIndex:[self.shippingSubdivisionSelection length] + 3];
    [self.shippingStoreSelectButton setTitle:[title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forState:UIControlStateNormal];
}

- (void)clearShippingSelection
{
    self.mainShippingSelection = nil;
    [self.mainShippingSelectButton setTitle:[self.shippingOptionsDefaultText lastObject] forState:UIControlStateNormal];
    [self.mainShippingSelectButton setUserInteractionEnabled:YES];
    [self.subShippingStoreSelectView removeFromSuperview];
}

#pragma mark - Convenience store fetch data KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSString *path;
    if ([self shippingStoreType] == PPConvenienceStoreTypeFamilymart)
        path = NSStringFromSelector(@selector(fetchedFamilymartStores));
    else if ([self shippingStoreType] == PPConvenienceStoreType711)
        path = NSStringFromSelector(@selector(fetched711Stores));
    
    if ([keyPath isEqualToString:path])
    {
        [self.stores removeObserver:self forKeyPath:keyPath];
        [self __hideHud];
        [self shippingSubdivisionClicked:nil];
    }
}

#pragma mark - helper functions

- (void)presentActionSheetWithTitle:(NSString *)title buttons:(NSArray *)buttons tag:(PPOptionsActionSheetTag)tag;
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    actionSheet.tag = tag;
    
    for (NSString *option in buttons)
        [actionSheet addButtonWithTitle:option];
    [actionSheet addButtonWithTitle:@"取消"];
    actionSheet.cancelButtonIndex = [buttons count];
    
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (UIButton *)buttonWithAction:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [button setTitle:@"請選擇..." forState:UIControlStateNormal];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (PPConvenienceStoreType)shippingStoreType
{
    if (!_mainShippingSelection)
        return -1;
    
    NSInteger selection = [self.mainShippingSelection integerValue];
    
    if (selection == PPShippingOptionType711 || selection == PPShippingOptionType711COD)
        return PPConvenienceStoreType711;
    else if (selection == PPShippingOptionTypeFamilymart || selection == PPShippingOptionTypeFamilymartCOD)
        return PPConvenienceStoreTypeFamilymart;
    else
        return -1;
}

- (void)formatCreditCardNumber:(UITextField *)textField
{
    // check how many numbers are before cursor, and calculate final cursor position
    NSInteger startPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
    NSString *beforeCursor = [[textField.text substringToIndex:startPosition] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    startPosition = [beforeCursor length];
    if (startPosition > 4)
        startPosition += (startPosition - 1) / 4;

    // recreate string
    NSMutableString *trimmedCardNumbers = [[textField.text stringByReplacingOccurrencesOfString:@"-" withString:@""] mutableCopy];
    if ([trimmedCardNumbers length] > 12)
        [trimmedCardNumbers insertString:@"-" atIndex:12];
    if ([trimmedCardNumbers length] > 8)
        [trimmedCardNumbers insertString:@"-" atIndex:8];
    if ([trimmedCardNumbers length] > 4)
        [trimmedCardNumbers insertString:@"-" atIndex:4];
    textField.text = trimmedCardNumbers;

    UITextPosition *position = [textField positionFromPosition:textField.beginningOfDocument offset:startPosition];
    [textField setSelectedTextRange:[textField textRangeFromPosition:position toPosition:position]];
}

#pragma mark - HUD
#pragma mark - 顯示 Hud
- (void)__showHud
{
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
}

#pragma mark - 隱藏 Hud
- (void)__hideHud
{
    [MBProgressHUD hideAllHUDsForView:self.parentViewController.view animated:YES];
}

@end
