
#import <UIKit/UIKit.h>
#import "PPMediaPlayerView.h"
#import "PPUser.h"
#import "PPAPI.h"
#import "PPAWSS3.h"
#import "PPRatingCell.h"
#import <UIButton+WebCache.h>
#import <UIImageView+WebCache.h>
#import "PPModifyProductDetailViewsModel.h"
#import "PPModifyProductDetailController.h"


@interface PPModifyProductDetailViewsModel ()
<
    UIScrollViewDelegate,
    UITableViewDataSource
>
{
    AVPlayerItem *playerItem;
}
@property (nonatomic, strong) NSArray  *dataSource; // 個別評分資料
@property (nonatomic, strong) AVPlayer *player;     // 影片播放器

@property (nonatomic, weak) IBOutlet UIScrollView  *imageScroller;   // 圖片 scroller
@property (nonatomic, weak) IBOutlet UIPageControl *imagePage;       // 圖片 page
@property (nonatomic, weak) IBOutlet PPPlayerView  *playerView;      // 播放器 View

@property (weak, nonatomic) IBOutlet UIView        *s0c0ContentView;//播放器 及 圖片的容器

@property (weak, nonatomic) IBOutlet UIButton      *playBtn;      //播放按鈕



@property (nonatomic, weak) IBOutlet UIButton      *userIcon;        // 商品 owner 頭像
@property (nonatomic, weak) IBOutlet UILabel       *userName;        // 商品 owner 姓名

@property (nonatomic, strong)        NSNumber      *productId;       // 商品 ID
@property (nonatomic, weak) IBOutlet UILabel       *productType;     // 商品分類
@property (nonatomic, weak) IBOutlet UILabel       *productPrice;    // 商品價格
@property (nonatomic, weak) IBOutlet UILabel       *productTitle;    // 商品名稱
@property (nonatomic, weak) IBOutlet UILabel       *productDesc;     // 商品描述
@property (weak, nonatomic) IBOutlet UILabel       *productUseStatus;//使用狀態

@property (nonatomic, weak) IBOutlet UILabel       *productTime;     // 商品上傳時間
@property (nonatomic, weak) IBOutlet UILabel       *productDistance; // 商品距離

@property (weak, nonatomic) IBOutlet UILabel *stockLbl; //庫存數量

@property (weak, nonatomic) IBOutlet UILabel       *cashLbl;         // 付款方式
@property (weak, nonatomic) IBOutlet UILabel       *deliveryLbl;     // 交貨方式
@property (weak, nonatomic) IBOutlet UIView        *s0c1LastView;    // 目的在計算s0c1 高

@property (nonatomic, weak) IBOutlet UILabel       *ratingCount;     // 商品總評分次數
@property (nonatomic, weak) IBOutlet UILabel       *ratineAverage;   // 商品總評分平均
@property (nonatomic, weak) IBOutlet UITableView   *ratingTable;     // 商品個別評分內容的 Table


@property (weak, nonatomic) IBOutlet UIButton *reportProductBtn;



@end



@implementation PPModifyProductDetailViewsModel

@synthesize typeButton;

#pragma mark - LifeCycle
- (void)dealloc
{
    [self __removeObserver];
}

#pragma mark - Properties Getter
- (CGFloat)scoreTableHeight
{
    return _ratingTable.rowHeight * _dataSource.count;
}

- (CGFloat)s0c1Height
{
    return _s0c1LastView.frame.origin.y + _s0c1LastView.frame.size.height+3;
}

#pragma mark - 初始 UI
- (void)configureWithProduct:(PPAPIGetNearProductResponse *)product
{
    UINib *nib = [UINib nibWithNibName:@"PPRatingCell" bundle:nil];
    
    [_ratingTable registerNib:nib forCellReuseIdentifier:@"ratingCell"];
    
    [self __setupPlayerViewWithProduct:product];
    [self __setupDetailWithProduct:product];
    
    
}

#pragma mark - 更新評分內容
- (void)updateRatingDetail:(RatingDetail *)detail
{
    _ratingCount.text   = [NSString stringWithFormat:@"(%@)", detail.count];
    _ratineAverage.text = [NSString stringWithFormat:@"(%1.1f)", [detail.average floatValue]];
}

#pragma mark - 更新評分資訊
- (void)updateRatingDatasource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    
    [_ratingTable reloadData];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat width  = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    _imagePage.currentPage = page;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPRatingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ratingCell"];
    Rating *rating     = _dataSource[indexPath.row];
    
    [cell configureWithRating:rating];
    
    return cell;
}

#pragma mark - IBAction
#pragma mark 播放 / 暫停影片
- (IBAction)videoButtonClicked:(id)sender
{

    if(_player.rate > 0)
    {
        [_player pause];
    }
    else
    {
        [_player seekToTime:kCMTimeZero];
        [_player play];
    }
    
    [_playerView setHidden:NO];
    [_s0c0ContentView bringSubviewToFront:_playerView];
}


#pragma mark - IBActions


/*
#pragma mark 購買
- (IBAction)buyButtonClicked:(id)sender
{
    NSDictionary *params = @{ @"ProductID" : _productId };
    [[PPAPI singleton] APIAddToCart:params callback:^(NSError *err)
    {
        if (err)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"錯誤" message:@"無法加入購物車" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
        } else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"成功加入購物車" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
        }
    }];
}
 */

#pragma mark - Private methods
#pragma mark 設置圖片
- (void)setupImageScrollerWithProduct:(PPAPIGetNearProductResponse *)product
{
    NSUInteger imageCount = [product.picNumber integerValue];
    CGFloat x             = 0.0;
    CGFloat width         = CGRectGetWidth(_imageScroller.bounds);
    CGFloat height        = CGRectGetHeight(_imageScroller.bounds);

    for(NSUInteger i= 1; i<= imageCount; i++)
    {
        CGRect frame = CGRectMake(x, 0, width, height);
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
        imageView.backgroundColor = [UIColor greenColor];
        
        [imageView setClipsToBounds:YES];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        
        NSURL *imageURL = nil;
        
        if ([product.hide integerValue]!=4)
        {
            imageURL = [PPAWSS3 imageURLWithProductId:product.productId inIndex:i];
        }
        else
        {
            imageURL = [PPAWSS3 imageURLWithProductId:@([product.parentId intValue]) inIndex:i];
        }
        
        [imageView sd_setImageWithURL:imageURL placeholderImage:nil];
        x+=width;
        
        [_imageScroller addSubview:imageView];
    }
    
    [_imageScroller setContentSize:CGSizeMake(x, height)];
    
    [_imagePage setNumberOfPages:imageCount];
}

#pragma mark - 設置播放器
- (void)__setupPlayerViewWithProduct:(PPAPIGetNearProductResponse *)product
{
    self.player          = [[AVPlayer alloc]init];
    AVPlayerLayer *layer = (AVPlayerLayer *)_playerView.layer;
    layer.player         = _player;
    layer.videoGravity   = AVLayerVideoGravityResizeAspectFill;
    NSURL *videoURL      = [PPAWSS3 videoURLWithProductId:product.productId];
    AVURLAsset *asset    = [AVURLAsset assetWithURL:videoURL];
    NSArray *keys        = @[@"playable"];
    
    [_playerView setHidden:YES];
    
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        
        playerItem = [AVPlayerItem playerItemWithAsset:asset];

        [playerItem addObserver:self forKeyPath:@"status"
                     options:NSKeyValueObservingOptionNew context:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        

        [_player replaceCurrentItemWithPlayerItem:playerItem];
    }];
}

#pragma mark - 設置商品 UI
- (void)__setupDetailWithProduct:(PPAPIGetNearProductResponse *)product
{
    CLLocation *userLoaction = [PPUser singleton].location;
    NSURL *iconURL           = [PPAWSS3 userIconWithUserId:product.ID];
    NSArray *productTypes    = [self __productTypes];
    _userName.text           = product.ID;
    //_productType.text        = productTypes[[product.type integerValue]];
    
    //typeButton.titleLabel.text = productTypes[[product.type integerValue]];
    [typeButton setTitle:productTypes[[product.type integerValue]] forState:UIControlStateNormal];
    
    
    
    _stockLbl.text           = [NSString stringWithFormat:@"剩餘:%li件",(long)[product.stock integerValue]];
    
    //_productPrice.text       = [NSString stringWithFormat:@"NT$%@", product.price];
    
    
    
    /*
    _productTitle.text       = [NSString stringWithFormat:@"%@%@",product.brand?
                                product.brand:@"",product.title?product.title:@""];
    */
    
    
    _productDesc.text        = product.des.length ? product.des : @"沒有描述";
    _productTime.text        = [product.time substringWithRange:NSMakeRange(5, 5)];
    _productUseStatus.text   = [product getNewOldStatusStr];
    _productId               = product.productId;
    _deliveryLbl.text        = [self __deliveryTypes:[product.delivery integerValue]];
    _cashLbl.text            = [self __cashTypesWithCode:[product.payment integerValue]];
    
    _productDistance.text = ({
        NSString *distance;
        CLLocationDegrees lat       = [product.lat doubleValue];
        CLLocationDegrees lng       = [product.lng doubleValue];
        CLLocation *produtcLocation = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
        CLLocationDistance dist     = [userLoaction distanceFromLocation:produtcLocation];
        double toKM                 = dist / 1000.0;
        
        distance = toKM > 10 ? @"> 10 km" : [NSString stringWithFormat:@"%.1f km", toKM];
    });

    _userIcon.layer.masksToBounds = YES;
    _userIcon.layer.cornerRadius  = 4.0;
    
    [_userIcon sd_setBackgroundImageWithURL:iconURL forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"nohead"]];

}

#pragma mark - 返回商品類型
- (NSArray *)__productTypes
{
    return @[@"未知", @"其他", @"3C", @"服飾", @"書籍", @"票卷", @"生活", @"食品", @"服務", @"美妝", @"寵物"];
}

#pragma mark - 返回金流狀態
- (NSString *)__cashTypesWithCode:(NSInteger)storeCash
{
    NSMutableString *cashTypeStr= [NSMutableString stringWithCapacity:0];
    
    NSArray *set = @[@1, @2, @4, @8, @16];
    NSArray *cashArray = @[@"信用卡",@"超商",@"轉帳",@"餘額",@"面交"];
    
    [set enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
        
        NSInteger number = [obj integerValue];
        
        if(number > storeCash)
        {
            *stop = YES;
        }
        else if((storeCash & number) > 0)
        {
            NSString *cashStr = [cashArray objectAtIndex:idx];
            
            if (idx == cashArray.count-1)
            {
                [cashTypeStr appendFormat:@"%@",cashStr];
            }
            else
            {
                [cashTypeStr appendFormat:@"%@,",cashStr];
            }
        }
    }];
    
    if (!cashTypeStr.length)
    {
        [cashTypeStr appendString:@"沒有金流資料"];
    }
    
    return cashTypeStr;
}

#pragma mark - 返回物流狀態
- (NSString *)__deliveryTypes:(NSInteger)storeDelivery
{
    NSMutableString *deliveryTypeStr= [NSMutableString stringWithCapacity:0];
    
    NSArray *set = @[@1, @2, @16];
    NSArray *deliveryArray = @[@"全家",@"7-11",@"面交"];
    
    [set enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
        
        NSInteger number = [obj integerValue];
        
        if(number > storeDelivery)
        {
            *stop = YES;
        }
        else if((storeDelivery & number) > 0)
        {
            NSString *deliveryStr = [deliveryArray objectAtIndex:idx];
            
            if (idx == deliveryArray.count-1)
            {
                 [deliveryTypeStr appendFormat:@"%@",deliveryStr];
            }
            else
            {
                [deliveryTypeStr appendFormat:@"%@,",deliveryStr];
            }
        }
    }];
    
    
    if (!deliveryTypeStr.length)
    {
        [deliveryTypeStr appendString:@"沒有物流資料"];
    }
    
    return deliveryTypeStr;
}


#pragma mark Observer


- (void)__removeObserver
{
    [playerItem removeObserver:self forKeyPath:@"status"];
    [_player pause];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"status"])
    {
        if ([playerItem status] == AVPlayerStatusReadyToPlay)
        {
            [self.playBtn setHidden:NO];
        }
        else if ([playerItem status] == AVPlayerStatusFailed)
        {
            [self.playBtn setHidden:YES];
        }
    }
}


- (void)playDidEnd:(id)sender
{
    [_playBtn setHidden:NO];
    [_playerView setHidden:YES];
    [_s0c0ContentView bringSubviewToFront:_imageScroller];
    [_s0c0ContentView bringSubviewToFront:_imagePage];
    [_s0c0ContentView bringSubviewToFront:_playBtn];
}

-(BOOL)textViewDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    //CGRect frame = self.view.frame; frame.origin.y = -100;
    //[self.view setFrame:frame];
    [UIView commitAnimations];
    return YES;
}

@end
