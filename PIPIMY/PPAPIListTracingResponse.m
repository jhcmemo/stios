
#import "PPAPIListTracingResponse.h"


@implementation PPAPIListTracingResponse

#pragma mark - Key Map
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)pushJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *pushString) {
        return @([pushString integerValue]);
    }];
}

@end
