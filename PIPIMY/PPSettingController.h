
#import <UIKit/UIKit.h>


/**
 *  設定 MVC
 */
@interface PPSettingController : UITableViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *userBackgroundBtn;

@property (weak, nonatomic) IBOutlet UIButton *userIconBtn;



@end
