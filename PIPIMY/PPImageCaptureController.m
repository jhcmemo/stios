
#import "SRPAlertView.h"
#import "UIImage+SRPKit.h"
#import "SRPActionSheet.h"
#import "PPMediaPlayerView.h"
#import "PPImageCaptureController.h"


@interface PPImageCaptureController ()

@property (nonatomic, weak) IBOutlet UIButton      *uploadButton;// 上傳的按鈕
@property (nonatomic, weak) IBOutlet PPCaptureView *captureView; // 客制的相機 display View

@property (nonatomic, strong) AVCaptureSession          *session;       // 相機 Session
@property (nonatomic, strong) AVCaptureStillImageOutput *output;        // 拍照 Output
@property (nonatomic, strong) NSMutableDictionary       *uploadFiles;   // 上傳的圖片檔名
@property (nonatomic, assign) NSInteger                 takePictureIndex; // 目前拍到哪張照片的 index

/*
 *  拍完照的 Button
 *
 *  注意 imageButtons 的 IBOutlet 需要依序關聯到 UIButton
 */
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *imageButtons;

@end


@implementation PPImageCaptureController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(![_session isRunning])
    {
        [_session startRunning];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if([_session isRunning])
    {
        [_session stopRunning];
    }
}

#pragma mark - Properties Getter
- (void)setTakePictureIndex:(NSInteger)takePictureIndex
{
    /*
     *  takePictureIndex 不可能 > _imageButtons.count
     *  大於的話就設成 0
     */
    _takePictureIndex = takePictureIndex >= _imageButtons.count ? 0 : takePictureIndex;
}

#pragma mark - IBAction

- (IBAction)uploadButtonClicked:(id)sender
{
    if([_delegate respondsToSelector:@selector(imageCaptureController:uploadButtonClicked:)])
    {
        [_delegate imageCaptureController:self uploadButtonClicked:_uploadFiles.allValues];
    }
}

- (IBAction)captureButtonClicked:(id)sender
{
    AVCaptureConnection *connection = [_output connectionWithMediaType:AVMediaTypeVideo];

    [_output captureStillImageAsynchronouslyFromConnection:connection completionHandler:^
     (CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
         
         NSData *imageData    = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
         
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
         
             // 拍完照後縮放成 320 x 320, 並 save to tmp/tempImageN.jpg
             
             UIImage *tempImage   = [[UIImage alloc] initWithData:imageData];
             UIImage *resizeImage = [tempImage srp_resizedImageByMagick:@"320x320#"];
             NSData *saveData     = UIImageJPEGRepresentation(resizeImage, 1.0);
             NSString *fileName   = [NSString stringWithFormat:@"tempImage%@.jpg", @(_takePictureIndex)];
             NSString *savePath   = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
             
             if([saveData writeToFile:savePath atomically:YES])
             {
                 _uploadFiles[@(_takePictureIndex)] = fileName;
             }

             dispatch_async(dispatch_get_main_queue(), ^{
                
                 [self __updateImageButtons];
             });
        });
    }];
}

- (IBAction)imageButtonsDidClicked:(UIButton *)sender
{
    SRPActionSheet *actionSheet = [[SRPActionSheet alloc]initWithTitle:nil
                                                               delegate:nil
                                                      cancelButtonTitle:@"取消"
                                                 destructiveButtonTitle:@"刪除"
                                                      otherButtonTitles:nil];
    
    [actionSheet showFromTabBar:self.tabBarController.tabBar callback:^(UIActionSheet *sheet, NSInteger buttonIndex) {
         
         if(buttonIndex == sheet.destructiveButtonIndex)
         {
             NSInteger index = [_imageButtons indexOfObject:sender];
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                 
                 NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:_uploadFiles[@(index)]];
                 
                 if([[NSFileManager defaultManager]removeItemAtPath:filePath error:nil])
                 {
                     [_uploadFiles removeObjectForKey:@(index)];
                 }

                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     [self __updateImageButtons];
                 });
             });
         }
    }];
}

#pragma mark - Private methods

- (void)__setup
{
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        
        if(granted)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                [self __setupCamera];
                
                dispatch_async(dispatch_get_main_queue(), ^{
               
                    [self __updateImageButtons];
                });
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self __showAlertMessageForDenyUsingCamera];
            });
        }
    }];
}

- (void)__setupCamera
{
    _takePictureIndex           = -1;
    _uploadFiles                = [NSMutableDictionary dictionary];
    AVCaptureDevice *device     = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    _session                    = [[AVCaptureSession alloc]init];
    _output                     = [[AVCaptureStillImageOutput alloc]init];
    
    [_session setSessionPreset:AVCaptureSessionPreset640x480];
    [_session addInput:input];
    [_output setOutputSettings:@{AVVideoCodecKey :AVVideoCodecJPEG}];
    [_session addOutput:_output];
    
    AVCaptureVideoPreviewLayer *layer = (AVCaptureVideoPreviewLayer *)_captureView.layer;
    layer.videoGravity                = AVLayerVideoGravityResizeAspectFill;
    layer.session                     = _session;
    
    /*
     *  從暫存的地方抓回圖片檔名, 塞到 _uploadFiles, 結構為:
     *  {
     *      0 = "tempImage0.jpg",
     *      ...
     *      N = "tempImageN.jpg"
     *  }
     */
    [_imageButtons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *fileName = [NSString stringWithFormat:@"tempImage%@.jpg", @(idx)];
        NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        
        if([[NSFileManager defaultManager]fileExistsAtPath:filePath])
        {
            _uploadFiles[@(idx)] = fileName;
        }
    }];
}

- (void)__updateImageButtons
{
    _uploadButton.enabled = _uploadFiles.count;
    
    [_imageButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
        
        button.enabled = NO;
        NSString *fileName = _uploadFiles[@(idx)];
        NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        UIImage *image     = [UIImage imageWithContentsOfFile:filePath];
        
        if(image)
        {
            [button setBackgroundImage:image forState:UIControlStateNormal];
            
            button.enabled = YES;
        }
        else
        {
            [button setBackgroundImage:nil forState:UIControlStateNormal];
        }
    }];
    
    NSInteger index = [self __imageButtonHasNoImageAtIndex];
    
    if(index < 0)
    {
        self.takePictureIndex++;
    }
    else
    {
        self.takePictureIndex = index;
    }
}

- (NSInteger)__imageButtonHasNoImageAtIndex
{
    // -1 代表每個按鈕都有圖片
    
    __block NSInteger result = -1;
    
    [_imageButtons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
        
        UIImage *image = [button backgroundImageForState:UIControlStateNormal];
        
        if(!image)
        {
            result = idx;
            *stop = YES;
        }
    }];

    return result;
}

- (void)__showAlertMessageForDenyUsingCamera
{
    SRPAlertView *alert = [[SRPAlertView alloc]initWithTitle:nil
                                                     message:@"請至設定允許使用相機"
                                                    delegate:nil
                                           cancelButtonTitle:@"確定"
                                           otherButtonTitles:nil];
    
    [alert showWithCallback:^(UIAlertView *alert, NSInteger buttonIndex) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

@end
