
#import "AppDelegate.h"
#import "PPCartListViewController.h"
#import "UIColor+SRPKit.h"
#import "SRPAlertView.h"
#import "PPAPI.h"
#import "PPCartListTableViewCell.h"
#import "PPShoppingCart.h"
#import "PPOrderViewController.h"
#import "PPQuantityPickerViewController.h"
#import "PPCartListHeaderView.h"
#import "PPCartListFooterView.h"

@interface PPCartListViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, PPCartListTableViewCellDelegate, PPQuantityPickerDelegate, PPCartListHeaderViewDelegate, PPCartListFooterViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) PPShoppingCart *cart;

@end

@implementation PPCartListViewController

#pragma mark - Life cycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.cart = [[PPShoppingCart alloc] init];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PPCartListHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"HeaderView"];
    [self.tableView registerNib:[UINib nibWithNibName:@"PPCartListFooterView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"FooterView"];
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    [self.cart updateCartWithBlock:^(NSError *error) {
        
        if (!error)
            [self.tableView reloadData];
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.cart numberOfSections];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 56.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    PPCartListHeaderView *headerView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"HeaderView"];
    
    [headerView configureHeaderWithID:[self.cart storeUserIdForSection:section] section:section delegate:self checked:[self.cart allBoxesCheckedForSection:section]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 48.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    PPCartListFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"FooterView"];
    
    [footerView configureFooterWithSection:section delegate:self];
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 116.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPCartListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.delegate = self;
    PPAPICartListResponse *item = [self.cart itemAtIndexPath:indexPath];
    NSNumber *quantity = [self.cart quantityForItem:item];
    BOOL checked = [self.cart boxCheckedForItem:item];
    [cell configureCellWithCartItem:item quantity:quantity checked:checked];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.cart numberOfItemsInSection:section];
}

#pragma mark - Table view delegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"刪除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSInteger itemsLeft = [self.cart removeItemAtIndexPath:indexPath];
        
        if (itemsLeft == 0)
        {
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            // 重整 table，不然在 header & footer 的按鍵 tag 沒有更新成新的 section number
            [self.tableView reloadData];
        }
        else
        {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}


#pragma mark - PPCartListTableViewCell delegate

- (void)quantityButtonClickedInCell:(PPCartListTableViewCell *)cell value:(NSNumber *)value
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    PPAPICartListResponse *item = [self.cart itemAtIndexPath:indexPath];
    
    // 如果是議價商品，則不能選擇數量
    if ([item.hide isEqualToNumber:@4])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"不可更改數量" message:@"此為議價商品，不可更改數量" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil];
        [alertView show];
        
        return;
    }
    
    // quantity picker
    PPQuantityPickerViewController *pickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PPQuantityPickerViewController"];
    pickerViewController.maxStock = [item.stock integerValue];
    pickerViewController.delegate = self;
    pickerViewController.indexPath = indexPath;
    pickerViewController.previousQuantity = [self.cart quantityForItem:item];
    
    [self addChildViewController:pickerViewController];
    [self.view addSubview:pickerViewController.view];
    [pickerViewController didMoveToParentViewController:self];
}

- (void)checkboxClickedInCell:(PPCartListTableViewCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    PPAPICartListResponse *item = [self.cart itemAtIndexPath:indexPath];
    [self.cart toggleCheckBoxForItem:item];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Header view delegate
- (void)checkAllForSection:(NSInteger)section
{
    [self.cart toggleCheckBoxForSection:section];
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Footer view delegate
- (void)createOrderFromSection:(NSInteger)section
{
    NSArray *orderList = [self.cart orderListForSection:section];
    
    if ([orderList count] > 0)
    {
        [self performSegueWithIdentifier:@"createOrder" sender:orderList];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"請選擇商品" message:@"請選擇欲購買的商品" delegate:nil cancelButtonTitle:@"確定" otherButtonTitles: nil];
        [alertView show];
    }
}

- (void)removeCheckedItemsInSection:(NSInteger)section
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"刪除商品" message:@"您確定要刪除勾選的商品嗎？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
    alertView.tag = section;
    [alertView show];
}

#pragma mark - Alert view delegate (remove section items)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex)
        return;
    
    if ([alertView.title isEqualToString:@"刪除商品"])
    {
        NSInteger section = alertView.tag;
        NSArray *indexPaths = [self.cart indexPathsForCheckedItemsInSection:section];
        NSInteger itemsLeft = [self.cart removeCheckedItemsInSection:section];
        
        if (itemsLeft == 0)
        {
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView reloadData];
        }
        else
        {
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:YES];
        }
    }
}

#pragma mark - Picker view delegate
- (void)setQuantity:(NSNumber *)quantity forIndexPath:(NSIndexPath *)indexPath
{
    [self.cart setQuantity:quantity forItemAtIndexPath:indexPath];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"createOrder"])
    {
        if ([sender isKindOfClass:[NSArray class]])
        {
            NSArray *orderList = sender;
            PPOrderViewController *orderViewController = (PPOrderViewController *)[segue destinationViewController];
            orderViewController.orderedItems = orderList;
        }
    }
}


@end
