
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

/**
 *  AVCapture 系列的播放 View;
 */
@interface PPCaptureView : UIView;

@end


/**
 *  AVPlayer 系列的播放 View
 */
@interface PPPlayerView : UIView;

@end
