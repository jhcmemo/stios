
#import <UIKit/UIKit.h>
#import "PPProtocols.h"

/**
 *  客製化拍照 MVC
 */
@interface PPImageCaptureController : UIViewController



/**
 *  按下上傳按鈕的委託
 */
@property (nonatomic, weak) id<PPProtocols>delegate;

@end
