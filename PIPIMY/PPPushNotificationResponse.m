
#import "PPPushNotificationResponse.h"

@implementation PPPushNotificationResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return nil;
}

+ (NSValueTransformer *)chatIdJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(id idString) {
        return [NSString stringWithFormat:@"%@", idString];
    }];
}

+ (NSValueTransformer *)sendTimeJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^NSDate *(NSString *timeString) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        return [dateFormatter dateFromString:timeString];
    }];
}

@end
