
#import <UIKit/UIKit.h>
@class PPAPIListTracingResponse;

@protocol PPTracingCellerCellDelegate <NSObject>

- (void)removeTracedSeller:(PPAPIListTracingResponse *)seller;

@end

@interface PPTracingSellerCollectionViewCell : UICollectionViewCell

- (void)configureCellWithTracedSeller:(PPAPIListTracingResponse *)seller;

@property (nonatomic, weak) id <PPTracingCellerCellDelegate> delegate;

@end
