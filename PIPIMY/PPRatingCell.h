
#import <UIKit/UIKit.h>
#import "PPAPIGetSomeoneRatingResponse.h"


/**
 *  評分的 Cell
 */
@interface PPRatingCell : UITableViewCell



/**
 *  設置 Cell
 *
 *  @param rating 評分內容
 */
- (void)configureWithRating:(Rating *)rating;

@end
