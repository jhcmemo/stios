
#import "PPUser.h"
#import "PPProductListController+Delegates.h"


@implementation PPProductListController (Delegates)

#pragma mark - UICollectionView delegate / datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    
    if(self.cellType == DisplayCellTypeSmall)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"smallCell"
                                                         forIndexPath:indexPath];
    }
    else if(self.cellType == DisplayCellTypeMedium)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mediumCell"
                                                         forIndexPath:indexPath];

    }
    else if(self.cellType == DisplayCellTypeBig)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bigCell"
                                                         forIndexPath:indexPath];

    }
    
    if([cell isKindOfClass:[PPProductSmallCell class]])
    {
        PPProductSmallCell *pCell = (PPProductSmallCell *)cell;
        pCell.tag                 = indexPath.row;
        pCell.delegate            = self;
        
        PPAPIGetNearProductResponse *product = self.dataSource[indexPath.row];
        
        [pCell configureWithProduct:product];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [PPProductSmallCell layoutSizeWithCellType:self.cellType InCollectionView:collectionView];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPAPIGetNearProductResponse *item = self.dataSource[indexPath.row];
    
    [self performSegueWithIdentifier:@"toPPProductDetailController" sender:item];
}

#pragma mark - 商品 Cell delegate
- (void)productCell:(id)cell favoriteClickedAtIndex:(NSInteger)index
{
    PPAPIGetNearProductResponse *product = self.dataSource[index];
    NSDateFormatter *formatter           = [[NSDateFormatter alloc]init];
    formatter.dateFormat                 = @"MM-dd";
    NSString *addToFavoriteDate          = [formatter stringFromDate:[NSDate date]];
    product.addToFavoriteDate            = addToFavoriteDate;
    
    [[PPUser singleton]updateFavorites:product];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (void)productCell:(id)cell videoButtonClickedAtIndex:(NSInteger)index
{
    PPAPIGetNearProductResponse *item = self.dataSource[index];
    PPVideoPlayController *mvc        = [self.storyboard instantiateViewControllerWithIdentifier:@"PPVideoPlayController"];
    mvc.delegate                      = self;
    
    [mvc playVideos:@[item] inViewController:self.tabBarController];
}

- (void)productCell:(id)cell userIconClickedAtIndex:(NSInteger)index
{
    PPAPIGetNearProductResponse *item = self.dataSource[index];
    
    // 按到自己的小店
    /*
    if([item.ID isEqualToString:[PPUser singleton].name])
    {
        UIStoryboard *store = [UIStoryboard storyboardWithName:@"Tab4" bundle:nil];
        id mvc = [store instantiateViewControllerWithIdentifier:@"PPMyStoreViewController"];
        [self.navigationController pushViewController:mvc animated:YES];
    }
    else
    {
        [self performSegueWithIdentifier:@"toPPSomeoneStoreController" sender:item.ID];
    }
     */
    
    /**
     *  更改成都進入這頁，影響的部分為，原本可以從 PPMyStoreViewController 進行我的小店編輯，
     *  變成只能從設定中進行編輯。
     */
    
    [self performSegueWithIdentifier:@"toPPSomeoneStoreController" sender:item.ID];

}

#pragma mark - PPVideoPlayController delegate
- (void)videoPlayerController:(PPVideoPlayController *)mvc shouldShowProductDetailWithId:(NSNumber *)productId
{
    __block PPAPIGetNearProductResponse *product;
    
    [self.dataSource enumerateObjectsUsingBlock:^(PPAPIGetNearProductResponse *obj, NSUInteger idx, BOOL *stop) {
        
        if([obj.productId integerValue] == [productId integerValue])
        {
            product = obj;
            *stop = YES;
        }
    }];
    
    [mvc exitPlayVideo];
    
    if(product)
    {
        [self performSegueWithIdentifier:@"toPPProductDetailController" sender:product];
    }
}

#pragma mark - Private methods
#pragma mark 返回客製化 UITableViewCell
- (PPProductSmallCell *)__smallCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    PPProductSmallCell *cell             = [tableView dequeueReusableCellWithIdentifier:@"smallCell"];
    cell.delegate                        = self;
    cell.tag                             = indexPath.row;
    PPAPIGetNearProductResponse *product = self.dataSource[indexPath.row];
    
    [cell configureWithProduct:product];
    
    return cell;
}

- (PPProductBigCell *)__bigCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    PPProductBigCell *cell               = [tableView dequeueReusableCellWithIdentifier:@"bigCell"];
    cell.delegate                        = self;
    cell.tag                             = indexPath.row;
    PPAPIGetNearProductResponse *product = self.dataSource[indexPath.row];
    
    [cell configureWithProduct:product];
    
    return cell;
}

#pragma mark
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag)
    {
        case k_Tag_searchItemAlert:
        {
            switch (buttonIndex)
            {
                case 0:
                {
                    /*
                     * 不搜尋
                     */
                }
                    break;
                    
                case 1:
                {
                    UITextField *textField = [alertView textFieldAtIndex:0];
                    if (textField.text.length >0)
                    {
                        self.searchKeyWord = textField.text;
                        [self callAPISearchProductWithKeyWord];
                    }
                    else
                    {
                        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@": (" message:@"請輸入您所要搜尋的商品關鍵字." delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil, nil];
                        
                        [alert show];
                        
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark TypeViewDelegat
- (void)TypeView:(TypeView *)typeView didSelectedType:(NSString *)typeContent
{
    [self callAPIGetTypeProduct];
    
}

@end
