
#import <Foundation/Foundation.h>
@class PPImageCaptureController;
@class PPVideoRecordController;
@class PPProductMediumCell;
@class PPProductSmallCell;
@class PPProductBigCell;
@class PPVideoPlayController;


/**
 *  拍拍賣需要用到的 delegate protocol 集合
 */
@protocol PPProtocols <NSObject>

@optional



/**
 *  PPImageCaptureController 按下上傳按鈕
 *
 *  @param mvc       PPImageCaptureController Object
 *  @param filesName 需要上傳的圖片檔名
 */
- (void)imageCaptureController:(PPImageCaptureController *)mvc uploadButtonClicked:(NSArray *)filesName;

/**
 *  PPVideoRecordController 按下上傳按鈕
 *
 *  @param mvc PPVideoRecordController Object
 */
- (void)uploadButtonClickedInVideoRecordController:(PPVideoRecordController *)mvc;



/**
 *  商品 Cell 按下加入最愛
 *
 *  @param cell  商品 Cell
 *  @param index 索引, 基本上是 Cell 的 tag
 */
- (void)productCell:(id)cell favoriteClickedAtIndex:(NSInteger)index;

/**
 *  商品 Cell 按下播放影片
 *
 *  @param cell  商品 Cell
 *  @param index 索引, 基本上是 Cell 的 tag
 */
- (void)productCell:(id)cell videoButtonClickedAtIndex:(NSInteger)index;

/**
 *  商品 Cell 按下使用者頭像
 *
 *  @param cell  商品 Cell
 *  @param index 索引, 基本上是 Cell 的 tag
 */
- (void)productCell:(id)cell userIconClickedAtIndex:(NSInteger)index;



/**
 *  影片播放器跳轉到商品詳細頁
 *
 *  @param mvc       PPVideoPlayController Object
 *  @param productId 商品 Id
 */
- (void)videoPlayerController:(PPVideoPlayController *)mvc shouldShowProductDetailWithId:(NSNumber *)productId;

@end
