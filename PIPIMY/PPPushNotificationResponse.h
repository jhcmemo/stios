
#import <Mantle.h>

@interface PPPushNotificationResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, readonly) NSString *alert;
@property (nonatomic, readonly) NSString *badge;
@property (nonatomic, readonly) NSString *chatId;       // broadcast chatId = -2
@property (nonatomic, readonly) NSString *content;
@property (nonatomic, readonly) NSString *isFrom;
@property (nonatomic, readonly) NSDate   *sendTime;
@property (nonatomic, readonly) NSString *sound;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSString *userId;

@end
