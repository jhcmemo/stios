
#import <UIKit/UIKit.h>

@protocol PPCartListHeaderViewDelegate <NSObject>

- (void)checkAllForSection:(NSInteger)section;

@end


@interface PPCartListHeaderView : UITableViewHeaderFooterView

- (void)configureHeaderWithID:(NSString *)storeID section:(NSInteger)section delegate:(id<PPCartListHeaderViewDelegate>)delegate checked:(BOOL)checked;

@end
