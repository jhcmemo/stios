
#import "PPAPIGetOrderListResponse.h"

@implementation PPAPIGetOrderListResponse

static NSSet *datesKeySet;
static NSSet *numbersKeySet;

+ (void)initialize
{
    if (!datesKeySet)
        datesKeySet = [NSSet setWithObjects:@"createdAt", @"logisticsDate3", @"logisticsDate4", @"logisticsDate5", @"logisticsDate6", @"paymentDate", @"pendingExpireDate", @"updatedAt", nil];
    if (!numbersKeySet)
        numbersKeySet = [NSSet setWithObjects:@"Id", @"logisticsCost", @"logisticsType", @"paymentStatus", @"paymentType", @"pending", @"queue", @"status", @"sum", nil];
}

#pragma mark - Key Map
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{ @"createdAt"    : @"created_at",
              @"Id"           : @"id",
              @"sellerUserId" : @"seller_user_id",
              @"updatedAt"    : @"updated_at",
              @"userId"       : @"user_id" };
}

#pragma mark - Value transformers

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([datesKeySet containsObject:key])
        return [MTLValueTransformer transformerWithBlock:^NSDate *(NSString *dateString) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            return [dateFormatter dateFromString:dateString];
        }];
    
    else if ([numbersKeySet containsObject:key])
        return [MTLValueTransformer transformerWithBlock:^NSNumber *(NSString *numberString) {
            // debug
            if (numberString && ![numberString isKindOfClass:[NSString class]])
                NSLog(@"%@ is of type %@", key, [numberString class]);
            
            return @([numberString integerValue]);
        }];
    else
        return nil;
}


@end
