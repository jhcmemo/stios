
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


/**
 *  在 PPPostViewController 的 SubView 管理 model
 */
@interface PPPostViewsModel : NSObject



/**
 *  更新上傳影片的進度
 *
 *  @param progress 上傳進度
 */
- (void)updateVideoUploadProgress:(CGFloat)progress;

/**
 *  更新上傳圖片的進度
 *
 *  @param progress 圖片的進度
 */

/**
 *  上傳影片是否成功
 *
 *  @param success 是否成功
 */
- (void)videoUploadSuccess:(BOOL)success;

/**
 *  上傳圖片是否成功
 *
 *  @param success 是否成功
 */

/**
 *  返回刊登商品 Api 需要的參數
 *
 *  @return 返回刊登商品 Api 需要的參數
 */
- (NSMutableDictionary *)apiParams;

@end
