
#import "PPAPIAllpayGetStoreInfoResponse.h"

@implementation PPAPIAllpayGetStoreInfoResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"storeAddress" : @"Store_Addr",
             @"storeClose"   : @"Store_Close",
             @"storeName"    : @"Store_Name",
             @"storePhone"   : @"Store_Phone",
             @"storeID"      : @"Store_id"};
}

@end
