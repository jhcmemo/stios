
#import "PPAPI.h"
#import "PPUser.h"
#import "PPMyRatingsListController.h"


@interface PPMyRatingsListController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) RatingDetail *detail;     // 我的評價總覽
@property (nonatomic, strong) NSArray      *dataSource; // 個別評價資料

@property (nonatomic, weak) IBOutlet UITableView *tableView; // 個別評價列表
@property (nonatomic, weak) IBOutlet UILabel *ratingAverage; // 平均評價
@property (nonatomic, weak) IBOutlet UILabel *ratingCount;   // 被評價次數
@property (nonatomic, weak) IBOutlet UILabel *dealCount;     // 交易次數

@end

@implementation PPMyRatingsListController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __callAPIGetSomeoneRating];
}

#pragma mark - Properties Setter

- (void)setDetail:(RatingDetail *)detail
{
    _detail             = detail;
    _ratingAverage.text = [NSString stringWithFormat:@"%@", _detail.average];
    _ratingCount.text   = [NSString stringWithFormat:@"%@", _detail.count];
    _dealCount.text     = [NSString stringWithFormat:@"%@", _detail.dealTimes];
}

- (void)setDataSource:(NSArray *)dataSource
{
    _dataSource = dataSource;
    
    if(!_dataSource.count)
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UILabel *emptyLabel       = [[UILabel alloc]initWithFrame:_tableView.bounds];
        emptyLabel.textAlignment  = NSTextAlignmentCenter;
        emptyLabel.text           = @"沒有評分紀錄";
        _tableView.backgroundView = emptyLabel;
    }
    else
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.backgroundView = nil;
    }
    
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    Rating *rating = _dataSource[indexPath.row];
    
    cell.textLabel.text = rating.userIDFrom;
    
    return cell;
}

#pragma mark - Private methods

- (void)__callAPIGetSomeoneRating
{
    NSDictionary *params = @{@"MemberID" : [PPUser singleton].name};
    
    [[PPAPI singleton]APIGetSomeoneRating:params callback:^(PPAPIGetSomeoneRatingResponse *item, NSError *err) {
        
        if(err)
        {
            [self __showAlertViewWithMessage:err.localizedDescription];
        }
        else
        {
            self.detail = item.detail;
            self.dataSource = item.ratings;
        }
    }];
}

- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}

@end
