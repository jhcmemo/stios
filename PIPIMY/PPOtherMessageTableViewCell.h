
#import <UIKit/UIKit.h>
@class PPAPIGetChatContentResponse;

@interface PPOtherMessageTableViewCell : UITableViewCell

- (void)configureCellWithMessage:(PPAPIGetChatContentResponse *)message chat:(NSDictionary *)chat;

@end
