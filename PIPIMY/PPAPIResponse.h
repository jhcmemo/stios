
#ifndef PIPIMY_PPAPIResponse_h
#define PIPIMY_PPAPIResponse_h

#import "PPAPILoginResponse.h"
#import "PPAPISuccessResponse.h"
#import "PPAPIRegisterResponse.h"
#import "PPAPIGetProductIDResponse.h"
#import "PPAPIGetNearProductResponse.h"
#import "PPAPIGetStoreResponse.h"
#import "PPAPIListTracedResponse.h"
#import "PPAPIGetMyDealResponse.h"
#import "PPAPIGetSomeoneRatingResponse.h"
#import "PPAPIGetChatIndexResponse.h"
#import "PPAPIGetChatContentResponse.h"
#import "PPAPIListTracingResponse.h"
#import "PPAPICartListResponse.h"
#import "PPAPIAllpayGetStoreInfoResponse.h"
#import "PPAPIGetOrderListResponse.h"
#import "PPAPIGetOrderResourceResponse.h"
#import "PPAPITracePushTriggerResponse.h"
#import "PPAPICartCheckPayResponse.h"
#import "PPAPIConfirmOrderFinishResponse.h"

#import "PPPushNotificationResponse.h"

#import "PPAPICheckIDResponse.h"

#endif
