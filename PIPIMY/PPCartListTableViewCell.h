
#import <UIKit/UIKit.h>
@class PPAPICartListResponse;
@class PPCartListTableViewCell;


@protocol  PPCartListTableViewCellDelegate <NSObject>

- (void)quantityButtonClickedInCell:(PPCartListTableViewCell *)cell value:(NSNumber *)value;
- (void)checkboxClickedInCell:(PPCartListTableViewCell *)cell;

@end



@interface PPCartListTableViewCell : UITableViewCell

@property (nonatomic, weak) id <PPCartListTableViewCellDelegate> delegate;

- (void)configureCellWithCartItem:(PPAPICartListResponse *)item quantity:(NSNumber *)quantity checked:(BOOL) checked;

@end
