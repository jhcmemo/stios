
#import <UIKit/UIKit.h>


/**
 *  我的最愛與追蹤賣家列表
 *
 *  注意: 我的最愛資料是存在 Local, 追蹤賣家資料是從 API 來
 */
@interface PPMyFavoriteListController : UIViewController

@end
