
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, OrderSuccessButtonType)
{
    OrderSuccessButtonTypeBackToCart = 0,
    OrderSuccessButtonTypeSeeList    = 1
};

@interface PPOrderViewController : UIViewController

@property (nonatomic, strong) NSArray *orderedItems;

@end
