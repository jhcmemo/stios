
#import "PPAPI.h"
#import "PPUser.h"
#import "PPProductListController.h"
#import "PPSomeoneStoreController.h"
#import "PPProductDetailController.h"
#import "PPProductListController+Delegates.h"
#import "PPMyFavoriteListController.h"
#import "PPPostViewController.h"

#import "SRPPopupMenu.h"
#import "UIColor+SRPKit.h"


@interface PPProductListController ()

@property (nonatomic, strong) NSArray          *dataSource; // 商品資料
@property (nonatomic, strong) UIRefreshControl *refresh;    // 下拉更新物件
@property (nonatomic, strong) TypeView         *categoryView;   // 分類

@end


@implementation PPProductListController

#define k_Tag_selectDisPlayTypeStartTag   20000;




#pragma mark - LifeCycle
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:SRPPOPUPMENU_BUTTON_CLICKED object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    [[NSNotificationCenter defaultCenter]addObserverForName:UIApplicationWillEnterForegroundNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
    
        [weakSelf.refresh endRefreshing];
    }];
    
    [self __setup];
    [self __setupLeftItems];
    [self __setupRightItems];
    [self __addSRPOPMenuObserver];
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if(status == kCLAuthorizationStatusNotDetermined) // User 還未確定是否要定位
    {
        [self __addObserver];
    }
    else if(status == kCLAuthorizationStatusDenied) // User 不允許使用定位
    {
        [self __showAlertViewWithMessage:@"請檢查定位狀態"];
    }
    else // User 已經允許
    {
        [self __callAPIGetNearProduct];
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    /* 
     *  切換 TabBar 時, 再切回來 UIRefreshControl 會有一種 Bug
     *  @see http://stackoverflow.com/questions/24341192/uirefreshcontrol-stuck-after-switching-tabs-in-uitabbarcontroller
     */
    [_refresh endRefreshing];
    [[SRPPopupMenu singleton]showWithColor:[UIColor blackColor]];
    [_collectionView reloadData];
    self.navigationController.navigationBar.topItem.title = @"";
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[SRPPopupMenu singleton]hide];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPPProductDetailController"])
    {
        PPProductDetailController *mvc = segue.destinationViewController;
        [mvc setHidesBottomBarWhenPushed:YES];
        mvc.product = sender;
    }
    else if([segue.identifier isEqualToString:@"toPPSomeoneStoreController"])
    {
        PPSomeoneStoreController *mvc = segue.destinationViewController;
        mvc.memberId = sender;
    }
    else if ([segue.identifier isEqualToString:@"toPPMyFavoriteListController"])
    {
        PPMyFavoriteListController *mvc = segue.destinationViewController;
        mvc.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark - Properties Setter
- (void)setCellType:(DisplayCellType)cellType // Setter cellType 時, 順便 register 所需要的 Cell
{
    _cellType = cellType > 2 ? DisplayCellTypeSmall : cellType;

    if(_cellType == DisplayCellTypeSmall)
    {
        UINib *smallCell   = [UINib nibWithNibName:@"PPProductSmallCell" bundle:nil];
        [_collectionView registerNib:smallCell forCellWithReuseIdentifier:@"smallCell"];
    }
    else if(_cellType == DisplayCellTypeMedium)
    {
        UINib *mediumCell   = [UINib nibWithNibName:@"PPProductMediumCell" bundle:nil];
        [_collectionView registerNib:mediumCell forCellWithReuseIdentifier:@"mediumCell"];
    }
    else if(_cellType == DisplayCellTypeBig)
    {
        UINib *bigCell   = [UINib nibWithNibName:@"PPProductBigCell" bundle:nil];
        [_collectionView registerNib:bigCell forCellWithReuseIdentifier:@"bigCell"];
    }
    
    [_collectionView reloadData];
}

- (void)setDataSource:(NSArray *)dataSource // Setter dataSource, 順便 refreas 畫面
{
    //[self setDataSourceSortType:_dataSourceSortType];
    _dataSource = dataSource;
    //NSLog(@"=======================================================");
}

/*
 * Setter contentDataSourceType 時, 順便改變按鈕相關狀態
 */

- (void)setContentDataSourceType:(ContentDataSourceType)contentDataSourceType
{
    _dataSourceType = contentDataSourceType;
    
    UIBarButtonItem *nearbyBarItem = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
    UIButton *nearbyBtn = (UIButton*)nearbyBarItem.customView;
    
    //hidden by Ivanka
    //UIBarButtonItem *categoryBarItem = [self.navigationItem.rightBarButtonItems objectAtIndex:2];
    //UIButton *categoryBtn = (UIButton*)categoryBarItem.customView;
    
    UIBarButtonItem *searchBarItem = [self.navigationItem.rightBarButtonItems objectAtIndex:2];
    UIButton *searchBtn = (UIButton*)searchBarItem.customView;
    

    switch (_dataSourceType)
    {
        case ContentDataSourceTypeKeyWord:
        {
            [nearbyBtn   setSelected:NO];
            //[categoryBtn setSelected:NO]; //hidden by Ivanka
            [searchBtn   setSelected:YES];
            
        }
            break;
            
        case ContentDataSourceTypeCategory:
        {
            [nearbyBtn   setSelected:NO];
            //[categoryBtn setSelected:YES]; //hidden by Ivanka
            [searchBtn   setSelected:NO];
        }
            break;
            
        case ContentDataSourceTypeDefault:
        {
            [nearbyBtn   setSelected:YES];
            //[categoryBtn setSelected:NO]; //hidden by Ivanka
            [searchBtn   setSelected:NO];
        }
            break;
            
        default:
            break;
    }
}


/*
 * Setter contentDataSourceSortType.
 */

- (void)setDataSourceSortType:(ContentDataSourceSortType)contentDataSourceSortType
{
    _dataSourceSortType = contentDataSourceSortType;
    [self __sortingDataSourceWithSortType];
}


#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    self.cellType    = DisplayCellTypeSmall;
    self.contentDataSourceType = ContentDataSourceTypeDefault;
    self.dataSourceSortType    = ContentDataSourceSortTypeByTime;
    
    _refresh         = [[UIRefreshControl alloc]init];
    
    /*
    [_refresh addTarget:self
                 action:@selector(__callAPIGetNearProduct:)
       forControlEvents:UIControlEventValueChanged];
    */
    
    [_refresh addTarget:self action:@selector(__refreshControllWillRefresh) forControlEvents:UIControlEventValueChanged];
    
    [_collectionView addSubview:_refresh];

    
    /*
     *  上點選分類所需要的分類列表
     */
    
    self.categoryView = [[TypeView alloc]initWithFrame:CGRectMake(0,
                                                            0,
                                                            [UIScreen mainScreen].bounds.size.width,
                                                             [UIScreen mainScreen].bounds.size.height) source:@"post"];

    
    [self.categoryView setDelegate:self];
}

-(void)__hideSelectDisplayTyepView
{
    if (self.selectDisplayTypeView.hidden == NO)
    {
        self.selectDisplayTypeView.hidden = YES;
    }
}

- (void)__sortingDataSourceWithSortType
{
    NSArray *sortedArray;
    
    switch (_dataSourceSortType)
    {
        case ContentDataSourceSortTypeByTime:
        {
            NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"time" ascending:NO];
            
            NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
            
            NSArray *sortedArr=[_dataSource sortedArrayUsingDescriptors:sortDescriptors];
            
            [self setDataSource:sortedArr];
            
        }
            break;
            
        case ContentDataSourceSortTypeByHot:
        {
            sortedArray = [_dataSource sortedArrayUsingComparator:^NSComparisonResult(PPAPIGetNearProductResponse *p1, PPAPIGetNearProductResponse *p2)
                           {
                               NSNumber *p1favorite = @([p1.favorite integerValue]);
                               NSNumber *p2favorite = @([p2.favorite integerValue]);
                               return [p2favorite compare:p1favorite];
                           }];
            
            [self setDataSource:sortedArray];
        }
            break;
            
        case ContentDataSourceSortTypeByPrice:
        {
            sortedArray = [_dataSource sortedArrayUsingComparator:^NSComparisonResult(PPAPIGetNearProductResponse *p1, PPAPIGetNearProductResponse *p2)
                           {
                               NSNumber *p1price = @([p1.price integerValue]);
                               NSNumber *p2price = @([p2.price integerValue]);
                               return [p1price compare:p2price];
                           }];
            
            [self setDataSource:sortedArray];
        }
            break;
            
        case ContentDataSourceSortTypeByLocation:
        {
            CLLocation *userLocation = [[PPUser singleton] location];
            
            for (PPAPIGetNearProductResponse *product in _dataSource)
            {
                CLLocationDegrees lat       = [product.lat doubleValue];
                CLLocationDegrees lng       = [product.lng doubleValue];
                CLLocation *productLocation = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
                CLLocationDistance dist     = [userLocation distanceFromLocation:productLocation];
                product.distance            = dist / 1000.0;
            }
            
            NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"distance" ascending:YES];
            
            NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArr=[_dataSource sortedArrayUsingDescriptors:sortDescriptors];
            [self setDataSource:sortedArr];
        }
            break;
            
        default:
            break;
    }
    
    [_collectionView reloadData];
    [_collectionView setContentOffset:CGPointZero animated:NO];
}

#pragma mark - 設置 NavigationItem LeftItems

- (void)__setupLeftItems
{
    //UIBarButtonItem *favItem               = [self __favoriteItem];
    UIBarButtonItem *browseCommodityItem = [self __browseCommodityItem];
    self.navigationItem.leftBarButtonItems = @[browseCommodityItem];
}

/*//先保留原來的leftBarButtonItem
- (UIBarButtonItem *)__favoriteItem
{
    return [[UIBarButtonItem alloc]initWithTitle:@"最愛"
                                           style:UIBarButtonItemStylePlain
                                          target:self
                                          action:@selector(__favoriteItemClicked:)];
}

- (void)__favoriteItemClicked:(id)sender
{
    [self performSegueWithIdentifier:@"toPPMyFavoriteListController" sender:nil];
}
*/

- (UIBarButtonItem *)__browseCommodityItem
{
    UIButton *browseCommodityBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,80,40)];
    [browseCommodityBtn setTitle:@"瀏覽商品" forState:UIControlStateNormal];
    [browseCommodityBtn setTitleColor:[UIColor srp_colorWithHEX:@"#FF514F4E"] forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:browseCommodityBtn];
}


#pragma mark - 設置 NavigationItem rightItems
- (void)__setupRightItems
{
    //UIBarButtonItem *playVideoItem          = [self __playAllVideoItem];
    UIBarButtonItem *displayItem            = [self __changeDisplayItem];
    UIBarButtonItem *nearbyItem             = [self __nearbyItem];
    //UIBarButtonItem *categoryItem           = [self __categoryItem];  //hidden by Ivanka
    UIBarButtonItem *searchItme             = [self __searchItem];
    
    //self.navigationItem.rightBarButtonItems = @[displayItem,nearbyItem,searchItme,categoryItem];    //hidden by Ivanka
    self.navigationItem.rightBarButtonItems = @[displayItem,nearbyItem,searchItme];  //hidden __categoryItem
}


#pragma mark 播放影片
- (UIBarButtonItem *)__playAllVideoItem
{
    return [[UIBarButtonItem alloc]initWithTitle:@"影片"
                                           style:UIBarButtonItemStylePlain
                                          target:self
                                          action:@selector(__playAllVideoItemClicked:)];
}

- (void)__playAllVideoItemClicked:(id)sender
{
    PPVideoPlayController *mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PPVideoPlayController"];
    mvc.delegate               = self;
    
    [mvc playVideos:_dataSource inViewController:self.tabBarController];
}

#pragma mark 變更顯示模式
- (UIBarButtonItem *)__changeDisplayItem
{
    UIImage *btnImg = [UIImage imageNamed:@"NaviBar-Show1-BarButton.png"];

    UIButton *disBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnImg.size.width, btnImg.size.height)];
    
    [disBtn setBackgroundImage:btnImg forState:UIControlStateNormal];
    
    [disBtn addTarget:self action:@selector(__changeDisplayItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [disBtn setAdjustsImageWhenHighlighted:NO];
    
    return [[UIBarButtonItem alloc] initWithCustomView:disBtn];
}

- (void)__changeDisplayItemClicked:(id)sender
{
    
    if (_selectDisplayTypeView.hidden == YES)
    {
        [_selectDisplayTypeView setHidden:NO];
    }
    else
    {
        [_selectDisplayTypeView setHidden:YES];
    }
}

#pragma mark 鄰近商品
- (UIBarButtonItem *)__nearbyItem
{
    UIImage *nearbyBtnDefaultImg   = [UIImage imageNamed:@"NaviBar-HomeDefault-BarButton.png"];
    UIImage *nearbyBtnDidSelectImg = [UIImage imageNamed:@"NaviBar-HomeDidSelect-BarButton.png"];
    
    UIButton *nearbyBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, nearbyBtnDefaultImg.size.width, nearbyBtnDefaultImg.size.height)];
    
    [nearbyBtn setBackgroundImage:nearbyBtnDefaultImg forState:UIControlStateNormal];
    
    [nearbyBtn setBackgroundImage:nearbyBtnDidSelectImg forState:UIControlStateSelected];

    [nearbyBtn addTarget:self action:@selector(__nearbyItemClicked:) forControlEvents:UIControlEventTouchUpInside];

    return [[UIBarButtonItem alloc] initWithCustomView:nearbyBtn];
}

- (void)__nearbyItemClicked:(id)sender
{
    [self __callAPIGetNearProduct];
    [self __hideSelectDisplayTyepView];
}

#pragma mark 分類清單
- (UIBarButtonItem *)__categoryItem
{
    UIImage *categoryBtnDefaultImg   = [UIImage imageNamed:@"NaviBar-CatalogDefault-BarButton.png"];
    UIImage *categoryBtnDidSelectImg = [UIImage imageNamed:@"NaviBar-CatalogDidSelect-BarButton.png"];
    
    UIButton *categoryBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, categoryBtnDefaultImg.size.width, categoryBtnDefaultImg.size.height)];
    
    [categoryBtn setBackgroundImage:categoryBtnDefaultImg forState:UIControlStateNormal];

    [categoryBtn setBackgroundImage:categoryBtnDidSelectImg forState:UIControlStateSelected];
    
    [categoryBtn addTarget:self action:@selector(__categoryItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:categoryBtn];
}

- (void)__categoryItemClicked:(id)sender
{
    // * show category View
    [self.tabBarController.view addSubview:self.categoryView];
    [self __hideSelectDisplayTyepView];
}

#pragma mark 關鍵字搜尋
- (UIBarButtonItem *)__searchItem
{
    UIImage *searchBtnDefaultImg   = [UIImage imageNamed:@"NaviBar-SearchDefault-BarButton.png"];
    UIImage *searchBtnDidSelectImg = [UIImage imageNamed:@"NaviBar-SearchDidSelect-BarButton"];
    
    UIButton *searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, searchBtnDefaultImg.size.width, searchBtnDefaultImg.size.height)];
    
    [searchBtn setBackgroundImage:searchBtnDefaultImg forState:UIControlStateNormal];
    
    [searchBtn setBackgroundImage:searchBtnDidSelectImg forState:UIControlStateSelected];
    
    [searchBtn addTarget:self action:@selector(searchItemClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:searchBtn];
}

- (void)searchItemClick:(id)sender
{
    UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"請輸入關鍵字"
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"取消"
                                          otherButtonTitles:@"搜尋",nil];

    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alert setDelegate:self];
    [alert setTag:k_Tag_searchItemAlert];
    [alert show];
    [self __hideSelectDisplayTyepView];
    
    
}


#pragma mark - Observer
- (void)__addObserver
{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(__handleUserLocationUpdated:)
                                                name:USER_LOCATION_UPDATED object:nil];
}

- (void)__addSRPOPMenuObserver
{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(__SRPOPMenuButtonClicked:) name:SRPPOPUPMENU_BUTTON_CLICKED object:nil];
}


- (void)__handleUserLocationUpdated:(NSNotification *)sender
{
    /*
     *  基本上只要監聽一次, 就是第一次詢問 User 定位
     *  之後就是 User 手動下拉更新了
     */
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];

    if (self.dataSourceType == ContentDataSourceTypeDefault)
    {
        [self __callAPIGetNearProduct];
    }
}


-(void)__SRPOPMenuButtonClicked:(NSNotification *)notification
{
    int srPOPMenuBtnTag = [notification.object intValue];
    
    switch (srPOPMenuBtnTag)
    {
        case ContentDataSourceSortTypeByTime:
        {
            self.dataSourceSortType = ContentDataSourceSortTypeByTime;
        }
            break;
            
        case ContentDataSourceSortTypeByHot:
        {
            self.dataSourceSortType = ContentDataSourceSortTypeByHot;
        }
            break;
            
        case ContentDataSourceSortTypeByPrice:
        {
            self.dataSourceSortType = ContentDataSourceSortTypeByPrice;
        }
            break;
            
        case ContentDataSourceSortTypeByLocation:
        {
            self.dataSourceSortType = ContentDataSourceSortTypeByLocation;
        }
            break;
            
        case 5:
        {
            [self __playAllVideoItemClicked:nil];
        }
            break;
        case 6:
        {
            [self performSegueWithIdentifier:@"toPPMyFavoriteListController" sender:nil];
        }
            break;
            
        case 7:
        {
            PPUser *user = [PPUser singleton];
            
            if ((user.storeCash && user.storeDelivery)>0)
            {
                UIStoryboard *tab4Storyboard = [UIStoryboard storyboardWithName:@"Tab4" bundle: nil];
                
                PPPostViewController *mvc = (PPPostViewController*)[tab4Storyboard instantiateViewControllerWithIdentifier: @"PPPostViewController"];

               
                [mvc setHidesBottomBarWhenPushed:YES];
                
                [self.navigationController pushViewController:mvc animated:YES];
            }
            else
            {
                [self __showAlertViewWithMessage:@"請設定金物流方式."];
            }
            
        }
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Call API

- (void)__callAPIGetNearProduct
{
    PPUser *user         = [PPUser singleton];
    NSDictionary *params = @{@"CC" : @"TW",
                             @"Lat" : @(user.location.coordinate.latitude),
                             @"Lng" : @(user.location.coordinate.longitude)
                             };
    
    [[PPAPI singleton]APIGetNearProduct:params callback:^(NSArray *products, NSError *err) {
        
        [self.refresh endRefreshing];
        self.dataSource = products;
        self.contentDataSourceType = ContentDataSourceTypeDefault;
        [self __sortingDataSourceWithSortType];

    }];
}

- (void)__refreshControllWillRefresh
{
    //取得目前時間
    NSDate * date = [NSDate date];
    NSTimeInterval sec = [date timeIntervalSinceNow];
    NSDate * currentDate = [[NSDate alloc] initWithTimeIntervalSinceNow:sec];
    
    //輸出格式
    NSDateFormatter * dateformate = [[NSDateFormatter alloc] init ];
    //[df setDateFormat:@"yyyy年MM月dd日 HH小時mm分ss秒"];
    [dateformate setDateFormat:@"MM/dd HH:mm"];
    NSString *strDate = [dateformate stringFromDate:currentDate];
    NSLog(@"系統目前時間為：%@",strDate);
    
    //refresh 標題
    NSString *string = [@"上次更新時間:" stringByAppendingString:strDate];
    _refresh.attributedTitle = [[NSAttributedString alloc]initWithString:string];

    
    switch (self.dataSourceType)
    {
        case ContentDataSourceTypeDefault:
        {
            [self __callAPIGetNearProduct];
        }
            break;
            
        case ContentDataSourceTypeCategory:
        {
            [self callAPIGetTypeProduct];
        }
            break;
            
        case ContentDataSourceTypeKeyWord:
        {
            [self callAPISearchProductWithKeyWord];
        }
            break;
            
        default:
            break;
    }
}


- (void)callAPIGetTypeProduct
{
    
    NSDictionary *params = @{@"CC" : @"TW",
                             @"Type" :[NSString stringWithFormat:@"%i",[self.categoryView getTypeInt]]
                             };
    
    [[PPAPI singleton]APIGetTypeProduct:params callback:^(NSArray *products, NSError *err) {
        
        [self.refresh endRefreshing];
        self.dataSource = products;
        self.contentDataSourceType = ContentDataSourceTypeCategory;
        [self __sortingDataSourceWithSortType];
    }];
}

- (void)callAPISearchProductWithKeyWord
{
    
    NSDictionary *params = @{@"CC" : @"TW",
                             @"Keyword" :self.searchKeyWord
                             };
    
    [[PPAPI singleton]APIGetSearchProduct:params callback:^(NSArray *products, NSError *err) {
        
        [self.refresh endRefreshing];
        self.dataSource = products;
        self.contentDataSourceType = ContentDataSourceTypeKeyWord;
        [self __sortingDataSourceWithSortType];
    }];
}

#pragma mark
- (void)__showAlertViewWithMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"錯誤"
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"確定"
                                         otherButtonTitles:nil];
    
    [alert show];
}


#pragma mark
#pragma mark IBAction
-(IBAction)selectDisplayCellType:(id)sender
{
    UIButton *senderBtn = (UIButton *)sender;
    _cellType = senderBtn.tag - k_Tag_selectDisPlayTypeStartTag;
    
    UIBarButtonItem *displayBarItem = [self.navigationItem.rightBarButtonItems objectAtIndex:0];
    UIButton *displayBtn = (UIButton*)displayBarItem.customView;
 
    switch (_cellType)
    {
        case DisplayCellTypeSmall:
            self.cellType = DisplayCellTypeSmall;
            [displayBtn setBackgroundImage:[UIImage imageNamed:@"NaviBar-Show1-BarButton.png"] forState:UIControlStateNormal];
            break;
            
        case DisplayCellTypeMedium:
            self.cellType = DisplayCellTypeMedium;
            [displayBtn setBackgroundImage:[UIImage imageNamed:@"NaviBar-Show3-BarButton.png"] forState:UIControlStateNormal];
            break;
            
        case DisplayCellTypeBig:
            self.cellType = DisplayCellTypeBig;
            [displayBtn setBackgroundImage:[UIImage imageNamed:@"NaviBar-Show2-BarButton.png"] forState:UIControlStateNormal];
            break;
    }
    
    [_selectDisplayTypeView setHidden:YES];
}

@end
