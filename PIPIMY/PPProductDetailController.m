
#import "PPAPI.h"
#import "PPUser.h"
#import "PPRatingCell.h"
#import "PPProductDetailController.h"
#import "PPProductDetailViewsModel.h"

#define kAlertViewEnterMessage 1
#define kAlertViewMessageSent 2

enum ViewsTag
{
    k_Tag_ReportProductAlert = 21000,
};



@interface PPProductDetailController () <UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet PPProductDetailViewsModel *viewsModel; // Subviews 管理的 Model
@property (nonatomic, weak) IBOutlet UIButton      *favorite;        // 加入我的最愛按鈕

@property (weak, nonatomic) IBOutlet UIButton *reportProductBtn;

@end


@implementation PPProductDetailController

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self __setup];
    [_viewsModel configureWithProduct:_product];
    [self __callAPIGetProductRating];
    [self __checkAndChangeFavoriteStatus];
}

- (void)viewWillAppear:(BOOL)animated
{
   self.navigationController.navigationBar.topItem.title = @"商品資訊";
}

- (void)viewDidAppear:(BOOL)animated
{
    [_viewsModel setupImageScrollerWithProduct:_product];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"";
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section)
    {
        case 0:
        {
            if (indexPath.row == 1)
            {
                return _viewsModel.s0c1Height;
            }
        }
            break;
        case 2:
        {
            return _viewsModel.scoreTableHeight;
        }
            break;
            
        default:
            break;
    }

    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - IBAction
#pragma mark 聯絡賣家
- (IBAction)contactItemDidClicked:(id)sender
{
    // 顯示可輸入訊息的對話框
    UIAlertView *startChatAlert = [[UIAlertView alloc] initWithTitle:@"交易訊息" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"送出", nil];
    startChatAlert.tag = kAlertViewEnterMessage;
    startChatAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[startChatAlert textFieldAtIndex:0] setAutocorrectionType:UITextAutocorrectionTypeYes];
    [[startChatAlert textFieldAtIndex:0] becomeFirstResponder];
    [startChatAlert show];
}

#pragma mark 加入我的最愛
- (IBAction)favoriteBtnTouchUpInside:(id)sender
{
    
    NSDateFormatter *formatter           = [[NSDateFormatter alloc]init];
    formatter.dateFormat                 = @"MM-dd";
    NSString *addToFavoriteDate          = [formatter stringFromDate:[NSDate date]];
    _product.addToFavoriteDate            = addToFavoriteDate;
    [[PPUser singleton]updateFavorites:_product];
    [self __checkAndChangeFavoriteStatus];
}


-(void)__checkAndChangeFavoriteStatus
{
    UIImage *likeImage;
    
    if([[PPUser singleton]isFavoriteWithProduct:_product]) // 如果有加入過我的最愛...
    {
        likeImage = [UIImage imageNamed:@"Like-Enable-Button"];
        
    }
    else
    {
        likeImage = [UIImage imageNamed:@"Like-Disable-Button"];
    }
    
    [_favorite setBackgroundImage:likeImage forState:UIControlStateNormal];
}

#pragma mark - Private methods
#pragma mark 初始設置
- (void)__setup
{
    //self.title = _product.title;
    self.title = @"";

    if([_product.ID isEqualToString:[PPUser singleton].name])
    {
        // 因為看自己刊登的商品, 所以沒有檢舉跟聯絡的按鈕
        self.tableView.tableFooterView = nil;
        [_reportProductBtn setHidden:YES];
        
    }
    else
    {
        // 因為看到的不是自己刊登的商品, 所以右上有聯絡的 Item,hide = 4 則不加入。
        if ([_product.hide integerValue] !=4)
        {
            self.navigationItem.rightBarButtonItem = [self __contactItem];
        }
        
    }
}

- (void)addCartButtonTouchUpInside:(id)sender
{
    NSDictionary *params = @{ @"ProductID" : _product.productId };
    [[PPAPI singleton] APIAddToCart:params callback:^(NSError *err)
     {
         if (err)
         {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"錯誤" message:@"無法加入購物車" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alertView show];
         } else
         {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"成功加入購物車" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alertView show];
         }
     }];
}


- (IBAction)reportProductBtnTouchUpInside:(id)sender
{
    PPUser *user = [PPUser singleton];
    
    if(!user.isLogin)
    {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        id mvc              = [story instantiateInitialViewController];
        
        [self presentViewController:mvc animated:YES completion:nil];
    }
    else
    {
        UIAlertView *reportAlertView = [[UIAlertView alloc]initWithTitle:@"我要檢舉" message:@"請輸入檢舉原因" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        [reportAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [reportAlertView setTag:k_Tag_ReportProductAlert];
        [reportAlertView show];
    }
}


#pragma mark - Call API getProductRating
- (void)__callAPIGetProductRating
{
    NSDictionary *params = @{@"ProductID" : _product.productId};
    
    [[PPAPI singleton]APIGetProductRating:params callback:^(PPAPIGetSomeoneRatingResponse *item, NSError *err) {
       
        if(!err)
        {
            [_viewsModel updateRatingDetail:item.detail];
            [_viewsModel updateRatingDatasource:item.ratings];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Call API startChatContent
- (void)__callAPIStartChatContent:(NSString *)content
{
    
    // 檢查訊息長度
    if ([content length] == 0)
        return;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSDictionary *params = @{
                             @"UserIDTo": _product.ID,
                             @"ProductID": _product.productId,
                             @"ProductTitle": _product.title,
                             @"Content": content,
                             @"SendTime": dateString
                             };
    
    [[PPAPI singleton] APIStartChatContent:params callback:^(NSError *err) {
        if (err == nil) {
            UIAlertView *startChatSuccessful = [[UIAlertView alloc] initWithTitle:@"訊息傳送成功" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            startChatSuccessful.tag = kAlertViewMessageSent;
            [startChatSuccessful show];
        } else {
            // error handling
            UIAlertView *startChatFailed = [[UIAlertView alloc] initWithTitle:@"傳送有誤，請稍候再試" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [startChatFailed show];
        }
    }];
}

#pragma mark - 返回右上聯絡賣家 Item
- (UIBarButtonItem *)__contactItem
{
    /*
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithTitle:@"聯絡賣家"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(contactItemDidClicked:)];
    
     */
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"NaviBar-ContactDefault-BarButton"] style:UIBarButtonItemStylePlain target:self action:@selector(contactItemDidClicked:)];
    
    return item;
}

#pragma mark - 聯絡賣家 & 成功送出訊息 & 檢舉 UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kAlertViewEnterMessage & buttonIndex == 1)
    {
        // 送出訊息
        [self __callAPIStartChatContent:[alertView textFieldAtIndex:0].text];
    }
    else if (alertView.tag == kAlertViewMessageSent)
    {
        // 切換到聊天列表
        [[self.tabBarController.viewControllers objectAtIndex:2] popToRootViewControllerAnimated:NO];
        [self.tabBarController setSelectedIndex:2];
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else if (alertView.tag == k_Tag_ReportProductAlert & buttonIndex ==1)
    {
        UITextField *textField = [alertView textFieldAtIndex:0];
        
        if (textField.text.length >0)
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
            
            NSDictionary *params = @{
                                    @"ProductID": _product.productId,
                                    @"Reason": textField.text,
                                    @"ReportTime": dateString,
                                    };

            
            [[PPAPI singleton]APIReportProduct:params callback:^(id jsonObject, NSError *err)
            {
                NSDictionary *dic = jsonObject;
                
                if(!err && [dic objectForKey:@"result"]!=[NSNull null] && [dic objectForKey:@"result"])
                {
                    NSString *result = [dic objectForKey:@"result"];
                    NSString *message = nil;
                    
                    if ([result isEqualToString:@"success"])
                    {
                        message = @"檢舉成功";
                    }
                    else if ([result isEqualToString:@"alreadyReport"])
                    {
                        message = @"您已檢舉過了";
                    }
                    else if ([result isEqualToString:@"error"])
                    {
                        message = @"發生錯誤";
                    }
                    else
                    {
                        message = @"未知的錯誤";
                    }
                    
        
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:message message:nil delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil, nil];
                    [alert show];

                }
                else if (err)
                {
                    
                    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[err localizedDescription] message:nil delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil, nil];
                    [alert show];
                }

            }];

        }
        else
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@": (" message:@"請輸入檢舉原因." delegate:nil cancelButtonTitle:@"確定" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
    }
}

@end
